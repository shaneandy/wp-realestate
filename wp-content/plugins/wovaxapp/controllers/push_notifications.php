<?php
/*
Controller name: Push Notification Controller
Controller description: This controller controlls how devices interract with the server for Push Notifications
*/

class JSON_API_Push_Notifications_Controller {
  public function get_excluded() {
    global $json_api;
    global $wpdb;
    { // ID
      $excluded_id_arr = get_option('wovax_excluded_categories');
      $excluded_id = explode(",",$excluded_id_arr);
      array_shift($excluded_id); // removes null value
      for($i=0;$i<sizeof($excluded_id);$i++) {
        $result[$i]['id'] = $excluded_id[$i];
      }
    }
    { // Name
      for($i=0;$i<sizeof($excluded_id);$i++) {
        $sql = "SELECT `name` FROM `".$wpdb->prefix."terms` WHERE `term_id`=".$excluded_id[$i];
        $name = $wpdb->get_results($sql);
        $result[$i]['name'] = $name[0]->name;
      }
    }
    { // Slug
      for($i=0;$i<sizeof($excluded_id);$i++) {
        $sql = "SELECT `slug` FROM `".$wpdb->prefix."terms` WHERE `term_id`=".$excluded_id[$i];
        $slug = $wpdb->get_results($sql);
        $result[$i]['slug'] = $slug[0]->slug;
      }
    }
    return array('excluded_categories' => $result);
  }

  // {protocol}://{domain}.{TLD}/{API Key}/push_notifications/get_push_categories/?deviceId={Unique ID}&platform={apns || gcm}
  public function get_push_categories() {
    global $json_api;
    global $wpdb;
    $categories = get_option('wovax_pushnotification_categories');
    $categories = json_decode($categories);
    $apns_suffix = "";
    $gcm_suffix = "";

    if ($_GET['deviceId'] != "" && $_GET['platform'] != "") {
      if (get_option('wovax_ios_apns_environment') != "2") {
        $apns_suffix = "_development";
      }
      if (get_option('wovax_ios_apns_environment') != "2") {
        $gcm_suffix = "_development";
      }

      if ($_GET['platform'] == "apns") {
        $sql = "SELECT blacklisted_categories FROM ".$wpdb->prefix."wovaxapp_apns_device".$apns_suffix." WHERE device_id = '".$_GET['deviceId']."'";
      }elseif ($_GET['platform'] == "gcm") {
        $sql = "SELECT blacklisted_categories FROM ".$wpdb->prefix."wovaxapp_gcm_device".$gcm_suffix." WHERE device_id = '".$_GET['deviceId']."'";
      }
      $subscribed = $wpdb->get_results($sql);
      $subscribed = explode(",",$subscribed[0]->blacklisted_categories);

      for ($i=0;$i<sizeof($categories);$i++) {
        if(in_array($categories[$i]->id,$subscribed)) {
          $categories[$i]->subscribed = "NO";
        }else {
          $categories[$i]->subscribed = "YES";
        }
      }
    }

    return array(
      'categories' => $categories
    );
  }

  // {protocol}://{domain}.{TLD}/{API Key}/push_notifications/blacklist_categories/?deviceId={Unique ID}&platform={apns || gcm}&categories={xyz}
  // the categories list should be a JSON encoded list of the category ID's
  // which can be found by querying the get_push_categories function
  public function blacklist_categories() {
    global $json_api;
    global $wpdb;
    $deviceToken = $_GET['deviceId'];
    $devicePlatform = $_GET['platform'];
    $categories = $_GET['categories'];

    if ($devicePlatform == "apns") {
      $sql = "UPDATE ".$wpdb->prefix."wovaxapp_apns_device SET blacklisted_categories = '".$categories."' WHERE device_id = '".$deviceToken."'";
    }elseif ($devicePlatform == "apns_development") {
      $sql = "UPDATE ".$wpdb->prefix."wovaxapp_apns_device_development SET blacklisted_categories = '".$categories."' WHERE device_id = '".$deviceToken."'";
    }elseif ($devicePlatform == "gcm") {
      $sql = "UPDATE ".$wpdb->prefix."wovaxapp_gcm_device SET blacklisted_categories = '".$categories."' WHERE device_id = '".$deviceToken."'";
    }elseif ($devicePlatform == "gcm_development") {
      $sql = "UPDATE ".$wpdb->prefix."wovaxapp_gcm_device_development SET blacklisted_categories = '".$categories."' WHERE device_id = '".$deviceToken."'";
    }
    $wpdb->query($sql);
    return array(
      "response" => 1
    );
  }

  // {protocol}://{domain}.{TLD}/{API Key}/push_notifications/register_device/?deviceId={Unique ID}&platform={apns || gcm}
  public function register_device() {
    global $json_api;
    global $wpdb;
    $deviceToken = $_GET['deviceId'];
    $devicePlatform = $_GET['platform'];
    $username = $_GET['name'];
    $email = $_GET['email'];
    if ($devicePlatform == "apns") {
      $suffix = "";
      $platform = "apns";
      $sql = "SELECT * FROM ".$wpdb->prefix."wovaxapp_apns_device WHERE device_id = '".$deviceToken."'";
    }elseif ($devicePlatform == "apns_development") {
      $suffix = "_development";
      $platform = "apns";
      $sql = "SELECT * FROM ".$wpdb->prefix."wovaxapp_apns_device_development WHERE device_id = '".$deviceToken."'";
    }elseif ($devicePlatform == "gcm") {
      $suffix = "";
      $platform = "gcm";
      $sql = "SELECT * FROM ".$wpdb->prefix."wovaxapp_gcm_device WHERE device_id = '".$deviceToken."'";
    }elseif ($devicePlatform == "gcm_development") {
      $suffix = "_development";
      $platform = "gcm";
      $sql = "SELECT * FROM ".$wpdb->prefix."wovaxapp_gcm_device_development WHERE device_id = '".$deviceToken."'";
    }
    $wpdb->get_results($sql);
    $rows = $wpdb->num_rows;
    if ($rows != 0) {
      $response = "Device already registered";
    }else {
      $ins_pn_id = "INSERT INTO ".$wpdb->prefix."wovaxapp_".$platform."_device".$suffix." SET device_id='".$deviceToken."',registered_on=NOW()";
      $res_email = $wpdb->query($ins_pn_id);
      $response = 1;
    }
    return array(
      "response" => $response
    );
  }

  // {protocol}://{domain}.{TLD}/{API Key}/push_notifications/unregister_device/?deviceId={Unique ID}&platform={apns || gcm}
  public function unregister_device() {
    global $json_api;
    global $wpdb;
    $deviceToken = $_GET['deviceId'];
    $devicePlatform = $_GET['platform'];
    $response = 1;
    if ($devicePlatform == "apns") {
      if (get_option('wovax_ios_apns_environment') != "2") {
        $suffix = "_development";
      }
      $sql = "DELETE FROM " . $wpdb->prefix . "wovaxapp_apns_device".$suffix." WHERE device_id='" . $deviceToken . "'";
    }elseif ($devicePlatform == "gcm") {
      if (get_option('wovax_ios_apns_environment') != "2") {
        $suffix = "_development";
      }
      $sql = "DELETE FROM " . $wpdb->prefix . "wovaxapp_gcm_device".$suffix." WHERE device_id='" . $deviceToken . "'";
    }else {
      $response = "Unknown platform type: ".$devicePlatform;
    }
    $wpdb->query($sql);

    return array(
      "response" => $response
    );
  }
}

?>
