<?php

/*
Controller name: Options
Controller description: Retrieve WordPress options
*/


class JSON_API_options_Controller {

	// Get WordPress option settings from the options table, e.g. the blog name, see http://codex.wordpress.org/Option_Reference
    public function get_options() {
        global $json_api;
        if ($_GET['options'] != "") {
          $options = explode(",",$_GET['options']);
          $return = array();
          foreach ($options as $option) {
            $return[$option] = get_option($option);
          }
        }else {
          $json_api->error("Include 'options' var in your request, options comma separated. See for a list of options: http://codex.wordpress.org/Option_Reference");
          return null;
        }
        return $return;
    }

}

?>
