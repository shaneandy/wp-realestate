<?php
/*
Controller name: Posts
Controller description: Data manipulation methods for posts
*/

class JSON_API_Posts_Controller {

  public function create_post() {
    global $json_api;
    if (!$json_api->query->auth) {
      $json_api->error("You must include a 'auth' var in your request. Go away.");
    }
    if (!$json_api->query->author) {
      $json_api->error("You must include 'author' var in your request.");
    }
    nocache_headers();
    $post = new JSON_API_Post();
    $post->set_author_value($json_api->query->author);
    $id = $post->create($_REQUEST);
    if (empty($id)) {
      $json_api->error("Could not create post.");
    }
    return array(
      'post' => $post
    );
  }

  public function update_post() {
    global $json_api;
    $post = $json_api->introspector->get_current_post();
    if (empty($post)) {
      $json_api->error("Post not found.");
    }
    if (!current_user_can('edit_post', $post->ID)) {
      $json_api->error("You need to login with a user that has the 'edit_post' capacity for that post.");
    }
    if (!$json_api->query->nonce) {
      $json_api->error("You must include a 'nonce' value to update posts. Use the `get_nonce` Core API method.");
    }
    $nonce_id = $json_api->get_nonce_id('posts', 'update_post');
    if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
      $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
    }
    nocache_headers();
    $post = new JSON_API_Post($post);
    $post->update($_REQUEST);
    return array(
      'post' => $post
    );
  }

  public function delete_post() {
    global $json_api;
    $post = $json_api->introspector->get_current_post();
    if (empty($post)) {
      $json_api->error("Post not found.");
    }
    if (!current_user_can('edit_post', $post->ID)) {
      $json_api->error("You need to login with a user that has the 'edit_post' capacity for that post.");
    }
    if (!current_user_can('delete_posts')) {
      $json_api->error("You need to login with a user that has the 'delete_posts' capacity.");
    }
    if ($post->post_author != get_current_user_id() && !current_user_can('delete_other_posts')) {
      $json_api->error("You need to login with a user that has the 'delete_other_posts' capacity.");
    }
    if (!$json_api->query->nonce) {
      $json_api->error("You must include a 'nonce' value to update posts. Use the `get_nonce` Core API method.");
    }
    $nonce_id = $json_api->get_nonce_id('posts', 'delete_post');
    if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
      $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
    }
    nocache_headers();
    wp_delete_post($post->ID);
    return array();
  }

  public function search_meta() {
    global $json_api;
    global $wpdb;

    $all_sql = array();
    $result = array();
    foreach ($_GET as $name => $value) {
      $sql = "SELECT * FROM ".$wpdb->prefix."postmeta WHERE `meta_key` = '".$name."' AND `meta_value` != 'null'";
      $output = $wpdb->get_results($sql,ARRAY_A);
    }

    $post_id = array();
    for ($i=0;$i<sizeof($output);$i++) {
      if (!in_array($output[$i]['post_id'],$post_id)) {
        $post_id[] = $output[$i]['post_id'];
      }
    }

    $post_keep = array();
    for ($i=0;$i<sizeof($post_id);$i++) {
      $sql = "SELECT * FROM ".$wpdb->prefix."postmeta WHERE `post_id` = '".$post_id[$i]."'";
      $post_meta = $wpdb->get_results($sql,ARRAY_A);
      for ($j=0;$j<sizeof($post_meta);$j++) {
        foreach ($_GET as $name => $value) {
          $haystack = json_decode($post_meta[$j]['meta_value']);
          if ($post_meta[$j]['meta_key'] == $name && in_array($value,$haystack)) {
            $post_keep[] = $post_meta[$j]['post_id'];
          }
        }
      }
    }

    $post_keep = array_unique($post_keep);
    $posts = array();
    for ($i=0;$i<sizeof($post_keep);$i++) {
      $sql = "SELECT post_type FROM ".$wpdb->prefix."posts WHERE `ID` = '".$post_keep[$i]."'";
      $type = $wpdb->get_results($sql,ARRAY_A);
      $url = get_option('siteurl')."?json=get_post&post_id=".$post_keep[$i]."&post_type=".$type[0]['post_type'];
      $json = json_decode(file_get_contents($url));
      $posts[] = $json->post;
    }

    $url = explode("?",$_SERVER['REQUEST_URI']);
    $query = array(
      "path" => $url[0],
      "query" => $url[1],
    );
    $result = array(
      "count" => sizeof($posts),
      "count_total" => sizeof($posts), // implement pages later
      "pages" => 1,
      "posts" => $posts,
      "url" => $query
    );
    return $result;
  }

}

?>
