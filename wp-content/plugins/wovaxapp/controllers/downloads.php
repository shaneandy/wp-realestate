<?php
/*
Controller name: Downloads
Controller description: Checks if a device is registered, and if not registers their device
*/

class JSON_API_Downloads_Controller {

	public function register_download() {
		global $json_api;
		global $wpdb;
		
		// ----User Information------------
			// Unique user id
			$userID = $_GET['userID'];
			// Time
			$time = time();
			// gs4, iphone, ipad, etc
			$device = $_GET['device'];
			// android, ios7, ios8, etc
			$OS = $_GET['OS'];
		
		// ----Count Rows------------------
			$sql = "SELECT * FROM " . $wpdb->prefix . "wovaxapp_downloads WHERE userID = '" . $userID . "';";
			$sql_data = $wpdb->get_results($sql, ARRAY_A);
			$row_count = count($sql_data);
		
		// ----If !exists, add-------------
			if ($row_count != 0) {
				if ($sql_data[0]['OS'] != $OS) {
					$wpdb->query("UPDATE " . $wpdb->prefix . "wovaxapp_downloads SET OS = '" . $OS . "' WHERE userID = '" . $userID . "'");
				}
				$response = "exists";
			}else {
				$wpdb->query("INSERT INTO " . $wpdb->prefix . "wovaxapp_downloads (userID, unixtime, device, OS) VALUES ('$userID', '$time', '$device', '$OS')");
				$response = "success";
			}
		
		// ----Print Response--------------
			return array(
				"Response" => $response
			);
	}

}

?>
