<?php

	/*
	  Controller name: Search
	  Controller description: A pretty cool search controller
	*/

	class JSON_API_Wovax_Search_Controller {
		public function get_filters() {
			global $json_api;
			// function can be found in wovaxapp.php
			$filters = wovax_get_search_filters();
			$results = array();
			$count = 0;
			foreach ($filters['filter'] as $filter) {
				if ($filter['in_app'] == "YES"){
					unset($filter['in_app']);
					unset($filter['item_order']);
					$results[] = $filter;
					$count++;
				}
			}
			unset($filters['filter']);
			unset($filters['count']);
			$filters['count'] = $count;
			$filters['filter'] = $results;
			return $filters;
		}

		public function search_things() {
			$start = microtime();
			global $wpdb;
			global $json_api;
			if (isset($_GET['page'])) {
				$page = $_GET['page'];
				unset($_GET['page']);
			}else {
				$page = 1;
			}

			if ($page == 1) {
				$cache = array();
				$from_cache = 0;
				foreach ($_GET as $query => $value) {
					if ($this->is_cached($query,$value,$this->rows($query,$value))) {
						$cache[] = $this->fetch_cache($this->rows($query,$value));
						$from_cache++;
					}else {
						$results = wovax_search(json_encode(array($query=>$value)),$page);
						$this->load_cache($query,$value,json_encode($results['posts']));
						$cache[] = $results['posts'];
					}
				}
				$ids = $this->get_unique_ids($cache);
				$index = $this->index_ids($cache);
				$posts = $this->get_posts_from_ids($cache,$ids,$index);
				$filters = $this->get_queried_filters($_GET);
				$end = microtime();
				$time = $end - $start . " ms";
				$results = array(
					"filters" => $filters,
					"posts_processed" => "N/A",
					"from_cache" => $from_cache."/".sizeof($filters),
					"processing_time" => $time,
					"memory_usage" => memory_get_usage() / 1024 / 1024 . " MB",
					"count" => sizeof($posts),
					"page_number" => 1,
					"posts" => $posts,
				);
			}else {
				$results = wovax_search(json_encode($_GET),$page);
			}

			return $results;
		}

		private function get_unique_ids($cache) {
			$ids = array();
			$keep_ids = array();
			for ($j=0;$j<sizeof($cache);$j++) {
				for ($i=0;$i<sizeof($cache[$j]);$i++) {
					$ids[$j][] = $cache[$j][$i]['id'];
					$keep_ids[] = $cache[$j][$i]['id'];
				}
			}
			foreach ($ids as $list) {
				$keep_ids = array_intersect($keep_ids,$list);
			}
			return array_values(array_unique($keep_ids));
		}

		private function index_ids($cache) {
			$index = array();
			for ($j=0;$j<sizeof($cache);$j++) {
				for ($i=0;$i<sizeof($cache[$j]);$i++) {
					$index[$cache[$j][$i]['id']] = array(
						"group" => $j,
						"element" => $i,
					);
				}
			}
			return $index;
		}

		private function get_posts_from_ids($cache,$ids,$index) {
			$post_data = array();
			for ($i=0;$i<sizeof($ids);$i++) {
				$group = $index[$ids[$i]]['group'];
				$element = $index[$ids[$i]]['element'];
				$post_data[] = $cache[$group][$element];
			}
			return $post_data;
		}

		private function get_queried_filters($query) {
			global $wpdb;
			$filters = array();
			foreach ($query as $index => $needle) {
				if (!is_numeric($index)) {
					return array("error" => "$index is not a valid variable type");
				}else {
					$sql = "SELECT `no`,`post_type`,`custom_field`,`display_name`,`type` FROM " . $wpdb->prefix . "wovaxapp_search_filter WHERE no=".$index;
					$filter = $wpdb->get_results($sql,ARRAY_A);
					$filter[0]['query'] = $needle;
					$filters[] = $filter[0];
				}
			}
			return $filters;
		}
	}
