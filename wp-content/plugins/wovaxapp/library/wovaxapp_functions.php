<?php

class wovax_functions {

	var $options = array(
		'ad_option',
		'android_action_bar_menu_icon',
		'android_action_bar_settings_icon',
		'app_home_page_id',
		'app_home_post_type',
		'app_home_screen',
		'android_comment_button',
		'android_gcm_api_key',
		'android_gcm_environment',
		'android_maps_api_key',
		'android_menu_home_button',
		'android_post_comment_placeholder',
		'android_save_button',
		'android_settings_title',
		'app_home_post_type',
		'app_name',
		'custom_fields_filter_placeholder',
		'custom_fields_filter_results_title',
		'default_search_sort',
		'google_analytics_id',
		'html_custom_ad',
		'ios_apns_environment',
		'ios_back_button',
		'ios_comment_button',
		'ios_development_pem_path',
		'ios_external_push_server_path',
		'ios_ipad_segment_menu',
		'ios_ipad_segment_posts',
		'ios_menu_home_button',
		'ios_nav_logo_url',
		'ios_nav_logo',
		'ios_nav_menu_icon',
		'ios_nav_settings_icon',	
		'ios_pem_password',
		'ios_post_comment_placeholder',
		'ios_production_pem_path',
		'ios_push_host',
		'ios_push_setup_indicator',
		'ios_save_button',	
		'ios_settings_title',
		'js_custom_ad',
		'network_enabled',
		'user_accounts',
		'user_registration',
		'user_accounts_guests',
		'user_accounts_launch_screen',
		'favorites',
		'login_segment',
		'register_segment',
		'login_table_title',
		'register_table_title',
		'login_button',
		'register_button',
		'terms_sentence_beginning',
		'terms_sentence_link',
		'terms_sentence_end',
		'registration_terms',
		'terms_alert_title',
		'terms_alert_text',
		'terms_alert_cancel_button',
		'terms_alert_agree_button',
		'continue_as_guest',
		'sign_out_button',
	  );
	var $no_stripslash = array(
		'app_home_webview_url',
		'link_banner',
		'phone_portrait_banner',
		'tablet_portrait_banner',   
	);
	var $checkboxes = array(
		'excluded_categories',
	);
	var $color_picker = array(
		'android_action_bar_buttons_color',
		'android_action_bar_color',
		'android_action_bar_title_color',
		'android_menu_background_color',
		'android_menu_divide_color',
		'android_menu_highlight_background_color',
		'android_menu_highlight_text_color',
		'android_menu_text_color',
		'android_status_bar_color',
		'android_tint_color',
		'ios_menu_background_color',
		'ios_menu_divide_color',
		'ios_menu_highlight_background_color',
		'ios_menu_highlight_text_color',
		'ios_menu_text_color',
		'ios_nav_bar_color',
		'ios_nav_buttons_color',
		'ios_nav_title_color',
		'ios_search_bar_color',
		'ios_status_bar_color',
		'ios_tint_color',
		'ios_apple_watch_primary_color',
		'ios_apple_watch_secondary_color',
		'ios_ui_switch_color',
		'ios_search_bar_cancel_text_color',
		'ios_loading_indicator_color',
		'ios_tab_bar_buttons_color',
		'ios_tab_bar_background_color'
	);

	public function save_data($post) {
		foreach($this->options as $option) {
			$response = "wovax_" . $option;
			if ($post[$response] != "") {
		  		update_option($response,stripslashes($post[$response]));
			}
		}
		foreach($this->no_stripslash as $option) {
			$response = "wovax_" . $option;
			if ($post[$response] != "") {
				update_option($response,$post[$response]);
			}
		}
		foreach($this->checkboxes as $checkbox) {
			$response = "wovax_" . $checkbox;
			if (!empty($post[$response])) {
				$new_data = "," . implode(",",$post[$response]);
				update_option($response,$new_data);
			} else {
				update_option($response,"");
			}
		}
		foreach($this->color_picker as $color) {
			$response = "wovax_" . $color;
			if ( $post[$response] != "" ) {
				update_option($response,$post[$response]);
			}
		}
	}

	/**
	* Function that will check if value is a valid HEX color.
	*/
	public function check_color( $value ) { 
		if ( preg_match( '/^#[a-f0-9]{6}$/i', $value ) ) { // if user insert a HEX color with #     
			return true;
		}
		return false;
	}

	public function table_generator($elements) {
	  foreach ($elements as $element) {
		if ($element["type"] == "heading") {
		  ?>
		  <tr>
			<th scope="row">
			  <label for="<?php echo $element['name']; ?>">
				<?php echo "<h3>".$element['name']."</h3>";?>
			  </label>
			</th>
			<td></td>
		  </tr>
		  <?php
		} elseif ($element["type"] == "Y/N") {
		  ?>
		  <tr>
			<th scope="row">
			  <label for="<?php echo $element['name']; ?>">
				<?php echo $element['name'];?>
			  </label>
			</th>
			  <td>
				<?php
				  if ($element["disabled"] !== false) {
					$disabled = "disabled";
					$sel = $element["disabled"];
				  } else {
					$sel = get_option("wovax_".$element['db_name']);
				  }
				  if (isset($element["class"])) {
					$class = "class='".$element["class"]."'";
				  } else {
					$class = "";
				  }
				?>
				<select <?php echo $class;?> name="<?php echo "wovax_".$element['db_name'];?>">
				  <option value='NO' <?php if ($sel == "NO") { echo "selected"; } ?>>NO</option>
				  <option value='YES' <?php if ($sel == "YES") { echo "selected"; } ?>>YES</option>
				</select>
				<p class="description"><?php echo $element["description"];?></p>
			</td>
		  </tr>
		  <?php
		} elseif ($element["type"] == "text") {
		  ?>
		  <tr>
			<th scope="row">
			  <label for="<?php echo $element['name']; ?>">
				<?php echo $element['name'];?>
			  </label>
			</th>
			  <td>
				<?php
				  if (isset($element["class"])) {
					$class = $element["class"];
				  } else {
					$class = "";
				  }
				  if (isset($element["maxlength"])) {
					$max_length = "maxlength='".$element["maxlength"]."'";
				  } else {
					$max_length = "";
				  }
				  $value = htmlspecialchars(get_option('wovax_'.$element['db_name']));
				?>
				<input
				  <?php echo $max_length;?>
				  type="text"
				  name="<?php echo "wovax_" . $element['db_name']; ?>"
				  id="<?php echo "wovax_" . $element['db_name']; ?>"
				  class="<?php echo $class;?>"
				  value='<?php echo $value;?>'
			  />
			  <p class="description"><?php echo $element['description'];?></p>
			</td>
		  </tr>
		  <?php
		} elseif ($element["type"] == "textarea") {
		  ?>
		  <tr>
			<th scope="row">
			  <label for="<?php echo $element['name']; ?>">
				<?php echo $element['name'];?>
			  </label>
			</th>
			  <td>
				<?php
				  if (isset($element["class"])) {
					$class = "class='".$element["class"]."'";
				  } else {
					$class = "";
				  }
				?>
				<textarea
				  <?php echo $class;?>
				  rows="10" cols="40"
				  name="<?php echo "wovax_" . $element['db_name']; ?>"
				  id="<?php echo "wovax_" . $element['db_name']; ?>"
				  class="regular-text"
				  ><?php	echo htmlspecialchars(get_option('wovax_'.$element['db_name']));?></textarea>
			  <p class="description"><?php echo $element['description'];?></p>
			</td>
		  </tr>
		  <?php
		 } elseif ($element["type"] == "select") {
		  ?>
		  <tr>
			<th scope="row">
			  <label for="<?php echo $element['name']; ?>">
				<?php echo $element['name'];?>
			  </label>
			</th>
			<td>
			  <?php
				if (isset($element["class"])) {
				  $class = "class='".$element["class"]."'";
				} else {
				  $class = "";
				}
			  ?>
			  <select <?php echo $class;?> id="<?php echo "wovax_".$element["db_name"];?>" name='<?php echo "wovax_".$element["db_name"];?>'>
				<?php
				  $sel = get_option("wovax_".$element["db_name"]);
				  foreach ($element["options"] as $option) {
					if ($sel == $option) {
					  $selected = " selected ";
					} else {
					  $selected = "";
					}
					echo "<option".$selected." value='".$option."'>".ucwords($option)."</option>";
				  }
				?>
			  </select>
			  <p class="description"><?php echo $element['description'];?></p>
			</td>
		  </tr>
		  <?php
		} elseif($element['type'] == 'tinymce') {
			?>
		  <tr>
			<th scope="row">
			  <label for="<?php echo $element['name']; ?>">
				<?php echo $element['name'];?>
			  </label>
			</th>
			  <td>
				<?php
					$editor_id = "wovax_" . $element['db_name'];
				  	$content = get_option('wovax_'.$element['db_name']);
					wp_editor( $content, $editor_id );
				?>
			  <p class="description"><?php echo $element['description'];?></p>
			</td>
		  </tr>
		  <?php
		}
	  }
	}

	public function selected_option($compare,$against) {
	  if ($compare == $against) {
		return " selected='selected' ";
	  } else {
		return " ";
	  }
	}

	/*
	 * Function to add saved searches 
	 *
	 */
	public function add_saved_search($search_name,$search_query,$user_id,$site_id = '') {
		if(is_multisite() && empty($site_id)) {
			$site_id = get_current_blog_id();
		} else if(!is_multisite()) {
			$site_id = 1;
		} else {
			$site_id = $site_id;
		}
		$search_array = array(
			'name' => $search_name,
			'search' => $search_query
		);
		$current_searches = get_user_meta($user_id,'wovax_saved_searches',true);
		if(empty($current_searches)) {
			$current_searches = array();
			$site_array = array(
				'site_id' => $site_id,
				'searches' => array()
			);
			$current_searches[] = $site_array;
		}
		if(!in_array($site_id,array_column($current_searches,'site_id'))) {
			$new_site_array = array(
				'site_id' => $site_id,
				'searches' => array()
			);
			$current_searches[] = $new_site_array;
		}
		foreach($current_searches as $key => $site_searches) {
			if ( $site_searches['site_id'] !== $site_id ) {
				 continue;
			}
			$current_searches[$key]['searches'][] = $search_array;
		}
		$add = update_user_meta( $user_id, 'wovax_saved_searches', $current_searches );
		return $add;
	}

	/*
	 * Function to remove existing saved searches 
	 *
	 */
	public function remove_saved_search($search_name,$search_query,$user_id,$site_id = '') {
		if(is_multisite() && empty($site_id)) {
			$site_id = get_current_blog_id();
		} else if(!is_multisite()) {
			$site_id = 1;
		} else {
			$site_id = $site_id;
		}
		$search_array = array(
			'name' => $search_name,
			'search' => $search_query
		);
		$current_searches = get_user_meta($user_id,'wovax_saved_searches',true);
		foreach($current_searches as $key => $site_searches) {
			if ( $site_searches['site_id'] !== $site_id ) continue;
			if(in_array($search_array,$site_searches['searches'])) {
				$matched_key = array_search($search_array,$site_searches['searches']);
				unset($current_searches[$key]['searches'][$matched_key]);
			}
		}
		$remove = update_user_meta( $user_id, 'wovax_saved_searches', $current_searches );
	}

	/*
	 * Function to get an array of saved searches 
	 *
	 */
	public function get_saved_searches($user_id,$site_id = '') {
		if(is_multisite() && empty($site_id)) {
			$site_id = get_current_blog_id();
		} else if(!is_multisite()) {
			$site_id = 1;
		} else {
			$site_id = $site_id;
		}
		$current_searches = get_user_meta($user_id,'wovax_saved_searches',true);
		if(empty($current_searches)) {
			return false;
		}
		foreach($current_searches as $key => $site_searches) {
			if ( $site_searches['site_id'] !== $site_id ) continue;
			$searches = $site_searches['searches'];
		}
		return $searches;
	}
}

?>
