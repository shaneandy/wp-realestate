<?php
  	// cron job for API heartbeat
    if (!wp_next_scheduled('wovax_cron_api_heartbeat')) {
      wp_schedule_event(time(),'hourly','wovax_cron_api_heartbeat');
    }
    add_action('wovax_cron_api_heartbeat','wovax_cron_api_heartbeat');
    function wovax_cron_api_heartbeat() {
      $payload = array();
      $plugin_data = get_plugin_data(__FILE__);

      if (get_option('wovaxapp_activated') == 'Activated') {
        $payload['product_activated'] = "TRUE";
      }else {
        $payload['product_activated'] = "FALSE";
      }

      $payload['version'] = $plugin_data['Version'];
      $payload['site_id'] = get_option('siteurl');

      $payload = json_encode($payload);
      $url = ('http://api.wovax.io/api_key/master/version_heartbeat/?payload='.$payload);
      $response = file_get_contents($url);
      unset($response); //cuz i just dont care
    }

	//cron job for updating existing search type cache
	if ( ! wp_next_scheduled( 'wovax_update_search_filter_cache' ) ) {
		wp_schedule_event( time(), 'hourly', 'wovax_update_search_filter_cache' );
	}

	add_action( 'wovax_update_search_filter_cache', 'wovax_update_existing_search_filter_cache' );

	function wovax_update_existing_search_filter_cache() {
		global $wpdb;
		//Select all search filters
		$search_filter_sql = "SELECT * FROM " . $wpdb->prefix . "wovaxapp_search_filter ORDER BY item_order ASC";
		$filters = $wpdb->get_results($search_filter_sql,ARRAY_A);
		//Grab the 'existing' type filter ids
		$existing_filter_ids = array();
		foreach($filters as $key => $filter){
			if($filter['type'] === 'existing') {
				$existing_filter_ids[$key]['id'] = $filter['no'];
				$existing_filter_ids[$key]['field'] = $filter['custom_field'];
			}
		}
		//foreach, check through for new or removed entries and update
		foreach($existing_filter_ids as $filter){
			$id = $filter['id'];
			$custom_field = $filter['field'];
			$cache_sql = "SELECT `value` FROM " . $wpdb->prefix . "wovaxapp_search_filter_dropdowns WHERE filter_id=".$id."";
    	$cached_options = $wpdb->get_col($cache_sql);
			$existing_sql = "SELECT `meta_value` FROM `".$wpdb->postmeta."` WHERE meta_key='".$custom_field."';";
			$live_options = $wpdb->get_col($existing_sql);
			$check = array();
			foreach($live_options as $option){
				if(!empty($option)){
					$option = ucwords(strtolower($option));
					$check[trim($option)] = true;
				}
			}
			$live_options = array_keys($check);
			$new_options = array_diff($live_options,$cached_options);
			$inactive_options = array_diff($cached_options,$live_options);
			//and then insert the new options in the db
			$table = $wpdb->prefix."wovaxapp_search_filter_dropdowns";
			if(!empty($new_options)){
				$insert_sql = "INSERT INTO ".$table." (`filter_id`,`display_name`,`value`) VALUES ";
				foreach($new_options as $option){
					$insert_sql .= "(";
					$insert_sql .= "'".$id."',";
					$insert_sql .= "'".addslashes($option)."',";
					$insert_sql .= "'".addslashes($option)."'";
					$insert_sql .= "),";
				}
				$insert_sql = rtrim($insert_sql, ",");
				$wpdb->query($insert_sql);
			}
			//and remove the inactive ones
			if(!empty($inactive_options)){
				$inactive_options = esc_sql($inactive_options);
				$inactive_option_string = implode("', '",$inactive_options);
				$delete_sql = "DELETE FROM ".$table." WHERE `filter_id` = ".$id." AND `value` IN ('".$inactive_option_string."')";
				$wpdb->query($delete_sql);
			}
		}
	}
?>
