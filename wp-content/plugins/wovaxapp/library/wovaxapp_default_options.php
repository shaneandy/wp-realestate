<?php

$options = array(
     
      "ad_option" => "no_ad",
      "android_action_bar_buttons_color" => "167EFC",
      "android_action_bar_color" => "00A9F3",
      "android_action_bar_menu_icon" => "YES",
      "android_action_bar_settings_icon" => "YES",
      "android_action_bar_title_color" => "FFFFFF",
      "android_comment_button" => "Post",
      "android_gcm_api_key" => "API Key",
      "android_gcm_environment" => "1",
      "android_maps_api_key" => "",
      "android_menu_background_color" => "FFFFFF",
      "android_menu_highlight_background_color" => "00A9F3",
      "android_menu_highlight_text_color" => "FFFFFF",
      "android_menu_home_button" => "Home",
      "android_menu_text_color" => "000000",
      "android_post_comment_placeholder" => "Post a comment",
      "android_save_button" => "Save",
      "android_settings_title" => "Settings",
      "android_status_bar_color" => "0087D0",
      "android_tint_color" => "00A9F3",
      "api_controller_paths" => WOVAX_APP_DIR."api/controllers,",
      "app_home_post_type" => "",
      "app_home_screen" => "blog",
      "app_home_webview_url" => "https://wovax.com/",
      "app_home_page_id" => "",
      "app_name" => "App Name",
      "custom_fields_filter_placeholder" => "Any",
      "custom_fields_filter_results_title" => "Results",
      "db_version" => "",
      "excluded_categories" => "",
      "filter_display" => "firstrun",
      "filter_names" => "",
      "google_analytics_id" => "UA-00000000-0",
      "html_custom_ad" => "",
      "ios_apns_environment" => "1",
      "ios_back_button" => "Back",
      "ios_body_font" => "HelveticaNeue",
      "ios_comment_button" => "Post",
      "ios_development_pem_path" => "development.pem",
      "ios_external_push_server_path" => "example.com/apns.php",
      "ios_ipad_segment_menu" => "Menu",
      "ios_ipad_segment_posts" => "All Posts",
      "ios_menu_background_color" => "FFFFFF",
      "ios_menu_divide_color" => "D9D9D9",
      "ios_menu_highlight_background_color" => "D9D9D9",
      "ios_menu_highlight_text_color" => "000000",
      "ios_menu_home_button" => "Home",
      "ios_menu_text_color" => "000000",
      "ios_meta_font" => "HelveticaNeue",
      "ios_nav_bar_color" => "F8F8F8",
      "ios_nav_buttons_color" => "007AFF",
      "ios_nav_logo_url" => "",
      "ios_nav_logo" => "NO",
      "ios_nav_menu_icon" => "YES",
      "ios_nav_settings_icon" => "YES",
      "ios_nav_title_color" => "000000",
      "ios_pem_password" => "",
      "ios_post_comment_placeholder" => "Post a comment",
      "ios_production_pem_path" => "production.pem",
      "ios_push_host" => "1",
      "ios_push_setup_indicator" => "",
      "ios_save_button" => "Save",
      "ios_search_bar_color" => "C9C9CE",
      "ios_settings_title" => "Settings",
      "ios_status_bar_color" => "black",
      "ios_title_font" => "HelveticaNeue",
      "ios_tint_color" => "007AFF",
      "ios_apple_watch_primary_color" => "007AFF",
      "ios_apple_watch_secondary_color" => "FFFFFF",
      "ios_ui_switch_color" => "4BD863",
      "ios_search_bar_cancel_text_color" => "007AFF",
      "ios_loading_indicator_color","000000",
      "ios_tab_bar_buttons_color" => "007AFF",
      "ios_tab_bar_background_color" => "FFFFFF",
      "js_custom_ad" => "",
      "json_api_base" => "json_api",
      "link_banner" => "",
      "network_enabled" => "NO",
      "network_sites" => "",
      "phone_portrait_banner" => "",
      "pushnotification_categories" => '[{"id":1111111111,"name":"General"}]',
	"search_posts_per_page" => 9,
      "tablet_portrait_banner" => "",
      "user_accounts" => "NO",
      "user_registration" => "NO",
      "user_accounts_guests" => "YES",
      "user_accounts_launch_screen" => "NO",
      "favorites" => "NO",
      "login_segment" => "Login",
      "register_segment" => "Register",
      "login_table_title" => "Login",
      "register_table_title" => "Register",
      "login_button" => "Login",
      "register_button" => "Register",
      "terms_sentence_beginning" => "By registering you agree to the terms and conditions of our app,",
      "terms_sentence_link" => "please read the terms and conditions here",
      "terms_sentence_end" => ".",
      "registration_terms" => "<h2>Welcome to Our Website!</h2>
			<p>These terms and conditions outline the rules and regulations for the use of this Website. <br />
			By accessing this website we assume you accept these terms and conditions in full. Do not continue to use this website if you do not accept all of the terms and conditions stated on this page.
			</p>
			<p>The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and any or all Agreements: 'Client', 'You' and 'Your' refers to you, the person accessing this website and accepting the Company’s terms and conditions. 'The Company', 'Ourselves', 'We', 'Our' and 'Us', refers to our Company. 'Party', 'Parties', or 'Us', refers to both the Client and ourselves, or either the Client or ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner, whether by formal meetings of a fixed duration, or any other means, for the express purpose of meeting the Client’s needs in respect of provision of the Company’s stated services/products, in accordance with and subject to, prevailing law of United States. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.</p>

			<h2>Cookies</h2>
			<p>We employ the use of cookies. By using this website you consent to the use of cookies in accordance with Our privacy policy.</p>
			<p>We, like most modern day interactive web sites, use cookies to enable us to retrieve user details for each visit. Cookies are used in some areas of our site to enable the functionality of these areas and ease of use for vistors. Some of our affiliate / advertising partners may also use cookies.</p>

			<h2>License</h2>
			<p>Unless otherwise stated, our Company and/or it’s licensors own the intellectual property rights for all material on this website. All intellectual property rights are reserved. You may view and/or print pages from this website for your own personal use subject to restrictions set in these terms and conditions.</p>
			<p>You must not:</p>
			<ul>
				<li>Republish material from this website</li>
				<li>Sell, rent or sub-license material from this website</li>
				<li>Reproduce, duplicate or copy material from this website</li>
			</ul>
			<p>Redistribute content from our Company (unless content is specifically made for redistribution).</p>

			<h3>User Comments</h3>
			<ol>
				<li>This Agreement shall begin on the date hereof.</li>
				<li>Certain parts of this website offer the opportunity for users to post and exchange opinions, information, material and data ('Comments') in areas of the website. Our Company does not screen, edit, publish or review Comments prior to their appearance on the website and Comments do not reflect the views or opinions of Our Company, its agents or affiliates. Comments reflect the view and opinion of the person who posts such view or opinion. To the extent permitted by applicable laws Our Company shall not be responsible or liable for the Comments or for any loss cost, liability, damages or expenses caused and or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website.</li>
				<li>Our Company reserves the right to monitor all Comments and to remove any Comments which it considers in its absolute discretion to be inappropriate, offensive or otherwise in breach of these Terms and Conditions.</li>
				<li>You warrant and represent that:
					<ol>
						<li>You are entitled to post the Comments on our website and have all necessary licenses and consents to do so;</li>
						<li>The Comments do not infringe any intellectual property right, including without limitation copyright, patent or trademark, or other proprietary right of any third party;</li>
						<li>The Comments do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful material or material which is an invasion of privacy</li>
						<li>The Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity.</li>
					</ol>
				</li>
				<li>You hereby grant to <strong>Our Company</strong> a non-exclusive royalty-free license to use, reproduce, edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or media.</li>
			</ol>

			<h2>Iframes</h2>
			<p>Without prior approval and express written permission, you may not create frames around our Web pages or use other techniques that alter in any way the visual presentation or appearance of our Web site.</p>

			<h2>Content Liability</h2>
			<p>We shall have no responsibility or liability for any content appearing on your Web site. You agree to indemnify and defend us against all claims arising out of or based upon your Website. No link(s) may appear on any page on your Web site or within any context containing content or materials that may be interpreted as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.</p>

			<h2>Reservation of Rights</h2>
			<p>We reserve the right at any time and in its sole discretion to request that you remove all links or any particular link to our Web site. You agree to immediately remove all links to our Web site upon such request. We also reserve the right to amend these terms and conditions and its linking policy at any time. By continuing to link to our Web site, you agree to be bound to and abide by these linking terms and conditions.</p>

			<h2>Removal of links from our website</h2>
			<p>If you find any link on our Web site or any linked web site objectionable for any reason, you may contact us about this. We will consider requests to remove links but will have no obligation to do so or to respond directly to you.</p>
			<p>Whilst we endeavor to ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we commit to ensuring that the website remains available or that the material on the website is kept up to date.</p>

			<h2>Disclaimer</h2>
			<p>To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website (including, without limitation, any warranties implied by law in respect of satisfactory quality, fitness for purpose and/or the use of reasonable care and skill). Nothing in this disclaimer will:</p>

			<ol>
				<li>limit or exclude our or your liability for death or personal injury resulting from negligence;</li>
				<li>limit or exclude our or your liability for fraud or fraudulent misrepresentation;</li>
				<li>limit any of our or your liabilities in any way that is not permitted under applicable law; or</li>
				<li>exclude any of our or your liabilities that may not be excluded under applicable law.</li>
			</ol>
			<p>The limitations and exclusions of liability set out in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer or in relation to the subject matter of this disclaimer, including liabilities arising in contract, in tort (including negligence) and for breach of statutory duty.</p>
			<p>To the extent that the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature.</p>", //LEGAL TEXT
      "terms_alert_title" => "Terms & Conditions",
      "terms_alert_text" => "By creating an account you agree to our terms & conditions.",
      "terms_alert_cancel_button" => "Cancel",
      "terms_alert_agree_button" => "Agree",
      "continue_as_guest" => "Continue as a guest user?",
      "sign_out_button" => "Sign Out",
      "sort_fields" => array(
            0 => array(
                  "display_name" => "Date - Newest to Oldest",
                  "value" => "date-desc"
            ),
            1 => array(
                  "display_name" => "Date - Oldest to Newest",
                  "value" => "date-asc" 
            )      
      ),
	  "default_search_sort" => "date-desc"
    );