<?php

  { // functions needed in app ads page
	add_action( 'wp_ajax_banner_image', 'app_ads_banner_image' );
	function app_ads_banner_image() {
	  $ajax = "banner_image";
	  include WOVAX_APP_DIR."/pages/app_ads.php";
	  die();
	}

	add_action( 'wp_ajax_html_upload', 'app_ads_html_upload' );
	function app_ads_html_upload() {
	  $ajax = "html_upload";
	  include WOVAX_APP_DIR."/pages/app_ads.php";
	  die();
	}

	add_action( 'wp_ajax_no_ad', 'app_ads_no_ad' );
	function app_ads_no_ad() {
	  $ajax = "no_ad";
	  include WOVAX_APP_DIR."/pages/app_ads.php";
	  die();
	}
  }

  { // functions needed in advanced menu page
	add_action( 'wp_ajax_APNS_current', 'apple_push_settings_banner_image' );
	function apple_push_settings_banner_image() {
	  ob_start();
	  ?>
	  <p>
		*<?php echo __('App must be installed on a device with push notifications enabled.', 'wovax_translation')?>
	  </p>
	  <?php
	  $output = ob_get_clean();
	  echo $output;
	  die();
	}

	add_action( 'wp_ajax_APNS_external', 'apple_push_settings_html_upload' );
	function apple_push_settings_html_upload() {
	  ob_start();
	  ?>
	  <p>
		<?php echo __('If your current server fails to send a push notification you can use an external server.', 'wovax_translation')?><br>
		<?php echo __('Please upload apns.php to an external server that allows TCP port 2195 & 2196 for APNS in & out connections and set the URL to the correct path.', 'wovax_translation')?>
		e.g http://www.yourdomain.com/apns.php <?php echo __('and paste it into the text box below.', 'wovax_translation')?>
	  </p>
	  http://
	  <input
	  type="text"
	  id="wovax_ios_external_push_server_path"
	  name="wovax_ios_external_push_server_path"
	  size="30"
	  <?php
	  echo " value='" . get_option("wovax_ios_external_push_server_path") . "'";
	  ?>
	  > e.g http://www.yourdomain.com/apns.php
	  <br><br>
	  <p>
		*<?php echo __('App must be installed on a device with push notifications enabled.', 'wovax_translation')?>
	  </p>
	  <?php
	  $output = ob_get_clean();
	  echo $output;
	  die();
	}

	add_action( 'wp_ajax_APNS_no', 'apple_push_settings_no_ad' );
	function apple_push_settings_no_ad() {
	  ob_start();
	  ?>
	  <p>
		<?php echo __('Do not worry. You can always setup APNS push notifications later.', 'wovax_translation')?>
	  </p>
	  <?php
	  $output = ob_get_clean();
	  echo $output;
	  die();
	}
  }

  { // functions needed for notification categories page
	add_action('wp_ajax_wx_rearrange_rows', 'wovax_rearrange_rows');
	function wovax_rearrange_rows() {
	  $current = json_decode(get_option('wovax_pushnotification_categories'));
	  $data = explode(",",$_POST['data']);
	  unset($data[0]); // removes undefined javascript thing
	  $data = array_values($data);
	  $new_data = array();

	  for ($i=0;$i<sizeof($data);$i++) {
		for ($j=0;$j<sizeof($current);$j++) {
		  if ($data[$i] == $current[$j]->name) {
			$new_data[] = $current[$j];
		  }
		}
	  }
	  update_option('wovax_pushnotification_categories',json_encode($new_data));
	  echo "The order of the things has been changed to: ".implode(", ",$data);
	  die();
	}

	add_action('wp_ajax_wx_delete_categories', 'wovax_delete_categories');
	function wovax_delete_categories() {
	  $categories = json_decode(get_option('wovax_pushnotification_categories'));
	  $new_categories = array();
	  $delete = $_POST['data'];
	  for ($i=0;$i<sizeof($categories);$i++) {
		if ($categories[$i]->name != $delete) {
		  $new_categories[] = $categories[$i];
		}
	  }
	  echo 'You have successfully removed the "'.$delete.'" category!';
	  update_option('wovax_pushnotification_categories',json_encode($new_categories));
	  die();
	}
  }

  { // functions needed on send-notification page
	add_action('wp_ajax_wx_delete_notification', 'wx_delete_notification');
	function wx_delete_notification() {
	  global $wpdb;
		$delete = stripslashes($_POST['data']);
		$sql = "DELETE FROM " . $wpdb->prefix . "wovaxapp_pushnotification WHERE unixtime='".$delete."'";
		$wpdb->query($sql);
	  echo "The pushnotification sent on ".gmdate('D, d M Y H:i:s T', $delete)." has been deleted from the database.";
	  die();
	}

	add_action('wp_ajax_wovax_notification_number', 'wovax_notification_number');
	function wovax_notification_number() {
	  global $wpdb;
	  ?>
	  <table class="widefat" cellspacing="0">
	  	<thead>
	  		<tr height="30px">
	  			<th class="manage-column" ><?php echo __('Message', 'wovax_translation');?></th>
	  			<th class="manage-column" ><?php echo __('Time Sent', 'wovax_translation');?></th>
	  			<th class="manage-column" style="text-align:right"><?php echo __('Delete', 'wovax_translation');?></th>
	  		</tr>
	  	</thead>
		<?php
		  { // Set number of messages to recall
	  			$sql = "SELECT * FROM " . $wpdb->prefix . "wovaxapp_pushnotification ORDER BY unixtime DESC";
	  			$messages = $wpdb->get_results($sql,ARRAY_A);
	  			if ($_POST['data'] != "") {
	  				if ($_POST['data'] == "ALL") {
	  					$count = count($messages);
	  				}elseif ($_POST['data'] <= count($messages)){
	  					$count = $_POST['data'];
	  				}else {
	  					$count = count($messages);
	  				}
	  			}else {
	  				if (count($messages) <= 5) {
	  					$count = count($messages);
	  				}else {
	  					$count = 5;
	  				}
	  			}
	  		}
	  		{ // Iterate through messages to display
	  			for ($i=0;$i<$count;$i++) {
	  				if ($messages[$i]['unixtime'] != 0) {
	  					$dt = new DateTime('@'.$messages[$i]['unixtime']);
	  					$dt->setTimeZone(new DateTimeZone(get_option('timezone_string')));
	  					$date = $dt->format(get_option('date_format')." \a\\t ".get_option('time_format'));
	  				}else {
	  					$date = "";
	  				}
	  				$unixtime = $messages[$i]['unixtime'];
	  				if ($unixtime != 0) {
	  					$checkbox = "<input type='checkbox' name='delete[]' value=$unixtime>";
	  				}else {
	  					$checkbox = "";
	  				}
	  				if (($i % 2) == 1) {
	  				  $class = "alternate";
	  				} else {
	  				  $class = "";
	  				}
	  				echo "<tr class='$class' height='30px'>";
	  				echo "<td >" . $messages[$i]['pushmsg'] . "</td>";
	  				echo "<td align='left' >" . $date . "</td>";
	  				echo "<td class='wx-notification-delete' align='right'><a href='#'>Delete</a></td>";
	  				echo "<td class='wx-unixtime' hidden>$unixtime</td>";
	  				echo "</tr>";
	  			}
	  		}
		?>
	  </table>
	  <?php
		{ // displays "showing x of y notifications sent" at bottom of table
				if ($_POST['data'] != "") {
					if ($_POST['data'] == "ALL") {
						$show = count($messages);
					}elseif ($_POST['data'] <= count($messages)){
						$show = $_POST['data'];
					}else {
						$show = count($messages);
					}
				}else {
					if (count($messages) > 5) {
						$show = 5;
					}else {
						$show = count($messages);
					}
				}
				echo "<small>";
				echo __('Showing', 'wovax_translation')." <span class='wx-shown-number'>".$show."</span> ".__('of', 'wovax_translation')." <span class='wx-total-number'>".count($messages)."</span> ".__('Push Notifications Sent', 'wovax_translation');
				echo "</small>";
			}
	  die();
	}

	add_action('wp_ajax_wx_page_post_autocomplete', 'wx_page_post_autocomplete');
	function wx_page_post_autocomplete() {
	  global $wpdb;
	  $sql = "SELECT `ID`,`post_title`,`post_type` FROM `".$wpdb->prefix."posts` WHERE `post_title` LIKE '%".$_POST['data']['term']."%' AND `post_type` NOT LIKE 'revision' ";
	  $titles = $wpdb->get_results($sql,ARRAY_A);
	  $results = array();
	  for ($i=0;$i<sizeof($titles);$i++) {
		$results[] = array(
		  "label" => $titles[$i]['post_title'] . ' - ' . ucfirst($titles[$i]['post_type']),
		  "value" => $titles[$i]['ID']
		);
	  }
	  echo json_encode($results);
	  die();
	}

	add_action('wp_ajax_wx_send_push_notification', 'wovax_send_push_notification');
	function wovax_send_push_notification() {
			global $wpdb;
		$device_count = 0;

		{ // begin APNS
		  { // define data to send
			$payload['aps'] = array(
			  'alert' => stripslashes($_POST['message']),
			  'badge' => 1,
			  'sound' => 'default',
			  'redirectType' => $_POST['redirectType'],
			  'redirectValue' => $_POST['redirectValue']
			);
			$payload = json_encode($payload);
		  }

		  { // setup APNS connection
			$passphrase = get_option('wovax_ios_pem_password');
			if (get_option('wovax_ios_apns_environment') != "2") {
			  $sandbox = ".sandbox";
			  $apnsCert = "../".get_option('wovax_ios_development_pem_path');
			}else {
			  $sandbox = "";
			  $apnsCert = "../".get_option('wovax_ios_production_pem_path');
			}

			$streamContext = stream_context_create();
			stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
			stream_context_set_option($streamContext, 'ssl', 'passphrase', $passphrase);
			$apns = stream_socket_client('ssl://gateway'.$sandbox.'.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $streamContext);
		  }

		  { // execute APNS
			if (!$apns) {
			  echo "Error: $err ($errstr)<br />\n";
			} else {
			  if (get_option('wovax_ios_apns_environment') != "2") {
				$sql = "SELECT * FROM ".$wpdb->prefix."wovaxapp_apns_device_development";
			  }else {
				$sql = "SELECT * FROM ".$wpdb->prefix."wovaxapp_apns_device";
			  }
			  $APNS_devices = $wpdb->get_results($sql);
			  { // send to array of subscribers
				for ($i=0;$i<sizeof($APNS_devices);$i++) {
				  $blacklisted_categories = explode(",",$APNS_devices[$i]->blacklisted_categories);
				  if (!in_array($_POST['categories'],$blacklisted_categories)) {
					$deviceToken = $APNS_devices[$i]->device_id;
					$apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
					fwrite($apns, $apnsMessage);

					$total_sent = $APNS_devices[$i]->total_sent + 1;
					$sql = "UPDATE ".$wpdb->prefix."wovaxapp_apns_device SET total_sent=".$total_sent." WHERE device_id='".$deviceToken."'";
					$wpdb->query($sql);
					$device_count++;
				  }
				}
			  }
			  fclose($apns);
			  $sql = "INSERT INTO ".$wpdb->prefix."wovaxapp_pushnotification (pushmsg, unixtime, type) VALUES ('".$_POST['message']."', '".time()."', 'none')";
			  $wpdb->query($sql);
			}
		  }

		  { // APNS feedback
			$stream_context = stream_context_create();
			stream_context_set_option($stream_context, 'ssl', 'local_cert', $apnsCert);
			$apns = stream_socket_client('ssl://feedback.push.apple.com:2196', $errcode, $errstr, 60, STREAM_CLIENT_CONNECT, $stream_context);
			if(!$apns) {
				echo "ERROR $errcode: $errstr\n";
			}

			$feedback_tokens = array();
			//and read the data on the connection:
			while(!feof($apns)) {
				$data = fread($apns, 38);
				if(strlen($data)) {
					$feedback_tokens[] = unpack("N1timestamp/n1length/H*devtoken", $data);
				}
			}
			fclose($apns);
			if (sizeof($feedback_tokens != 0)) {
			  for ($i=0;$i<sizeof($feedback_tokens);$i++) {
				$wpdb->delete($wpdb->prefix."wovaxapp_apns_device",array('device_id' => $feedback_tokens[$i]['devtoken']));
				//echo $feedback_tokens[$i]['devtoken']."<br>";
			  }
			}
		  }
		}

		{ // begin GCM
		  { // define devices to send
			$sql = "SELECT * FROM ".$wpdb->prefix."wovaxapp_gcm_device";
			$devices = $wpdb->get_results($sql,ARRAY_A);
			$ids = array();
			for ($i=0;$i<sizeof($devices);$i++) {
			  $ids[] = $devices[$i]['device_id'];
			  $device_count++;
			}
		  }

		  { // define data to send to devices
			$data = array("message" => stripslashes($_POST['message']));
			$url = 'https://android.googleapis.com/gcm/send';
			$post = array(
			  'registration_ids'  => $ids,
			  'data'              => $data,
			);
			$headers = array(
				'Authorization: key='.get_option('wovax_android_gcm_api_key'),
				'Content-Type: application/json'
			);
		  }

		  { // execute CURL request
			$ch = curl_init();

			curl_setopt( $ch, CURLOPT_URL,$url);
			curl_setopt( $ch, CURLOPT_POST,true);
			curl_setopt( $ch, CURLOPT_HTTPHEADER,$headers);
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER,true);
			curl_setopt( $ch, CURLOPT_POSTFIELDS,json_encode($post));

			$result = curl_exec($ch);
		  }

		  { // handle errors, close CURL connection
			if (curl_errno($ch)) {
				wp_mail("developer@wovax.com","error",'GCM error: '.curl_error($ch));
			}else {
				$result = json_decode($result);
				if ($result->failure != 0 || $result->canonical_ids != 0) {
					$reports = $result->results;
					for ($i=0;$i<sizeof($reports);$i++) {
					  if ($reports[$i]->registration_id != "") {
						  $devices[$i]['device_id'] = $reports[$i]->registration_id;
						  $devices[$i]['action'] = "update_id";
					  }elseif ($reports[$i]->error == "NotRegistered") {
						  $devices[$i]['action'] = "remove";
					  }else {
						  $devices[$i]['action'] = "false";
					  }
					}

					for ($i=0;$i<sizeof($devices);$i++) {
					  if ($devices[$i]['action'] == "update_id") {
						$wpdb->update(
						  "wp_wovaxapp_gcm_device",
						  array(
							'device_id' => $devices[$i]['device_id']
						  ),
						  array(
							'id' => $devices[$i]['id']
						  )
						);
					  }elseif ($devices[$i]['action'] == "remove") {
						$wpdb->delete(
						  "wp_wovaxapp_gcm_device",
						  array(
							'id' => $devices[$i]['id']
						  )
						);
					  }
					}
				}
			}
			curl_close($ch);
		  }
		}

			{ // send push number to Wovax
		  $msg = file_get_contents("http://api.wovax.io/api_key/master/update_push_count/?count=".$device_count);
				unset($msg);
			}

		echo "Message sent!";
	  die();
	}
  }

  { // functions needed on search filter page
	add_action('wp_ajax_wx_set_post_types', 'wx_set_post_types');
	function wx_set_post_types() {
	  $args = array(
		'post_type' => $_POST['data'],
	  );
	  $posts = get_posts($args);
	  $custom_fields = array();
	  for ($i=0;$i<sizeof($posts);$i++) {
		$id = $posts[$i]->ID;
		$meta = get_post_meta($id);
		foreach ($meta as $k=>$v) {
		  $custom_fields[] = $k;
		}
	  }
	  $custom_fields = array_values(array_unique($custom_fields));
	  for ($i=0;$i<sizeof($custom_fields);$i++) {
		echo "<option value='".$custom_fields[$i]."'>".$custom_fields[$i]."</option>";
	  }
	  die();
	}

	add_action('wp_ajax_wx_rearrange_search_filter_rows', 'wx_rearrange_search_filter_rows');
	function wx_rearrange_search_filter_rows() {
	  global $wpdb;
		$sql = "SELECT * FROM " . $wpdb->prefix . "wovaxapp_search_filter ORDER BY item_order DESC";
		$current = $wpdb->get_results($sql,ARRAY_A);

	  $new = rtrim(str_replace("undefined,","",stripslashes($_POST['data'])),",");
	  $new = explode(",",$new);

	  for ($i=0;$i<sizeof($new);$i++) {
		$wpdb->update(
		  $wpdb->prefix."wovaxapp_search_filter",
		  array("item_order" => $i+1),
		  array("display_name" => $new[$i])
		);
	  }
	  print_r("The order has been changed to: ".implode(", ",$new));
	  die();
	}

	add_action('wp_ajax_wx_delete_search_filter_rows', 'wx_delete_search_filter_rows');
	function wx_delete_search_filter_rows() {
	  global $wpdb;
	  $delete = stripslashes($_POST['data']);
	  $sql = 'DELETE FROM ' . $wpdb->prefix . 'wovaxapp_search_filter WHERE display_name="'.$delete.'"';
	  $wpdb->query($sql);
	  echo "The ".$delete." object has been removed";
	  die();
	}

	add_action('wp_ajax_wx_save_search_filter_name', 'wx_save_search_filter_name');
	function wx_save_search_filter_name() {
	  	$name = array(
			array(
				"name" => $_POST['data'],
				"type" => $_POST['more_data']
			)
		);
		$posts_per_page = $_POST['posts_per_page'];
		update_option("wovax_filter_names",json_encode($name));
		update_option('wovax_search_posts_per_page', $posts_per_page);
	  	print_r($name);
	  	die();
	}

	add_action('wp_ajax_wx_save_search_filter_checkbox', 'wx_save_search_filter_checkbox');
	function wx_save_search_filter_checkbox() {
	  global $wpdb;
	  $wpdb->update(
		$wpdb->prefix."wovaxapp_search_filter",
		array(
		  'in_app' => $_POST['data']
		),
		array(
		  'no' => $_POST['id']
		)
	  );
	  if ($_POST['data'] === "true") {
		echo "The filter with ID of '".$_POST['id']."' will be shown in the app ".$_POST['data'];
	  }else {
		echo "The filter with ID of '".$_POST['id']."' will not be shown in the app";
	  }
	  die();
	}

	add_action('wp_ajax_wx_add_search_filter', 'wx_add_search_filter');
	function wx_add_search_filter() {
		global $wpdb;
		if ($_POST['data']['content'] == "true") {
			$custom_field = "search__content";
		} else {
			$custom_field = $_POST['data']['field'];
		}
		$values = array(
			"in_app" => "true",
			"item_order" => 0,
			"post_type" => "",
			"custom_field" => $custom_field,
			"display_name" => stripslashes($_POST['data']['label']),
			"default_value" => stripslashes($_POST['data']['placeholder']),
			"type" => $_POST['data']['search_type'],
		);
		$wpdb->insert(
			$wpdb->prefix.'wovaxapp_search_filter',
			$values
		);
		$filter_id = $wpdb->insert_id;
		if($_POST['data']['search_type'] === 'existing'){
			$sql = "SELECT `meta_value` FROM `".$wpdb->postmeta."` WHERE meta_key='".$custom_field."';";
			$options = $wpdb->get_col($sql);
			$check = array();
			foreach($options as $option){
	   if(!empty($option)){
					$option = ucwords(strtolower($option));
					$check[trim($option)] = true;
				}
			}
			$options = array_keys($check);
			sort($options, SORT_NATURAL);
			$table = $wpdb->prefix."wovaxapp_search_filter_dropdowns";
			$insert_sql = "INSERT INTO ".$table." (`filter_id`,`display_name`,`value`) VALUES ";
			foreach($options as $option){
				$insert_sql .= "(";
				$insert_sql .= "'".$id."',";
				$insert_sql .= "'".addslashes($option)."',";
				$insert_sql .= "'".addslashes($option)."'";
				$insert_sql .= "),";
			}
			$insert_sql = rtrim($insert_sql, ",");
			$wpdb->query($insert_sql);
		}

		$sql = "SELECT * FROM " . $wpdb->prefix . "wovaxapp_search_filter ORDER BY item_order ASC";
		$filters = $wpdb->get_results($sql,ARRAY_A);
		for ($i=0;$i<sizeof($filters);$i++) {
			if (($i % 2) == 1) {
				$class = "alternate";
			} else {
				$class = "";
			}

			if ($filters[$i]['in_app'] === "true") {
				$checked = " checked ";
			} else {
				$checked = "";
			}

			?>
			<tr class='wx-search-filter-row <?php echo $class; ?>' height="30px">
				<td class='wx-search-filter-id'><?php echo $filters[$i]['no'];?></td>
				<td><input class='wx-search-filter-display-checkbox' type='checkbox' name='display' value='Display' <?php echo $checked;?>></td>
				<td><?php echo $filters[$i]['custom_field'];?></td>
				<td class="wx-search-filter-name"><?php echo stripslashes($filters[$i]['display_name']);?></td>
				<td class="wx-search-filter-placeholder"><?php echo stripslashes($filters[$i]['default_value']);?></td>
				<td><?php echo $filters[$i]['type'];?></td>
				<td><a class="wx-search-filter-edit" href='#'>Edit</a></td>
				<td class="wx-search-filter-delete" style="text-align:right"><a href='#'>Delete</a></td>
			</tr>
			<?php
		}
		die();
	}
	
	add_action('wp_ajax_wx_save_edited_filter_value','wx_save_edited_filter_value');
	function wx_save_edited_filter_value(){
		global $wpdb;
		$data = $_POST['data'];
		$id = $data['id'];
		$new_display_value = $data['new_display_value'];
		$new_placeholder = $data['new_placeholder'];
		$table = $wpdb->prefix . 'wovaxapp_search_filter';
		$new_data = array('display_name' => $new_display_value, 'default_value' => $new_placeholder);
		$where = array('no' => $id);
		//$sql = 'UPDATE ' . $wpdb->prefix . 'wovaxapp_search_filter SET display_name="'.$new_display_value.'" WHERE id="'.$range_id.'"';
		$update = $wpdb->update($table,$new_data,$where);
		if(false === $update) {
			die('FAILURE');
		} else {
			die();
		}
	}
	
	add_action('wp_ajax_wx_add_sort_type', 'wx_add_sort_type');
	function wx_add_sort_type() {
	  $data = $_POST['data'];
	  extract($data);
	  $sort_fields = get_option('wovax_sort_fields');
	  if($sort_type == 'alphabetical') {
		$desc_name = " - Z to A";
		$asc_name = " - A to Z";
	  } else if ($sort_type == 'numeric') {
		$desc_name = " - High to Low";
		$asc_name = " - Low to High";
	  }
	  $new_sort_desc = array(
		'display_name' => $field.$desc_name,
		'value' => $field."-".$sort_type."-desc",
	  );
	  $new_sort_asc = array(
		'display_name' => $field.$asc_name,
		'value' => $field."-".$sort_type."-asc",
	  );
	  $sort_fields[] = $new_sort_desc;
	  $sort_fields[] = $new_sort_asc;
	  update_option('wovax_sort_fields', $sort_fields);
	  ?>
		<tr class='wx-search-sort-row' height="30px">
									<td class="wx-search-sort-field"><?php echo $field; ?></td>
									<td class="wx-search-sort-name"><?php echo $new_sort_desc['display_name']; ?></td>
									<td class="wx-search-sort-value"><?php echo $new_sort_desc['value']; ?></td>
									<td class="wx-search-sort-direction">desc</td>
									<td class="wx-search-sort-type"><?php echo $sort_type; ?></td>
									<td style="text-align:right"><a class="wx-search-sort-edit" href='#'>Edit</a></td>
									<td class="wx-search-sort-delete" style="text-align:right"><a href='#'>Delete</a></td>
				</tr>
		<tr class='wx-search-sort-row alternate' height="30px">
									<td class="wx-search-sort-field"><?php echo $field; ?></td>
									<td class="wx-search-sort-name"><?php echo $new_sort_asc['display_name']; ?></td>
									<td class="wx-search-sort-value"><?php echo $new_sort_asc['value']; ?></td>
									<td class="wx-search-sort-direction">asc</td>
									<td class="wx-search-sort-type"><?php echo $sort_type; ?></td>
									<td style="text-align:right"><a class="wx-search-sort-edit" href='#'>Edit</a></td>
									<td class="wx-search-sort-delete" style="text-align:right"><a href='#'>Delete</a></td>
				</tr>
	  <?php
	  die();
	}
	
	add_action('wp_ajax_wx_delete_search_sort_rows', 'wx_delete_search_sort_rows');
	function wx_delete_search_sort_rows() {
	  $delete_value = $_POST['data'];
	  $sort_options = get_option('wovax_sort_fields');
	  foreach($sort_options as $key => $values){
		if($values["value"] == $delete_value) {
		  unset($sort_options[$key]);
		}
	  }
	  $sort_options = array_values($sort_options);
	  update_option('wovax_sort_fields', $sort_options);
	  echo "Sort Value Deleted";
	  die();
	}
	
	add_action('wp_ajax_wx_save_edited_sort_value', 'wx_save_edited_sort_value');
	function wx_save_edited_sort_value() {
	  $data = $_POST['data'];
	  $identifying_value = $data['sort_value'];
	  $updated_display_name = $data['new_display_value'];
	  $sort_options = get_option('wovax_sort_fields');
	  
	  foreach($sort_options as $key => $values){
		if($values['value'] == $identifying_value){
		  $sort_options[$key]['display_name'] = $updated_display_name;
		}
	  }
	  update_option('wovax_sort_fields', $sort_options);
	  echo "Value Updated!";
	  die();
	}
	
	add_action('wp_ajax_wx_save_default_sort_value','wx_save_default_sort_value');
	function wx_save_default_sort_value(){
		$data = $_POST['data'];
		$default_sort = get_option('wovax_default_search_sort');
		if($data != $default_sort){
			$update = update_option('wovax_default_search_sort', $data);
			echo $update;
			die();
		} else {
			echo 'No Change';
			die();
		}
	}
	
	add_action('wp_ajax_wx_range_getter','wx_range_getter');
	function wx_range_getter(){
		global $wpdb;
		$range_number = $_POST['data'];
		$sql = "SELECT * FROM ".$wpdb->prefix."wovaxapp_search_filter_dropdowns WHERE filter_id='".$range_number."' ORDER BY item_order ASC";
		$existing_ranges = $wpdb->get_results($sql, ARRAY_A);
		if(!empty($existing_ranges)){
			$count = 0;
			foreach($existing_ranges as $range) {
				$range['item_order'] = intval($range['item_order']); 
				if($count++ % 2 == 1) {
					$alternate = 'class="alternate"';
				} else {
					$alternate = '';
				}
				?>
				<tr <?php echo $alternate;?>>
					<td class="wx-search-range-id <?php echo $alternate;?>"><?php echo $range['id']; ?></td>
					<td class="wx-search-range-name"><?php echo $range['display_name']; ?></td>
					<td><?php echo $range['value']; ?></td>
					<td><a href="#" class="wx-search-range-edit">Edit</a></td>
					<td><a href="#" class="wx-search-range-delete">Delete</a></td>
				</tr>
			<?php    
			}
			die();  
		} else {
			die();
		}
			   
	}
	
	add_action('wp_ajax_wx_add_new_range', 'wx_add_new_range');
	function wx_add_new_range(){
		global $wpdb;
		$data = $_POST['data'];
		$low = $data['low'];
		$high = $data['high'];
		$currency = $data['currency'];
		$filter = $data['filter'];
		$low_formatted = number_format($low);
		$high_formatted = number_format($high);
		$search_value = $low.'-'.$high;
		if($currency === "true") {
			$low_formatted = '$'.$low_formatted;
			$high_formatted = '$'.$high_formatted;
			$display_name = $low_formatted.' - '.$high_formatted;
		} else {
		   $display_name = $low_formatted.' - '.$high_formatted; 
		}
		$sql = "SELECT item_order from ".$wpdb->prefix."wovaxapp_search_filter_dropdowns WHERE filter_id='%s'";
		$sql = $wpdb->prepare($sql, $filter);
		$order = $wpdb->get_col($sql);
		foreach($order as $ind => $pos){
			$order[$ind] = intval($pos);
		}
		$last_item = empty($order) ? 0 : max($order)+1;
	   
		$row = array(
			'item_order' => $last_item,
			'filter_id' => $filter,
			'display_name' => $display_name,
			'value' => $search_value
		);
		$table = $wpdb->prefix."wovaxapp_search_filter_dropdowns";
		$insert = $wpdb->insert($table, $row);
		$error = $wpdb->last_error;
		$ID = $wpdb->get_var('SELECT id from '.$table.' WHERE display_name = "'.$display_name.'"');
		if(!$insert) {
			$error_message = substr($error, 0, 15);
			if($error_message !== "Duplicate entry"){
				$error_message = "ERROR: Unknown Error";
			} else {
				$error_message = "ERROR: Entry already exists, please change values.";
			}
			die($error_message);
		} else { ?>
			<tr>
				<td class="wx-search-range-id"><?php echo $ID;?></td>
				<td class="wx-search-range-name"><?php echo $display_name;?></td>
				<td><?php echo $search_value;?></td>
				<td><a href="#" class="wx-search-range-edit">Edit</a></td>
				<td><a href="#" class="wx-search-range-delete">Delete</a></td>
			</tr>
			<?php
			die();
		}
	}
	
	add_action('wp_ajax_wx_delete_search_range_row', 'wx_delete_search_range_row');
	function wx_delete_search_range_row(){
		global $wpdb;
		$range_id = $_POST['data'];
		$sql = 'DELETE FROM ' . $wpdb->prefix . 'wovaxapp_search_filter_dropdowns WHERE id="'.$range_id.'"';
		$wpdb->query($sql);
		echo "The range with ID ".$range_id." has been removed";
		die();
	}
	
	add_action('wp_ajax_wx_save_edited_range_value', 'wx_save_edited_range_value');
	function wx_save_edited_range_value(){
		global $wpdb;
		$data = $_POST['data'];
		$range_id = $data['range_id'];
		$new_display_value = $data['new_display_value'];
		$table = $wpdb->prefix . 'wovaxapp_search_filter_dropdowns';
		$new_data = array('display_name' => $new_display_value);
		$where = array('id' => $range_id);
		//$sql = 'UPDATE ' . $wpdb->prefix . 'wovaxapp_search_filter_dropdowns SET display_name="'.$new_display_value.'" WHERE id="'.$range_id.'"';
		$update = $wpdb->update($table,$new_data,$where);
		if(false === $update) {
			die('FAILURE');
		} else {
			die();
		}
	}
	
	add_action('wp_ajax_wx_rearrange_search_range_rows','wx_rearrange_search_range_rows');
	function wx_rearrange_search_range_rows(){
		global $wpdb;
		$sql = "SELECT * FROM " . $wpdb->prefix . "wovaxapp_search_filter_dropdowns ORDER BY item_order DESC";
		$current = $wpdb->get_results($sql,ARRAY_A);
		$new = rtrim(str_replace("undefined,","",stripslashes($_POST['data'])),",");
		$new = explode(",",$new);

		for ($i=0;$i<sizeof($new);$i++) {
			$wpdb->update(
				$wpdb->prefix."wovaxapp_search_filter_dropdowns",
				array("item_order" => $i+1),
				array("id" => $new[$i])
		);
		}
		print_r("The order has been changed to: ".implode(", ",$new));
		die();
	}
	
  }
  
  

  { // functions needed on network settings page
	add_action('wp_ajax_wx_network_probe', 'wx_network_probe');
	function wx_network_probe() {
	  $url = $_POST['data']['url'];
	  $key = $_POST['data']['key'];

	  if (substr($url, 0, 7) != "http://" && substr($url, 0, 8) != "https://") {
		$url = "http://".$url;
	  }

	  echo "-----Probing Started-------<br>";
	  echo "-site:<br>";
	  echo "--".$url."<br>";

	  $time_start = microtime();
	  $api = file_get_contents($url."/".$key);
	  $api = json_decode($api);
	  $time_end = microtime();

	  $errors = array();
	  if (json_last_error() !== JSON_ERROR_NONE) {
		$errors[] = "Error parsing response, check URL and API Key";
	  }elseif (!in_array("multisite",$api->controllers)) {
		$errors[] = "Multisite controller not active on remote site";
	  }else {
		$enabled = file_get_contents($url."/".$key."/multisite/multisite_enabled/");
		$enabled = json_decode($enabled);
		echo "-multisite enabled:<br>";
		echo "--".$enabled->multisite_enabled."<br>";
		if ($enabled->multisite_enabled != "YES") {
		  $errors[] = "Multite not enabled in remote site settings";
		}
	  }

	  echo "-errors:<br>";
	  if (sizeof($errors) != 0) {
		foreach ($errors as $error) {
		  echo "--".$error."<br>";
		}
	  }else {
		echo "--No errors detected<br>";
	  }

	  echo "-----Probe Finished-----<br>";
	  echo "Response Time: ". round($time_end - $time_start) . "ms<br><br>";

	  if (sizeof($errors) == 0) {
		echo "Probe successfull. The site may be added to a network";
	  }else {
		echo "Probe failed. You may add the site to a network, but it will not work";
	  }

	  die();
	}

	//Function for adding sites to network settings table
	add_action('wp_ajax_wx_network_add_site','wx_network_add_site');
	function wx_network_add_site() {
	  $current = get_option("wovax_network_sites");

	  $new = new stdClass();
	  $new->id = sizeof($current);
	  $new->name = $_POST['data']['name'];
	  $new->url = $_POST['data']['url'];
	  $new->key = $_POST['data']['key'];

	  $current[] = $new;

	  update_option("wovax_network_sites",$current);

	  for($i=0;$i<sizeof($current);$i++) {
		if (($i % 2) == 1) {
		  $class = "alternate";
		} else {
		  $class = "";
		}
		?>
		<tr class='wx-net-row <?php echo $class; ?>' height="30px">
		  <td class='wx-net-id'><?php echo $current[$i]->id; ?></td>
		  <td class='wx-net-name'><?php echo $current[$i]->name; ?></td>
		  <td class='wx-net-url'><?php echo $current[$i]->url; ?></td>
		  <td class='wx-api-key'><?php echo $current[$i]->key; ?></td>
		  <td class="wx-net-delete" style="text-align:right"><a href="#">Delete</a></td>
		</tr>
		<?php
	  }

	  die();
	}

	//Function for deleting networked sites
	add_action('wp_ajax_wx_delete_networked_site','wx_delete_networked_site');
	function wx_delete_networked_site() {
	  $id = $_POST['data'];
	  $sites = get_option("wovax_network_sites");
	  $new_sites = array();
	  foreach ($sites as $site) {
		if ($site->id != $id) {
		  $new_sites[] = $site;
		}else {
		  $removed = $site->url;
		}
	  }
	  update_option("wovax_network_sites",$new_sites);
	  echo $removed." has been removed.";
	  die();
	}
  }
  
  { //functions for widget-forms
	//function for getting post taxonomies
	add_action('wp_ajax_wx_get_post_taxonomies', 'wx_get_post_taxonomies');
	function wx_get_post_taxonomies() {
	  $post_type = $_POST['post_type'];
	  $taxonomy_objects = get_object_taxonomies($post_type, 'objects');
	  echo '<option value="select_taxonomy">Select Taxonomy</option>';
	  foreach ($taxonomy_objects as $object) {
						echo "<option value='".$object->name."'>".$object->label."</option>";
			}
	  die();
	}
	
	//function for getting taxonomy terms
	add_action('wp_ajax_wx_get_taxonomy_terms', 'wx_get_taxonomy_terms');
	function wx_get_taxonomy_terms() {
	  $taxonomy = $_POST['taxonomy'];
	  $term_objects = get_terms($taxonomy);
	  if(empty($term_objects)) {
		echo '<option value="none" disabled>No Terms Found</option>';
	  } else {
		foreach ($term_objects as $term) {
			  echo "<option value='".$term->slug."'>".$term->name."</option>";
		}
	  }
	  die();
	}
	
  }

  // functions for front end

	//function for saving searches
	add_action('wp_ajax_wx_save_search','wx_save_search');
	function wx_save_search() {
		$data = $_POST['data'];
		$search_name = $data['name'];
		$search_query = $data['query'];
		$user_id = get_current_user_id();
		$site_id = get_current_blog_id();
		$wovax = new wovax_functions();
		$add = $wovax->add_saved_search($search_name,$search_query,$user_id,$site_id);
		echo "Success";
		wp_die();
	}

	// function for removing saved searches
	add_action('wp_ajax_wx_remove_search','wx_remove_search');
	function wx_remove_search() {
		$data = $_POST['data'];
		$search_name = $data['name'];
		$search_query = $data['query'];
		$user_id = get_current_user_id();
		$site_id = get_current_blog_id();
		$wovax = new wovax_functions();
		$remove = $wovax->remove_saved_search($search_name,$search_query,$user_id,$site_id);
		echo "Success";
		wp_die();
	}

?>
