<?php

  global $wpdb;
  global $wp_rewrite;

  { // multisite initialization
    if (is_multisite() && get_current_blog_id() == 1) {
      $status = "master";
    }elseif (is_multisite() && get_current_blog_id() != 1) {
      $status = "slave";
    }else {
      $status = false;
    }
    add_option("wovax_multisite_status", $status);
  }

  { // database option creation
    require WOVAX_APP_DIR."/library/wovaxapp_default_options.php";

    foreach ($options as $option => $value) {
      if ($option == "json_api_base") {
        add_option($option,$value);
      } else if ($value != "") {
        add_option("wovax_".$option,$value);
      }else {
        add_option("wovax_".$option);
      }
    }
  }

    // database table creation
    $wpdb->query(
      'CREATE TABLE IF NOT EXISTS `' . $wpdb->prefix . 'wovaxapp_apns_device` (
        `id` int(20) NOT NULL AUTO_INCREMENT,
        `device_id` varchar(150) NOT NULL,
        `total_sent` int(20) NOT NULL DEFAULT "0",
        `registered_on` date NOT NULL,
        `blacklisted_categories` varchar(150) NOT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=0 ;'
    );

    $wpdb->query(
      'CREATE TABLE IF NOT EXISTS `' . $wpdb->prefix . 'wovaxapp_apns_device_development` (
        `id` int(20) NOT NULL AUTO_INCREMENT,
        `device_id` varchar(150) NOT NULL,
        `total_sent` int(20) NOT NULL DEFAULT "0",
        `registered_on` date NOT NULL,
        `blacklisted_categories` varchar(150) NOT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=0 ;'
    );

    $wpdb->query(
      'CREATE TABLE IF NOT EXISTS `' . $wpdb->prefix . 'wovaxapp_gcm_device` (
        `id` int(20) NOT NULL AUTO_INCREMENT,
        `device_id` VARCHAR(512) CHARACTER SET ASCII COLLATE ascii_bin NOT NULL,
        `total_sent` int(20) NOT NULL DEFAULT "0",
        `registered_on` date NOT NULL,
        `blacklisted_categories` varchar(150) NOT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=0 ;'
    );

    $wpdb->query(
      'CREATE TABLE IF NOT EXISTS `' . $wpdb->prefix . 'wovaxapp_gcm_device_development` (
        `id` int(20) NOT NULL AUTO_INCREMENT,
        `device_id` VARCHAR(512) CHARACTER SET ASCII COLLATE ascii_bin NOT NULL,
        `total_sent` int(20) NOT NULL DEFAULT "0",
        `registered_on` date NOT NULL,
        `blacklisted_categories` varchar(150) NOT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=0 ;'
    );

    $wpdb->query(
      'CREATE TABLE IF NOT EXISTS `' . $wpdb->prefix . 'wovaxapp_downloads` (
        `no` bigint(20) NOT NULL AUTO_INCREMENT,
        `userID` text NOT NULL,
        `unixtime` bigint(20) NOT NULL,
        `device` text NOT NULL,
        `OS` text NOT NULL,
        PRIMARY KEY (`no`)
      ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=0 ;'
    );

    $wpdb->query(
      'CREATE TABLE IF NOT EXISTS `' . $wpdb->prefix . 'wovaxapp_pushnotification` (
        `no` bigint(20) NOT NULL AUTO_INCREMENT,
        `pushmsg` text NOT NULL,
        `unixtime` bigint(20) NOT NULL,
        `type` text NOT NULL,
        `pID` text NOT NULL,
        PRIMARY KEY (`no`)
      ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=0 ;'
    );

    $wpdb->query(
      'CREATE TABLE IF NOT EXISTS `' . $wpdb->prefix . 'wovaxapp_search_filter` (
        `no` bigint(20) NOT NULL AUTO_INCREMENT,
        `in_app` varchar(16) NOT NULL,
        `item_order` bigint(20) NOT NULL,
        `post_type` varchar(64) NOT NULL,
        `custom_field` varchar(64) NOT NULL,
        `display_name` varchar(64) NOT NULL,
        `default_value` varchar(64) NOT NULL,
        `type` varchar(64) NOT NULL,
        PRIMARY KEY (`no`)
      ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=0 ;'
    );
    
    $wpdb->query(
        'CREATE TABLE IF NOT EXISTS `' . $wpdb->prefix . 'wovaxapp_search_filter_dropdowns` (
        `id` int(20) NOT NULL AUTO_INCREMENT UNIQUE,
        `filter_id` bigint(20) NOT NULL,
        `item_order` int(20) NOT NULL,
        `display_name` varchar(128) NOT NULL,
        `value` varchar(128) NOT NULL,
        PRIMARY KEY (`id`),
        UNIQUE KEY `wx_filter_id_display` (`filter_id`,`display_name`),
        FOREIGN KEY (`filter_id`) REFERENCES `'. $wpdb->prefix .'wovaxapp_search_filter` (`no`) ON DELETE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=0;'
    );

  // Add the rewrite rule on activation
  add_filter('rewrite_rules_array', 'json_api_rewrites');
  $wp_rewrite->flush_rules();