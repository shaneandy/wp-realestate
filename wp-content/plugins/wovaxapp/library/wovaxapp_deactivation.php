<?php

  global $wp_rewrite;

  $payload = array();
  $plugin_data = get_plugin_data(__FILE__);

  if ( get_option('wovaxapp_activated') == 'Activated') {
    $payload['product_activated'] = "TRUE";
  }else {
    $payload['product_activated'] = "FALSE";
  }

  $payload['version'] = $plugin_data['Version'];
  $payload['site_id'] = get_option('siteurl');

  $payload = json_encode($payload);
  $url = ('https://wovax.com/wc_order_554a6e6bb7f32_am_eZPbRvFki4n6/master/version_heartbeat/?payload='.$payload."&uninstall=true");
  $response = file_get_contents($url);
  unset($response); //cuz i just dont care

  { // un-cool api stuffs
    $content_dir = scandir(WP_CONTENT_DIR);
    if (in_array("wovaxapi",$content_dir)) {
      $contents_of_dir = scandir(WP_CONTENT_DIR."/wovaxapi/");
      if (sizeof($contents_of_dir) <= 2) {
        rmdir(WP_CONTENT_DIR."/wovaxapi");
      }
    }
  }

  $wp_rewrite->flush_rules();

?>
