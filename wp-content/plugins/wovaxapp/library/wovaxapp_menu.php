<?php
  add_menu_page('Wovax', 'Wovax', 'administrator', 'wovaxDashboard', 'wovaxDashboard',  'dashicons-smartphone', '44' );
  add_submenu_page('wovaxDashboard', __('App Dashboard', 'wovax_translation'), __('Dashboard', 'wovax_translation'), 'administrator', 'wovaxDashboard', 'wovaxDashboard');
  add_submenu_page('wovaxDashboard', __('Send Notification', 'wovax_translation'), __('Send Notification', 'wovax_translation'), 'administrator', 'wovaxSendPush', 'wovaxSendPush');
  add_submenu_page('wovaxDashboard', __('Design', 'wovax_translation'), __('Design', 'wovax_translation'), 'administrator', 'wovaxDesign', 'wovaxDesign');
  add_submenu_page('wovaxDashboard', __('Translations', 'wovax_translation'), __('Translations', 'wovax_translation'), 'administrator', 'wovaxTranslations', 'wovaxTranslations');
  //add_submenu_page('wovaxDashboard', 'Excluded Categories', 'Excluded Categories', 'administrator', 'wovaxExcludedCategories', wovaxExcludedCategories);
  add_submenu_page('wovaxDashboard', __('Push Categories', 'wovax_translation'), __('Push Categories', 'wovax_translation'), 'administrator', 'wovaxPushNotificationCategories', 'wovaxPushNotificationCategories');
  //add_submenu_page('wovaxDashboard', __('Ad Settings', 'wovax_translation'), __('Ad Settings', 'wovax_translation'), 'administrator', 'wovaxAds', wovaxAds);
  add_submenu_page('wovaxDashboard', 'Advanced', 'Advanced', 'administrator', 'wovaxAdvancedMenu', 'wovaxAdvancedMenu');
  add_submenu_page('wovaxDashboard', 'Search Filter', 'Search Filter', 'administrator', 'wovaxSearchFilter', 'wovaxSearchFilter');
  add_submenu_page('wovaxDashboard', 'Network Settings', 'Network Settings', 'administrator', 'wovaxNetworkSettings', 'wovaxNetworkSettings');
  /*if (wovax_is_plugin_active('wp-views/wp-views.php')) {
        add_submenu_page('wovaxDashboard', 'Plugin Settings', 'Plugin Settings', 'administrator', 'wovaxPluginSettings', wovaxPluginSettings);
  } */

  function wovaxDashboard() {
    require WOVAX_APP_DIR."/pages/dashboard_statistics.php";
  }
  function wovaxSendPush() {
    require WOVAX_APP_DIR."/pages/send_notification.php";
  }
  function wovaxDesign() {
    require WOVAX_APP_DIR."/pages/design.php";
  }
  function wovaxTranslations() {
    require WOVAX_APP_DIR."/pages/translations.php";
  }
  function wovaxExcludedCategories() {
    require WOVAX_APP_DIR."/pages/excluded_categories.php";
  }
  function wovaxPushNotificationCategories() {
    require WOVAX_APP_DIR."/pages/pushnotification_categories.php";
  }
  function wovaxAdvancedMenu() {
    require WOVAX_APP_DIR."/pages/advanced_menu.php";
  }
  function wovaxAds() {
    require WOVAX_APP_DIR."/pages/app_ads.php";
  }
  function wovaxSearchFilter() {
    require WOVAX_APP_DIR."/pages/search_filter.php";
  }
  function wovaxPluginSettings() {
    require WOVAX_APP_DIR."/pages/plugin_settings.php";
  }
  function wovaxNetworkSettings() {
    require WOVAX_APP_DIR."/pages/network_settings.php";
  }
?>
