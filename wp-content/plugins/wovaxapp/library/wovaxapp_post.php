<?php

  class wovaxapp_post {

    private function format_excerpt($content){
      $the_excerpt = $content; //Gets post_content to be used as a basis for the excerpt
      $excerpt_length = 35; //Sets excerpt length by word count
      $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
      $words = explode(' ', $the_excerpt, $excerpt_length + 1);
      if(count($words) > $excerpt_length) {
          array_pop($words);
          array_push($words, '…');
          $the_excerpt = implode(' ', $words);
      }
      $the_excerpt = '<p>'.$the_excerpt.'</p>';
      return $the_excerpt;
    }

    public function format_posts($posts,$args = array()) {
      $users = $this->format_users();
      $count_total = $args['count_total'];
      $page_total = $args['page_total'];
      if(isset($args)){
        $page = $args['paged'];
      }
      $post_count = count($posts);
      for ($i=0;$i<$post_count;$i++) {
        unset($post);
        global $post;
        $post = $posts[$i];
        setup_postdata($posts[$i]);
        if (is_array($posts[$i])) {
          $posts[$i] = (object) $posts[$i];
        }
        { // setup post thumbnail
          $thumbnail_id = get_post_thumbnail_id($posts[$i]->ID);
          if ($thumbnail_id != '') {
            $thumbnail_meta = wp_get_attachment_metadata($thumbnail_id,true);
            if(!empty($thumbnail_meta)){
              foreach($thumbnail_meta['sizes'] as $size => $values){
                $meta = wp_get_attachment_image_src($thumbnail_id,$size);
                $thumbnail_meta['sizes'][$size]['url'] = $meta[0];
                unset($thumbnail_meta['sizes'][$size]['mime-type']);
                unset($thumbnail_meta['sizes'][$size]['width']);
              }
              unset($thumbnail_meta['file']);
              unset($thumbnail_meta['image_meta']);
              $thumb = array(
                "meta" => $thumbnail_meta,
                "url" => wp_get_attachment_url($thumbnail_id)
              );
            } else {
              $thumb = null;
            }        
          } else {
            $thumb = null;
          }
          $posts[$i]->post_thumbnail = $thumb;
        }
        if (empty($posts[$i]->post_excerpt)) {
          $posts[$i]->post_excerpt = $this->format_excerpt($posts[$i]->post_content);
        }
        $posts[$i]->postmeta = get_post_meta($posts[$i]->ID);
        $posts[$i]->post_author = $users[$posts[$i]->post_author];
        $posts[$i]->post_content = apply_filters("the_content",$posts[$i]->post_content);
        $posts[$i]->post_date = mysql2date(get_option("date_format"),$posts[$i]->post_date);
        $posts[$i]->guid = html_entity_decode(get_post_permalink($posts[$i]->ID));
        $posts[$i]->permalink = html_entity_decode(get_post_permalink($posts[$i]->ID));
        unset($posts[$i]->post_date_gmt);
        unset($posts[$i]->post_modified_gmt);
        unset($posts[$i]->post_modified);
        unset($posts[$i]->post_mime_type);
        unset($posts[$i]->ping);
        unset($posts[$i]->ping_status);
        unset($posts[$i]->to_ping);
        unset($posts[$i]->pinged);
        unset($posts[$i]->filter);
        unset($posts[$i]->post_password);
        unset($posts[$i]->post_content_filtered);
      }
      return array(
        "count" => count($posts),
        "count_total" => $count_total,
        "page" => $page,
        "pages" => $page_total,//ceil($count_total->publish / (get_option('posts_per_page'))),
        "args" => (sizeof($args) != 0 ? $args : null),
        "posts" => (sizeof($args) != 0 ? $this->parse_shortcodes($posts) : $this->parse_shortcodes($posts,true)),
      );
    }

    public function format_users() {
      $wp_users = get_users();
      $users = array();
      for ($i=0;$i<sizeof($wp_users);$i++) {
        $id = $wp_users[$i]->data->ID;
        $users[$id] = array(
          "user_id" => $id,
          "user_login" => $wp_users[$i]->data->user_login,
          "user_nicename" => $wp_users[$i]->data->user_nicename,
          "user_email" => $wp_users[$i]->data->user_email,
          "display_name" => $wp_users[$i]->data->display_name
        );
      }
      return $users;
    }

    private function parse_shortcodes($posts,$singular = false) {
      if ($singular) {
        if (gettype($posts[0]) == "array") {
          $posts['post_content'] = do_shortcode($posts['post_content']);
        }elseif (gettype($posts[0]) == "object") {
          $posts[0]->post_content = do_shortcode($posts[0]->post_content);
        }
      }else {
        for ($i=0;$i<sizeof($posts);$i++) {
          if (gettype($posts[$i]) == "array") {
            $posts[$i]["post_content"] = do_shortcode($posts[$i]["post_content"]);
          }elseif (gettype($posts[$i]) == "object") {
            $posts[$i]->post_content = do_shortcode($posts[$i]->post_content);
          }
        }
      }
      return $posts;
    }

    public function get_query_data($override=array()) {
      $result = array();
      $elements = array(
        'exclude' => array(),
        'include' => array(),
        'meta_key' => "",
        'meta_value' => "",
        'numberposts' => get_option('posts_per_page'),
        'offset' => 0,
        'order' => "",
        'orderby' => "date",
        'paged' => 1,
        'post_type' => "post",
        'post_status' => "publish",
      );
      foreach ($elements as $element => $default) {
        if (isset($override[$element])) {
          $result[$element] = $override[$element];
        }elseif (isset($_GET[$element])) {
          if ($_GET['post_type'] != 'feedback') {
            $result[$element] = $_GET[$element];
          }
        }else if (isset($_GET['page'])) {
          if ($element == 'paged') {
            $result[$element] = $_GET['page'];
          }
        } else {
          $result[$element] = $default;
        }
      }
      if(isset($_GET['category'])){
        $result['cat'] = $_GET['category']; //expects category ids to be passed in.
      }
      if(isset($_GET['tag'])){
        $result['tag_id'] = $_GET['tag']; // expects tag ids to be passed in.
      }
      if( isset($_GET['taxonomy']) && isset($_GET['term']) ) {
        $result['tax_query'] = array(
          'taxonomy' => $_GET['taxonomy'],
          'field' => 'term_id',
          'term' => $_GET['term'] //expects term ids to be passed in.
        );
      }

      return $result;
    }

  }

?>
