<?php

  { // notices

    { // general
      if (phpversion() < 5.3) {
        add_action('admin_notices', 'wovaxapp_version_warning');
        return;
      }
      function wovaxapp_version_warning() {
        echo "<div id='wovaxapp-php-warning' class='error'><p>Sorry, JSON API requires PHP version 5.3 or greater.</p></div>";
      }
    }

    { // json-api stuff
      if (!class_exists('JSON_API')) {
        add_action('admin_notices', 'json_api_class_warning');
      }
      function json_api_class_warning() {
        echo "<div id='json-api-warning' class='updated fade'><p>Oops, JSON_API class not found. If you've defined a JSON_API_DIR constant, double check that the path is correct.</p></div>";
      }
    }
  }

  { // registers custom image sizes
    //Adds custom image size for search etc
    add_image_size( 'jt_wovaxproperty_thumb', 500, 400, true );
  }

  { // registers shortcodes
    add_shortcode('wovaxsearch', 'wovax_search_form');
    add_shortcode('wovaxsearchresults', 'wovax_search_results');
    add_shortcode('wovaxrecent', 'wovax_recent_shortcode');
    add_shortcode('wovaxsavedsearch','wovax_saved_search');
  }
  
?>
