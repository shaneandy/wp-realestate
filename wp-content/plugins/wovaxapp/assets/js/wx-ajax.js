/* global wp */
/* global data */
/* global count */
jQuery(document).ready(function($) {

  //
  //  MISCELLANEOUS FUNCTIONS
  //
  /////////////////

  //Image upload button for navigation logo
  $(document).on('click', '.wx-btn-upload', function(event) {
        event.preventDefault();
        console.log('hey');
        var correct = $(this).parent().children('.wx-image-url');
        var image = wp.media({
            title: 'Upload PNG Image',
            // mutiple: true if you want to upload multiple files at once
            multiple: false
        }).open()
        .on('select', function(e){
            // This will return the selected image from the Media Uploader, the result is an object
            var uploaded_image = image.state().get('selection').first();
            // Output to the console uploaded_image
            console.log(uploaded_image);
            if (uploaded_image.attributes.subtype === 'png') {
              // We convert uploaded_image to a JSON object to make accessing it easier
              var image_url = uploaded_image.toJSON().url;
              // Let's assign the url value to the input field
              $(correct).val(image_url);
            } else {
              alert('Files must be in .png format.');
            };

        });
    });

  //Show or hide the png upload bar
  $('#wovax_ios_nav_logo').change(function() {
    if ($(this).val() === 'YES') {
      $('#wovax_ios_nav_logo_url').closest('tr').show('slow');
    } else {
      $('#wovax_ios_nav_logo_url').closest('tr').hide('slow');
    }
  });

  //Instantiates the native wordpress colorpicker
  $('.wx-design-color').wpColorPicker();

  // Function to display a textbox if the right option is selected
  $('#wx-home-screen-select').change(function() {
    if ($(this).val() === 'webview') {
      $('#wovax_app_home_post_type').closest('tr').hide('slow');
      $('#wovax_app_home_page_id').closest('tr').hide('slow');
      $('#wovax_app_home_webview_url').closest('tr').show('slow');
    } else if ($(this).val() === 'page') {
      $('#wovax_app_home_post_type').closest('tr').hide('slow');
      $('#wovax_app_home_webview_url').closest('tr').hide('slow');
      $('#wovax_app_home_page_id').closest('tr').show('slow');
    } else if ($(this).val() === 'blog') {
      $('#wovax_app_home_webview_url').closest('tr').hide('slow');
      $('#wovax_app_home_page_id').closest('tr').hide('slow');
      $('#wovax_app_home_post_type').closest('tr').show('slow');
    } else {
      $('#wovax_app_home_post_type').closest('tr').hide('slow');
      $('#wovax_app_home_webview_url').closest('tr').hide('slow');
      $('#wovax_app_home_page_id').closest('tr').hide('slow');
    }
  });

  //
  //  FUNCTIONS FOR PUSH NOTIFICATIONS PAGES
  //
  ///////////////

  //Helper function to stop table cells from collapsing when moved.
  var wovaxHelperMod = function(e, tr) {
    var $originals = tr.children();
    var $helper = tr.clone();
    $helper.children().each(function(index) {
      $(this).width($originals.eq(index).width());
    });
    return $helper;
  };

  //Function to reorder push category table rows.
  $('#wx-cat-table tbody').sortable({
    helper: wovaxHelperMod,
    placeholder: "wx-placeholder",
    stop: function(event, ui) {
      renumber_table('#wx-cat-table');
    }
  });
  $('#wx-cat-table tbody').disableSelection();

  //Renumber Table Rows
  function renumber_table(tableID) {
    var slugs = "";
    var slug = "";
    $(tableID + " tr").each(function() {
      count = $(this).parent().children().index($(this)) + 1;
      $(this).find('.wx-cat-order').html(count);
      slug = $(this).find('.wx-cat-name').html();
      slugs += slug + ",";
    });
    var table = $(tableID + " tr");
    table.filter(":odd").removeClass("alternate");
    table.filter(":even").addClass("alternate");
    data = {
      action: 'wx_rearrange_rows',
      data: slugs
    };

    $.post(ajaxurl, data, function(response) {
      console.log(response);
    });
  }

  // Function to delete fields and rows on the push notification categories page and AJAX call to delete them in the database.
  $('.wx-cat-delete').click(function(event){
    event.preventDefault();
    var row = $(this).closest('tr');
    row.hide('slow');
    row.remove();
    var slug = row.find('.wx-cat-name').html();
    data = {
      action: 'wx_delete_categories',
      data: slug
    };

    $.post(ajaxurl, data, function(response) {
      console.log(response);
      renumber_table('#wx-cat-table');
    });
  });

// Function to delete fields and rows on the send push notification page and AJAX call to delete them in the database.
  $(document).on('click', '.wx-notification-delete', function(event){
    event.preventDefault();
    var row = $(this).closest('tr');
    row.hide('slow');
    row.remove();
    var slug = row.find('.wx-unixtime').html();
    data = {
      action: 'wx_delete_notification',
      data: slug
    };

    $.post(ajaxurl, data, function(response) {
      console.log(response);
      var shown = $('.wx-shown-number').html();
      var total = $('.wx-total-number').html();
      var newShown = shown - 1;
      var newTotal = total - 1;
      $('.wx-shown-number').html(newShown);
      $('.wx-total-number').html(newTotal);
    });
  });

  //Function to clear message box on click - no longer needed with placeholder attribute
  /*$('#wx-message-box').focusin(function() {
    var message = $('#wx-message-box').val();
    if (message === 'Enter message here') {
      var empty = "";
      $('#wx-message-box').val(empty);
      $('#wx-message-box').removeClass('rssSummary');
    };
  });

  $('#wx-message-box').focusout(function() {
    var message = $('#wx-message-box').val();
    if (message === '') {
      var empty = 'Enter message here';
      $('#wx-message-box').val(empty);
      $('#wx-message-box').addClass('rssSummary');
    };
  }); */

  //Function to dynamically tell you how many characters left in message box.
  function renumber_box() {
    var message = $('#wx-message-box').val();
    var length = message.length;
    var total = (110 - length);
    $('.wx-message-number').html(total);
  };

  $('#wx-message-box').keypress(function() {
    renumber_box();
  });
  $('#wx-message-box').keydown(function() {
    renumber_box();
  });
  $('#wx-message-box').click(function() {
    renumber_box();
  });

  //Show or hide the push notification history
  $('.wx-show-history').click(function(event) {
    event.preventDefault();
    if ($('#previouspost').is(':hidden')) {
      $('#previouspost').show('slow');
      $('.wx-show-history').val('Hide History');
    } else {
      $('#previouspost').hide('slow');
      $('.wx-show-history').val('Show History');
    }
  });

  //Show or hide the Redirection value
  $('select[name|="redirectType"]').change(function() {
    if ($(this).val() === 'none') {
      $('.wovax-redirection-value').hide('slow');
      $('.wovax-post-type-selector').hide('slow');
      $('.wovax-user-selector').hide('slow');
      $('.wovax-push-category-selector').show('slow');
    } else if (($(this).val() === 'link')) {
      $('.wovax-post-type-selector').hide('slow');
      $('.wovax-redirection-value').show('slow');
      $('.wovax-push-category-selector').show('slow');
      $('.wovax-user-selector').hide('slow');
    } else if (($(this).val() === 'content')) {
      $('.wovax-redirection-value').hide('slow');
      $('.wovax-post-type-selector').show('slow');
      $('.wovax-push-category-selector').show('slow');
      $('.wovax-user-selector').hide('slow');
    } else if (($(this).val() === 'user')) {
      $('.wovax-user-selector').show('slow');
      $('.wovax-redirection-value').hide('slow');
      $('.wovax-post-type-selector').hide('slow');
      $('.wovax-push-category-selector').hide('slow');
    }
  });

  //AJAX call to update the number of push notifications shown
  $('.wx-notification-number').click(function(event) {
    event.preventDefault();
    var notification = $('select[name|="recallNumber"]').val();
    $('.wx-notification-table').hide('slow');
    data = {
      action: 'wovax_notification_number',
      data: notification
    };

    $.post(ajaxurl, data, function(response) {
      console.log(response);
      $('.wx-notification-table').html(response);
      $('.wx-notification-table').show('slow');
    });
  });

  //Autocomplete for page and post selection on the send pushnotifications page
  $('#postid').autocomplete({
    source: function( request, response) {
      data = {
        action: 'wx_page_post_autocomplete',
        data: request
      }
      $.ajax({
        type: "POST",
        url: ajaxurl,
        data: data,
        success: response,
        dataType: 'json',
        minLength: 2,
        delay: 100
            });
    },
    minLength: 2,
  });

  //Validation function for sending push messages.
  $('input[name|="push"]').click(function(event) {
    var category = $('select[name|="categories"]').val();
    var redirect = $('select[name|="redirectType"]').val();
    var message = $('#wx-message-box').val();
    var link = $('#wx-redirect-value').val();
    var post = $('#postid').val();
    var user = $('#userid').val();
    if(redirect === 'link') {
      redirectValue = link;
    } else if (redirect === 'content') {
      redirectValue = post;
    } else if(redirect === 'user') {
      redirectValue = user;
    } else {
      redirectValue = '';
    }
    console.log(redirectValue);
    if (message === 'Enter message here' || message === '') {
      event.preventDefault();
      alert('You must enter a message to push');
    } else if ( redirect === null ) {
      event.preventDefault();
      alert('You must select a redirect type');
    } else if (category === null) {
      event.preventDefault();
      alert('You must select a category to push to');
    } else if (redirect === "link" && typeof redirectValue === "undefined" ) {
      event.preventDefault();
      alert('You must enter a link to redirect to.');
    } else if (redirect === "content" && typeof redirectValue === "undefined") {
      event.preventDefault();
      alert('You must enter a post ID number to redirect to.');
    } else {
       event.preventDefault();
       data = {
         action: 'wx_send_push_notification',
         message: message,
         categories: category,
         redirectType: redirect,
         redirectValue: redirectValue
       };
       
       $('.wx-push-spinner.spinner').addClass('is-active');
       
       $.post(ajaxurl, data, function(response) {
         console.log(response);
         $('.wx-push-spinner.spinner').removeClass('is-active');
         $('#wx-message-box').val('');
         renumber_box();
         alert('Message Sent');
         var notification = $('select[name|="recallNumber"]').val();
         $('.wx-notification-table').hide('slow');
         data = {
            action: 'wovax_notification_number',
            data: notification
          };

          $.post(ajaxurl, data, function(response) {
            console.log(response);
            $('.wx-notification-table').html(response);
            $('.wx-notification-table').show('slow');
          });
       });
    }
  });

  //
  //  FUNCTIONS FOR SEARCH FILTER PAGE
  //
  ////////////////////

  //AJAX function for setup of filter page
  $('.wovax-filter-setup').click(function(event) {
    event.preventDefault();
    var view;
    view = $('.wx-filter-checkbox:checked').map(function() {
      return this.value;
    }).get();
    data = {
      action: 'wx_filter_firstrun',
      data: view
    };
    if (view != '') {
      $('#wovax-filter-content').hide('slow');
      $.post(ajaxurl, data, function(response) {
      	console.log(response);
      	$('#wovax-filter-content').html(response);
        $('#wovax-filter-content').show('slow');
      });
    } else {
      alert ('You must select at least one post type to filter.');
    };


  });

  //Function to send the Post type to the server for on-the-fly calculation of custom fields on the Search Filter page.
  $('select[name|="wx_post_type_select"]').change(function() {
    var wx_post_type_select = $('select[name|="wx_post_type_select"]').val();

    data = {
      action: 'wx_set_post_types',
      data: wx_post_type_select
    };

    $.post(ajaxurl, data, function(response) {
      var select = $('select[name|="wx_custom_fields"]');
      select.html(response);
    });
  });

   //Renumber Search Filter table Rows
  function renumber_search_filter_table(tableID) {
    var slugs = "";
    var slug = "";
    $(tableID + " tr").each(function() {
      slug = $(this).find('.wx-search-filter-name').html();
      slugs += slug + ",";
    });
    var table = $(tableID + " tr");
    table.filter(":odd").removeClass("alternate");
    table.filter(":even").addClass("alternate");
    data = {
      action: 'wx_rearrange_search_filter_rows',
      data: slugs
    };

    $.post(ajaxurl, data, function(response) {
      console.log(response);
    });
  }

  // Function to delete fields and rows on the Search Filter page and AJAX call to delete them in the database.
  $(document).on('click', '.wx-search-filter-delete', function(event){
    event.preventDefault();
    var row = $(this).closest('tr');
    row.hide('slow');
    row.remove();
    var slug = row.find('.wx-search-filter-name').html();
    data = {
      action: 'wx_delete_search_filter_rows',
      data: slug
    };

    $.post(ajaxurl, data, function(response) {
      console.log(response);
      renumber_search_filter_table('#wx-search-filter-table');
      var rows = $('.wx-search-filter-row').length;
      if (rows < 1) {
        $('#wx-search-filter-table').hide('slow');
        $('.wovax-search-filter-empty').show('slow');
      }
    });
  });

  //Function to reorder Search Filter table rows.
  $('#wx-search-filter-table tbody').sortable({
    helper: wovaxHelperMod,
    placeholder: "wx-placeholder",
    stop: function(event, ui) {
      renumber_search_filter_table('#wx-search-filter-table');
    }
  });
  //$('#wx-search-filter-table tbody').disableSelection();

  //Function to disable the Custom field row when the checkbox is checked on the Search Filter page.
  $("#wx-search-filter-content").change(function() {
    var select = $('select[name|="wx_custom_fields"]');
    var select_type = $('select[name|="wx_type"]');
    var exist = select_type.find('option[value="existing"]');
    var binary = select_type.find('option[value="binary"]');
    var numeric = select_type.find('option[value="numeric"]');
    var min = select_type.find('option[value="numeric_min"]');
    var max = select_type.find('option[value="numeric_max"]');
    var text = select_type.find('option[value="text"]');
    var checkbox = $('#wx-search-filter-content');
    if (checkbox.is(":checked")) {
      select.prop('disabled', true);
      exist.prop('disabled', true);
      binary.prop('disabled', true);
      numeric.prop('disabled', true);
      min.prop('disabled', true);
      max.prop('disabled', true);
      text.prop('selected', true);
    }else {
      select.prop('disabled', false);
      exist.prop('disabled', false);
      binary.prop('disabled', false);
      numeric.prop('disabled', false);
      min.prop('disabled', false);
      max.prop('disabled', false);
      text.prop('selected', false);
    }
  });

  //Function to disable the Post Type selector when the Save button is pushed on the Search Filter page.
  $('.wx-search-filter-save').click(function(event) {
    event.preventDefault();
    var select = $('.wx-post-type-select');
    var name = $('.wx-search-name');
	var posts = $('.wx-search-results-per-page').val();
    if (!name.val()) {
      alert('You must enter a Search Name')
    } else if (select.val() === 'select') {
      alert('You must select a Post Type');
    } else {
      select.prop('disabled', true);
      var name_val = name.val();
      var select_val = select.val();

      data = {
        action: 'wx_save_search_filter_name',
        data: name_val,
        more_data: select_val,
		posts_per_page: posts,
      };

      $.post(ajaxurl, data, function(response) {
        console.log(response);
        select.prop('disabled', true);
        $('#wx-hidden-search-filter').show();
      });
    }
  });

  //Function to save the checked/unchecked state of checkboxes on the search filter page.
  $('.wx-search-filter-display-checkbox').change(function() {

    var checkbox = $('.wx-search-filter-display-checkbox');
    var row = $(this).closest('tr');
    var id = $(row).find('.wx-search-filter-id').html();
    console.log(id);
    if ($(this).prop("checked") === true) {
      checked = true;
    }else {
      checked = false;
    }

    data = {
        action: 'wx_save_search_filter_checkbox',
        data: checked,
        id: id,
      };

      $.post(ajaxurl, data, function(response) {
        console.log(response);
      });
  });
  
  //Function to add search filter rows.
  $('.wx-add-search-filter').click(function(event) {
    event.preventDefault();
    var field = $('select[name|="wx_custom_fields"]').val();
    var content = $('#wx-search-filter-content').prop( "checked" );
    var search_type = $('select[name|="wx_type"]').val();
    var label = $('input[name|="wx_display_text"]').val();
    var placeholder = $('input[name|="wx_default_value"]').val();
    
    data = {
        action: 'wx_add_search_filter',
        data: {
          field: field,
          content: content,
          search_type: search_type,
          label: label,
          placeholder: placeholder,
        }
      };

   $.post(ajaxurl, data, function(response) {
        console.log(response);
        $('.wovax-search-filter-empty').hide('slow');
        $('.wx-search-filter-body').html(response);
        $('#wx-search-filter-table').show('slow');
        
        $('input[name|="wx_display_text"]').val([]);
        $('input[name|="wx_default_value"]').val([]);
      });
  });
  
  //Function for making Label and Placeholder editable.
  $(document).on('click', '.wx-search-filter-edit', function(event){
    event.preventDefault;
    $(this).html("Save");
    $(this).addClass("wx-search-filter-save");
    $(this).removeClass("wx-search-filter-edit");
    var row = $(this).closest('tr');
    var editableName = row.find('.wx-search-filter-name');
    var editablePlaceholder = row.find('.wx-search-filter-placeholder');
    editableName.addClass("wx-search-editing");
    editablePlaceholder.addClass("wx-search-editing");
    editableName.attr("contentEditable","true");
    editablePlaceholder.attr("contentEditable","true");
    $( "#wx-search-filter-table tbody" ).sortable( "option", "disabled", true );
  })
  
  //Function for saving Label and Placeholder after edit.
  $(document).on('click', '.wx-search-filter-save', function(event){
    event.preventDefault;
    var row = $(this).closest('tr');
    var id = row.find('.wx-search-filter-id').html();
    console.log(id);
    var new_display_value = row.find('.wx-search-filter-name').html();
    var new_placeholder = row.find('.wx-search-filter-placeholder').html();
    console.log(new_display_value);
    console.log(new_placeholder);
    var editableName = row.find('.wx-search-filter-name');
    var editablePlaceholder = row.find('.wx-search-filter-placeholder');
    editableName.removeClass("wx-search-editing");
    editablePlaceholder.removeClass("wx-search-editing");
    $(this).html('Edit');
    $(this).addClass("wx-search-range-edit");
    $(this).removeClass("wx-search-range-save");
    editableName.attr("contentEditable","false");
    editablePlaceholder.attr("contentEditable","false");
    $( "#wx-search-filter-table tbody" ).sortable( "option", "disabled", false );
    data = {
        action: 'wx_save_edited_filter_value',
        data: {
            id: id,
            new_display_value: new_display_value,
            new_placeholder: new_placeholder
            }
    }
    
    $.post(ajaxurl, data, function(response){
        console.log(response);
    });
  })
  
    //
    // FUNCTIONS FOR SORT TAB OF SEARCH FILTER PAGE
    //
    
  //Function for adding search sorts
  $('.wx-add-search-sort').click(function(event) {
    event.preventDefault();
    var field = $('select[name|="wx_custom_field_sort"]').val();
    var sort_type = $('select[name|="wx-sort-type-selector"]').val();
    
    data = {
        action: 'wx_add_sort_type',
        data: {
          field: field,
          sort_type: sort_type,
        }
      };
     
    $.post(ajaxurl, data, function(response) {
      console.log('Input Saved');
      console.log(response);
      $('.wx-search-sort-table-body').append(response);
      
      //Reset form to default.
      $('select[name|="wx_custom_field_sort"]').val('select');
      $('select[name|="wx-sort-type-selector"]').val('alphabetical');
    });
  });
  
  //Function for deleting search sorts
  $(document).on('click', '.wx-search-sort-delete', function(event){
    event.preventDefault();
    var row = $(this).closest('tr');
    var sort_value = row.find('.wx-search-sort-value').html();
    if(sort_value === 'date-desc' || sort_value === 'date-asc'){
      console.log(sort_value);
    } else {
      row.hide('slow');
      row.remove();
      console.log(sort_value);
      data = {
        action: 'wx_delete_search_sort_rows',
        data: sort_value
      };
  
      $.post(ajaxurl, data, function(response) {
        console.log(response);
      });
    } 
  });
  //Function for allowing some cells to be editable.
  $(document).on('click', '.wx-search-sort-edit', function(event){
    event.preventDefault();
    $(this).html("Save");
    $(this).addClass("wx-search-sort-save");
    $(this).removeClass("wx-search-sort-edit");
    var row = $(this).closest('tr');
    var editable = row.find('.wx-search-sort-name');
    editable.addClass("wx-search-editing");
    editable.attr("contentEditable","true");
  });
  
  //Function for saving sort cells after editing.
  $(document).on('click', '.wx-search-sort-save', function(event){
    event.preventDefault;
    var row = $(this).closest('tr');
    var sort_value = row.find('.wx-search-sort-value').html();
    console.log(sort_value);
    var new_display_value = row.find('.wx-search-sort-name').html();
    console.log(new_display_value);
    var editable = row.find('.wx-search-sort-name');
    editable.removeClass("wx-search-editing");
    $(this).html('Edit');
    $(this).addClass("wx-search-sort-edit");
    $(this).removeClass("wx-search-sort-save");
    editable.attr("contentEditable","false");
    data = {
      action: 'wx_save_edited_sort_value',
      data: {
          sort_value: sort_value,
          new_display_value: new_display_value
        }
    }
    
    $.post(ajaxurl, data, function(response){
      console.log(response);
    });
  });
  
  //Function for setting the default search sort
  $('#wovax_default_sort_select').change(function(){
	var sort_default = $(this).val();
	  
	data = {
		action: 'wx_save_default_sort_value',
		data: sort_default
	}
    
    $.post(ajaxurl, data, function(response){
      console.log(response);
    });
  });
  
    //
    // FUNCTIONS FOR RANGE BUILDER TAB OF SEARCH FILTER PAGE
    //
    
  //Function to send range info to server, and return the range builder/current info.
  $('.wx-range-selector').change(function(){
     var range = $(this).val();
     var creation = $('.wx-range-creation-div');
     creation.show('slow');
     $('.wx-range-settings-div').show('slow');
     console.log(range);
     
     var data = {
         action: 'wx_range_getter',
         data: range
     }
     
     $.post(ajaxurl, data, function(response){
        console.log('Got Range Data');
        $('#wovax-range-table-body').html(response); 
     }); 
  });
  
  //Function to add a new range to the group.
  $('#wovax-add-new-range-row').click(function(){
      var filter = $('.wx-range-selector').val();
      var low = $('.wx-range-low-input').val();
      var high = $('.wx-range-high-input').val();
      var currency = $('.wx-range-currency-checkbox').prop( "checked" );
      low = parseInt(low);
      high = parseInt(high);
      
      
      if(low < high) {
          data = {
              action: 'wx_add_new_range',
              data: {
                  filter: filter,
                  low: low,
                  high: high,
                  currency: currency
              }
          }
          console.log(data);
          $.post(ajaxurl, data, function(response){
             console.log(response);
             var error_check = response.substring(0, 5);
             console.log('Var Error Check:',error_check);
             if(error_check === "ERROR") {
                 alert(response.substring(6));
             } else {
                 $('#wovax-range-table-body').append(response);
             } 
          });
      } else {
          alert('Your Range is Invalid');
      }
  });
  
  //Function for deleteing search filter rows.
  $(document).on('click', '.wx-search-range-delete', function(event){
    event.preventDefault;
    var row = $(this).closest('tr');
    var range_id = row.find('.wx-search-range-id').html();
    row.hide('slow');
    row.remove();
    console.log(range_id);
    data = {
        action: 'wx_delete_search_range_row',
        data: range_id
    };

    $.post(ajaxurl, data, function(response) {
        console.log(response);
    });
  });
  
  //Function for making display name editable.
  $(document).on('click', '.wx-search-range-edit', function(event){
    event.preventDefault();
    $(this).html("Save");
    $(this).addClass("wx-search-range-save");
    $(this).removeClass("wx-search-range-edit");
    var row = $(this).closest('tr');
    var editable = row.find('.wx-search-range-name');
    editable.addClass("wx-search-editing");
    editable.attr("contentEditable","true");
    $( "#wovax-range-table tbody" ).sortable( "option", "disabled", true );
  });
  
  //Function for saving range display name after editing.
  $(document).on('click', '.wx-search-range-save', function(event){
    event.preventDefault;
    var row = $(this).closest('tr');
    var range_id = row.find('.wx-search-range-id').html();
    console.log(range_id);
    var new_display_value = row.find('.wx-search-range-name').html();
    console.log(new_display_value);
    var editable = row.find('.wx-search-range-name');
    editable.removeClass("wx-search-editing");
    $(this).html('Edit');
    $(this).addClass("wx-search-range-edit");
    $(this).removeClass("wx-search-range-save");
    editable.attr("contentEditable","false");
    $( "#wovax-range-table tbody" ).sortable( "option", "disabled", false );
    data = {
        action: 'wx_save_edited_range_value',
        data: {
            range_id: range_id,
            new_display_value: new_display_value
            }
    }
    
    $.post(ajaxurl, data, function(response){
        console.log(response);
    });
  });
  
  //Function for reordering the range table
  function reorder_search_filter_range_table(tableID) {
    var slugs = "";
    var slug = "";
    $(tableID + " tr").each(function() {
        slug = $(this).find('.wx-search-range-id').html();
        slugs += slug + ",";
    });
    var table = $(tableID + " tr");
    table.filter(":odd").removeClass("alternate");
    table.filter(":even").addClass("alternate");
    console.log(slugs);
    data = {
        action: 'wx_rearrange_search_range_rows',
        data: slugs
    };

    $.post(ajaxurl, data, function(response) {
        console.log(response);
    });
  }
  
  //Function for making the range rows sortable
  $('#wovax-range-table tbody').sortable({
    helper: wovaxHelperMod,
    placeholder: "wx-placeholder",
    stop: function(event, ui) {
      reorder_search_filter_range_table('#wovax-range-table');
    }
  });
  //$('#wovax-range-table tbody').disableSelection();
  
  //
  //    FUNCTIONS FOR NETWORK PAGE
  //
  /////////////////////

  //Function to send input from Probe to server, and display the results.
  $('#wx-probe-button').click(function(event) {
    event.preventDefault();
    var url = $('#wx-network-url').val();
    var key = $('#wx-network-api-key').val();

    data = {
        action: 'wx_network_probe',
        data: {
          url: url,
          key: key,
        }
      };

   $.post(ajaxurl, data, function(response) {
        $('.wovax-network-results').html(response);
        $('.wovax-add-network').show('slow');
      });
  });

  //Function to add site to network settings table
  $('#wx-add-site-button').click(function(event) {
    event.preventDefault();
    var name = $('#wx-network-nicename').val();
    var url = $('#wx-network-url').val();
    var key = $('#wx-network-api-key').val();

    data = {
        action: 'wx_network_add_site',
        data: {
          name: name,
          url: url,
          key: key,
        }
      };

   $.post(ajaxurl, data, function(response) {
        console.log(response)
        $('.wovax-add-network').hide('slow');
        $('.wovax-network-empty-sites').hide('slow');
        $('.wovax-net-body').html(response);
        $('#wx-net-table').show('slow');
      });
  });

  //Function to delete linked sites on the Network Settings page
  $(document).on('click', '.wx-net-delete', function(event){
    event.preventDefault();
    var row = $(this).closest('tr');
    var id = row.find('.wx-net-id').html();
    row.hide('slow');
    row.remove();
    data = {
      action: 'wx_delete_networked_site',
      data: id
    };

    $.post(ajaxurl, data, function(response) {
      console.log(response);
      var rows = $('.wx-net-row').length;
      if (rows < 1) {
        $('#wx-net-table').hide('slow');
        $('.wovax-network-empty-sites').show('slow');
      }
    });
  });
  
  //
  //  FUNCTIONS FOR WIDGET FORM SETTINGS
  //
  ////////////////////
  
  //Function to show/hide taxonomy and term divs based on user input
  $(document).on('change', '.wx-recent-form-display', function() {
    var display = $(this).val();
    var tax = $(this).parent().parent().find('.wx-recent-form-taxonomy').val();
    if (display === 'recent') {
      $('.wx-taxonomy-select').hide('slow');
      $('.wx-term-select').hide('slow');
    } else if (display === 'taxonomy_display') {
      $('.wx-taxonomy-select').show('slow');
      if(tax !== 'select_taxonomy') {
        $('.wx-term-select').show('slow');
      }
    }
  });
  
  //Function to dynamically generate the list of taxonomies based on post type.
  $(document).on('change', '.wx-recent-post-type-select', function() {
    var post_type = $(this).val();
    
    data = {
      action: 'wx_get_post_taxonomies',
      post_type: post_type
    };
    
    $.post(ajaxurl, data, function(response) {
      $('.wx-recent-form-taxonomy').html(response);
      $('.wx-term-select').hide('slow');
      console.log(response);
    });
  });
  
  //Function to dynamically generate the list of associated terms based on selected taxonomy
  $(document).on('change', '.wx-recent-form-taxonomy', function() {
    var taxonomy = $(this).val();
    
    data = {
      action: 'wx_get_taxonomy_terms',
      taxonomy: taxonomy
    };
    
    if(taxonomy !== 'select_taxonomy') {
      $.post(ajaxurl, data, function(response) {
        $('.wx-recent-form-term').html(response);
        $('.wx-term-select').show('slow');
        console.log(response);
      });
    } else if (taxonomy === 'select_taxonomy') {
      $('.wx-term-select').hide('slow');
    }
  });
  
  //Function to hide/show filter checkboxes based on input.
  $(document).on('change', '.wx-search-form-all-filters', function() {
    var all_filters = $(this).val();
    var checkboxes = $('.wx-search-sections-div');
    if (all_filters === 'yes') {
      checkboxes.hide('slow');
    } else if ( all_filters === 'no') {
      checkboxes.show('slow');
    }
  });
  
  //
  //  FUNCTIONS FOR ADVANCED SETTINGS PAGE
  //
  ////////////////////
  
  //Reveal correct info on tab click
  $('.wovax-nav-wrapper.nav-tab-wrapper a').click(function() {
    $('section').hide();
		$('section').eq($(this).index()).show();
    $('.nav-tab-wrapper a').removeClass("nav-tab-active");
    $(this).addClass("nav-tab-active");
		return false;
  });
  
  //Show/hide correct info based on user settings
  $('#wovax_user_accounts').change(function() {
    var user = $('#wovax_user_accounts').val();
    var registration = $('#wovax_allow_registration');
    var guest = $('#wovax_allow_guests');
    var register = $('#wovax_register_launch');
    var favorites = $('#wovax_enable_favorites');
    var favorites_val = $('#wovax_favorites').val();
    if (user === 'YES') {
      registration.show('slow');
      guest.show('slow');
      register.show('slow');
      favorites.show('slow');
    } else if (user === 'NO') {
      registration.hide('slow');
      guest.hide('slow');
      register.hide('slow');
      favorites.hide('slow');
    }
  });
  
  //Show correct ad info based on setting.
  $('#wovax_ad_option').change(function() {
    var ad_option = $('#wovax_ad_option').val();
    var no_ad = $('#wovax_noad_div');
    var banner = $('#wovax_banner_div');
    var html_up = $('#wovax_htmlup_div');
    if ( ad_option === 'no_ad' ) {
      no_ad.show('slow');
      banner.hide('slow');
      html_up.hide('slow');
    } else if ( ad_option === 'banner_image' ) {
      no_ad.hide('slow');
      banner.show('slow');
      html_up.hide('slow');
    } else if ( ad_option === 'html_upload' ) {
      no_ad.hide('slow');
      banner.hide('slow');
      html_up.show('slow');
    } else {
      no_ad.hide('slow');
      banner.hide('slow');
      html_up.hide('slow');
    }
  });
  
  //Reveal an option based on another options setting.
  $('#wovax_user_accounts_guests').change(function() {
    var guests = $('#wovax_user_accounts_guests').val();
    var launch = $('#wovax_register_launch');
    var launch_negative = launch.find('option[value="NO"]');
    if (guests === 'NO') {
      launch_negative.prop('disabled', true);
      launch.find('option[value="YES"]').prop('selected', true);
    } else {
      launch_negative.prop('disabled', false);
    }
  });
  
  //
  //  FUNCTIONS FOR DESIGN SETTINGS PAGE
  //
  ////////////////////
  $('select[name|="wovax_ios_nav_logo"]').change(function() {
    var nav = $('select[name|="wovax_ios_nav_logo"]').val();
    var nav_url = $('#wovax_nav_url_row');
    if(nav === "YES") {
      nav_url.show('slow');
    } else {
      nav_url.hide('slow');
    }
  });

});
