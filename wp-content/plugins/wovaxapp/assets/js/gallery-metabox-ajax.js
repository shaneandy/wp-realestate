/* global ajaxurl */
/* global jQuery */
// **************************************************************
// image removal function - fired at load and after AJAX
// **************************************************************

	function imageRemoval() {

		jQuery('span.be-image-remove').on('click', function(event) {
			
			var image	= jQuery(this).attr('rel');
			var parent	= jQuery('form#post input#post_ID').prop('value');
			
			var data = {
				action: 'gallery_remove',
				image: image,
				parent: parent
			};

			jQuery.post(ajaxurl, data, function(response) {
				var obj;
				try {
					obj = jQuery.parseJSON(response);
				}
				catch(e) { // bad JSON catch
					// add some error messaging ?
					}

				if(obj.success === true) { // it worked. AS IT SHOULD.
					jQuery('div#be_gallery_metabox').find('div.be-image-wrapper').replaceWith(obj.gallery);
					imageRemoval();
					// add some success messaging ?
				}
				else { // something else went wrong
					// add some error messaging?
				}
			});

		});

	}

// **************************************************************
// now start the engine
// **************************************************************

jQuery(document).ready( function($) {


// **************************************************************
//  call the image removal function at load
// **************************************************************

	imageRemoval();

// **************************************************************
// AJAX function resetting the gallery with changes
// **************************************************************

	function gallery_refresh() {
		var parent	= $('form#post input#post_ID').prop('value');

		var data = {
			action: 'refresh_metabox',
			parent: parent
		};

		jQuery.post(ajaxurl, data, function(response) {
			var obj;
			try {
				obj = jQuery.parseJSON(response);
			}
			catch(e) {  // bad JSON catch
				// add some error messaging ?
				}

			if(obj.success === true) { // it worked. AS IT SHOULD.
				$('div#be_gallery_metabox').find('div.be-image-wrapper').replaceWith(obj.gallery);
				imageRemoval();
				// add some success messaging ?
			}
			else {  // something else went wrong
				// add some error messaging ?
			}
		});
	};
	
// **************************************************************
// AJAX function resetting the gallery with changes pt 2
// **************************************************************

	//Gallery Refresh Button
	$('div#be_gallery_metabox input#update-gallery').on('click', gallery_refresh());

	//Refresh on media modal close.
	wp.media.view.Modal.prototype.on('close', function() {
		var delay = 500;
		setTimeout(gallery_refresh, delay);
	});
// **************************************************************
//  what, you're still here? it's over. go home.
// **************************************************************

});