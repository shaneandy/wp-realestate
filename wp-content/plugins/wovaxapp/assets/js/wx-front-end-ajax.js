/* global wp */
/* global data */
/* global count */
jQuery(document).ready(function($) { 
	
    //Fix for safari number validation bug, where invalid characters would cause the value to be blank.
    // @since 2.3.6
    $('.wovax-search-section input[type|="number"]').keyup(function(){
        var obj = $(this);
        if (obj[0].checkValidity() === false) {
            obj[0].value = this[obj[0].getAttribute("name")];
        } else {
            this[obj[0].getAttribute("name")] = obj[0].value;
        }
    });

    //Reveals save search text box
    // @since 2.5
    $('.wx-save-search-start').click(function(event) {
        event.preventDefault();
        $(this).hide('fast');
        $('.wx-save-search-name').show('fast');
        $('.wx-save-search-submit').show('fast');
    });

    //AJAX call for saving searches.
    // @since 2.5
    $(document).on('click','.wx-save-search-submit',function(event) {
        event.preventDefault();
        var name = $('.wx-save-search-name').val();
        var query = window.location.search;

        var data = {
            action: 'wx_save_search',
            data: {
                name: name,
                query: query
            }
        }

         $.post(ajaxurl, data, function(response){
            console.log(response);
            $('.wx-save-search-name').hide('fast');
            $('.wx-save-search-submit').hide('fast');
            $('.wx-save-search-start').show('fast');
        });
    });

    // Saved search list edit
    $('.wx-saved-search-edit').click(function(event) {
        event.preventDefault();
        $('.wx-saved-search-list').toggleClass('wx-saved-search');
        $('.wx-saved-search-list li').toggleClass('wx-saved-search-remove');
        var value = $(this).text();
        if(value === 'Edit') {
            $(this).text('Done');
        } else if (value === 'Done') {
            $(this).text('Edit');
        }
    });

    // Saved search remove
    $(document).on('click','.wx-saved-search-remove',function(event) {
        event.preventDefault();
        var name = $(this).data('name');
        var query = $(this).data('query');

        var data = {
            action: 'wx_remove_search',
            data: {
                name: name,
                query: query
            }
        };
        $(this).hide('fast');
        console.log(data);
        $.post(ajaxurl, data, function(response){
            console.log(response);
        });

    })

});