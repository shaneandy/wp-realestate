function WoxaxImageColumnPopup(){
	var cur_post_id = 0;
	var wp_media =  window.wp.media({
		title: 'Select Featured Image',
		multiple: false,
		button: {
			text: 'Set Featured Image'
		}
	});
	this.showModal = function(post_id){
		cur_post_id = post_id;
		wp_media.open();
	};
	this.onSelect = function(){
		var chosen_img = wp_media.state().get('selection').first().toJSON();
		var chosen_img_id = chosen_img.id;
		var thumb_url = chosen_img.sizes.thumbnail.url;
		//prepare ajax call
		var xhttp = new XMLHttpRequest();
		//call this function when http request state changes.
		xhttp.onreadystatechange = function() {
			//only do somthing when the request is done.
			if(xhttp.readyState === XMLHttpRequest.DONE){
				if(xhttp.status !== 200) {
					console.log('Wovax Image Column: Ajax Conection Error!');
					return false;
				}
				if(xhttp.responseText !== 'success'){
					console.log(xhttp.responseText);
					return false;
				}
				//success now set the thumbnail
				wx_set_col_thumb_nail(cur_post_id, thumb_url);
			}
		};
		xhttp.open('GET', 'admin-ajax.php/?action=wx_img_col_action&post_id='+cur_post_id+'&img_id='+chosen_img_id, true);
		xhttp.send();
		return true;
	};
	wp_media.on('select', this.onSelect);
}

function wx_set_col_thumb_nail(post_id, url){
	var col_node = document.getElementById('wx-img-col-'+post_id);
	var img = document.createElement('img');
	img.style.width = '55p';
	img.style.height = '55px';
	img.style.border = '1px solid rgba(0,0,0, 0.4)';
	img.src = url;
	col_node.innerHTML = '';
	col_node.appendChild(img);
}

//create a new instance when the all the content as loaded.
document.addEventListener("DOMContentLoaded", function(event_stuff) {
	window.wx_img_col = new WoxaxImageColumnPopup();
});