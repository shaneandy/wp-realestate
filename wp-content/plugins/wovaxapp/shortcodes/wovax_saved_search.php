<?php
// Saved Search display shortcode
function wovax_saved_search($atts) {
	$atts = shortcode_atts(
    	array(
			'user_id' => '',
			'site_id' => ''
    	), $atts, 'wovaxsavedsearch');
	// set up user id
	if(empty($atts['user_id'])) {
		$user_id = get_current_user_id();
	} else {
		$user_id = $atts['user_id'];
	}
	// set up site id
	if(is_multisite() && empty($atts['site_id'])) {
		$site_id = get_current_blog_id();
	} else if(!is_multisite()) {
		$site_id = 1;
	} else {
		$site_id = $atts['site_id'];
	}
	$wovax = new wovax_functions();
	$saved_searches = $wovax->get_saved_searches($user_id,$site_id);
	ob_start();
	?>
	<div class='wx-saved-search-container'>
		<ul class='wx-saved-search-list'>
		<?php
		foreach($saved_searches as $search) {
			?>
			<li data-name="<?php echo $search['name']; ?>" data-query="<?php echo $search['search']; ?>">
				<a href="<?php echo site_url('search-results/'.$search['search']); ?>"><?php echo $search['name']; ?></a>
			</li>
			<?php
		}
		?>
		</ul>
		<?php
		if(!empty($saved_searches)) {
			?>
				<a href='#' class='wx-saved-search-edit button'>Edit</a>
			<?php
		}
		?>
	</div>
	<?php
	$output = ob_get_clean();
	return $output;
}