<?php
//Shortcode for pulling the most recent properties or other post types.
function wovax_recent_shortcode($atts) {
	$atts = shortcode_atts(
    	array(
			'posts' 	=> 3,
			'type'  	=> 'wovaxproperty',
			'excerpt' 	=> '',
			'taxonomy'	=> '',
			'term' 		=> '',
			'field' 	=> '',
			'meta_sort' => 'date',
			'direction' => 'DESC',
			'key' 		=> '',
			'value' 	=> '',
			'compare'	=> '',
			'days_ago'	=> '',
			'paginate'	=> 'no'
    	), $atts, 'wovaxrecent');
	$post_type = $atts['type'];
	$number_of_posts = $atts['posts'];
	$excerpt = $atts['excerpt'];
	$taxonomy = $atts['taxonomy'];
	$term = $atts['term'];
	$meta_key = $atts['field'];
	$orderby = $atts['meta_sort'];
	$order = $atts['direction'];
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$paginate = $atts['paginate'];
	$args = array(
		'posts_per_page'    => $number_of_posts,
		'meta_key'         	=> $meta_key,
		'orderby'          	=> $orderby,
		'order'            	=> $order,
		'post_type'        	=> $post_type,
		'paged'				=> $paged,
		'post_status'      	=> 'publish'
	);
	if( !empty($atts['key']) && !empty($atts['value']) && !empty($atts['compare'])) {
		if($atts['compare'] === 'MAX') {
			$compare = '<=';
			$type = 'NUMERIC';
		} else if ($atts['compare'] === 'MIN') {
			$compare = '>=';
			$type = 'NUMERIC';
		} else {
			$compare = $atts['compare'];
		}
		$meta_query = array(
			array(
				'key' => $atts['key'],
				'value' => $atts['value'],
				'compare' => $compare
			)	
		);
		if(isset($type)) {
			$meta_query[0]['type'] = $type;
		};
		$args['meta_query'] = $meta_query;
	}
	if (!empty($taxonomy) && !empty($term)) {
		if($taxonomy == 'category'){
			$taxonomy = 'category_name'; //special handling due to 'category' requiring an ID instead of a slug.
		}
		$args[$taxonomy] = $term;
	}
	if(!empty($atts['days_ago'])){
		$date_query = array(
			array(
				'after' => $atts['days_ago']." days ago"
			)
		);
		$args['date_query'] = $date_query;
	}
	$counter = 0;
	$wovax_posts = new WP_Query($args);
	//Puts the output in a buffer and then outputs the buffer when done.
	ob_start(); ?>
	<div class='clearfix'>
	<?php
	foreach ($wovax_posts->posts as $post) : setup_postdata( $post );
		$city = get_post_meta( $post->ID, 'City', true );
		$state = get_post_meta( $post->ID, 'State', true );
		$zip = get_post_meta( $post->ID, 'Zip_Code', true );
		$price = get_post_meta( $post->ID, 'Price', true );
		$bedrooms = get_post_meta( $post->ID, 'Bedrooms', true );
		$bathrooms = get_post_meta( $post->ID, 'Bathrooms', true );
		$sq_ft = get_post_meta( $post->ID, 'Square_Footage', true );
		$title = get_the_title( $post->ID);
		$post_excerpt = $post->post_excerpt;
		if (is_numeric($sq_ft)) {
			$sq_ft = number_format( $sq_ft );
		}
		if (is_numeric($price)) {
			$price = number_format( $price );
		}
		if ($counter % 3 == 0) {
			$first = ' first';
		} else {
			$first = '';
		}
		$counter++;
		if(file_exists(WX_CUSTOM_TEMPLATE_PATH.'/wx_search_result_template.php')) {
			include(WX_CUSTOM_TEMPLATE_PATH.'/wx_search_result_template.php');
		} else {
			include(WOVAX_APP_DIR.'/templates/wx_search_result_template.php');
		}
	endforeach;	
	?>
	</div>
	<?php
	if( $paginate == 'yes'){
			$nav_links = "<div class='wx-nav-links'>".get_previous_posts_link( '< Previous |').get_next_posts_link( '| Next >'."</div>",$wovax_posts->max_num_pages );
			echo $nav_links;
	}
	$output = ob_get_clean();
	wp_reset_postdata(); //necessary due to setting up the postdata earlier.
	return $output;  
}