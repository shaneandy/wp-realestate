<?php
// Search results display shortcode
function wovax_search_results($atts) {
	global $wp_query;
	global $wpdb;
	extract(
		shortcode_atts(
			array(
				"search_filters" => false,
				"col" => ''
			),
			$atts, 'wovaxsearchresults'
		)
	);
	ob_start(); //Puts all following output into a buffer
	if($search_filters) {
		$wovax_search_filters = "search_filters='".$search_filters."'";
	} else {
		$wovax_search_filters = "";
	}
	// The column override pass-in never really produces good results.
	/*
	if(isset($_REQUEST['cols'])){
		$cols = $_REQUEST['cols'];
	} else {
		$cols = $col;
	}*/
	// So only respect the shortcode value
	echo do_shortcode("[wovaxsearch ".$wovax_search_filters." col='".$col."']"); //Puts the search form at the top of the search results page
	$meta = $filters = $meta_bnry = array();
	$request = $_GET;
	unset($request['cols']);
	if(isset($request['orderby'])){
		$order = $request['orderby'];
		unset($request['orderby']);
	} else {
		unset($request['orderby']);
	}
	$filters = wovax_get_search_filter_info();
	$search = array();
	$size = count($filters['filter']);
	//This loop takes the filters array and the array sent by the search form and merges the necessary information into a third array.
	//To search properly, we need the custom field being searched in, the type of search, and the value being searched.
	foreach($request as $name => $value) {
		for ($j=0;$j<$size;$j++) {
			if ($filters['filter'][$j]['no'] == $name && strtolower($value) !== 'all' && strlen($value) !== 0) {
				$search[$name]['custom_field'] = $filters['filter'][$j]['custom_field'];
				$search[$name]['type'] = $filters['filter'][$j]['type'];
				$search[$name]['value'] = $value;
			}
		}
	}
	//This section prepares the save search section
	if(is_user_logged_in()) {
	?>
	<form class="wx-save-search-form">
		<input type="text" placeholder="Enter Search name" class="wx-save-search-name" hidden>
		<input type="submit" class="wx-save-search-submit" value="submit" hidden>
		<input type="submit" class="wx-save-search-start" value="Save Search">
	</form>
	<?php
	}
	//This section prepares the orderby form for use based on the original query that was passed in.
	?>
	<form class="wovax-orderby-form">
	<?php
    foreach($request as $name => $value) :
    	?>
		<input name="<?php echo $name; ?>" value="<?php echo $value; ?>" hidden>
		<?php  
	endforeach;
	?>
		<div class="clearfix">
			<div class="wx-sort-container">
			<div class="wx-orderby-select-div">
				<select class="wx-orderby-select wovax-search-filter" name="orderby">
				<?php
					$sort_options = get_option('wovax_sort_fields');
					foreach($sort_options as $key => $values) : ?>
					<option value="<?php echo $values['value'];?>"><?php echo $values['display_name'];?></option>
					<?php  
					endforeach;
				?>
				</select>
			</div>
			<div class="wx-orderby-submit">
				<input type='submit' class='bbutton-primary' value='Sort'>
			</div>
			</div>
		</div>
	</form>
    <?php
	$keyword = '';
	//This loop prepares the meta query based on the type of search being performed.
	foreach($search as $info) {
		if($info["type"] == "text" ) {
			if ($info["custom_field"] != "search__content"){
				$meta[] = array("key" => $info["custom_field"], "value" => stripslashes($info["value"]), "compare" => "LIKE");
			} else {
				$keyword = $info["value"]; //Special handling for searches of the content, since the content isn't postmeta, and content searches can only be text.
			}
		} else if ( $info["type"] == "numeric" || $info["type"] == "existing" ) {
			$meta[] = array("key" => $info["custom_field"], "value" => stripslashes($info["value"]), "compare" => "=");
		} else if ($info["type"] == "binary") {
			if( trim($info["value"]) == "yes" ) {
				$meta[] = array("key" => $info["custom_field"], "value" => 1, "compare" => "=");
			} else if( trim($info["value"]) == "no" ) {
				$meta[] = array("key" => $info["custom_field"], "value" => 0, "compare" => "=");
			};
		} else if ($info["type"] == "numeric_min") {
			$meta[] = array("key" => $info["custom_field"], "value" => $info["value"], "compare" => ">=", "type" => "NUMERIC");
		} else if ($info["type"] == "numeric_max") {
			$meta[] = array("key" => $info["custom_field"], "value" => $info["value"], "compare" => "<=", "type" => "NUMERIC");
		} else if ($info["type"] == "range") {
			$range = explode("-",$info["value"]);
			$meta[] = array("key" => $info["custom_field"], "value" => $range[0], "compare" => ">=", "type" => "NUMERIC");
			$meta[] = array("key" => $info["custom_field"], "value" => $range[1], "compare" => "<=", "type" => "NUMERIC");
		}
	}
	//This section prepares the orderby values if any are passed in.
	if(!isset($order)){
		$order = get_option('wovax_default_search_sort');
		if($order === FALSE){
			$order = 'date-desc';
		}
	}
	$order = explode("-",$order);
	if($order[0] == 'date'){
		$direction = strtoupper($order[1]);
		$orderby = 'date';
		$meta_key = '';
	} else if ($order[1] == 'numeric'){
		$direction = strtoupper($order[2]);
		$orderby = 'meta_value_num';
		$meta_key = $order[0];
	} else if ($order[1] == 'alphabetical'){
		$direction = strtoupper($order[2]);
		$orderby = 'meta_value';
		$meta_key = $order[0];
	} else {
		$direction = 'DESC';
		$orderby = 'date';
		$meta_key = '';
	}
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$posts_per_page = get_option('wovax_search_posts_per_page',9);
	$post_type = json_decode(get_option('wovax_filter_names'), true); //This reference may need to change if we move towards multiple searches per site.
	$post_type = $post_type[0]['type']; //Mostly because it does in two lines what could totally be done in one.
	$args = array(
		'post_type' => $post_type,
		'post_status' => 'publish', 
		's' => $keyword, 
		'posts_per_page' => $posts_per_page, 
		'paged' => $paged, 
		'meta_query' => $meta,
		'meta_key' => $meta_key,
		'orderby' => $orderby, 
		'order' => $direction, 
    );
	$wovax_posts = new WP_Query($args);
	$count = 0;
	$pcount = 1;
	if($wovax_posts){
		$pages_count = array();
		?>
		<div class='clearfix'>
		<?php
		foreach($wovax_posts->posts as $post){
			$count = 1;
			$pages_count[] = $pcount++;
			if(!isset($counter)){
				$counter = 0;
			}
			//puts search results into three columns.
			if($counter % 3 == 0) {
				$first = ' first';
			} else {
				$first = '';
			}
			$counter++;
			$city = get_post_meta( $post->ID, 'City', true );
			$state = get_post_meta( $post->ID, 'State', true );
			$zip = get_post_meta( $post->ID, 'Zip_Code', true );
			$price = get_post_meta( $post->ID, 'Price', true );
			$bedrooms = get_post_meta( $post->ID, 'Bedrooms', true );
			$bathrooms = get_post_meta( $post->ID, 'Bathrooms', true );
			$sq_ft = get_post_meta( $post->ID, 'Square_Footage', true );
			$title = get_the_title( $post->ID);
			if (is_numeric($sq_ft)) {
				$sq_ft = number_format( $sq_ft );
			} else {
				$sq_ft = "";
			}
			if (is_numeric($price)) {
				$price = number_format( $price );
			} else {
				$price = "";
			}
			if(file_exists(WX_CUSTOM_TEMPLATE_PATH.'/wx_search_result_template.php')){
				include(WX_CUSTOM_TEMPLATE_PATH.'/wx_search_result_template.php');
			} else {
				include(WOVAX_APP_DIR.'/templates/wx_search_result_template.php');
			}
		} ?>
    	</div>
		<?php
		$cpage = count($pages_count);
		if( $cpage > 8){
			$nav_links = "<div class='wx-nav-links'>".get_previous_posts_link( '< Previous |').get_next_posts_link( '| Next >'."</div>",$wovax_posts->max_num_pages );
			echo $nav_links;
		}
	}
	if( !$count ){
		echo "<div class='result_error'>No results found!</div>";
	}
	$results = ob_get_clean(); //empties buffer into variable and cleans buffer.
	return $results;
}