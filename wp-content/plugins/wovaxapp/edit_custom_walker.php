<?php
/**
 *  /!\ This is a copy of Walker_Nav_Menu_Edit class in core
 *
 * Create HTML list of nav menu input items.
 *
 * @package WordPress
 * @since 3.0.0
 * @uses Walker_Nav_Menu
 */
class Wovax_Walker_Nav_Menu_Edit_Custom extends Walker_Nav_Menu  {
/**
 * @see Walker_Nav_Menu::start_lvl()
 * @since 3.0.0
 *
 * @param string $output Passed by reference.
 */
function start_lvl(&$output, $depth = 0, $args = Array()) {
// x
}

/**
 * @see Walker_Nav_Menu::end_lvl()
 * @since 3.0.0
 *
 * @param string $output Passed by reference.
 */
function end_lvl(&$output, $depth = 0, $args = Array()) {
// x
}

/**
 * @see Walker::start_el()
 * @since 3.0.0
 *
 * @param string $output Passed by reference. Used to append additional content.
 * @param object $item Menu item data object.
 * @param int $depth Depth of menu item. Used for padding.
 * @param object $args
 */
function start_el(&$output, $item, $depth = 0, $args = Array(), $id = 0) {
global $_wp_nav_menu_max_depth;

$_wp_nav_menu_max_depth = $depth > $_wp_nav_menu_max_depth ? $depth : $_wp_nav_menu_max_depth;

$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

ob_start();
$item_id = esc_attr( $item->ID );
$removed_args = array(
	        'action',
	        'customlink-tab',
	        'edit-menu-item',
	        'menu-item',
	        'page-tab',
	        '_wpnonce',
);

$original_title = '';
if ( 'taxonomy' == $item->type ) {
$original_title = get_term_field( 'name', $item->object_id, $item->object, 'raw' );
if ( is_wp_error( $original_title ) )
$original_title = false;
}elseif ( 'post_type' == $item->type ) {
$original_object = get_post( $item->object_id );
$original_title = $original_object->post_title;
}

$classes = array(
	        'menu-item menu-item-depth-' . $depth,
	        'menu-item-' . esc_attr( $item->object ),
	        'menu-item-edit-' . ( ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? 'active' : 'inactive'),
);

$title = $item->title;

if ( ! empty( $item->_invalid ) ) {
$classes[] = 'menu-item-invalid';
/* translators: %s: title of menu item which is invalid */
$title = sprintf( __( '%s (Invalid)' ), $item->title );
}elseif ( isset( $item->post_status ) && 'draft' == $item->post_status ) {
$classes[] = 'pending';
/* translators: %s: title of menu item in draft status */
$title = sprintf( __('%s (Pending)'), $item->title );
}

$title = empty( $item->label ) ? $title : $item->label;
?>
<li id="menu-item-<?php echo $item_id; ?>"
	class="<?php echo implode(' ', $classes ); ?>">
<dl class="menu-item-bar">
	<dt class="menu-item-handle"><span class="item-title"><?php echo esc_html( $title ); ?></span>
	<span class="item-controls"> <span class="item-type"><?php echo esc_html( $item->type_label ); ?></span>
	<span class="item-order hide-if-js"> <a
		href="<?php
							echo wp_nonce_url(
							add_query_arg(
							array(
							'action' => 'move-up-menu-item',
							'menu-item' => $item_id,
							),
							remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
							),
							'move-menu_item'
							);
							?>"
		class="item-move-up"><abbr title="<?php esc_attr_e('Move up'); ?>">&#8593;</abbr></a>
	| <a
		href="<?php
							echo wp_nonce_url(
							add_query_arg(
							array(
							'action' => 'move-down-menu-item',
							'menu-item' => $item_id,
							),
							remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
							),
							'move-menu_item'
							);
							?>"
		class="item-move-down"><abbr title="<?php esc_attr_e('Move down'); ?>">&#8595;</abbr></a>
	</span> <a class="item-edit" id="edit-<?php echo $item_id; ?>"
		title="<?php esc_attr_e('Edit Menu Item'); ?>"
		href="<?php
					echo ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? admin_url( 'nav-menus.php' ) : add_query_arg( 'edit-menu-item', $item_id, remove_query_arg( $removed_args, admin_url( 'nav-menus.php#menu-item-settings-' . $item_id ) ) );
					?>"><?php _e( 'Edit Menu Item' ); ?></a> </span></dt>
</dl>

<div class="menu-item-settings wp-clearfix"
	id="menu-item-settings-<?php echo $item_id; ?>"><?php
	if (esc_attr( $item->ptype) == 0) {
	$urlDisplay = "";
	$twitterPage = "style='display:none;'";
	$oneClickCall = "style='display:none;'";
	$oneclickmail = "style='display:none;'";
	$contactUS = "style='display:none;'";
	}elseif (esc_attr( $item->ptype) == 1) {
	$urlDisplay = "";
	$twitterPage = "style='display:none;'";
	$oneClickCall = "style='display:none;'";
	$oneclickmail = "style='display:none;'";
	$contactUS = "style='display:none;'";
	}elseif (esc_attr( $item->ptype) == 2){
	$urlDisplay = "style='display:none;'";
	$twitterPage = "";
	$oneClickCall = "style='display:none;'";
	$oneclickmail = "style='display:none;'";
	$contactUS = "style='display:none;'";
	}elseif (esc_attr( $item->ptype) == 3){
	$urlDisplay = "style='display:none;'";
	$twitterPage = "style='display:none;'";
	$oneClickCall = "style='display:none;'";
	$oneclickmail = "style='display:none;'";
	$contactUS = "style='display:none;'";
	}elseif (esc_attr( $item->ptype) == 4){
	$urlDisplay = "style='display:none;'";
	$twitterPage = "style='display:none;'";
	$oneClickCall = "style='display:none;'";
	$oneclickmail = "style='display:none;'";
	$contactUS = "";
	}elseif (esc_attr( $item->ptype) == 5){
	$urlDisplay = "style='display:none;'";
	$twitterPage = "style='display:none;'";
	$oneClickCall = "";
	$oneclickmail = "style='display:none;'";
	$contactUS = "style='display:none;'";
	}elseif (esc_attr( $item->ptype) == 6){
	$urlDisplay = "style='display:none;'";
	$twitterPage = "style='display:none;'";
	$oneClickCall = "style='display:none;'";
	$oneclickmail = "";
	$contactUS = "style='display:none;'";
	}
	?> <?php if( 'custom' == $item->type ) : ?>
<p class="field-url description description-wide"
	id="edit-menu-item-url-<?php echo $item_id; ?>"
	<?php echo $urlDisplay; ?>><label
	for="edit-menu-item-url-<?php echo $item_id; ?>"> <?php _e( 'URL' ); ?><br />
<input type="text" id="edit-menu-item-url-<?php echo $item_id; ?>"
	class="widefat code edit-menu-item-url"
	name="menu-item-url[<?php echo $item_id; ?>]"
	value="<?php echo esc_attr( $item->url ); ?>" /> </label></p>
	<?php endif;
	if( 'taxonomy' == $item->type ) : ?>
<p class="field-url description description-wide"
	id="edit-menu-item-catType-<?php echo $item_id; ?>"
	<?php echo $urlDisplay; ?>><label
	for="edit-menu-item-catType-<?php echo $item_id; ?>"> <?php _e( 'Type' ); ?><br />
<select id="edit-menu-item-catType-<?php echo $item_id; ?>"
	name="menu-item-catType[<?php echo $item_id; ?>]">
	<?php if(esc_attr( $item->catType ) == "category"){ ?>
	<option value="category" selected="selected">Category Post List</option>
	<?php }else{ ?>
	<option value="category">Category Post List</option>
	<?php } ?>

	<?php if(esc_attr( $item->catType ) == "categoryPostMap"){ ?>
	<option value="categoryPostMap" selected="selected">Category Post Map</option>
	<?php }else{ ?>
	<option value="categoryPostMap">Category Post Map</option>
	<?php } ?>
</select> </label></p>
	<?php endif; ?>

<p class="description description-thin"><label
	for="edit-menu-item-title-<?php echo $item_id; ?>"> <?php _e( 'Navigation Label' ); ?><br />
<input type="text" id="edit-menu-item-title-<?php echo $item_id; ?>"
	class="widefat edit-menu-item-title"
	name="menu-item-title[<?php echo $item_id; ?>]"
	value="<?php echo esc_attr( $item->title ); ?>" /> </label></p>
<p class="field-title-attribute field-attr-title description description-thin"><label
	for="edit-menu-item-attr-title-<?php echo $item_id; ?>"> <?php _e( 'Title Attribute' ); ?><br />
<input type="text"
	id="edit-menu-item-attr-title-<?php echo $item_id; ?>"
	class="widefat edit-menu-item-attr-title"
	name="menu-item-attr-title[<?php echo $item_id; ?>]"
	value="<?php echo esc_attr( $item->post_excerpt ); ?>" /> </label></p>
<p class="field-link-target description"><label
	for="edit-menu-item-target-<?php echo $item_id; ?>"> <input
	type="checkbox" id="edit-menu-item-target-<?php echo $item_id; ?>"
	value="_blank" name="menu-item-target[<?php echo $item_id; ?>]"
	<?php checked( $item->target, '_blank' ); ?> /> <?php _e( 'Open link in a new window/tab' ); ?>
</label></p>
<p class="field-css-classes description description-thin"><label
	for="edit-menu-item-classes-<?php echo $item_id; ?>"> <?php _e( 'CSS Classes (optional)' ); ?><br />
<input type="text" id="edit-menu-item-classes-<?php echo $item_id; ?>"
	class="widefat code edit-menu-item-classes"
	name="menu-item-classes[<?php echo $item_id; ?>]"
	value="<?php echo esc_attr( implode(' ', $item->classes ) ); ?>" /> </label>
</p>
<p class="field-xfn description description-thin"><label
	for="edit-menu-item-xfn-<?php echo $item_id; ?>"> <?php _e( 'Link Relationship (XFN)' ); ?><br />
<input type="text" id="edit-menu-item-xfn-<?php echo $item_id; ?>"
	class="widefat code edit-menu-item-xfn"
	name="menu-item-xfn[<?php echo $item_id; ?>]"
	value="<?php echo esc_attr( $item->xfn ); ?>" /> </label></p>
<p class="field-description description description-wide"><label
	for="edit-menu-item-description-<?php echo $item_id; ?>"> <?php _e( 'Description' ); ?><br />
<textarea id="edit-menu-item-description-<?php echo $item_id; ?>"
	class="widefat edit-menu-item-description" rows="3" cols="20"
	name="menu-item-description[<?php echo $item_id; ?>]"><?php echo esc_html( $item->description ); // textarea_escaped ?></textarea>
<span class="description"><?php _e('The description will be displayed in the menu if the current theme supports it.'); ?></span>
</label></p>
	<?php
	/* New fields insertion starts here */
	?>
<p class="field-custom description description-wide"><label
	for="edit-menu-item-thumbnail-<?php echo $item_id; ?>"> <?php _e( 'Thumbnail ID' ); ?><br />
<input type="text" id="edit-menu-item-thumbnail-<?php echo $item_id; ?>"
	class="widefat code edit-menu-item-custom"
	name="menu-item-thumbnail[<?php echo $item_id; ?>]"
	value="<?php echo esc_attr( $item->thumbnail ); ?>" /><small>Thumbnail IDs can be found <a href='http://wovax.com'>here</a></small> </label></p>
<p class="field-custom description description-wide"><label
	for="edit-menu-item-thumbnailbgcolor-<?php echo $item_id; ?>"> <?php _e( 'Thumbnail Background Color' ); ?><br />
<input type="text" id="edit-menu-item-thumbnailbgcolor-<?php echo $item_id; ?>"
	class="widefat code edit-menu-item-custom"
	name="menu-item-thumbnailbgcolor[<?php echo $item_id; ?>]"
	value="<?php echo esc_attr( $item->thumbnailbgcolor ); ?>" /></label></p>
	<?php if( 'custom' == $item->type ) : ?>
<p class="field-custom description description-wide"><label
	for="edit-menu-item-ptype-<?php echo $item_id; ?>"> <?php _e( 'Page Type' ); ?><br />
<select id="edit-menu-item-ptype-<?php echo $item_id; ?>"
	name="menu-item-ptype[<?php echo $item_id; ?>]"
	onchange="hideUrl_<?php echo $item_id; ?>()">
	<?php if(esc_attr( $item->ptype) == 0){ ?>
	<option value="0" selected="selected">Link</option>
	<?php }else{ ?>
	<option value="0">Link</option>
	<?php } ?>

	<?php if(esc_attr( $item->ptype) == 1){ ?>
	<option value="1" selected="selected">Custom Field Filter</option>
	<?php }else{ ?>
	<option value="1">Custom Field Filter</option>
	<?php } ?>

	<?php if(esc_attr( $item->ptype) == 2){ ?>
	<option value="2" selected="selected">Twitter - Page</option>
	<?php }else{ ?>
	<option value="2">Twitter - @screen_name</option>
	<?php } ?>

	<?php if(esc_attr( $item->ptype) == 3){ ?>
	<option value="3" selected="selected">Post Map Page Template</option>
	<?php }else{ ?>
	<option value="3">Post Map Page Template</option>
	<?php } ?>

	<?php if(esc_attr( $item->ptype) == 4){ ?>
	<option value="4" selected="selected">Contact Us Page Template</option>
	<?php }else{ ?>
	<option value="4">Contact Us Page Template</option>
	<?php } ?>

	<?php if(esc_attr( $item->ptype) == 5){ ?>
	<option value="5" selected="selected">One Click Call</option>
	<?php }else{ ?>
	<option value="5">One Click Call</option>
	<?php } ?>

	<?php if(esc_attr( $item->ptype) == 6){ ?>
	<option value="6" selected="selected">One Click Mail</option>
	<?php }else{ ?>
	<option value="6">One Click Mail</option>
	<?php } ?>
</select> </label> <script>
					function hideUrl_<?php echo $item_id; ?>()
					{
						var menu = document.getElementById('edit-menu-item-ptype-<?php echo $item_id; ?>').value;
						if(menu == 0) {
							document.getElementById('edit-menu-item-url-<?php echo $item_id; ?>').style.display = 'block';
							document.getElementById('edit-menu-item-twitterpage-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-oneclickcall-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-oneclickmail-<?php echo $item_id; ?>').style.display = 'none';

							document.getElementById('edit-menu-item-companyname-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companyaddress-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companylatlong-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companyphonenumber-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companyemailaddress-<?php echo $item_id; ?>').style.display = 'none';
						}
						else if(menu == 1)
						{
							document.getElementById('edit-menu-item-url-<?php echo $item_id; ?>').style.display = 'block';
							document.getElementById('edit-menu-item-twitterpage-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-oneclickcall-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-oneclickmail-<?php echo $item_id; ?>').style.display = 'none';

							document.getElementById('edit-menu-item-companyname-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companyaddress-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companylatlong-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companyphonenumber-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companyemailaddress-<?php echo $item_id; ?>').style.display = 'none';
						}
						else if(menu == 2)
						{
							document.getElementById('edit-menu-item-url-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-twitterpage-<?php echo $item_id; ?>').style.display = 'block';
							document.getElementById('edit-menu-item-oneclickcall-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-oneclickmail-<?php echo $item_id; ?>').style.display = 'none';

							document.getElementById('edit-menu-item-companyname-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companyaddress-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companylatlong-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companyphonenumber-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companyemailaddress-<?php echo $item_id; ?>').style.display = 'none';
						}
						else if(menu == 3)
						{
							document.getElementById('edit-menu-item-url-<?php echo $item_id; ?>').style.display = 'none';;
							document.getElementById('edit-menu-item-twitterpage-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-oneclickcall-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-oneclickmail-<?php echo $item_id; ?>').style.display = 'none';

							document.getElementById('edit-menu-item-companyname-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companyaddress-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companylatlong-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companyphonenumber-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companyemailaddress-<?php echo $item_id; ?>').style.display = 'none';
						}
						else if(menu == 4)
						{
							document.getElementById('edit-menu-item-url-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-twitterpage-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-oneclickcall-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-oneclickmail-<?php echo $item_id; ?>').style.display = 'none';

							document.getElementById('edit-menu-item-companyname-<?php echo $item_id; ?>').style.display = 'block';
							document.getElementById('edit-menu-item-companyaddress-<?php echo $item_id; ?>').style.display = 'block';
							document.getElementById('edit-menu-item-companylatlong-<?php echo $item_id; ?>').style.display = 'block';
							document.getElementById('edit-menu-item-companyphonenumber-<?php echo $item_id; ?>').style.display = 'block';
							document.getElementById('edit-menu-item-companyemailaddress-<?php echo $item_id; ?>').style.display = 'block';
						}
						else if(menu == 5)
						{
							document.getElementById('edit-menu-item-url-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-twitterpage-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-oneclickcall-<?php echo $item_id; ?>').style.display = 'block';
							document.getElementById('edit-menu-item-oneclickmail-<?php echo $item_id; ?>').style.display = 'none';

							document.getElementById('edit-menu-item-companyname-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companyaddress-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companylatlong-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companyphonenumber-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companyemailaddress-<?php echo $item_id; ?>').style.display = 'none';
						}
						else if(menu == 6)
						{
							document.getElementById('edit-menu-item-url-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-twitterpage-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-oneclickcall-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-oneclickmail-<?php echo $item_id; ?>').style.display = 'block';

							document.getElementById('edit-menu-item-companyname-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companyaddress-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companylatlong-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companyphonenumber-<?php echo $item_id; ?>').style.display = 'none';
							document.getElementById('edit-menu-item-companyemailaddress-<?php echo $item_id; ?>').style.display = 'none';
						}
					}
				</script></p>

<p class="field-custom description description-wide"
	id="edit-menu-item-twitterpage-<?php echo $item_id; ?>"
	<?php echo $twitterPage; ?>><label
	for="edit-menu-item-twitterpage-<?php echo $item_id; ?>"> <?php _e( 'Twitter Account e.g. @wordpresstoios' ); ?><br />
<input type="text"
	id="edit-menu-item-twitterpage-<?php echo $item_id; ?>"
	class="widefat code edit-menu-item-custom"
	name="menu-item-twitterpage[<?php echo $item_id; ?>]"
	value="<?php echo esc_attr( $item->twitterpage ); ?>" /> </label></p>

<p class="field-custom description description-wide"
	id="edit-menu-item-oneclickcall-<?php echo $item_id; ?>"
	<?php echo $oneClickCall; ?>><label
	for="edit-menu-item-oneclickcall-<?php echo $item_id; ?>"> <?php _e( 'Please add phone number' ); ?><br />
<input type="text"
	id="edit-menu-item-oneclickcall-<?php echo $item_id; ?>"
	class="widefat code edit-menu-item-custom"
	name="menu-item-oneclickcall[<?php echo $item_id; ?>]"
	value="<?php echo esc_attr( $item->oneclickcall ); ?>" /> </label></p>

<p class="field-custom description description-wide"
	id="edit-menu-item-oneclickmail-<?php echo $item_id; ?>"
	<?php echo $oneclickmail; ?>><label
	for="edit-menu-item-oneclickmail-<?php echo $item_id; ?>"> <?php _e( 'Please add email address' ); ?><br />
<input type="text"
	id="edit-menu-item-oneclickmail-<?php echo $item_id; ?>"
	class="widefat code edit-menu-item-custom"
	name="menu-item-oneclickmail[<?php echo $item_id; ?>]"
	value="<?php echo esc_attr( $item->oneclickmail ); ?>" /> </label></p>

<p class="field-custom description description-wide"
	id="edit-menu-item-companyname-<?php echo $item_id; ?>"
	<?php echo $contactUS; ?>><label
	for="edit-menu-item-companyname-<?php echo $item_id; ?>"> <?php _e( 'Please write your contact name / company name' ); ?><br />
<input type="text"
	id="edit-menu-item-companyname-<?php echo $item_id; ?>"
	class="widefat code edit-menu-item-custom"
	name="menu-item-companyname[<?php echo $item_id; ?>]"
	value="<?php echo esc_attr( $item->companyname ); ?>" /> </label></p>

<p class="field-custom description description-wide"
	id="edit-menu-item-companyaddress-<?php echo $item_id; ?>"
	<?php echo $contactUS; ?>><label
	for="edit-menu-item-companyaddress-<?php echo $item_id; ?>"> <?php _e( 'Please write your address' ); ?><br />
<input type="text"
	id="edit-menu-item-companyaddress-<?php echo $item_id; ?>"
	class="widefat code edit-menu-item-custom"
	name="menu-item-companyaddress[<?php echo $item_id; ?>]"
	value="<?php echo esc_attr( $item->companyaddress ); ?>" /> </label></p>

<p class="field-custom description description-wide"
	id="edit-menu-item-companylatlong-<?php echo $item_id; ?>"
	<?php echo $contactUS; ?>><label
	for="edit-menu-item-companylatlong-<?php echo $item_id; ?>"> <?php _e( 'Please write your address lat and long. e.g 1.238484,23.45585' ); ?><br />
<input type="text"
	id="edit-menu-item-companyaddress-<?php echo $item_id; ?>"
	class="widefat code edit-menu-item-custom"
	name="menu-item-companylatlong[<?php echo $item_id; ?>]"
	value="<?php echo esc_attr( $item->companylatlong ); ?>" /> </label></p>

<p class="field-custom description description-wide"
	id="edit-menu-item-companyphonenumber-<?php echo $item_id; ?>"
	<?php echo $contactUS; ?>><label
	for="edit-menu-item-companyphonenumber-<?php echo $item_id; ?>"> <?php _e( 'Please write phone number' ); ?><br />
<input type="text"
	id="edit-menu-item-companyphonenumber-<?php echo $item_id; ?>"
	class="widefat code edit-menu-item-custom"
	name="menu-item-companyphonenumber[<?php echo $item_id; ?>]"
	value="<?php echo esc_attr( $item->companyphonenumber ); ?>" /> </label>
</p>

<p class="field-custom description description-wide"
	id="edit-menu-item-companyemailaddress-<?php echo $item_id; ?>"
	<?php echo $contactUS; ?>><label
	for="edit-menu-item-companyemailaddress-<?php echo $item_id; ?>"> <?php _e( 'Please write your email' ); ?><br />
<input type="text"
	id="edit-menu-item-companyemailaddress-<?php echo $item_id; ?>"
	class="widefat code edit-menu-item-custom"
	name="menu-item-companyemailaddress[<?php echo $item_id; ?>]"
	value="<?php echo esc_attr( $item->companyemailaddress ); ?>" /> </label>
</p>
<?php endif; ?> <?php
/* New fields insertion ends here */
?>
<div class="menu-item-actions description-wide submitbox"><?php if( 'custom' != $item->type && $original_title !== false ) : ?>
<p class="link-to-original"><?php printf( __('Original: %s'), '<a href="' . esc_attr( $item->url ) . '">' . esc_html( $original_title ) . '</a>' ); ?>
</p>
<?php endif; ?> <a class="item-delete submitdelete deletion"
	id="delete-<?php echo $item_id; ?>"
	href="<?php
	                echo wp_nonce_url(
	                    add_query_arg(
	                        array(
	                            'action' => 'delete-menu-item',
	                            'menu-item' => $item_id,
	                        ),
	                        remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
	                    ),
	                    'delete-menu_item_' . $item_id
	                    ); ?>"><?php _e('Remove'); ?></a> <span
	class="meta-sep"> | </span> <a class="item-cancel submitcancel"
	id="cancel-<?php echo $item_id; ?>"
	href="<?php echo esc_url( add_query_arg( array('edit-menu-item' => $item_id, 'cancel' => time()), remove_query_arg( $removed_args, admin_url( 'nav-menus.php' ) ) ) );
	?>#menu-item-settings-<?php echo $item_id; ?>"><?php _e('Cancel'); ?></a>
</div>

<input class="menu-item-data-db-id" type="hidden"
	name="menu-item-db-id[<?php echo $item_id; ?>]"
	value="<?php echo $item_id; ?>" /> <input
	class="menu-item-data-object-id" type="hidden"
	name="menu-item-object-id[<?php echo $item_id; ?>]"
	value="<?php echo esc_attr( $item->object_id ); ?>" /> <input
	class="menu-item-data-object" type="hidden"
	name="menu-item-object[<?php echo $item_id; ?>]"
	value="<?php echo esc_attr( $item->object ); ?>" /> <input
	class="menu-item-data-parent-id" type="hidden"
	name="menu-item-parent-id[<?php echo $item_id; ?>]"
	value="<?php echo esc_attr( $item->menu_item_parent ); ?>" /> <input
	class="menu-item-data-position" type="hidden"
	name="menu-item-position[<?php echo $item_id; ?>]"
	value="<?php echo esc_attr( $item->menu_order ); ?>" /> <input
	class="menu-item-data-type" type="hidden"
	name="menu-item-type[<?php echo $item_id; ?>]"
	value="<?php echo esc_attr( $item->type ); ?>" /></div>
<!-- .menu-item-settings-->
<ul class="menu-item-transport"></ul>
	<?php

	$output .= ob_get_clean();

}
}
