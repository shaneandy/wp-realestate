<?php
//Puts the output in a buffer and then outputs the buffer when done.
ob_start(); ?>
<div class='clearfix'>
	<h4 class="widget-title widgettitle"><?php echo $instance['title']?></h4>
<?php
foreach ($wovax_posts as $post) : setup_postdata( $post );
	$city = get_post_meta( $post->ID, 'City', true );
	$state = get_post_meta( $post->ID, 'State', true );
	$zip = get_post_meta( $post->ID, 'Zip_Code', true );
	$price = get_post_meta( $post->ID, 'Price', true );
	$bedrooms = get_post_meta( $post->ID, 'Bedrooms', true );
	$bathrooms = get_post_meta( $post->ID, 'Bathrooms', true );
	$sq_ft = get_post_meta( $post->ID, 'Square_Footage', true );
	$title = get_the_title( $post->ID);
	if (is_numeric($sq_ft)) {
	$sq_ft = number_format( $sq_ft );
	} else {
	$sq_ft = "";
	}
	if (is_numeric($price)) {
	$price = number_format( $price );
	} else {
	$price = "";
	}
	if ($counter % 3 == 0) {
	$first = ' first';
	} else {
	$first = '';
	}
	$counter++;
	?>
	<div id="post-<?php the_ID(); ?>" class="property one-third<?php echo $first?>">
			<a href=' <?php echo esc_url( get_permalink($post->ID) ); ?>' title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'grauke' ), get_the_title($post->ID) ) ); ?>" rel="bookmark">
				<?php echo get_the_post_thumbnail($post->ID, 'jt_wovaxproperty_thumb' ); ?>
				<h4 class="address"><?php echo $title; ?></h4>
			</a>
			<?php if($post_type == 'wovaxproperty') : ?>
			<p class="city"><?php echo $city . ', ' . $state . ' ' . $zip; ?></p>
			<h3 class="price">$<?php echo $price; ?></h3>
			<ul class="property-details">
							<li><?php echo $bedrooms; ?> Bed</li>
							<li><?php echo $bathrooms; ?> Bath</li>
							<li><?php echo $sq_ft; ?> Sq Ft</li>
					</ul>
			<?php endif; ?>
	</div>
<?php endforeach; ?>
</div>
<?php
$output = ob_get_clean();
echo $output;
wp_reset_postdata(); //necessary due to setting up the postdata earlier.