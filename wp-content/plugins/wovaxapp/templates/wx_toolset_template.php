<?php
//Puts the output in a buffer and then outputs the buffer when done.
ob_start(); ?>
<div class='clearfix'>
	<h4 class="widget-title widgettitle"><?php echo $instance['title']?></h4>
<?php
foreach ($wovax_posts as $post) : setup_postdata( $post );
	if ($counter % 3 == 0) {
	$first = ' first';
	} else {
	$first = '';
	}
	$counter++;
	?>
	<div id="post-<?php the_ID(); ?>" class="property one-third<?php echo $first?>">
			<?php if($post_type == 'apartments') { //For Dabco. There must be a better way.
                echo render_view_template('246',$post);
            } ?>
	</div>
<?php endforeach; ?>
</div>
<?php
$output = ob_get_clean();
echo $output;
wp_reset_postdata(); //necessary due to setting up the postdata earlier.