<?php
//Puts the output in a buffer and then outputs the buffer when done.
ob_start(); ?>
<div class='clearfix'>
	<h4 class="widget-title widgettitle"><?php echo $instance['title']?></h4>
<?php
foreach ($wovax_posts as $post) : setup_postdata( $post );
	if ($counter % 3 == 0) {
	$first = ' first';
	} else {
	$first = '';
	}
	$counter++;
	?>
	<div id="post-<?php the_ID(); ?>" class="wx-post one-third<?php echo $first?>">
			<a href=' <?php echo esc_url( get_permalink($post->ID) ); ?>' title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'grauke' ), get_the_title($post->ID) ) ); ?>" rel="bookmark">
				<?php echo get_the_post_thumbnail($post->ID, 'jt_wovaxproperty_thumb' ); ?>
				<h4 class="wx-post-title"><?php echo get_the_title( $post->ID); ?></h4>
			</a>
			<p><?php echo the_excerpt(); ?></p>			
	</div>
<?php endforeach; ?>
</div>
<?php
$output = ob_get_clean();
echo $output;
wp_reset_postdata(); //necessary due to setting up the postdata earlier.