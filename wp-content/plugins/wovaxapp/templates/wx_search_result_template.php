<?php 

?>
			<div id="post-<?php the_ID(); ?>" class="property one-third<?php echo $first; ?>">
            <?php if($post_type == 'apartments') { //For Dabco.
				echo render_view_template('246',$post);
            } else { ?>
                <a href=' <?php echo esc_url( get_permalink($post->ID) ); ?>' title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'grauke' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark">
                    <?php echo get_the_post_thumbnail($post->ID, 'jt_wovaxproperty_thumb'); ?>
                    <h4 class="address"><?php echo $title; ?></h4>
                </a>
				<?php if(isset($excerpt) && $excerpt == 'yes') : ?>
					<p class='excerpt'><?php echo $post_excerpt ?></p>
				<?php endif; ?>
			<?php } ?>
			<?php
			if($post_type == 'wovaxproperty') { //For Real Estate
				?>
				<p class="city"><?php echo $city.', '.$state.' '.$zip; ?></p>
				<h3 class="price">$<?php echo $price; ?></h3>
				<ul class="property-details">
					<li><?php echo $bedrooms; ?> Bed</li>
					<li><?php echo $bathrooms; ?> Bath</li>
					<li><?php echo $sq_ft; ?> Sq Ft</li>
				</ul>
				<?php
			}
			?>
            </div>
