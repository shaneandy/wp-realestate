<?php
	// saves post data
	$wx_functions = new wovax_functions();
	$wx_functions->save_data($_POST);
	// resets to default
	if (isset($_POST["reset"])) {
  		update_option("wovax_app_name", "App Name");
  		// ios reset
		update_option("wovax_ios_menu_home_button", "Home");
		update_option("wovax_ios_ipad_segment_menu", "Menu");
		update_option("wovax_ios_ipad_segment_posts", "All Posts");
		update_option("wovax_ios_back_button", "Back");
		update_option("wovax_ios_save_button", "Save");
		update_option("wovax_ios_comment_button", "Post");
		update_option("wovax_ios_post_comment_placeholder", "Post a comment");
		update_option("wovax_ios_settings_title", "Settings");
		update_option("wovax_custom_fields_filter_placeholder", "Any");
		update_option("wovax_custom_fields_filter_results_title", "Results");
		// android reset
		update_option("wovax_android_menu_home_button", "Home");
		update_option("wovax_android_save_button", "Save");
		update_option("wovax_android_comment_button", "Post");
		update_option("wovax_android_post_comment_placeholder", "Post a comment");
		update_option("wovax_android_settings_title", "Settings");
		// user reset
		update_option("wovax_login_segment", "Login");
      	update_option("wovax_register_segment", "Register");
      	update_option("wovax_login_table_title", "Login");
      	update_option("wovax_register_table_title", "Register");
		update_option("wovax_login_button", "Login");
		update_option("wovax_register_button", "Register");
		update_option("wovax_terms_sentence_beginning", "By registering you agree to the terms and conditions of our app,");
		update_option("wovax_terms_sentence_link", "please read the terms and conditions here");
		update_option("wovax_terms_sentence_end", ".");
		update_option("wovax_registration_terms", "GENERIC LEGAL TEXT");
		update_option("wovax_terms_alert_title", "Terms & Conditions");
		update_option("wovax_terms_alert_text", "By creating an account you agree to our terms & conditions.");
		update_option("wovax_terms_alert_cancel_button", "Cancel");
		update_option("wovax_terms_alert_agree_button", "Agree");
		update_option("wovax_continue_as_guest", "Continue as a guest user?");
		update_option("wovax_sign_out_button", "Sign Out");
	}
	$data_general = array(
		array(
			'name' => __('App Name', 'wovax_translation'),
			'db_name' => 'app_name',
			'description' => 'Changes the app title on the navigation bar.',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => "Custom Fields Filter Placeholder",
			'db_name' => 'custom_fields_filter_placeholder',
			'description' => 'Changes the "Any" placeholder text in the search filter.',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => "Custom Fields Filter Results",
			'db_name' => 'custom_fields_filter_results_title',
			'description' => 'Changes the results page title text in the custom fields filter results.',
			'type' => 'text',
			'disabled' => false,
		),
	);
	$data_ios = array (
		array(
			'name' => __('Home Menu Button', 'wovax_translation'),
			'db_name' => 'ios_menu_home_button',
			'description' => 'Changes the Home Menu Button.',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('iPad Menu Segment', 'wovax_translation'),
			'db_name' => 'ios_ipad_segment_menu',
			'description' => 'Changes the iPad menu segment.',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('iPad Posts Segment', 'wovax_translation'),
			'db_name' => 'ios_ipad_segment_posts',
			'description' => 'Changes the iPad Posts Segment.',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Back Button', 'wovax_translation'),
			'db_name' => 'ios_back_button',
			'description' => 'Changes the back button text.',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Save Button', 'wovax_translation'),
			'db_name' => 'ios_save_button',
			'description' => 'Changes the save button text.',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Post Comment Button', 'wovax_translation'),
			'db_name' => 'ios_comment_button',
			'description' => 'Changes the post comment button text.',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Post Comment Placeholder', 'wovax_translation'),
			'db_name' => 'ios_post_comment_placeholder',
			'description' => 'Changes the "post a comment" placeholder text.',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Settings Page Title', 'wovax_translation'),
			'db_name' => 'ios_settings_title',
			'description' => 'Changes the settings page title text.',
			'type' => 'text',
			'disabled' => false,
		),
	);
	$data_android = array(
		array(
			'name' => __('Home Menu Button', 'wovax_translation'),
			'db_name' => 'android_menu_home_button',
			'description' => 'Changes the menu Home button.',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Post Comment Placeholder', 'wovax_translation'),
			'db_name' => 'android_post_comment_placeholder',
			'description' => 'Changes the "post a comment" placeholder text.',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Settings Page Title', 'wovax_translation'),
			'db_name' => 'android_settings_title',
			'description' => 'Changes the settings page title text.',
			'type' => 'text',
			'disabled' => false,
		),
	);
	$data_users = array (
		array(
			'name' => __('Login Segmented Controller Text', 'wovax_translation'),
			'db_name' => 'login_segment',
			'description' => 'Changes the Login text on the segmented controller',
			'maxlength' => '12',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Register Segmented Controller Text', 'wovax_translation'),
			'db_name' => 'register_segment',
			'description' => 'Changes the Register text on the segmented controller',
			'maxlength' => '12',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Login Table Title', 'wovax_translation'),
			'db_name' => 'login_table_title',
			'description' => 'Changes the heading of the Login Table',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Register Table Title', 'wovax_translation'),
			'db_name' => 'register_table_title',
			'description' => 'Changes the heading of the Register Table',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Login Button', 'wovax_translation'),
			'db_name' => 'login_button',
			'description' => 'Changes the user Login button text',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Register Button', 'wovax_translation'),
			'db_name' => 'register_button',
			'description' => 'Changes the user Register button text',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Terms Sentence Beginning', 'wovax_translation'),
			'db_name' => 'terms_sentence_beginning',
			'description' => 'Changes the Terms and Condition text before the link',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Terms Sentence Link', 'wovax_translation'),
			'db_name' => 'terms_sentence_link',
			'description' => 'Changes the Terms and Conditions link text',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Terms Sentence End', 'wovax_translation'),
			'db_name' => 'terms_sentence_end',
			'description' => 'Changes the Terms and Conditions text after the link',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Terms and Conditions', 'wovax_translation'),
			'db_name' => 'registration_terms',
			'description' => 'Changes the Terms and Conditions presented to users when they register',
			'type' => 'tinymce',
			'disabled' => false,
		),
		array(
			'name' => __('Terms Alert Title', 'wovax_translation'),
			'db_name' => 'terms_alert_title',
			'description' => 'Changes the Terms and Conditions alert box title',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Terms Alert Text', 'wovax_translation'),
			'db_name' => 'terms_alert_text',
			'description' => 'Changes the Terms and Conditions alert box text',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Terms Alert Cancel Button', 'wovax_translation'),
			'db_name' => 'terms_alert_cancel_button',
			'description' => 'Changes the Terms and Conditions alert box cancel button text',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Terms Alert Agree Button', 'wovax_translation'),
			'db_name' => 'terms_alert_agree_button',
			'description' => 'Changes the Terms and Conditions alert box agree button text',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Continue as Guest Text', 'wovax_translation'),
			'db_name' => 'continue_as_guest',
			'description' => 'Changes the link text for continuing without registering',
			'type' => 'text',
			'disabled' => false,
		),
		array(
			'name' => __('Sign Out Button', 'wovax_translation'),
			'db_name' => 'sign_out_button',
			'description' => 'Changes the text on the Sign Out button',
			'type' => 'text',
			'disabled' => false,
		),
	);
?>

<div class="wrap">

<form action="" method="post" >

	<table style="width:100%">
	  <tr>
	    <td>
	      <h2><?php echo __('Translations', 'wovax_translation')?></h2>
	    </td>
	    <td>
	      <form action="" method="post" >
	        <input
	          type="submit" value="<?php echo __('Reset to Default Settings', 'wovax_translation')?>" name="reset"
	          class="button button-primary"
	        />
			<input
  				type="submit"
					value="<?php echo __('Save', 'wovax_translation')?>"
					name="save"
    			class="button button-primary"
  			/>
	      </form>
	    </td>
	  </tr>
	</table>


	<p><?php echo __('Customize the text/translations of your iOS and Android app.', 'wovax_translation')?></p>
	<h2 class="wovax-nav-wrapper nav-tab-wrapper">
		<a class="nav-tab nav-tab-active" href="#">General Translations</a>
		<a class="nav-tab" href="#">iOS Translations</a>
		<a class="nav-tab" href="#">Android Translations</a>
		<?php if (get_option('wovax_user_accounts') == 'YES' ) { ?>
			<a class="nav-tab" href="#">User Translations</a>
		<?php } ?>
	</h2>
	<div id="wovax_translation_sections">
		<section id="wovax_general_translation_section">
			<table class="form-table">
				<?php
					$wx_functions->table_generator($data_general);
				?>
			</table>
		</section>
		<section id="wovax_ios_translation_section" hidden>
			<table class="form-table">
				<?php
					$wx_functions->table_generator($data_ios);
				?>
			</table>
		</section>
		<section id="wovax_android_translation_section" hidden>
			<table class="form-table">
				<?php
					$wx_functions->table_generator($data_android);
				?>
			</table>
		</section>
		<section id="wovax_users_translation_section" hidden>
			<table class="form-table">
				<?php
					$wx_functions->table_generator($data_users);
				?>
			</table>
		</section>
	</div>
    <p>
      <input
      	type="submit"
		value="<?php echo __('Save', 'wovax_translation')?>"
		name="save"
        class="button button-primary"
      />
   </p>
</form>
</div>
