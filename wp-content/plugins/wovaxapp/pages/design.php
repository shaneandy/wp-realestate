<?php
  if  (isset($_POST["save"])) {
    $wx_functions = new wovax_functions();
  	$wx_functions->save_data($_POST);
  }

  if (isset($_POST["reset"])) {
    //android reset
    update_option("wovax_android_action_bar_color","00A9F3");
    update_option("wovax_android_action_bar_menu_icon","YES");
    update_option("wovax_android_action_bar_settings_icon","YES");
    update_option("wovax_android_action_bar_title_color","FFFFFF");
    update_option("wovax_android_menu_background_color","FFFFFF");
    update_option("wovax_android_menu_highlight_background_color","00A9F3");
    update_option("wovax_android_menu_highlight_text_color","FFFFFF");
    update_option("wovax_android_menu_text_color","000000");
    update_option("wovax_android_status_bar_color","0087D0");
    update_option("wovax_android_tint_color","00A9F3");
    // ios reset
    update_option("wovax_ios_menu_background_color","FFFFFF");
    update_option("wovax_ios_menu_divide_color","D9D9D9");
    update_option("wovax_ios_menu_highlight_background_color","D9D9D9");
    update_option("wovax_ios_menu_highlight_text_color","000000");
    update_option("wovax_ios_menu_text_color","000000");
    update_option("wovax_ios_menu_divide_color","D9D9D9");
    update_option("wovax_ios_nav_bar_color","F8F8F8");
    update_option("wovax_ios_nav_buttons_color","007AFF");
    update_option("wovax_ios_nav_menu_icon","YES");
    update_option("wovax_ios_nav_settings_icon","YES");
    update_option("wovax_ios_nav_title_color","000000");
    update_option("wovax_ios_search_bar_color","C9C9CE");
    update_option("wovax_ios_status_bar_color","black");
    update_option("wovax_ios_tab_bar_buttons_color","007AFF");
    update_option("wovax_ios_tab_bar_background_color","FFFFFF");
    update_option("wovax_ios_tint_color","007AFF");
    update_option("wovax_ios_ui_switch_color","4BD863");
    update_option("wovax_ios_search_bar_cancel_text_color","007AFF");
    update_option("wovax_ios_loading_indicator_color","000000");
  }

  echo '<script type="text/javascript" src="'.WOVAX_APP_URL.'/assets/js/jscolor.js" ></script> ';

  $elements_ios = array(
    array(
      "name" => "Status Bar Text",
      "db_name" => "ios_status_bar_color",
      "description" => "Changes the color of the status bar text.",
      "type" => "select",
      "options" =>
        array(
          "black",
          "white",
        ),
      "disabled" => false,
    ),
    array(
      "name" => "Navigation Menu Icon",
      "db_name" => "ios_nav_menu_icon",
      "description" => "Removes the menu icon in the navigation bar if set to NO.",
      "type" => "Y/N",
      "disabled" => false,
    ),
    array(
      "name" => "Navigation Settings Icon",
      "db_name" => "ios_nav_settings_icon",
      "description" => "Removes the settings/gear icon in the navigation bar if set to NO.",
      "type" => "Y/N",
      "disabled" => false,
    ),
    array(
      "name" => "Navigation Bar",
      "db_name" => "ios_nav_bar_color",
      "description" => "Changes the background color of the navigation bar.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Navigation Buttons",
      "db_name" => "ios_nav_buttons_color",
      "description" => "Changes the color of the navigation bar icons.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Navigation Title",
      "db_name" => "ios_nav_title_color",
      "description" => "Changes the color of the navigation bar title.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Menu Highlight",
      "db_name" => "ios_menu_highlight_background_color",
      "description" => "Changes the color of the background color highlights.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Menu Background",
      "db_name" => "ios_menu_background_color",
      "description" => "Changes the color of the background.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Menu Highlight Text",
      "db_name" => "ios_menu_highlight_text_color",
      "description" => "Changes the menu highlight text color.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Menu Text",
      "db_name" => "ios_menu_text_color",
      "description" => "Changes the menu text color.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Menu Divider",
      "db_name" => "ios_menu_divide_color",
      "description" => "Changes the color of the menu divider.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Search Bar",
      "db_name" => "ios_search_bar_color",
      "description" => "Changes the search bar surround color.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Tab Bar Buttons",
      "db_name" => "ios_tab_bar_buttons_color",
      "description" => "Changes the tab bar buttons color.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Tab Bar Background",
      "db_name" => "ios_tab_bar_background_color",
      "description" => "Changes the tab bar background color.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Tint Color",
      "db_name" => "ios_tint_color",
      "description" => "Changes the tint color.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Apple Watch Primary Color",
      "db_name" => "ios_apple_watch_primary_color",
      "description" => "Changes the Apple Watch primary color. This color should be chosen to be 
	  					visible against the black background of the Apple Watch.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
	array(
      "name" => "Apple Watch Secondary Color",
      "db_name" => "ios_apple_watch_secondary_color",
      "description" => "Changes the Apple Watch secondary color. This is used as a contrast to the primary color.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "UI Switch",
      "db_name" => "ios_ui_switch_color",
      "description" => "Changes the background color of push notification style On/Off switches.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Cancel Button Text Color",
      "db_name" => "ios_search_bar_cancel_text_color",
      "description" => "Changes the color of the cancel button text in the search bar.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Loading Indicator Color",
      "db_name" => "ios_loading_indicator_color",
      "description" => "Changes the color of the spinning loading indicator.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Navigation Logo",
      "db_name" => "ios_nav_logo",
      "description" => "Removes the navigation icon in the navigation bar.",
      "type" => "Y/N",
      "disabled" => false,
    ),
  );

  $elements_android = array( 
    array(
      "name" => "Action Bar Menu Icon",
      "db_name" => "android_action_bar_menu_icon",
      "description" => "Removes the menu icon in the action bar.",
      "type" => "Y/N",
      "disabled" => false,
    ),
    array(
      "name" => "Action Bar Settings Icon",
      "db_name" => "android_action_bar_settings_icon",
      "description" => "Removes the settings/gear icon in the acton bar.",
      "type" => "Y/N",
      "disabled" => false,
    ),
    array(
      "name" => "Action Bar",
      "db_name" => "android_action_bar_color",
      "description" => "Changes the background color of the action bar.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Action Bar Title",
      "db_name" => "android_action_bar_title_color",
      "description" => "Changes the color of the navigation bar title.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Menu Highlight Background",
      "db_name" => "android_menu_highlight_background_color",
      "description" => "Changes the color of the background color highlights.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Menu Background",
      "db_name" => "android_menu_background_color",
      "description" => "Changes the color of the background.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Menu Divider",
      "db_name" => "android_menu_divide_color",
      "description" => "Changes the color of the menu divide.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Menu Highlight Text",
      "db_name" => "android_menu_highlight_text_color",
      "description" => "Changes the menu highlight text color.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Menu Text",
      "db_name" => "android_menu_text_color",
      "description" => "Changes the menu text color.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Status Bar Color",
      "db_name" => "android_status_bar_color",
      "description" => "Changes the status bar background color.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
    array(
      "name" => "Tint Color",
      "db_name" => "android_tint_color",
      "description" => "Changes the general tint color.",
      "type" => "text",
      "disabled" => false,
      "class" => "color",
    ),
  );
?>

<div class="wrap">

  <form action="" method="post">

    <table style="width:100%">
      <tr>
        <td>
          <h2><?php echo __('App Design', 'wovax_translation');?></h2>
        </td>
        <td>
          <form action="" method="post" >
            <input
              type="submit" value="<?php echo __('Reset All Design Settings', 'wovax_translation')?>" name="reset"
              class="button button-primary"
            />
            <input type="submit" value="<?php echo __('Save', 'wovax_translation')?>" name="save"
            	class="button button-primary" />
          </form>
        </td>
      </tr>
    </table>

    <p><?php echo __('Customize the colors of your iOS and Android app.', 'wovax_translation');?></p>
    <h2 class="wovax-nav-wrapper nav-tab-wrapper">
      <a class="nav-tab nav-tab-active" href="#">General Design</a>
      <a class="nav-tab" href="#">iOS Design</a>
      <a class="nav-tab" href="#">Android Design</a>
      <?php //<a class="nav-tab" href="#">Logo Settings</a> ?>
	 </h2>
	<div id="wovax_design_sections">
    <section id="wovax_general_design">
      <table class="form-table">
        <tr>
          <th scope="row"><label for="wovaxapphomescreen"><?php echo __('App Home Screen', 'wovax_translation');?></label></th>
          <td>
            <select id="wx-home-screen-select" name='wovax_app_home_screen'>
              <?php $sel = get_option('wovax_app_home_screen'); ?>
              <?php if (is_plugin_active('wovax-property-management/wovax-property-management.php')) { $active = 1; } ?>
              <option <?php if ($sel == "blog") { echo "selected"; } ?> value='blog'>Archive</option>
              <option <?php if ($sel == "page") { echo "selected"; } ?> value='page'>Page</option>
              <option <?php if ($sel == "webview") { echo "selected"; } ?> value='webview'>WebView</option>
              <option
                <?php if ($sel == "custom-fields-filter") { echo "selected"; } ?>
                value='custom-fields-filter'>
                Custom Fields Filter
              </option>
            </select>
            <p class="description"><?php echo __('Changes the home screen of the app.', 'wovax_translation');?></p>
          </td>
        </tr> 
        <tr <?php if (get_option('wovax_app_home_screen') != "blog") { echo "hidden"; } ?>>
          <th scope="row"><label for="wovaxapphomeposttype"><?php echo __('Post Type', 'wovax_translation');?></label></th>
          <td>
          <select id='wovax_app_home_post_type' name='wovax_app_home_post_type'>
            <?php
              if (get_option("wovax_app_home_post_type") != "") {
                echo "<option value='".get_option("wovax_app_home_post_type")."'>".get_option("wovax_app_home_post_type")."</option>";
              }
              $args = array(
                "public" => true
              );
              $post_types = get_post_types($args);
              foreach ($post_types as $post_type) {
                echo "<option value='".$post_type."'>".$post_type."</option>";
              }
            ?>
          </select>
          <p class="description"><?php echo __('Archive Post Type.', 'wovax_translation');?></p>
          </td>
        </tr>  
        <tr <?php if (get_option('wovax_app_home_screen') != "webview") { echo "hidden"; } ?>>
          <th scope="row"><label for="wovaxapphomeurl"><?php echo __('WebView URL', 'wovax_translation');?></label></th>
          <td>
            <input type="text" name="wovax_app_home_webview_url"
            id="wovax_app_home_webview_url" class="regular-text"
            value=<?php
            echo "'" . get_option("wovax_app_home_webview_url") . "'";
            ?> />
          <p class="description"><?php echo __('Home screen default URL.', 'wovax_translation');?></p>
          </td>
        </tr>
  
        <tr <?php if (get_option('wovax_app_home_screen') != "page") { echo "hidden"; } ?>>
          <th scope="row"><label for="wovaxhomepageid"><?php echo __('Page ID', 'wovax_translation');?></label></th>
          <td>
            <input type="text" name="wovax_app_home_page_id"
            id="wovax_app_home_page_id" class="regular-text"
            value=<?php
            echo "'" . get_option("wovax_app_home_page_id") . "'";
            ?> />
          <p class="description"><?php echo __('Home screen default page ID.', 'wovax_translation');?></p>
          </td>
        </tr>
      </table>
    </section>
    <section id="wovax_ios_design" hidden>
      <table class="form-table">
        <?php
          $wx_functions = new wovax_functions();
          $wx_functions->table_generator($elements_ios);
        ?>  
        <tr id="wovax_nav_url_row" <?php if (get_option('wovax_ios_nav_logo') != "YES") { echo "hidden"; } ?>>
          <th scope="row"><label for="iosnavigationbaricon"><?php echo __('Navigation Bar Icon', 'wovax_translation');?></label></th>
          <td>
            <input type="text" name="wovax_ios_nav_logo_url"
            id="wovax_ios_nav_logo_url" class="regular-text wx-image-url"
            value=<?php echo "'" . get_option("wovax_ios_nav_logo_url") . "'";?> />
            <input type='button' class='wx-btn-upload button-secondary' value='Upload PNG Image'>
            <p class="description"><?php echo __('The logo on the Navigation Bar.', 'wovax_translation');?></p>
          </td>
        </tr>
      </table>
    </section>
    <section id="wovax_android_design" hidden>
      <table class="form-table">
        <?php $wx_functions->table_generator($elements_android);?>
      </table>
    </section>
    <section id="wovax_logo_design" hidden>
      <table class="form-table">
      </table>
    </section> 
    <p>
      <input type="submit" value="<?php echo __('Save', 'wovax_translation')?>" name="save" class="button button-primary" />
    </p>
  </form>
</div>
