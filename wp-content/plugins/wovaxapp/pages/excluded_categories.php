<?php
  global $wpdb;

  $categories = $_POST['categories'];
  if  (isset($_POST["save"])) {
    if (!empty($categories)) {
      $new_data = "," . implode(",",$categories);
      update_option("wovax_excluded_categories",$new_data);
    }else {
      update_option("wovax_excluded_categories","");
    }
  }
?>

<div class="wrap">

<h2>Exclude a category</h2>
<p>Check the boxes next to the categories you would like excluded from the app. Posts in the selected categories will show up on the web, but not in the app.</p>
</div>

<div>
  <form action="" method="post">
    <h3>Check to exclude</h3>
      <?php
      // Gets excluded catagories
      $currently_excluded_sql = "SELECT option_value FROM " . $wpdb->prefix . "options WHERE option_name='wovax_excluded_categories';";
      $excluded_sql = $wpdb->get_results($currently_excluded_sql,ARRAY_N);
      $excluded_array = explode(",", $excluded_sql[0][0]);

      // Gets all catagories
      $category_sql = "SELECT term_id FROM " . $wpdb->prefix . "term_taxonomy WHERE taxonomy='category';";
      $category_id = $wpdb->get_results($category_sql,ARRAY_N);
      foreach($category_id as $value) {
        // Gets names of catagories
        $name_sql = "SELECT name FROM " . $wpdb->prefix . "terms WHERE term_id='" . $value[0] . "';";
        $name = $wpdb->get_results($name_sql,ARRAY_N);
        if ($name[0][0] != "Uncategorized") {
          ?>
          <input
          type="checkbox"
          name="categories[]"
          value=<?php echo "'" . $value[0] . "'"; ?>
          <?php
          if (in_array($value[0], $excluded_array)) {
            echo "checked=''";
          }
          ?>
          ><?php echo $name[0][0]; ?>
          <?php
          print "<br>";
        }
      }
      ?>
      <input
      type="submit"
      value="Save"
      name="save"
      class="button button-primary"
      style="margin-top: 10px;"
      />
    </div>
  </form>
</div>
