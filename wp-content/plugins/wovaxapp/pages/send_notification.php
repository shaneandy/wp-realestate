<?php
	global $wpdb;
?>
<div class="wrap">
	<h2>Push Notifications</h2>

	<p>
		Subscribers:<b>
		<?php
			global $wpdb;
			$sql = array();
			$sql[0] = "SELECT * FROM ".$wpdb->prefix."wovaxapp_apns_device";
			$sql[1] = "SELECT * FROM ".$wpdb->prefix."wovaxapp_gcm_device";
			$apns = $wpdb->get_results($sql[0],ARRAY_N);
			$gcm = $wpdb->get_results($sql[1],ARRAY_N);
			if (sizeof($apns) + sizeof($gcm) != 0) {
				echo " ".(sizeof($apns) + sizeof($gcm))." ";
			}else {
				echo " 0 ";
			}
		?>
	</b></p>

	<h3 class="title">Message</h3>
	<form action="" method="POST">
		<textarea id='wx-message-box' name="message" placeholder="Enter Message Here" maxlength="110" rows="4" style='max-width:630px; width:100%;' class='wx-push-textarea'></textarea>
		<br>
		<small><span class="wx-message-number">110</span> characters left</small>
		<p>
			<select name="redirectType" style="width: 145px">
				<option selected='true' value="disabled" disabled>Redirect Type</option>
				<option value="none">None</option>
				<option value="link">Link</option>
				<option value="content">Site Content</option>
				<?php
					if (get_option("wovax_user_registration") == "YES") {
						echo '<option value="user">User</option>';
					}
				?>
			</select>
		</p>
		<p class='wovax-redirection-value' hidden>
			<input id="wx-redirect-value" name="redirectValue" type="text" size="15">
		</p>
		<p class='wovax-post-type-selector' hidden>
			<input id='postid' name="postid" type="text" size="15">
		</p>
		<p class='wovax-user-selector' hidden>
			<input id='userid' name="userid" type="text" size="15">
		</p>
		<p class='wovax-push-category-selector'>
			<select name="categories" style="width: 145px">
				<option selected='true' name='push_category' value='disabled' disabled>Category</option>
			<?php
				$db = get_option("wovax_pushnotification_categories");
				$json = json_decode($db);
				for ($i=0;$i<sizeof($json);$i++) {
					echo "<option name='push_category' value='".$json[$i]->id."'>".$json[$i]->name."</option>";
				}
			?>
			</select>
		</p>
		<br>
		<input type="submit" class="button button-primary" name="push" value="Push Message">
		<div class="wx-push-spinner spinner"></div>
	</form>
	<br>
	<br>
	<input type='button' class='wx-show-history button button-secondary' value="Show History">
	<div id="previouspost" hidden>
		<form action="" method="POST">
			<strong style="padding-right:60px;"><?php echo __('Number of notifications to view:', 'wovax_translation');?></strong>
			<select name="recallNumber">

				<?php
					if (isset($_POST['recallNumber'])) {
						$recall = $_POST['recallNumber'];
					}else {
						$recall = false;
					}
				?>

				<option value="5"<?php if ($recall == 5) { echo " selected "; } ?>>5</option>
				<option value="10"<?php if ($recall == 10) { echo " selected "; } ?>>10</option>
				<option value="20"<?php if ($recall == 20) { echo " selected "; } ?>>20</option>
				<option value="50"<?php if ($recall == 50) { echo " selected "; } ?>>50</option>
				<option value="100"<?php if ($recall == 100) { echo " selected "; } ?>>100</option>
				<option value="ALL"<?php if ($recall == "ALL") { echo " selected "; } ?>>ALL</option>
			</select>
			<input type="submit" name="update" value="Update!" class="button button-primary wx-notification-number">
			<br>
			<div class="wx-notification-table">
				<table class="widefat" cellspacing="0">
					<thead>
						<tr height="30px">
							<th class="manage-column" ><?php echo __('Message', 'wovax_translation');?></th>
							<th class="manage-column" ><?php echo __('Time Sent', 'wovax_translation');?></th>
							<th class="manage-column" style="text-align:right"><?php echo __('Delete', 'wovax_translation');?></th>
						</tr>
					</thead>
					<?php
						{ // Set number of messages to recall
							$sql = "SELECT * FROM " . $wpdb->prefix . "wovaxapp_pushnotification ORDER BY unixtime DESC";
							$messages = $wpdb->get_results($sql,ARRAY_A);
							$odd = 1;
							if (isset($_POST['recallNumber'])) {
								if ($_POST['recallNumber'] == "ALL") {
									$count = count($messages);
								}elseif ($_POST['recallNumber'] <= count($messages)){
									$count = $_POST['recallNumber'];
								}else {
									$count = count($messages);
								}
							}else {
								if (count($messages) <= 5) {
									$count = count($messages);
								}else {
									$count = 5;
								}
							}
						}
						{ // Iterate through messages to display
							for ($i=0;$i<$count;$i++) {
								if ($messages[$i]['unixtime'] != 0) {
									$dt = new DateTime('@'.$messages[$i]['unixtime']);
                                    $datestring = get_option('timezone_string');
									$dt->setTimeZone(new DateTimeZone(empty($datestring) ? 'UTC' : $datestring ));
									$date = $dt->format(get_option('date_format')." \a\\t ".get_option('time_format'));
								}else {
									$date = "";
								}
                                $unixtime = $messages[$i]['unixtime'];							
								if (($i % 2) == 1) {
						          $class = "alternate";
						        } else {
						          $class = "";
						        }
								echo "<tr class='$class' height='30px'>";
								echo "<td >" . $messages[$i]['pushmsg'] . "</td>";
								echo "<td align='left' >" . $date . "</td>";
								echo "<td class='wx-notification-delete' align='right'><a href='#'>Delete</a></td>";
								echo "<td class='wx-unixtime' hidden>$unixtime</td>";
								echo "</tr>";
							}
						}
					?>
				</table>
				<?php
					{ // displays "showing x of y notifications sent" at bottom of table
						if ($_POST['recallNumber'] != "") {
							if ($_POST['recallNumber'] == "ALL") {
								$show = count($messages);
							}elseif ($_POST['recallNumber'] <= count($messages)){
								$show = $_POST['recallNumber'];
							}else {
								$show = count($messages);
							}
						}else {
							if (count($messages) > 5) {
								$show = 5;
							}else {
								$show = count($messages);
							}
						}
						echo "<small>";
						echo __('Showing', 'wovax_translation')." <span class='wx-shown-number'>".$show."</span> ".__('of', 'wovax_translation')." <span class='wx-total-number'>".count($messages)."</span> ".__('Push Notifications Sent', 'wovax_translation');
						echo "</small>";
					}
				?>
			</div>
		</form>
	</div>
</div>
