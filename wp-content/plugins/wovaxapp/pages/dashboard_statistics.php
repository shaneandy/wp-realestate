<tr>
	<td>
	<?php
	// ----DataBase Info---------------
		global $wpdb;

	// ----Date Input Vars-------------
	if(!$_POST['byYear'] || empty($_POST['byYear'])){
		$year = date('Y');
	}else{
		$year = $_POST['byYear'];
	}
	if(!$_GET['byMonth'] || empty($_GET['byMonth'])){
		$yearByDay = date('Y');
		$monthByDay = date('n');
	}else{
		$da = explode(",", $_GET['byMonth']);
		$monthByDay = $da['0'];
		$yearByDay = $da['1'];
	}
	$january_of_year = mktime(0,0,0,0,0,$year);

	// ----Recusion Variables----------
	$months=array(
		__("January", 'wovax_translation'),
		__("February", 'wovax_translation'),
		__("March", 'wovax_translation'),
		__("April", 'wovax_translation'),
		__("May", 'wovax_translation'),
		__("June", 'wovax_translation'),
		__("July", 'wovax_translation'),
		__("August", 'wovax_translation'),
		__("September", 'wovax_translation'),
		__("October", 'wovax_translation'),
		__("November", 'wovax_translation'),
		__("December", 'wovax_translation')
	);

	?>
	<!-----Monthly Downloads Script---->
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
			<script type="text/javascript">
				google.load("visualization", "1", {packages:["corechart"]});
				google.setOnLoadCallback(drawChart);
				function drawChart() {
					var data = google.visualization.arrayToDataTable([
						['<?php echo __("Month", 'wovax_translation');?>', '<?php echo __("Downloads", 'wovax_translation');?>'],
						<?php
							// ----Loop through 12 months----
							for ($month_count=0;$month_count<=12;$month_count++){
								$start = $january_of_year + (($month_count + 1) * (86400 * 30));
								$end = $january_of_year + (($month_count + 2) * (86400 * 30));

								$graph_sql = "SELECT * FROM " . $wpdb->prefix . "wovaxapp_downloads WHERE unixtime >= '$start' AND unixtime <= '$end';";
								$graph_arr = $wpdb->get_results($graph_sql,ARRAY_N);

								echo "['" . $months[$month_count] . "',";
								echo "(" . $month_count . "," . sizeof($graph_arr) . ")";

								if ($month_count < 12) {
									echo "],";
								}else{
									echo "]]);";
								}
							}
						?>
			var options = {
			backgroundColor:'transparent',
			colors:['23a2cc'],
			title: <?php echo '"' . __('App downloads', 'wovax_translation') . '"';?>,
			hAxis: {title: <?php echo "'" . __('Monthly downloads', 'wovax_translation') . "'";?>,  titleTextStyle: {color: '#333', fontName: 'Helvetica', fontSize: "18"}}
			};

			var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
			chart.draw(data, options);
		}
	</script>

	<!-----HTML Main Body------------>
	<div>
	<h2><?php echo __('App Dashboard', 'wovax_translation');?></h2>
			<div>
				<table style="width:100%">
					<tr>
						<td style="width:20%">
							<div class="total_downloads">
								<div align="center" class="count_number">
									<?php // ----Count Rows------------------
											$total_downloads_sql = "SELECT no FROM " . $wpdb->prefix . "wovaxapp_downloads;";
											$total_downloads_arr = $wpdb->get_results($total_downloads_sql,ARRAY_N);
											echo sizeof($total_downloads_arr);
									?>
								</div>
								<div align="center" class="count_number_desc"><?php echo __('Total downloads', 'wovax_translation');?></div>
							</div>
						</td>
						<td style="width:20%">
							<div class="push_subscribers">
								<div align="center" class="count_number">
									<?php // ----Total Downloads-------------
										$apns_subscribers_sql = "SELECT id FROM " . $wpdb->prefix . "wovaxapp_apns_device;";
										$apns_subscribers_arr = $wpdb->get_results($apns_subscribers_sql,ARRAY_N);
										$gcm_subscribers_sql = "SELECT id FROM " . $wpdb->prefix . "wovaxapp_gcm_device;";
										$gcm_subscribers_arr = $wpdb->get_results($gcm_subscribers_sql,ARRAY_N);
										echo sizeof($apns_subscribers_arr)+sizeof($gcm_subscribers_arr);
									?>
								</div>
							<div align="center" class="count_number_desc"><?php echo __('Push subscribers', 'wovax_translation');?></div>
							</div>
						</td>
						<td style="width:20%">
							<div class="today">
							<div align="center" class="count_number">
								<?php // ----Downloads Today-------------
									$this_today = time() - 86400;
									$apns = 0;
									$gcm = 0;

									$today_apns_sql = "SELECT * FROM " . $wpdb->prefix . "wovaxapp_apns_device;";
									$today_apns = $wpdb->get_results($today_apns_sql,ARRAY_A);
									for ($i=0;$i<sizeof($today_apns);$i++) {
										$date = date_create_from_format('Y-m-d', $today_apns[$i]['registered_on']);
										$time = date_timestamp_get($date);
										if ($time >= $this_today) {
											$apns++;
										}
										$today_apns[$i]['registered_on'] = $time;
									}

									$today_gcm_sql = "SELECT * FROM " . $wpdb->prefix . "wovaxapp_gcm_device;";
									$today_gcm = $wpdb->get_results($today_gcm_sql,ARRAY_A);
									for ($i=0;$i<sizeof($today_gcm);$i++) {
										$date = date_create_from_format('Y-m-d', $today_gcm[$i]['registered_on']);
										$time = date_timestamp_get($date);
										if ($time >= $this_today) {
											$gcm++;
										}
										$today_apns[$i]['registered_on'] = $time;
									}

									echo $apns+$gcm;
								?>
							</div>
								<div align="center" class="count_number_desc"><?php echo __('Today', 'wovax_translation');?></div>
							</div>
						</td>
						<td style="width:20%">
							<div class="this_month">
							<div align="center" class="count_number">
								<?php // ----Downloads This Month--------
									$this_month = time() - (86400 * 30);
									$apns = 0;
									$gcm = 0;

									$month_apns_sql = "SELECT * FROM " . $wpdb->prefix . "wovaxapp_apns_device;";
									$month_apns = $wpdb->get_results($month_apns_sql,ARRAY_A);
									for ($i=0;$i<sizeof($month_apns);$i++) {
										$date = date_create_from_format('Y-m-d', $month_apns[$i]['registered_on']);
										$time = date_timestamp_get($date);
										if ($time >= $this_month) {
											$apns++;
										}
										$month_apns[$i]['registered_on'] = $time;
									}

									$month_gcm_sql = "SELECT * FROM " . $wpdb->prefix . "wovaxapp_gcm_device;";
									$month_gcm = $wpdb->get_results($month_gcm_sql,ARRAY_A);
									for ($i=0;$i<sizeof($month_gcm);$i++) {
										$date = date_create_from_format('Y-m-d', $month_gcm[$i]['registered_on']);
										$time = date_timestamp_get($date);
										if ($time >= $this_month) {
											$gcm++;
										}
										$month_apns[$i]['registered_on'] = $time;
									}

									echo $apns+$gcm;
								?>
						</div>
							<div align="center" class="count_number_desc"><?php echo __('This Month', 'wovax_translation');?></div>
						</div>
					</td>
					<td style="width:20%">
							<div class="this_year">
							<div align="center" class="count_number">
								<?php // ----Downloads This Year---------
									$this_year = time() - (86400 * 30 * 12);
									$year_sql = "SELECT no FROM " . $wpdb->prefix . "wovaxapp_downloads WHERE unixtime >= '$this_year';";
									$year_arr = $wpdb->get_results($year_sql,ARRAY_N);
									echo sizeof($year_arr);
									?>
							</div>
								<div align="center" class="count_number_desc"><?php echo __('This Year', 'wovax_translation');?></div>
							</div>
						</td>
					</tr>
				</table>



				<div style="clear:both;"></div>
			</div>

			<!-----Monthly Downloads Chart---->
			<div style="margin-top:30px;">
					<div>
						<h3 class="title">
							<?php echo __('Monthly downloads', 'wovax_translation') . " " . $year; ?>
						</h3>
						<div style="float:right; width:39%;">
							<div align="right">
								<form action="" method="post" style="PADDING-RIGHT: 20px">
									<select name="byYear">
										<?php
											if (!isset($_POST['byYear']) || ($_POST['byYear'] == (date('Y')))) {
												echo "<option value=" . date('Y') . ">";
												echo date('Y');
												echo "</option>";

												echo "<option value=" . (date('Y') - 1) . ">";
												echo (date('Y') - 1);
												echo "</option>";
											}elseif ($_POST['byYear'] == ((date('Y') - 1))) {
												echo "<option value=" . (date('Y') - 1) . ">";
												echo (date('Y') - 1);
												echo "</option>";

												echo "<option value=" . date('Y') . ">";
												echo date('Y');
												echo "</option>";
											}
										?>
									</select>
									<input type="submit" class="button button-primary" value="<?php echo __('Submit', 'wovax_translation')?>">
								</form>

							</div>
							</div>
							<div style="clear:both;"></div>
						</div>
					<div id="chart_div" style="width: 100%; height:300px;"></div>
			</div>
		</tr>
</td>

<!-----Device Statistics---->
<td>
	<tr>
		<br><br>
		<div>
			<h3 class="title"> <?php echo __('Device Statistics', 'wovax_translation');?> </h3>
		</div>
		<div>
			<table style="width:100%">
				<tr>
					<td style="width:20%">
						<div class="iOS Devices">
							<div align="center" class="count_number">
								<?php // ----Count Rows------------------
										$sql = "SELECT no FROM " . $wpdb->prefix . "wovaxapp_downloads;";
										$arr = $wpdb->get_results($sql,ARRAY_N);
										echo sizeof($arr);
								?>
							</div>
							<div align="center" class="count_number_desc"><?php echo __('iOS Devices', 'wovax_translation');?></div>
						</div>
					</td>
					<td style="width:20%">
						<div class="Android Devices">
							<div align="center" class="count_number">
								<?php // ----Total Downloads-------------
									$android_sql = "SELECT * FROM " . $wpdb->prefix . "wovaxapp_gcm_device;";
									$android_device = $wpdb->get_results($android_sql,ARRAY_A);
									echo sizeof($android_device);
								?>
							</div>
						<div align="center" class="count_number_desc"><?php echo __('Android Devices', 'wovax_translation');?></div>
						</div>
					</td>
					<td style="width:20%">
						<div class="iPhone">
							<div align="center" class="count_number">
								<?php // ----Downloads Today-------------
									$sql = "SELECT no FROM " . $wpdb->prefix . "wovaxapp_downloads WHERE device = 'iPhone';";
									$arr = $wpdb->get_results($sql,ARRAY_N);
									echo sizeof($arr);
								?>
							</div>
							<div align="center" class="count_number_desc"><?php echo __('iPhone', 'wovax_translation');?></div>
						</div>
					</td>
					<td style="width:20%">
						<div class="iPad">
							<div align="center" class="count_number">
								<?php // ----Downloads This Month--------
									$sql = "SELECT no FROM " . $wpdb->prefix . "wovaxapp_downloads WHERE device = 'iPad';";
									$arr = $wpdb->get_results($sql,ARRAY_N);
									echo sizeof($arr);
								?>
						</div>
						<div align="center" class="count_number_desc"><?php echo __('iPad', 'wovax_translation');?></div>
					</div>
				</td>
				<td style="width:20%">
						<div class="Other">
							<div align="center" class="count_number">
								<?php // ----Downloads This Year---------
									$sql = "SELECT no FROM " . $wpdb->prefix . "wovaxapp_downloads WHERE device LIKE '%Simulator';";
									$arr = $wpdb->get_results($sql,ARRAY_N);
									echo sizeof($arr);
								?>
							</div>
							<div align="center" class="count_number_desc"><?php echo __('Other', 'wovax_translation');?></div>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</tr>
</td>
