<?php
  // saves post data
  $wx_functions = new wovax_functions();
  $wx_functions->save_data($_POST);

  function banner_image() {
    ?>
    <table style="width:100%" class='form-table'>
      <tr>
        <td style='padding:0px'>
          <h3 class='title' style='margin-top:30px'><?php echo __('Phone banner image', 'wovax_translation')?></h3>
        </td>
      </tr>
      <tr>
        <th scope="row">
          <b><?php echo __('Image URL:', 'wovax_translation')?></b>
        </th>
      <?php
        if (strlen(get_option("wovax_phone_portrait_banner")) >= 80 ) {
          $input_field_size = 80;
        }else{
          $input_field_size = strlen(get_option("wovax_phone_portrait_banner"));
        }
      ?>
        <td style='padding:0px'>
          <input
            type='text'
            name='wovax_phone_portrait_banner'
            id='wovax_phone_portrait_banner'
            class='wx-image-url'
            value=<?php echo "'" . get_option("wovax_phone_portrait_banner") . "'"; ?>
            size=<?php echo "'" . $input_field_size . "'"; ?>
          >
          <input type='button' class='wx-btn-upload button-secondary' value='Upload PNG Image'>
        </td>
      </tr>
      <tr>
        <th scope='row'>
          <b><?php echo __('Image Preview:', 'wovax_translation')?></b>
        </th>
        <td style='padding:0px'>
          <?php
            $data = get_option("wovax_phone_portrait_banner");
            $data_type = strtolower(end(explode(".",$data)));
            if ($data_type != "png") {
              echo "Image MUST be in 'png' format";
            }elseif (@!is_array(getimagesize($data))) {
              echo 'Data is not an image';
            }else{
              list($width, $height) = getimagesize($data);
              if ($width != 1242 && $height != 194) {
      			echo __('Your image is not the right size.', 'wovax_translation') . "<br>" . __('Although this will work, the best size would be 1242x194px.', 'wovax_translation') . "<br><br>";
              }
              echo '<img style="width:621px; height: 97px; border: 0;" src=' . $data . '>';
            }
          ?>
          <p><?php echo __('Banner should be 194px high by 1242px wide.', 'wovax_translation')?></p>
        </td>
      </tr>
      <tr>
        <th scope='row' style='padding:0px'>
          <h3 class='title' style='margin-top:30px'><?php echo __('Banner link', 'wovax_translation')?></h3>
        </th>
      </tr>
      <tr>
        <td style='padding:0px'>
          <input
            type='text'
            name='wovax_link_banner'
            id='wovax_link_banner'
            value=<?php echo "'" . get_option("wovax_link_banner") . "'"; ?>
          >
          <p><?php echo __('Ad Link', 'wovax_translation')?> i.e: https://www.wovax.com/</p>
        </td>
      </tr>
    </table>
    <?php
  }

  function html_upload() {
    ?>
    <table style="width:100%; padding: 0;" class='form-table'>
      <tr>
        <th scope='row'>
          <h3 class='title' style='margin-top:30px'><?php echo __('Custom Ad Code', 'wovax_translation')?></h3>
        </th>
      </tr>
      <tr>
        <td style='padding:0px'>
          <p><?php echo __('Enter your 2 part ad code in the boxes below.', 'wovax_translation')?></p>
        </td>
      </tr>
      <tr>
        <td style='padding:0px'>
          <?php echo __('HTML:', 'wovax_translation')?><br>
        </td>
      </tr>
      <tr>
        <td style='padding:0px'>
          <p><textarea rows='4' cols='50' id='html_snippet' name='wovax_html_custom_ad'><?php echo get_option("wovax_html_custom_ad"); ?></textarea></p>
        </td>
      </tr>
      <tr>
        <td style='padding:0px'>
          <p><?php echo __('JavaScript:', 'wovax_translation')?><br></p>
        </td>
      </tr>
      <tr>
        <td style='padding:0px'>
          <textarea rows='4' cols='50' id='javascript_snippet' name='wovax_js_custom_ad'><?php echo get_option("wovax_js_custom_ad"); ?></textarea>
        </td>
      </tr>
    </table>
    <?php
  }

  function no_ad() {
    ?>
    <table style="width:100%; padding: 0;" class='form-table'>
      <tr>
        <th scope='row'>
          <h3 class='title' style='margin-top:30px'><?php echo __('No Advertisements', 'wovax_translation')?></h3>
        </th>
      </tr>
      <tr>
        <td style='padding:0px'>
          <p><?php echo __('No advertisements will be displayed.', 'wovax_translation')?></p>
        </td>
      </tr>
    </table>
    <?php
  }

  // if = 1, return ajax response
  if ($ajax == "banner_image") {
    banner_image();
    exit();
  }elseif ($ajax == "html_upload") {
    html_upload();
    exit();
  }elseif ($ajax == "no_ad") {
    no_ad();
    exit();
  }

  $nonce = wp_create_nonce( 'app-ads.php' );
?>
<!-- client-side script -->
<script  type='text/javascript'>
  function AJAX_banner_image(){
    jQuery.ajax({
      type: "post",url: "admin-ajax.php",data: { action: 'banner_image', _ajax_nonce: '<?php echo $nonce; ?>' },
      success: function(html){ //so, if data is retrieved, store it in html
        jQuery("#response_div").html(html); //show the html inside helloworld div
        jQuery("#response_div").show("slow"); //animation
      }
    }); //close jQuery.ajax(
  }
  function AJAX_html_upload(){
    jQuery.ajax({
      type: "post",url: "admin-ajax.php",data: { action: 'html_upload', _ajax_nonce: '<?php echo $nonce; ?>' },
      success: function(html){ //so, if data is retrieved, store it in html
        jQuery("#response_div").html(html); //show the html inside helloworld div
        jQuery("#response_div").show("slow"); //animation
      }
    }); //close jQuery.ajax(
  }
  function AJAX_no_ad(){
    jQuery.ajax({
      type: "post",url: "admin-ajax.php",data: { action: 'no_ad', _ajax_nonce: '<?php echo $nonce; ?>' },
      success: function(html){ //so, if data is retrieved, store it in html
        jQuery("#response_div").html(html); //show the html inside helloworld div
        jQuery("#response_div").show("slow"); //animation
      }
    }); //close jQuery.ajax(
  }
</script>
<h2><?php echo __('App advertising', 'wovax_translation')?></h2>
<br>
<div>
  <form action="" method="post" enctype="multipart/form-data">
    <div>
      <table style="width:60%">
        <tr>
          <td style="width:33%">
            <input type="radio" name="wovax_ad_option" value="banner_image" id="radio_banner_image" onclick="AJAX_banner_image()"
            <?php
              if(get_option("wovax_ad_option") == "banner_image") {
                echo "CHECKED";
              }
            ?>><?php echo __('Banner & Link', 'wovax_translation')?>
          </td>

          <td style="width:33%">
            <input type="radio" name="wovax_ad_option" value="html_upload" id="radio_htmlup" onclick="AJAX_html_upload()"
            <?php
              if(get_option("wovax_ad_option") == "html_upload") {
                echo "CHECKED";
              }
            ?>><?php echo __('Custom Ad Code', 'wovax_translation')?>
          </td>

          <td style="width:34%">
            <input type="radio" name="wovax_ad_option" value="no_ad" id="radio_noad" onclick="AJAX_no_ad()"
            <?php
              if(get_option("wovax_ad_option") == "no_ad") {
                echo "CHECKED";
              }
            ?>><?php echo __('No Ads', 'wovax_translation')?>
          </td>
        </tr>
      </table>
    </div>

  <div id='response_div'>
    <?php
      if (get_option("wovax_ad_option") == "banner_image") {
        banner_image();
      }
      elseif (get_option("wovax_ad_option") == "html_upload") {
        html_upload();
      }
      elseif (get_option("wovax_ad_option") == "no_ad") {
        no_ad();
      }
    ?>
  </div>
  <br>
  <br>
  <br>
  <input
    type="submit" value="<?php echo __('Save', 'wovax_translation')?>" name="save"
    class="button button-primary"
  />
  </form>
</div>
