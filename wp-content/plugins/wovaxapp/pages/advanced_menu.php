<?php
  global $wpdb;
  $wx_functions = new wovax_functions();
  if  ($_POST["save"] != "") {
  	$wx_functions->save_data($_POST);
  }
?>
<div class="wrap">
	<h2><?php echo __('Wovax Advanced Menu','wovax_translation')?></h2>
	<h2 class="wovax-nav-wrapper nav-tab-wrapper">
		<a class="nav-tab nav-tab-active" href="#">Google Settings</a>
		<a class="nav-tab" href="#">GCM Settings</a>
		<a class="nav-tab" href="#">APNS Settings</a>
		<a class="nav-tab" href="#">User Settings</a>
		<a class="nav-tab" href="#">Ad Settings</a>
		<?php //<a class="nav-tab" href="#">Excluded Categories</a> ?>
	</h2>
	<form enctype="multipart/form-data" action="" method="post">
	<div id="wovax-advanced-sections">
		<section id="wovax-google-analytics">
			<h3><?php echo __('Google Analytics', 'wovax_translation')?></h3>
			<p><?php echo __('Enter your Google Analytics ID below to track your apps usage.', 'wovax_translation')?></p>
		
			<?php
			// Google Analytics
			$gAnalyticsId= get_option("wovax_google_analytics_id");
			?>
				<table class="form-table">
					<tr>
					<th scope="row"><label for="wovax_google_analytics_id"><?php echo __('Google Analytics ID', 'wovax_translation')?></label></th>
						<td>
							<input
								type="text" name="wovax_google_analytics_id" id="wovax_google_analytics_id"
								class="regular-text" value="<?php echo get_option("wovax_google_analytics_id");?>"
							/>
							<p class="description"><?php echo __('Usually starts with UA-. Create your app Google Analytics ID', 'wovax_translation')?> <a href="https://google.com/analytics/"><?php echo __('here', 'wovax_translation')?></a>.</p>
						</td>
					</tr>
				</table>
		
				<hr>
				
				<h3><?php echo __('Android Maps API Key', 'wovax_translation')?></h3>
				<p><?php echo __('Enter the Google Maps API key for your App.', 'wovax_translation')?></p>
				
				<table class="form-table">
					<tr>
						<th scope="row">
							<label for="wovax_android_maps_api_key"><?php echo __('Android Maps API Key', 'wovax_translation')?></label>
						</th>
						<td>
							<input
								type="text" name="wovax_android_maps_api_key" id="wovax_android_maps_api_key"
								class="regular-text" value="<?php echo get_option("wovax_android_maps_api_key");?>"
							/>
							<p class="description"><?php echo __('Your Google Maps API key is unique to your App, as shown ', 'wovax_translation')?> <a href="https://developers.google.com/maps/documentation/android-a…"><?php echo __('here', 'wovax_translation')?></a>.</p>
						</td>
					</tr>
				</table>
				
				<hr />
		</section>
		<section id="wovax-gcm-settings" hidden>
				<h3><?php echo __('Google Cloud Messaging for Android Settings', 'wovax_translation')?></h3>
				<p><?php echo __('It is recommended that only a Wovax developer adjust these settings.', 'wovax_translation')?></p>
				<table class='form-table'>
					<tr>
						<th scope='row'>
							<label><?php echo __('App environment', 'wovax_translation')?></label>
						</th>
						<td>
							<?php
								if (get_option("wovax_android_gcm_environment") == 1) {
									$google_development = " checked=''";
									$google_production = "";
								}elseif (get_option("wovax_android_gcm_environment") == 2) {
									$google_development = "";
									$google_production = " checked=''";
								}else {
									$google_development = "";
									$google_production = "";
								}
							?>
							<input
								type="radio"
								name="wovax_android_gcm_environment"
								value="1"
								<?php echo $google_development; ?>
							><?php echo __('Development', 'wovax_translation')?>
							<br />
							<input
								type="radio"
								name="wovax_android_gcm_environment"
								value="2"
								<?php echo $google_production; ?>
							><?php echo __('Production', 'wovax_translation')?>
						</td>
					</tr>
					<tr>
						<th scope='row'>
							<label for='gcmapikey'><?php echo __('GCM API Key', 'wovax_translation')?></label>
						</th>
						<td>
							<input
								type="text"
								name="wovax_android_gcm_api_key"
								value="<?php echo get_option("wovax_android_gcm_api_key");?>"
							>
						</td>
					</tr>
				</table>				
				<hr>
		</section>
				<?php
				// Response-div fill-in
				function APNS_current() {
					ob_start();
					?>
					<p>
						*<?php echo __('App must be installed on a device with push notifications enabled.', 'wovax_translation')?>
					</p>
					<?php
					$output = ob_get_clean();
					return $output;
				}
				function APNS_external() {
					ob_start();
					?>
					<p>
						<?php echo __('If your current server fails to send a push notification you can use an external server.', 'wovax_translation')?><br>
						<?php echo __('Please upload apns.php to an external server that allows TCP port 2195 & 2196 for APNS in & out connections and set the URL to the correct path.', 'wovax_translation')?>
						e.g http://www.yourdomain.com/apns.php <?php echo __('and paste it into the text box below.', 'wovax_translation')?>
					</p>
					http://
					<input
					type="text"
					id="wovax_ios_external_push_server_path"
					name="wovax_ios_external_push_server_path"
					size="30"
					<?php
					echo " value='" . get_option("wovax_ios_external_push_server_path") . "'";
					?>
					> e.g http://www.yourdomain.com/apns.php
					<br><br>
					<p>
						*<?php echo __('App must be installed on a device with push notifications enabled.', 'wovax_translation')?>
					</p>
					<?php
					$output = ob_get_clean();
					return $output;
				}
				function APNS_no() {
					ob_start();
					?>
					<p>
						<?php echo __('Do not worry. You can always setup APNS push notifications later.', 'wovax_translation')?>
					</p>
					<?php
					$output = ob_get_clean();
					return $output;
				}
		
				// AJAX server-side script
				if ($ajax == "APNS_current") {
					APNS_current();
					exit();
				}elseif ($ajax == "APNS_external") {
					APNS_external();
					exit();
				}elseif ($ajax == "APNS_no") {
					APNS_no();
					exit();
				}
		
				$nonce = wp_create_nonce( 'advanced_menu.php' );
				?>
		
				<!-- AJAX client-side script -->
				<script  type='text/javascript'>
					function APNS_current(){
					jQuery.ajax({
						type: "post",url: "admin-ajax.php",data: { action: 'APNS_current', _ajax_nonce: '<?php echo $nonce; ?>' },
						success: function(response){ //so, if data is retrieved, store it in html
							jQuery("#response_div").html(response); //show the html inside helloworld div
							jQuery("#response_div").show("slow"); //animation
						}
					});
					}
					function APNS_external(){
						jQuery.ajax({
							type: "post",url: "admin-ajax.php",data: { action: 'APNS_external', _ajax_nonce: '<?php echo $nonce; ?>' },
							success: function(response){ //so, if data is retrieved, store it in html
								jQuery("#response_div").html(response); //show the html inside helloworld div
								jQuery("#response_div").show("slow"); //animation
							}
						});
					}
					function APNS_no(){
						jQuery.ajax({
							type: "post",url: "admin-ajax.php",data: { action: 'APNS_no', _ajax_nonce: '<?php echo $nonce; ?>' },
							success: function(response){ //so, if data is retrieved, store it in html
								jQuery("#response_div").html(response); //show the html inside helloworld div
								jQuery("#response_div").show("slow"); //animation
							}
						});
					}
				</script>
		<section id="wovax-apns-settings" hidden>
				<h3><?php echo __('Apple Push Notification for iOS Settings', 'wovax_translation')?></h3>
				<p><?php echo __('It is recommended that only a Wovax developer adjust these settings.', 'wovax_translation')?></p>
				<table class='form-table'>
					<tr>
					<!-- App Environnment -->
						<th scope='row'>
							<label><?php echo __('App environment', 'wovax_translation')?></label>
						</th>
						<?php
							if (get_option("wovax_ios_apns_environment") == 1) {
								$development = " checked=''";
								$production = "";
							}elseif (get_option("wovax_ios_apns_environment") == 2) {
								$development = "";
								$production = " checked=''";
							}else {
								$development = "";
								$production = "";
							}
						?>
						<td>
							<input
							type="radio"
							name="wovax_ios_apns_environment"
							value="1"
							id="env_0"
							<?php echo $development; ?>
							><?php echo __('Development', 'wovax_translation')?>
							<br />
							<input
							type="radio"
							name="wovax_ios_apns_environment"
							value="2"
							id="env_1"
							<?php echo $production; ?>
							><?php echo __('Production', 'wovax_translation')?>
						</td>
					</tr>
					<tr>
					<!-- Certificate Path -->
						<th scope='row'>
							<label><?php echo __('Certificate path', 'wovax_translation')?></label>
						</th>
						<td>
							<?php echo site_url()."/"; ?>
							<input
							type="text"
							autocomplete="off"
							name="wovax_ios_development_pem_path"
							id="wovax_ios_development_pem_path"
							<?php
							echo " value='" . get_option("wovax_ios_development_pem_path") . "'";
							?>
							size="40"
							><?php echo __('Development', 'wovax_translation')?>
							<br />
							<?php echo site_url()."/"; ?>
							<input
							type="text"
							autocomplete="off"
							name="wovax_ios_production_pem_path"
							id="wovax_ios_production_pem_path"
							<?php
							echo " value='" . get_option("wovax_ios_production_pem_path") . "'";
							?>
							size="40"
							><?php echo __('Production', 'wovax_translation')?>
						</td>
					</tr>
					<tr>
					<!-- Certificate Password -->
						<th scope='row'>
							<label><?php echo __('Certificate Password', 'wovax_translation')?></label>
						</th>
						<td>
							<input
							type="password"
							autocomplete="off"
							name="wovax_ios_pem_password"
							id="wovax_ios_pem_password"
							<?php echo " value='" . get_option("wovax_ios_pem_password") . "'"; ?>
							size="30"
							>
							<p class="description">Typically, the last 6 characters of your Wovax API key</p>
						</td>
					</tr>
					<tr>
					<!-- APNS Compatible Server -->
						<th scope='row'>
							<label><?php echo __('APNS compatible server', 'wovax_translation')?></label>
						</th>
						<td>
							<input
							type="radio"
							name="wovax_ios_push_host"
							onclick="APNS_current()"
							value="1"
							id="ChooseHost_0"
							<?php
							if (get_option("wovax_ios_push_host") == 1) {
								echo " checked";
							}
							?>
							><?php echo __('Current server', 'wovax_translation')?>
							<br />
							<input
							type="radio"
							name="wovax_ios_push_host"
							onclick="APNS_external()"
							value="2"
							id="ChooseHost_1"
							<?php
							if (get_option("wovax_ios_push_host") == 2) {
								echo " checked";
							}
							?>
							><?php echo __('External server', 'wovax_translation')?>
							<br />
							<input
							type="radio"
							name="wovax_ios_push_host"
							onclick="APNS_no()"
							value="4"
							id="ChooseHost_3"
							<?php
							if (get_option("wovax_ios_push_host") == 4) {
								echo " checked";
							}
							?>
							><?php echo __('no APNS', 'wovax_translation')?>
						</td>
					</tr>
				</table>
				<div id="response_div">
					<?php
				//preload current settings
				if (get_option('wovax_ios_push_host') == 1) {
				APNS_current();
				}else if (get_option('wovax_ios_push_host') == 2) {
				APNS_external();
				}else if (get_option('wovax_ios_push_host') == 4) {
				APNS_no();
				}else {
				?>
				<p>
					<?php echo __('Something went wrong. Contact a Wovax developer.', 'wovax_translation')?>
				</p>
				<?php
				}
					?>
				</div>
			<hr>
		</section>
		<section id="wovax-user-settings" hidden>
			<h3>User Settings</h3>
			<table class='form-table'>
			<!--User Registration -->
			<tr>
				<th scope='row'>
					<label>User Accounts</label>
				</th>
				<td>
					<select id="wovax_user_accounts" name="wovax_user_accounts">
						<option <?php echo $wx_functions->selected_option(get_option("wovax_user_accounts"),"YES");?> value="YES">YES</option>
						<option <?php echo $wx_functions->selected_option(get_option("wovax_user_accounts"),"NO");?> value="NO">NO</option>
					</select>
					<p class="description">Allow app users to log in to their accounts in the app?</p>
				</td>
			</tr>
			<tr id="wovax_allow_registration" <?php if (get_option("wovax_user_accounts") === "NO") { echo 'hidden'; } ?>>
				<th scope='row'>
					<label>Allow Registration</label>
				</th>
				<td>
					<select id="wovax_user_registration" name="wovax_user_registration">
						<option <?php echo $wx_functions->selected_option(get_option("wovax_user_registration"),"YES");?> value="YES">YES</option>
						<option <?php echo $wx_functions->selected_option(get_option("wovax_user_registration"),"NO");?> value="NO">NO</option>
					</select>
					<p class="description">Allow app users to create an account in your app?</p>
				</td>
			</tr>
			<tr id="wovax_allow_guests" <?php if (get_option("wovax_user_accounts") === "NO") { echo 'hidden'; } ?>>
				<th scope='row'>
					<label>Allow Guests</label>
				</th>
				<td>
					<select id="wovax_user_accounts_guests" name="wovax_user_accounts_guests" >
						<option <?php echo $wx_functions->selected_option(get_option("wovax_user_accounts_guests"),"YES");?> value="YES">YES</option>
						<option <?php echo $wx_functions->selected_option(get_option("wovax_user_accounts_guests"),"NO");?> value="NO">NO</option>
					</select>
					<p class="description">Allow people to use the app without logging in?</p>
				</td>
			</tr>
			<tr id="wovax_register_launch" <?php if (get_option("wovax_user_accounts") === "NO") { echo 'hidden'; } ?>>
				<th scope='row'>
					<label>Show on Launch</label>
				</th>
				<td>
					<select id="wovax_user_accounts_launch_screen" name="wovax_user_accounts_launch_screen">
						<option <?php echo $wx_functions->selected_option(get_option("wovax_user_accounts_launch_screen"),"YES");?> value="YES">YES</option>
						<option <?php echo $wx_functions->selected_option(get_option("wovax_user_accounts_launch_screen"),"NO"); if (get_option("wovax_user_accounts_guests") == 'NO') { echo " disabled"; } ?> value="NO">NO</option>
					</select>
					<p class="description">Show the login/registration page to users when opening the app?</p>
				</td>
			</tr>
			<tr id="wovax_enable_favorites" <?php if (get_option("wovax_user_accounts") === "NO") { echo 'hidden'; } ?>>
				<th scope='row'>
					<label>Enable Favorites in App</label>
				</th>
				<td>
					<select id="wovax_favorites" name="wovax_favorites">
						<option <?php echo $wx_functions->selected_option(get_option("wovax_favorites"),"YES");?> value="YES">YES</option>
						<option <?php echo $wx_functions->selected_option(get_option("wovax_favorites"),"NO");?> value="NO">NO</option>
					</select>
					<p class="description">Allow app users to favorite posts in your app to look at later?</p>
				</td>
			</tr>
			</table>
		</section>
		<section id="wovax-ad-settings" hidden>
			<h3>Ad Settings</h3>
			<table class="form-table">
				<tr>
					<th scope="row">
						<label>Ad Display</label>
					</th>
					<td>
						<select id="wovax_ad_option" name="wovax_ad_option">
							<option <?php echo $wx_functions->selected_option(get_option("wovax_ad_option"),"no_ad");?> value="no_ad">No Ads</option>
							<option <?php echo $wx_functions->selected_option(get_option("wovax_ad_option"),"banner_image");?> value="banner_image">Banner Image</option>
							<option <?php echo $wx_functions->selected_option(get_option("wovax_ad_option"),"html_upload");?> value="html_upload">HTML Upload</option>
						</select>
					</td>
				</tr>
			</table>
			<?php
			if ( get_option("wovax_ad_option") == "no_ad" ) {
				$no_ad = '';
				$banner = ' hidden';
				$html_up = ' hidden';
			} else if ( get_option("wovax_ad_option") == "banner_image" ) {
				$no_ad = ' hidden';
				$banner = '';
				$html_up = ' hidden';
			} else if ( get_option("wovax_ad_option") == "html_upload" ) {
				$no_ad = ' hidden';
				$banner = ' hidden';
				$html_up = '';
			}
			?>
			<div id="wovax_noad_div" <?php echo $no_ad; ?>>
				<table style="width:100%; padding: 0;" class='form-table'>
					<tr>
						<th scope='row'>
						<h3 class='title' style='margin-top:30px'><?php echo __('No Advertisements', 'wovax_translation')?></h3>
						</th>
     				</tr>
					<tr>
						<td style='padding:0px'>
							<p><?php echo __('No advertisements will be displayed.', 'wovax_translation')?></p>
						</td>
					</tr>
				</table>
			</div>
			<div id="wovax_banner_div" <?php echo $banner; ?>>
				<table style="width:100%" class='form-table'>
					<tr>
						<td style='padding:0px'>
						<h3 class='title' style='margin-top:30px'><?php echo __('Phone banner image', 'wovax_translation')?></h3>
						</td>
					</tr>
					<tr>
						<th scope="row">
						<b><?php echo __('Image URL:', 'wovax_translation')?></b>
						</th>
					<?php
						if (strlen(get_option("wovax_phone_portrait_banner")) >= 80 ) {
						$input_field_size = 80;
						}else{
						$input_field_size = strlen(get_option("wovax_phone_portrait_banner"));
						}
					?>
						<td style='padding:0px'>
						<input
							type='text'
							name='wovax_phone_portrait_banner'
							id='wovax_phone_portrait_banner'
							class='wx-image-url'
							value=<?php echo "'" . get_option("wovax_phone_portrait_banner") . "'"; ?>
							size=<?php echo "'" . $input_field_size . "'"; ?>
						>
						<input type='button' class='wx-btn-upload button-secondary' value='Upload PNG Image'>
						</td>
					</tr>
					<tr>
						<th scope='row'>
						<b><?php echo __('Image Preview:', 'wovax_translation')?></b>
						</th>
						<td style='padding:0px'>
						<?php
							$data = get_option("wovax_phone_portrait_banner");
							$data_type = strtolower(end(explode(".",$data)));
							if ($data_type != "png") {
							echo "Image MUST be in 'png' format";
							}elseif (@!is_array(getimagesize($data))) {
							echo 'Data is not an image';
							}else{
							list($width, $height) = getimagesize($data);
							if ($width != 1242 && $height != 194) {
								echo __('Your image is not the right size.', 'wovax_translation') . "<br>" . __('Although this will work, the best size would be 1242x194px.', 'wovax_translation') . "<br><br>";
							}
							echo '<img style="width:621px; height: 97px; border: 0;" src=' . $data . '>';
							}
						?>
						<p><?php echo __('Banner should be 194px high by 1242px wide.', 'wovax_translation')?></p>
						</td>
					</tr>
					<tr>
						<th scope='row' style='padding:0px'>
						<h3 class='title' style='margin-top:30px'><?php echo __('Banner link', 'wovax_translation')?></h3>
						</th>
					</tr>
					<tr>
						<td style='padding:0px'>
						<input
							type='text'
							name='wovax_link_banner'
							id='wovax_link_banner'
							value=<?php echo "'" . get_option("wovax_link_banner") . "'"; ?>
						>
						<p><?php echo __('Ad Link', 'wovax_translation')?> i.e: https://www.wovax.com/</p>
						</td>
					</tr>
    			</table>
			</div>
			<div id="wovax_htmlup_div" <?php echo $html_up; ?>>
				<table style="width:100%; padding: 0;" class='form-table'>
					<tr>
						<th scope='row'>
						<h3 class='title' style='margin-top:30px'><?php echo __('Custom Ad Code', 'wovax_translation')?></h3>
						</th>
					</tr>
					<tr>
						<td style='padding:0px'>
						<p><?php echo __('Enter your 2 part ad code in the boxes below.', 'wovax_translation')?></p>
						</td>
					</tr>
					<tr>
						<td style='padding:0px'>
						<?php echo __('HTML:', 'wovax_translation')?><br>
						</td>
					</tr>
					<tr>
						<td style='padding:0px'>
						<p><textarea rows='4' cols='50' id='html_snippet' name='wovax_html_custom_ad'><?php echo get_option("wovax_html_custom_ad"); ?></textarea></p>
						</td>
					</tr>
					<tr>
						<td style='padding:0px'>
						<p><?php echo __('JavaScript:', 'wovax_translation')?><br></p>
						</td>
					</tr>
					<tr>
						<td style='padding:0px'>
						<textarea rows='4' cols='50' id='javascript_snippet' name='wovax_js_custom_ad'><?php echo get_option("wovax_js_custom_ad"); ?></textarea>
						</td>
					</tr>
   	 			</table>
			</div>
		</section>
		<section id="wovax_excluded_categories" hidden>
			<h3>Check to exclude</h3>
			<p>Check the boxes next to the categories you would like excluded from the app. Posts in the selected categories will show up on the web, but not in the app.</p>
			<?php
			// Gets excluded catagories
			$currently_excluded_sql = "SELECT option_value FROM " . $wpdb->prefix . "options WHERE option_name='wovax_excluded_categories';";
			$excluded_sql = $wpdb->get_results($currently_excluded_sql,ARRAY_N);
			$excluded_array = explode(",", $excluded_sql[0][0]);
		
			// Gets all catagories
			$category_sql = "SELECT term_id FROM " . $wpdb->prefix . "term_taxonomy WHERE taxonomy='category';";
			$category_id = $wpdb->get_results($category_sql,ARRAY_N);
			foreach($category_id as $value) {
				// Gets names of catagories
				$name_sql = "SELECT name FROM " . $wpdb->prefix . "terms WHERE term_id='" . $value[0] . "';";
				$name = $wpdb->get_results($name_sql,ARRAY_N);
				if ($name[0][0] != "Uncategorized") {
				?>
				<input
				type="checkbox"
				name="wovax_excluded_categories[]"
				value=<?php echo "'" . $value[0] . "'"; ?>
				<?php
				if (in_array($value[0], $excluded_array)) {
					echo "checked=''";
				}
				?>
				><?php echo $name[0][0]; ?>
				<?php
				print "<br>";
				}
			}
			?>
		</section>
	</div>
	<br><br><br>
	<input
	type="submit" value="<?php echo __('Save', 'wovax_translation')?>" name="save"
	class="button button-primary"
	/>
	</form>
</div>
