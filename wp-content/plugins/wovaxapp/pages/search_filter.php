<?php
	global $wpdb;

	if ($_POST['add'] != "") {
		if ($_POST['wx-search-filter-content'] != "") {
			$custom_field = "search__content";
		}else {
			$custom_field = $_POST['wx_custom_fields'];
		}
		$values = array(
			'in_app'		=>	"true",
			'item_order'	=>	0,
			'post_type'		=> 	$_POST['wx_post_type_select'],
			'custom_field'	=> 	$custom_field,
			'display_name'	=> 	stripslashes($_POST['wx_display_text']),
			'default_value' => 	stripslashes($_POST['wx_default_value']),
			'type'			=>	$_POST['wx_type']
		);
		$wpdb->insert(
			$wpdb->prefix.'wovaxapp_search_filter',
			$values
		);
	}

	if ($_POST['reset'] != "") {
		update_option('wovax_filter_names','');
	}
	$posts_per_page = get_option('wovax_search_posts_per_page', 9);
?>
<div class='wrap'>
	<h2>Search Setup</h2>
	<h2 class="wovax-nav-wrapper nav-tab-wrapper">
		<a class="nav-tab nav-tab-active" href="#">Search Filters</a>
		<a class="nav-tab" href="#">Sort Settings</a>
        <a class="nav-tab" href="#">Search Range Builder</a>
	</h2>
	<?php
		if (get_option('wovax_filter_names') != "") {
			$hidden = "";
			$disabled = " disabled ";
		}else {
			$hidden = " hidden ";
			$disabled = "";
		}
	?>
	<div id="wovax-search-sections">
		<section id="wovax-search-filters">
			<form action="" method="POST">
				<table class='form-table'>
					<tr>
						<th>Search Name</th>
						<td>
							<input class='wx-search-name' name="search_name" type='text' value='<?php $val = json_decode(get_option('wovax_filter_names'));echo $val[0]->name;?>'>
						</td>
					</tr>
					<tr>
						<th>
							Post Type to Search
						</th>
						<td>
							<select class="wx-post-type-select" name="wx_post_type_select" <?php echo $disabled; ?>>
								<option value="select">Select...</option>
								<?php
									$wx_post_types_select = get_post_types();
									$filters = json_decode(get_option('wovax_filter_names'));
									foreach ($wx_post_types_select as $k=>$v) {
										if ($filters[0]->type != $v) {
											$selected = "";
										}else {
											$selected = " selected ";
										}
										echo "<option".$selected." value='".$v."'>".ucwords(str_replace("_"," ",$v))."</option>";
									}
									unset($wx_post_types_select);
								?>
							</select>
						</td>
					</tr>
					<tr>
						<th>Number of Results per Page</th>
						<td><input class="wx-search-results-per-page" type="number" value="<?php echo $posts_per_page;?>" /></td>
					</tr>
					<tr>
						<th>
							
						</th>
						<td>
							<form action="" method="post" >
								<input
									type="submit"
									value="<?php echo __('Save', 'wovax_translation')?>"
									name="save"
									class="wx-search-filter-save button button-primary"
								/>
								<input
								type="submit" value="<?php echo __('Reset to Default', 'wovax_translation')?>" name="reset"
								class="button button-secondary"
								/>
							</form>
						</td>
					</tr>
				</table>
			<hr />
		
			<div id='wx-hidden-search-filter'<?php echo $hidden;?>>
				<table class='form-table'>
					<tr id="wx-custom-field-row">
						<th>
							Custom Field
						</th>
						<td>
							<select name="wx_custom_fields">
								<option value="select">Select Custom Field</option>
								<?php
									$post_type = json_decode(get_option('wovax_filter_names'));
									$args = array(
									'post_type' => $post_type[0]->type,
									);
									$posts = get_posts($args);
									$custom_fields = array();
									for ($i=0;$i<sizeof($posts);$i++) {
									$id = $posts[$i]->ID;
									$meta = get_post_meta($id);
									foreach ($meta as $k=>$v) {
										$custom_fields[] = $k;
									}
									}
									$custom_fields = array_values(array_unique($custom_fields));
									for ($i=0;$i<sizeof($custom_fields);$i++) {
									echo "<option value='".$custom_fields[$i]."'>".$custom_fields[$i]."</option>";
									}
								?>
							</select>
							<br>
							<br>
							<input type="checkbox" name="wx-search-filter-content" value="Content" id="wx-search-filter-content">
							<small>Search the Content instead</small>
						</td>
					</tr>
					<tr>
						<th>
							Search Type
						</th>
						<td>
							<select name="wx_type">
								<option value="existing">Existing Value Select</option>
								<option value="binary">Binary</option>
								<option value="numeric">Numeric Search</option>
								<option value="numeric_min">Numeric Minimum</option>
								<option value="numeric_max">Numeric Maximum</option>
								<option value="text">Text Search</option>
                                <option value="range">Custom Range</option>
							</select>
						</td>
					</tr>
					<tr>
						<th>
							Label
						</th>
						<td>
							<input type='text' name="wx_display_text">
						</td>
					</tr>
					<tr>
						<th>
							Placeholder Text
						</th>
						<td>
							<input type='text' name="wx_default_value">
						</td>
					</tr>
					<tr>
						<th>
							
						</th>
						<td>
							<p>
								<input type='submit' class='button button-primary wx-add-search-filter' name='add' value='Add Search Filter'>
							</p>
						</td>
					</tr>
				</table>
			<b><small>Existing Search Filters</small></b>
			<br />
			<?php
				$sql = "SELECT * FROM " . $wpdb->prefix . "wovaxapp_search_filter ORDER BY item_order ASC";
				$filters = $wpdb->get_results($sql,ARRAY_A);
		
				if (sizeof($filters) != 0) {
					$table = '';
					$empty = ' hidden';
				} else {
					$table = ' hidden';
					$empty = '';
				}
					?>
				<div class='wovax-search-filter-empty' <?php echo $empty; ?>>You currently have no filters added.</div>
				<table id="wx-search-filter-table" class='widefat' <?php echo $table; ?>>
					<thead>
						<tr scope='row' class='alternate'>
							<th class="manage-column">ID</th>
							<th class="manage-column">In App</th>
							<th class="manage-column">Custom Field</th>
							<th class="manage-column">Label</th>
							<th class="manage-column">Placeholder Text</th>
							<th class="manage-column">Type</th>
                            <th class="manage-column">Edit</th>
							<th class="manage-column" style="text-align:right">Delete</th>
						</tr>
					</thead>
					<tbody class='wx-search-filter-body'>
						<?php
							for ($i=0;$i<sizeof($filters);$i++) {
								if (($i % 2) == 1) {
									$class = "alternate";
								}else {
									$class = "";
								}
		
								if ($filters[$i]['in_app'] === "true") {
									$checked = " checked ";
								}else {
									$checked = "";
								}
		
								?>
								<tr class='wx-search-filter-row <?php echo $class; ?>' height="30px">
									<td class='wx-search-filter-id'><?php echo $filters[$i]['no'];?></td>
									<td><input class='wx-search-filter-display-checkbox' type='checkbox' name='display' value='Display' <?php echo $checked;?>></td>
									<td><?php echo $filters[$i]['custom_field'];?></td>
									<td class="wx-search-filter-name"><?php echo stripslashes($filters[$i]['display_name']);?></td>
									<td class="wx-search-filter-placeholder"><?php echo stripslashes($filters[$i]['default_value']);?></td>
									<td><?php echo $filters[$i]['type'];?></td>
                                    <td><a class="wx-search-filter-edit" href='#'>Edit</a></td>
									<td class="wx-search-filter-delete" style="text-align:right"><a href='#'>Delete</a></td>
								</tr>
								<?php
							}
						?>
					</tbody>
				</table>
			</div>
			</form>
		</section>
		<section id="wovax-sort-setup" hidden>
			<p class="wx-admin-page-details">Once your users have performed a search, they frequently want to be able to sort the results 
				as well. The default way of sorting search results is descending by date, that is, newest posts 
				to oldest posts. By default, you can also choose to sort by date ascending, that is, oldest 
				posts to newest posts. This page allows you to add further sorting by custom fields. When adding a field,
				both ascending and descending sort types are created and added to the sort dropdown on search pages. We do
				not recommend adding more than two additional sort types, as this can cause the sort dropdown to become difficult for
				the end user to use.
			</p>
			<form action="" method="POST">
				<table class="form-table">
					<tr>
						<th>Custom Field to Sort By</th>
						<td>
							<select name="wx_custom_field_sort">
								<option value="select">Select Custom Field</option>
								<?php
									$post_type = json_decode(get_option('wovax_filter_names'));
									$args = array(
									'post_type' => $post_type[0]->type,
									);
									$posts = get_posts($args);
									$custom_fields = array();
									for ($i=0;$i<sizeof($posts);$i++) {
									$id = $posts[$i]->ID;
									$meta = get_post_meta($id);
										foreach ($meta as $k=>$v) {
											$custom_fields[] = $k;
										}
									}
									$custom_fields = array_values(array_unique($custom_fields));
									for ($i=0;$i<sizeof($custom_fields);$i++) {
									echo "<option value='".$custom_fields[$i]."'>".$custom_fields[$i]."</option>";
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<th>Sort Type</th>
						<td>
							<select name="wx-sort-type-selector">
								<option value="alphabetical">Alphabetical (A to Z, Z to A)</option>
								<option value="numeric">Numeric (High to Low, Low to High)</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							<input type="submit" class='button button-primary wx-add-search-sort' value="Add Sort Type">
						</td>
					</tr>
				</table>
			</form>
			<hr>
			<small>Existing Sort Options</small>
			<div class="wx-existing-sort-display">
				<table id="wovax-sort-table" class="widefat">
					<thead>
						<tr scope='row' class='alternate'>
							<th class="manage-column">Custom Field</th>
							<th class="manage-column">Display Name</th>
							<th class="manage-column">Select Value</th>
							<th class="manage-column">Direction</th>
							<th class="manage-column">Type</th>
							<th class="manage-column" style="text-align:right">Edit Display Name</th>
							<th class="manage-column" style="text-align:right">Delete</th>
						</tr>
					</thead>
					<tbody class='wx-search-sort-table-body'>
						<?php
						$sort_options = get_option('wovax_sort_fields');
						$date_desc = array(
							"display_name" => "Date - Newest to Oldest",
							"value" => "date-desc"
						);
						$date_asc = array(
							"display_name" => "Date - Oldest to Newest",
							"value" => "date-asc"
						);
						$asc = 0;
						$desc = 0;
						foreach($sort_options as $sort){
							if($sort['value'] === 'date-desc'){
								$desc++;
							}
							if($sort['value'] === 'date-asc'){
								$asc++;
							}
						}
						if( $desc === 0 ) {
							$sort_options[] = $date_desc;
							update_option('wovax_sort_fields', $sort_options);
						}
						if( $asc === 0 ) {
							$sort_options[] = $date_asc;
							update_option('wovax_sort_fields', $sort_options);
						}
						foreach($sort_options as $key => $values) :
							if(($key % 2) == 1) {
								$alternate = ' alternate';
							} else {
								$alternate = '';
							}
							$sort_data = explode('-', $values['value']);
							if($sort_data[0] == 'date'){
								$field = "N/A";
								$direction = $sort_data[1];
								$sort_type = "numeric";
								$delete = "";
							} else {
								$field = $sort_data[0];
								$direction = $sort_data[2];
								$sort_type = $sort_data[1];
								$delete = "<a href='#'>Delete</a>";
							}
						?>
						<tr class='wx-search-sort-row <?php echo $alternate; ?>' height="30px">
									<td class="wx-search-sort-field"><?php echo $field; ?></td>
									<td class="wx-search-sort-name"><?php echo $values['display_name']; ?></td>
									<td class="wx-search-sort-value"><?php echo $values['value']; ?></td>
									<td class="wx-search-sort-direction"><?php echo $direction; ?></td>
									<td class="wx-search-sort-type"><?php echo $sort_type; ?></td>
									<td style="text-align:right"><a class="wx-search-sort-edit" href='#'>Edit</a></td>
									<td class="wx-search-sort-delete" style="text-align:right"><?php echo $delete; ?></td>
						</tr>
						<?php
							endforeach;
						?>
					</tbody>
				</table>
			</div>
			<hr>
			<div id="wovax-default-sort-div">
				<table class="form-table">
					<tr>
						<th>Set Default Search Sort</th>
						<td>
							<select id="wovax_default_sort_select" name="wx_default_search_sort">
								<?php
								$default_sort = get_option('wovax_default_search_sort');
								if(!$default_sort){
									$default_sort = 'date-desc';
									update_option('wovax_default_search_sort', $default_sort);
								}
								foreach($sort_options as $key => $values){
									if($values['value'] == $default_sort){
										$selected = ' selected';
									} else {
										$selected = '';
									}
									echo "<option value='".$values['value']."'".$selected.">".$values['display_name']."</option>";
								}
								?>
							</select>
						</td>
					</tr>
				</table>
			</div>
		</section>
        <section id="wovax-range-setup" hidden>
            <p class="wx-admin-page-details">Once you have created a search filter with the type of Custom Range, you'll need to populate its options.
                This page allows you to add custom range sets for users to select and search by.
            </p>
            <div class="wx-range-selection-div">
                <table class="form-table">
                    <tr>
                        <th>Custom Range</th>
                        <td>
                            <select class="wx-range-selector">
                                <?php
                                    $wx_range_list = array(); 
                                    foreach($filters as $filter) {
                                        if($filter['type'] === 'range') {
                                            $wx_range_list[] = $filter;
                                        }
                                    }
                                    if(!empty($wx_range_list)) {
                                        ?>
                                        <option value=''>Ranges</option>
                                        <?php 
                                        foreach($wx_range_list as $range){
                                            ?>
                                            <option value="<?php echo $range['no']; ?>"><?php echo $range['display_name']; ?></option>
                                            <?php 
                                        }
                                    } else {
                                        echo "<option value=''>No Ranges Found</option>";
                                    }
                                ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            <hr />
            <div class="wx-range-creation-div" hidden>
                <table class="form-table">
                    <tr>
                        <th>From</th>
                        <td><input class="wx-range-low-input" type="numeric" pattern="[0-9]*" value=""></td>
                    </tr>
                    <tr>
                        <th>To</th>
                        <td><input class="wx-range-high-input" type="numeric" pattern="[0-9]*" value=""></td>
                    </tr>
                    <tr>
                        <th>Currency?</th>
                        <td><input class="wx-range-currency-checkbox" type="checkbox"></td>
                    </tr>                        
                </table>
                <a class="button button-primary" id="wovax-add-new-range-row" href="#">Add Range</a>            
            </div>
            <hr />
            <div class="wx-range-settings-div" hidden>
                <table id="wovax-range-table" class="widefat">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Display Name</th>
                            <th>Value</th>
                            <th>Edit</th>
                            <th>Delete</th>     
                        </tr>
                    </thead>
                    <tbody id="wovax-range-table-body">
                        
                    </tbody>
                </table>
            </div>
        </section>
	</div>
</div>
