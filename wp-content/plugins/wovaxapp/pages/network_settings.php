<?php
  global $wpdb;
  if ($_POST['save'] != "") {
    $wx_functions = new wovax_functions();
    $wx_functions->save_data($_POST);
  }

  $elements = array(
    array(
      "name" => "Basic Network Settings",
      "db_name" => null,
      "description" => null,
      "type" => "heading",
      "disabled" => false,
    ),
    array(
      "name" => "Enable An App Network",
      "db_name" => "network_enabled",
      "description" => "Enables the networking feature.",
      "type" => "Y/N",
      "disabled" => false,
    )
  );

?>
<div class="wrap">
  <h2>Wovax Network Settings</h2>

  <form action="" method="post">
    <table class="form-table">
      <?php
        $wx_functions = new wovax_functions();
        $wx_functions->table_generator($elements);
      ?>
    </table>
    <input class='button button-primary' type='submit' value='Save' name='save'>

    <br><br>
    <input type="text" id="wx-network-nicename" name="wx-network-nicename"><br>
    <input type="text" id="wx-network-url" name="new_site_url" placeholder="Site Address" value=""><br>
    <input type="text" id="wx-network-api-key" name="new_site_api_key" placeholder="Site API Key" value=""><br>
    <b><input type="submit" id="wx-probe-button" class="button button-primary" name="probe_site" value="Probe Site"></b>
    <br />
    <br />
    <div class='wovax-add-network' hidden>
      <p class='wovax-network-results'>
      </p>
      <input type="submit" id="wx-add-site-button" class="button button-primary" name="add_site" value="Add Site">
    </div>
    <br />
    <?php
      $sites = get_option("wovax_network_sites");
      if (empty($sites)) {
        $empty = '';
        $table = ' hidden';
      }else {
        $empty = ' hidden';
        $table = '';
      }
    ?>
    <strong>Linked Sites:</strong>
    <div class='wovax-network-empty-sites'<?php echo $empty ?> >You currently have no other sites in your network.</div>
    <table id='wx-net-table' class='widefat' cellspacing="0" <?php echo $table; ?> >
      <thead>
        <tr height="30px" class="alternate">
          <th class="manage-column">ID</th>
          <th class="manage-column">Name</th>
          <th class="manage-column">URL</th>
          <th class="manage-column">API Key</th>
          <th class="manage-column" style="text-align:right"><?php echo __('Delete','wovax_translation');?></th>
        </tr>
      </thead>
      <tbody class='wovax-net-body'>
        <?php
          for($i=0;$i<sizeof($sites);$i++) {
            if (($i % 2) == 1) {
              $class = "alternate";
            } else {
              $class = "";
            }
            ?>
            <tr class='wx-net-row <?php echo $class; ?>' height="30px">
              <td class='wx-net-id'><?php echo $sites[$i]->id; ?></td>
              <td class='wx-net-name'><?php echo $sites[$i]->name; ?></td>
              <td class='wx-net-url'><?php echo $sites[$i]->url; ?></td>
              <td class='wx-api-key'><?php echo $sites[$i]->key; ?></td>
              <td class="wx-net-delete" style="text-align:right"><a href="#">Delete</a></td>
            </tr>
            <?php
          }
        ?>
      </tbody>
    </table>
  </form>
</div>
