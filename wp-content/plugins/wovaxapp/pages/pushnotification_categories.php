<?php
  global $wpdb;

  if ($_POST['add_category'] != "") {
    $categories = json_decode(get_option('wovax_pushnotification_categories'));
    $categories[] = array(
      'id' => time(),
      'name' => $_POST['add_category']
    );
    update_option('wovax_pushnotification_categories',json_encode($categories));
  }
?>

<div class="wrap">
  <h2><?php echo __('Push Notification Categories', 'wovax_translation')?></h2>
  <p><?php echo __('Use this page to create and update categories users may subscribe to.', 'wovax_translation')?></p>

  <form action="" method="POST">
    <b><input type="submit" class="button button-primary" name="save" value="Add Category"></b>
    <input type="text" placeholder="Category Name" name="add_category">
    <br><br><br><br>

    <b>Existing Categories:</b>
    <?php
      $category = get_option('wovax_pushnotification_categories');
      $category = json_decode($category);
      if (empty($category)) {
        echo "<p>You currently have no categories for your subscribers to subscribe to.</p>";
      }else {
        ?>
        <table id='wx-cat-table' class='widefat' cellspacing="0">
          <thead>
            <tr height="30px" class='alternate'>
              <th class="manage-column">Order</th>
              <th class="manage-column"><?php echo __('Name','wovax_translation');?></th>
              <th class="manage-column" style="text-align:right"><?php echo __('Delete','wovax_translation');?></th>
            </tr>
          </thead>
          <tbody class='wx-cat-body'>
            <?php
              for($i=0;$i<sizeof($category);$i++) {
                if (($i % 2) == 1) {
				          $class = "alternate";
				        } else {
				          $class = "";
				        }
                ?>
                <tr class='wx-cat-row <?php echo $class; ?>' height="30px">
                  <td class='wx-cat-order'><?php echo $i+1; ?></td>
                  <td class='wx-cat-name'><?php echo $category[$i]->name; ?></td>
                  <td class="wx-cat-delete" style="text-align:right"><a href="#">Delete</a></td>
                </tr>
                <?php
              }
            ?>
          </tbody>
        </table>
        <?php
      }
    ?>
  </form>
</div>
