<?php
global $post;
$current_template = get_post_meta($post->ID, 'wovaxListViewTemplate', TRUE);
$location = get_post_meta($post->ID, 'postMapEnable', TRUE);
$gallery = get_post_meta($post->ID, 'postGalleryEnable', TRUE);
$hide = get_post_meta($post->ID, 'postHiddenEnable', TRUE);
$webview = get_post_meta($post->ID, 'postWebViewEnable', TRUE); ?>

  <div>
    <label for="chooseWovaxAppTemplate">Post Template: </label>
    <select name="chooseWovaxAppTemplate" id="chooseWovaxAppTemplate">
      <option value="WT-TitleImageText" <?php if ($current_template == "WT-TitleImageText") { echo " selected "; } ?> >Title Image Text</option>
      <option value="WT-ImageTitle" <?php if ($current_template == "WT-ImageTitle") { echo " selected "; } ?>>Image Title Overlay</option>
      <option value="WT-RealEstate" <?php if ($current_template == "WT-RealEstate") { echo " selected "; } ?>>Real Estate</option>
    </select>
  </div>

  <div style="margin-top:10px;">
    <label for="addGallery">Enable Gallery in App: </label>
    <select name="addGallery" id="addGallery">
      <option value="0" <?php if ($gallery == "0") { echo " selected "; } ?>>No</option>
      <option value="1" <?php if ($gallery == "1") { echo " selected "; } ?>>Yes</option>
    </select>
  </div>

  <div style="margin-top:10px;">
    <label for="hidePost">Hide Post from App: </label>
    <select name="hidePost" id="hidePost">
      <option value="0" <?php if ($hide == "0") { echo " selected"; } ?>>No</option>
      <option value="1" <?php if ($hide == "1") { echo " selected"; } ?>>Yes</option>
    </select>
  </div>

  <div style="margin-top:10px;">
    <label for="openWebView">Open in Web View: </label>
    <select name="openWebView" id="openWebView">
      <option value="0" <?php if ($webview == "0") { echo " selected"; } ?>>No</option>
      <option value="1" <?php if ($webview == "1") { echo " selected"; } ?>>Yes</option>
    </select>
  </div>

        
                     