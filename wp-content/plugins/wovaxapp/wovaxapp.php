<?php
/*
	Plugin Name: Wovax App
	Plugin URI: https://www.wovax.com/
	Description: Plugin to support your Wovax apps. Deleting this plugin will stop your Wovax apps from functioning. Any unauthorized use, distribution, copying of this plugin is strictly prohibited by law unless granted permission in writing by Wovax, LLC.
	Version: 2.6.0
	Author: Wovax, LLC.
	Author URI: https://www.wovax.com/
*/

 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! defined( 'WOVAX_APP_VERSION' ) ) {
	define( 'WOVAX_APP_VERSION', "2.1.1");
}
if ( ! defined( 'WOVAX_APP_URL' ) ) {
	define( 'WOVAX_APP_URL', plugins_url()."/wovaxapp");
}
if ( ! defined( 'WOVAX_APP_DIR' ) ) {
	define( 'WOVAX_APP_DIR', plugin_dir_path(__FILE__));
}
if ( ! defined( 'WX_CUSTOM_TEMPLATE_PATH' ) ) {
	/** @var string WX_CUSTOM_TEMPLATE_PATH */
	define( 'WX_CUSTOM_TEMPLATE_PATH', WP_CONTENT_DIR . '/wovax_templates' );
}
if ( ! defined( 'WX_CUSTOM_TEMPLATE_URL' ) ) {
	/** @var string WX_CUSTOM_TEMPLATE_URL */
	define( 'WX_CUSTOM_TEMPLATE_URL', content_url() . '/wovax_templates' );
}

/**
 * WooCommerce API Manager Integration
 */
 /*
if ( ! defined( 'WOVAX_UPGRADE_URL' ) ) {
	define( 'WOVAX_UPGRADE_URL', 'https://wovax.com/' );
}

if ( ! defined( 'WOVAX_RENEW_LICENSE_URL' ) ) {
	define( 'WOVAX_RENEW_LICENSE_URL', 'https://wovax.com/my-account/' );
}

if ( ! defined( 'WOVAX_SETTINGS_MENU_TITLE' ) ) {
	define( 'WOVAX_SETTINGS_MENU_TITLE', 'WovaxApp License Activation' );
}

if ( ! defined( 'WOVAX_SETTINGS_TITLE' ) ) {
	define( 'WOVAX_SETTINGS_TITLE', 'WovaxApp Activation' );
}

if ( ! defined( 'WOVAX_TEXT_DOMAIN' ) ) {
	define( 'WOVAX_TEXT_DOMAIN', 'wovax_translation' );
}

if ( ! defined( 'WOVAX_FILE' ) ) {
	define( 'WOVAX_FILE', __FILE__ );
}
*/

require WOVAX_APP_DIR.'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = PucFactory::buildUpdateChecker(
    'http://wovax-plugin-releases.s3.amazonaws.com/wovaxapp/metadata.json',
    __FILE__
);

load_plugin_textdomain('wovax_translation', false, dirname(plugin_basename(__FILE__)) . '/languages/');

//require_once( 'am/am.php' );

include_once WOVAX_APP_DIR."/library/wovaxapp_functions.php";

include_once "gallery-metabox.php";

include_once WOVAX_APP_DIR."/shortcodes/wovax_search.php";
include_once WOVAX_APP_DIR."/shortcodes/wovax_recent.php";
include_once WOVAX_APP_DIR."/shortcodes/wovax_search_results.php";
include_once WOVAX_APP_DIR."/shortcodes/wovax_saved_search.php";

add_action('init','wovaxapp_init');
function wovaxapp_init() {
	require_once WOVAX_APP_DIR."/library/wovaxapp_init.php";
}

/* inserts CSS into header */
add_action('admin_enqueue_scripts', 'wovax_admin_register_css');
function wovax_admin_register_css() {
	wp_register_style('wovax', plugin_dir_url(__FILE__) . '/assets/css/wovax.css');
	wp_enqueue_style('wovax');
	if(isset($_GET['page']) && ($_GET['page'] == 'wovaxDesign' || $_GET['page'] == 'wovaxAdvancedMenu' || $_GET['page'] == 'retsSettings')){
		wp_enqueue_media();
	}
}
/* Add Meta Box In Post */
add_action('admin_menu', 'wovaxAppPostMetaBox');
/* Adds menu pages */
add_action('admin_menu', 'wx_create_menu');

/* Adds Meta-Box */
add_action('save_post', 'saveWovaxAppTemplate');
/* Returns external pages to the wordpress enviornment */
add_action('template_redirect', 'wovaxapp_media_template_redirect', 1);

function wovaxapp_activation() {
	require WOVAX_APP_DIR."/library/wovaxapp_activation.php";
	wovax_cron_api_heartbeat();
	// Add the rewrite rule on activation
	global $wp_rewrite;
}

function wovax_api_deactivation() {
	require WOVAX_APP_DIR."/library/wovaxapp_deactivation.php";
}

{ // require cron file
	require WOVAX_APP_DIR."/library/wovaxapp_cron.php";
}

function wovaxapp_media_template_redirect() {
	// not a search page; don't do anything and return
	$plugin_dir = dirname(__FILE__);
	if(isset($_GET['wovaxmenu'])){
		$data = $_GET['wovaxmenu'];
		if ($data == "comment") {
			include_wordpress_template($plugin_dir . "/commentajax.php");
			die();
		} elseif ($data == "get_post_gallery") {
			include_wordpress_template($plugin_dir . "/carousel.php");
			die();
		}
	}
}

function include_wordpress_template($t) {
	global $wp_query;
	if ($wp_query->is_404) {
		$wp_query->is_404 = false;
		$wp_query->is_archive = true;
	}
	header("HTTP/1.1 200 OK");
	include($t);
}

//Add Meta Box In Post
function wovaxAppPostMetaBox() {
	$post_types = get_post_types(); //adds the metabox to all post types
	foreach( $post_types as $post_type) {
		add_meta_box(
		'WovaxApp', // Unique ID
		'Wovax App Post Settings', // Title
		'wovaxAppTemplateOption', // Callback function
		$post_type, // Admin page (or post type)
		'side', // Context
		'high' // Priority
		);
	}
	remove_meta_box('WovaxApp', 'attachment', 'side');
}

// Options Update Check
add_action('admin_init', 'wovax_check_db_version');

function wovax_check_db_version() {
	$data = get_plugin_data(WOVAX_APP_DIR.'/wovaxapp.php');
	$version = $data['Version'];
	if( !get_option('wovax_db_version') ) {
		$db_version = 0;
	} else {
		$db_version = get_option('wovax_db_version');
	}
	if ( $version != $db_version ) {
		// Add any new options included in the current plugin update.
		require WOVAX_APP_DIR."/library/wovaxapp_default_options.php";
		foreach ($options as $option => $value) {
			if( !get_option( "wovax_".$option ) ) {
				add_option("wovax_".$option,$value);
			}
		}
		// Add new tables here as needed - this should eliminate the need to deactivate/reactivate when adding new tables.
		// I'm thinking we can cycle through new tables on this section - once everyone has a feature, we can remove it.
		global $wpdb;
		$wpdb->query(
			'CREATE TABLE IF NOT EXISTS `' . $wpdb->prefix . 'wovaxapp_search_filter_dropdowns` (
			`id` int(20) NOT NULL AUTO_INCREMENT UNIQUE,
			`filter_id` bigint(20) NOT NULL,
			`item_order` int(20) NOT NULL,
			`display_name` varchar(128) NOT NULL,
			`value` varchar(128) NOT NULL,
			PRIMARY KEY (`id`),
			UNIQUE KEY `wx_filter_id_display` (`filter_id`,`display_name`),
			FOREIGN KEY (`filter_id`) REFERENCES `'. $wpdb->prefix .'wovaxapp_search_filter` (`no`) ON DELETE CASCADE
			) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;'
		);
		//Then set the db version equal to the current plugin version.
		update_option('wovax_db_version',$version);
	}
}

function wovaxAppTemplateOption($post_id) {
	require "metabox.php";
}

function addPostMetaOverwrite($pID,$meta_name,$value) {
	global $wpdb;
	$sql = "INSERT INTO ".$wpdb->prefix."postmeta(post_id,meta_key,meta_value) values('".$pID."','".$meta_name."','".$value."')";
	$wpdb->query($sql);
}

function updatePostMetaOverwrite($pID,$meta_name,$value) {
	global $wpdb;
	$sql = "UPDATE ".$wpdb->prefix."postmeta SET meta_value='".$value."' WHERE post_id='".$pID."' AND meta_key='".$meta_name."'";
	$wpdb->query($sql);
}

function checkPostMeta($pID,$meta_name) {
	global $wpdb;
	$sql = "SELECT * FROM `".$wpdb->prefix."postmeta` WHERE post_id='".$pID."' AND meta_key='".$meta_name."'";
	$results = $wpdb->get_results($sql);
	$count=0;
	foreach ($results as $result) {
		$count++;
	}
	if($count == 0) {
		return false;
	} else {
		return true;
	}
}

function saveWovaxAppTemplate($post_id) {
	$post_type = get_post_type( $post_id );
	$post_status = get_post_status( $post_id );
	//If the post meta isn't set after post creation or saving, create it with default values.
	if ( ! get_post_meta($post_id, 'wovaxListViewTemplate', true ) ) {
		add_post_meta( $post_id, 'wovaxListViewTemplate', 'WT-TitleImageText', true );
	}
	if ( ! get_post_meta($post_id, 'postGalleryEnable', true ) ) {
		add_post_meta( $post_id, 'postGalleryEnable', 0, true );
	}
	if ( ! get_post_meta($post_id, 'postHiddenEnable', true ) ) {
		add_post_meta( $post_id, 'postHiddenEnable', 0, true );
	}
	if ( ! get_post_meta($post_id, 'postWebViewEnable', true ) ) {
		add_post_meta( $post_id, 'postWebViewEnable', 0, true );
	}
	//On post save, check metabox for values and update with any changes.
	if ( isset($_POST['chooseWovaxAppTemplate'] ) ) {
		update_post_meta( $post_id, 'wovaxListViewTemplate', $_POST['chooseWovaxAppTemplate'] );
	}
	if ( isset($_POST['addGallery'] ) ) {
		update_post_meta( $post_id, 'postGalleryEnable', $_POST['addGallery'] );
	}
	if ( isset($_POST['hidePost'] ) ) {
		update_post_meta( $post_id, 'postHiddenEnable', $_POST['hidePost'] );
	}
	if ( isset($_POST['openWebView'] ) ) {
		update_post_meta( $post_id, 'postWebViewEnable', $_POST['openWebView'] );
	}
}

function wovaxAppCleanData(&$arr) {
	if (is_array($arr)) {
		foreach ($arr as $i => $v) {
			if (is_array($arr[$i])) {
				wovaxAppCleanData($arr[$i]);
				if (!count($arr[$i])) {
					unset($arr[$i]);
				}
			} else {
				if (trim($arr[$i]) == '') {
					unset($arr[$i]);
				}
			}
		}	
		if (!count($arr)) {
			$arr = NULL;
		}
	}
}

function wx_create_menu() {
	require WOVAX_APP_DIR."/library/wovaxapp_menu.php";
}

// Add initialization and activation hooks
register_activation_hook(__FILE__, 'wovaxapp_activation');
register_deactivation_hook(__FILE__, 'wovax_api_deactivation');

$BE_Gallery_Metabox = new BE_Gallery_Metabox();

//Custom menu options
include_once('custom-wp-menu-class.php');

// instantiate menu plugins class
$GLOBALS['sweet_custom_menu'] = new wovax_custom_menu();
$GLOBALS['sweet_custom_backgroundcolor'] = new wovax_app_thumbnail_background_color();
$GLOBALS['sweet_custom_ptype'] = new wovax_custom_page_type();
$GLOBALS['sweet_custom_oneclickcall'] = new wovax_app_oneclick_call();
$GLOBALS['sweet_custom_oneclickmail'] = new wovax_app_oneclick_mail();
$GLOBALS['sweet_custom_companyname'] = new wovax_app_company_name();
$GLOBALS['sweet_custom_companyaddress'] = new wovax_app_company_address();
$GLOBALS['sweet_custom_companylatlong'] = new wovax_app_company_latlong();
$GLOBALS['sweet_custom_company_phonenumber'] = new wovax_app_company_phonenumber();
$GLOBALS['sweet_custom_company_emailaddress'] = new wovax_app_company_emailaddress();

include_once('edit_custom_walker.php');
include_once('custom_walker.php');

/* Wovax dashboard stats widget. */
add_action('wp_dashboard_setup', 'wovax_stats_dashboard_widget' );
function wovax_stats_dashboard_widget() {
	require WOVAX_APP_DIR."/widgets/stats_dashboard_widget.php";
}

/* Wovax dashboard news widget. */
add_action( 'wp_dashboard_setup', 'wovax_blog_dashboard_widget' );
function wovax_blog_dashboard_widget() {
	require WOVAX_APP_DIR."/widgets/blog_dashboard_widget.php";
}

//Adding the Wovax Widgets
add_action('widgets_init', 'wovax_register_custom_widgets');

function wovax_register_custom_widgets() {
	require WOVAX_APP_DIR."/widgets/recent_posts_widget.php";
	require WOVAX_APP_DIR."/widgets/search_form_widget.php";
}

//Is a plugin active? Can be used in places the default wordpress function cannot.
function wovax_is_plugin_active($plugin) {
	$plugins = get_option('active_plugins');
	return in_array($plugin, $plugins, true);
}

//-AJAX functions!
require plugin_dir_path(__FILE__)."/library/wovaxapp_ajax.php";

//Enqueue jquery for admin panel.
add_action('admin_enqueue_scripts', 'wovax_load_ajax');

function wovax_load_ajax() {
	wp_enqueue_style( 'wp-color-picker' );
	wp_enqueue_script( 'wx-ajax', plugin_dir_url(__FILE__) . '/assets/js/wx-ajax.js', array('jquery', 'jquery-ui-sortable', 'jquery-ui-autocomplete', 'wp-color-picker'));
}

//Enqueue jquery for front-end use.
add_action('wp_enqueue_scripts', 'wovax_load_front_end_ajax');

function wovax_load_front_end_ajax() {
	wp_enqueue_script('wx-front-end',  plugin_dir_url(__FILE__) . '/assets/js/wx-front-end-ajax.js', array('jquery', 'jquery-ui-sortable'));
	wp_localize_script( 'wx-front-end', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
}

//Enqueue styles for shortcodes (front-end use)
add_action('wp_enqueue_scripts', 'wovax_load_front_end_styles');

function wovax_load_front_end_styles() {
	wp_register_style('wx-shortcode', plugin_dir_url(__FILE__) . '/assets/css/wovax-shortcode.css');
	wp_enqueue_style('wx-shortcode');
}

//---------------------------------------------
// API
add_action('template_redirect', 'wovax_api', 1);

function wovax_api() {
	require(WOVAX_APP_DIR."/api/wovaxapp_api_class.php");
	$wxapi = new wovaxapi();

	if ($wxapi->api_called()) {
		$wxapi->setup_conn();
		// get request
		$request = $wxapi->getUri();
		$request = explode("/",$request);
		$request = array_values(array_filter($request));
		foreach($request as $key => $value) {
			if($value === "wovaxapi") {
				$controller_offset = $key + 1; //The controller will always be right after /wovaxapi/
				$method_offset = $key + 2; //The method will always be right after /wovaxapi/controller/
			}
		}
		$controller_info = $wxapi->check_controller($request[$controller_offset]);
		if ($controller_info["controller_exists"]) {
			if (in_array($request[$method_offset],$controller_info["methods"])) {
				$method = $request[$method_offset];
				$path = $controller_info["path"];
				$name = $controller_info["class_name"];
				$result = $wxapi->execute($path,$method,$name);
				$wxapi->return_content($result);
			} else {
				$wxapi->return_error(
					"Choose a valid method from the list",
					$wxapi->list_methods($controller_info["class_name"])
				);
			}
		} else {
			$wxapi->return_error(
			"Choose a valid controller from the list",
			$wxapi->list_controllers()
			);
		}
		die();
	}
}

add_action('register_wovaxapi_controller','register_wovaxapi_controller',10,1);

function register_wovaxapi_controller($path) {
	if (is_dir($path)) {
		$dir_contents = array_diff(
			scandir($directory),
			array('..', '.')
		);
		if (!empty($dir_contents)) {
			$controller_paths = explode(",",get_option("wovax_api_controller_paths"));
			$controller_paths[] = $path;
			$controller_paths = array_values(array_unique($controller_paths));
			update_option("wovax_api_controller_paths",implode(",",$controller_paths));
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}

add_action('unregister_wovaxapi_controller','unregister_wovaxapi_controller',10,1);

function unregister_wovaxapi_controller($path) {
	$current = explode(",",get_option("wovax_api_controller_paths"));
	if (in_array($path,$current)) {
		$current = array_values(array_diff($current,$path));
		update_option("wovax_api_controller_paths",implode(",",$current));
		return true;
	} else {
		return false;
	}
}

$json_api_dir = json_api_dir();
@include_once "$json_api_dir/singletons/api.php";
@include_once "$json_api_dir/singletons/query.php";
@include_once "$json_api_dir/singletons/introspector.php";
@include_once "$json_api_dir/singletons/response.php";
@include_once "$json_api_dir/models/post.php";
@include_once "$json_api_dir/models/comment.php";
@include_once "$json_api_dir/models/category.php";
@include_once "$json_api_dir/models/tag.php";
@include_once "$json_api_dir/models/author.php";
@include_once "$json_api_dir/models/attachment.php";

function json_api_init() {
	global $json_api;
	add_filter('rewrite_rules_array', 'json_api_rewrites');
	$json_api = new JSON_API();
}

function json_api_rewrites($wp_rules) {
	$base = get_option('json_api_base', 'json_api');
	if (empty($base)) {
		return $wp_rules;
	}
	$json_api_rules = array(
		"$base\$" => 'index.php?json=info',
		"$base/(.+)\$" => 'index.php?json=$matches[1]'
	);
	return array_merge($json_api_rules, $wp_rules);
}

function json_api_dir() {
	if (defined('JSON_API_DIR') && file_exists(JSON_API_DIR)) {
		return JSON_API_DIR;
	} else {
		return dirname(__FILE__);
	}
}

function wovax_get_search_filters() {
	global $wpdb;
	$sql = "SELECT * FROM " . $wpdb->prefix . "wovaxapp_search_filter ORDER BY item_order ASC";
	$filters = $wpdb->get_results($sql, ARRAY_A);
	foreach($filters as $key => $filter){
		if($filter['type'] === 'existing'){
			$options = wovax_get_existing_option_values($filter['no']);
			if(empty($options)){
				wovax_update_existing_search_filter_cache();
				$options = wovax_get_existing_option_values($filter['no']);
			}
			$filters[$key]['options'] = $options;
		}
		$filters[$key]['in_app'] = ($filter['in_app'] === 'true') ? 'YES' : 'NO';
		$filters[$key]['label'] = stripslashes($filters[$key]['display_name']);
		unset($filters[$key]['post_type']);
		unset($filters[$key]['display_name']);
	}
	$filter_name = json_decode(get_option('wovax_filter_names'));

	$post_type = json_decode(get_option('wovax_filter_names'));

	return array(
		"count" => sizeof($filters),
		"post_type" => $post_type[0]->type,
		"filter_name" => $filter_name[0]->name,
		"filter" => $filters
	);
}

// Gets search filter info without having to run through all the posts.
function wovax_get_search_filter_info(){
	global $wpdb;
	$sql = "SELECT * FROM " . $wpdb->prefix . "wovaxapp_search_filter ORDER BY item_order ASC";
	$filters = $wpdb->get_results($sql, ARRAY_A);
	foreach($filters as $key => $filter){
		$filters[$key]['in_app'] = ($filter['in_app'] === 'true') ? 'YES' : 'NO';
		$filters[$key]['label'] = stripslashes($filters[$key]['display_name']);
		unset($filters[$key]['post_type']);
		unset($filters[$key]['display_name']);
	}
	$filter_name = json_decode(get_option('wovax_filter_names'));

	$post_type = json_decode(get_option('wovax_filter_names'));

	return array(
		"count" => sizeof($filters),
		"post_type" => $post_type[0]->type,
		"filter_name" => $filter_name[0]->name,
		"filter" => $filters
	);
}

function wovax_get_search_ranges($id) {
    global $wpdb;
    $sql = "SELECT * FROM " . $wpdb->prefix . "wovaxapp_search_filter_dropdowns WHERE filter_id=".$id." ORDER BY item_order ASC";
    $ranges = $wpdb->get_results($sql,ARRAY_A);
    return $ranges;
}

function wovax_get_existing_option_values($id) {
	global $wpdb;
    $sql = "SELECT `value` FROM " . $wpdb->prefix . "wovaxapp_search_filter_dropdowns WHERE filter_id='".$id."'";
    $options = $wpdb->get_col($sql);
	if(!empty($options)){
		natcasesort($options);
		$options = array_values($options);
	} else {
		$options = FALSE;
	}
    return $options;
}

// Add initialization and activation hooks
add_action('init', 'json_api_init');

//****CUSTOM COLUMN STUFF****/
//add the image column filter.
add_filter('manage_posts_columns', 'wx_img_col_header');//effects everything but pages and media
add_filter('manage_page_posts_columns', 'wx_img_col_header');//add to pages
add_action('manage_posts_custom_column', 'wx_img_col_content', 10, 2);//effects everything but pages and media
add_action('manage_page_posts_custom_column', 'wx_img_col_content', 10, 2);//add to pages
//add column header
//@todo maybe move the image columns to the main wovax plugin. 
function  wx_img_col_header($defaults) {
	//if we don't have categories column lets just do this. 
	if(!array_key_exists('categories', $defaults)){
		$defaults['wx_image_col'] = 'Image';
		return $defaults;
	}
	//we have categories so insert after it. 
	$new = array();
	foreach($defaults as $key => $val){
		$new[$key] = $val;
		if($key === 'categories'){//add image after categorie.
			$new['wx_image_col'] = 'Image';
		}
	}
    return $new;
}
 
//add the information we want to display in the column
function wx_img_col_content($column_name, $post_id) {
    if ($column_name === 'wx_image_col') {
		$attachment_id = get_post_thumbnail_id($post_id);
		$images = wp_get_attachment_image_src($attachment_id);
		if ($images) {
            echo '<p id="wx-img-col-'.$post_id.'"><img style="border: 1px solid rgba(0,0,0, 0.4);" height="55" width="55" src="'.$images[0].'"/></p>';
        } else {
			echo '<p id="wx-img-col-'.$post_id.'" class="hide-if-no-js"><a class="wx-upload-img" href="javascript:wx_img_col.showModal('.$post_id.')">Add Image</a></p>';
		}
    }
}
//enque the needed scripts for the image column. 
function wx_img_col_enque_js(){
	wp_enqueue_media();
	wp_enqueue_script('wx_img_col_js', WOVAX_APP_URL.'/assets/js/wx-image-col.js');
}
//only load the scripts on load edit pages. 
add_action('load-edit.php', function() {
	add_action('admin_enqueue_scripts', 'wx_img_col_enque_js');
});
//don't see an ajax file so adding it here.
add_action('wp_ajax_wx_img_col_action', function(){
	$post_id = array_key_exists('post_id', $_GET) ? intval($_GET['post_id']) : 0;
	$img_id = array_key_exists('img_id', $_GET) ? intval($_GET['img_id']) : 0;
	if($post_id === 0 OR $img_id === 0){
		wp_die('Bad Ids');
	}
	if(!current_user_can('edit_post', $post_id) OR !current_user_can('edit_post', $img_id)){
		wp_die('Permission Error!');
	}
	$ret = set_post_thumbnail($post_id, $img_id);
	if($ret === FALSE){
		wp_die('Failed to set thumbnail!');
	}
	//if the image is unattched attach it.
	if(wp_get_post_parent_id($img_id) === 0){
		$img_post = array(
			'ID'			=> $img_id,
			'post_parent'	=> $post_id
		);
		$ret = wp_update_post($img_post);
		if($ret !== $img_id){
			delete_post_thumbnail($post_id);
			wp_die('Failed to attach thumbnail!');
		}
	}
	wp_die('success');
});
//add index to mime type if it does not exist to speed up sites loading
if(is_admin()){
	add_action('init', function(){
		global $wpdb;
		$sql = "SHOW INDEX FROM `".$wpdb->posts."` WHERE `Column_name` = 'post_mime_type'";
		$results = $wpdb->get_results($sql);
		if(count($results) < 1){
			$sql = "ALTER TABLE `".$wpdb->posts."` ADD INDEX (`post_mime_type`)";
			$wpdb->query($sql);
		}
	});
}