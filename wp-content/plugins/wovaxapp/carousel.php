<?php
  header('Access-Control-Allow-Origin: *');
  function getGalleryByPostID($pID) {
    global $wpdb;

    $sql = "SELECT * FROM `".$wpdb->prefix."posts` WHERE post_type='attachment' AND post_parent='".$pID."' ORDER BY menu_order ASC";
    $results = $wpdb->get_results($sql);
    $count=0;
    foreach ($results as $result) {
      $url = wp_get_attachment_thumb_url( $result->ID );
      ?>
        <li class='touchcarousel-item'>
            <a href='http://photo;<?php echo $count; ?>'><img src='<?php echo $url; ?>' style="width:80px; height:80px;" width='80' height='80' /></a>
        </li>
      <?php
      $count++;
    }
  }

  function getGalleryByPostIDcount($pID) {
    global $wpdb;
    $sql = "SELECT * FROM `".$wpdb->prefix."posts` WHERE post_type='attachment' AND post_parent='".$pID."' ORDER BY menu_order ASC";
    $results = $wpdb->get_results($sql);
    $count=0;
    foreach ($results as $result) {
      $count++;
    }
    return $count;
  }

  if(getGalleryByPostIDcount($_GET['id']) > 0) {
    ?>
    <div id="carousel-gallery" class="touchcarousel  black-and-white">
      <ul class='touchcarousel-container'>
        <?php echo getGalleryByPostID($_GET['id']); ?>
      </ul>
    </div>
    <script type="text/javascript">
      jQuery(function ($) {
        carouselInstance = $("#carousel-gallery").touchCarousel({
          itemsPerPage: 4,
          scrollbar: false,
          scrollbarAutoHide: true,
          scrollbarTheme: "dark",
          pagingNav: false,
          snapToItems: false,
          scrollToLast: false,
          useWebkit3d: false,
          loopItems: false,
          directionNav: false
        }).data('touchCarousel');
      });
    </script>
    <?php
  }
?>
