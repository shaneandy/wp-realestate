<?php
	class wovax_custom_menu {
		function __construct() {
			// load the plugin translation files
			add_action( 'init', array( $this, 'textdomain' ) );
			// add custom menu fields to menu
			add_filter( 'wp_setup_nav_menu_item', array( $this, 'wovax_add_custom_nav_fields' ) );
			add_action( 'wp_update_nav_menu_item', array( $this, 'wovax_update_custom_nav_fields'), 10, 3 );
			add_filter( 'wp_edit_nav_menu_walker', array( $this, 'wovax_edit_walker'), 10, 2 );
		}

		public function textdomain() {
			load_plugin_textdomain( 'wovax', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
		}

		function wovax_add_custom_nav_fields( $menu_item ) {
			$menu_item->thumbnail = get_post_meta( $menu_item->ID, '_menu_item_thumbnail', true );
			return $menu_item;
		}

		function wovax_update_custom_nav_fields( $menu_id, $menu_item_db_id, $args ) {
			// Check if element is properly sent
			if ( is_array( $_REQUEST['menu-item-thumbnail']) ) {
				$subtitle_value = $_REQUEST['menu-item-thumbnail'][$menu_item_db_id];
				update_post_meta( $menu_item_db_id, '_menu_item_thumbnail', $subtitle_value );
			}
		}

		function wovax_edit_walker($walker,$menu_id) {
			return 'Wovax_Walker_Nav_Menu_Edit_Custom';
		}
	}
	
	class wovax_app_thumbnail_background_color {
		function __construct() {
			// load the plugin translation files
			add_action( 'init', array( $this, 'textdomain' ) );
			// add custom menu fields to menu
			add_filter( 'wp_setup_nav_menu_item', array( $this, 'wovax_add_custom_nav_fields' ) );
			add_action( 'wp_update_nav_menu_item', array( $this, 'wovax_update_custom_nav_fields'), 10, 3 );
			add_filter( 'wp_edit_nav_menu_walker', array( $this, 'wovax_edit_walker'), 10, 2 );
		}

		public function textdomain() {
			load_plugin_textdomain( 'wovax', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
		}

		function wovax_add_custom_nav_fields( $menu_item ) {
			$menu_item->thumbnailbgcolor = get_post_meta( $menu_item->ID, '_menu_item_thumbnailbgcolor', true );
			return $menu_item;
		}

		function wovax_update_custom_nav_fields( $menu_id, $menu_item_db_id, $args ) {
			// Check if element is properly sent
			if ( is_array( $_REQUEST['menu-item-thumbnailbgcolor']) ) {
				$background_color_value = $_REQUEST['menu-item-thumbnailbgcolor'][$menu_item_db_id];
				update_post_meta( $menu_item_db_id, '_menu_item_thumbnailbgcolor', $background_color_value );
			}
		}

		function wovax_edit_walker($walker,$menu_id) {
			return 'Wovax_Walker_Nav_Menu_Edit_Custom';
		}
	}

	class wovax_custom_page_type {
	  function __construct() {
			add_action( 'init', array( $this, 'textdomain' ) );
			add_filter( 'wp_setup_nav_menu_item', array( $this, 'wovax_add_custom_nav_fields' ) );
	  	add_action( 'wp_update_nav_menu_item', array( $this, 'wovax_update_custom_nav_fields'), 10, 3 );
			add_filter( 'wp_edit_nav_menu_walker', array( $this, 'wovax_edit_walker'), 10, 2 );
		}

		public function textdomain() {
			load_plugin_textdomain( 'wovax', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
		}

	  function wovax_add_custom_nav_fields( $menu_item ) {
			$menu_item->ptype = get_post_meta( $menu_item->ID, '_menu_item_ptype', true );
			return $menu_item;
		}

		function wovax_update_custom_nav_fields( $menu_id, $menu_item_db_id, $args ) {
			// Check if element is properly sent
			if ( is_array( $_REQUEST['menu-item-ptype']) ) {
				$subtitle_value = $_REQUEST['menu-item-ptype'][$menu_item_db_id];
				update_post_meta( $menu_item_db_id, '_menu_item_ptype', $subtitle_value );
			}
		}

		function wovax_edit_walker($walker,$menu_id) {
			return 'Wovax_Walker_Nav_Menu_Edit_Custom';
		}
	}

	class wovax_app_twitter_page {
		function __construct() {
			add_action( 'init', array( $this, 'textdomain' ) );
			add_filter( 'wp_setup_nav_menu_item', array( $this, 'wovax_add_custom_nav_fields' ) );
			add_action( 'wp_update_nav_menu_item', array( $this, 'wovax_update_custom_nav_fields'), 10, 3 );
			add_filter( 'wp_edit_nav_menu_walker', array( $this, 'wovax_edit_walker'), 10, 2 );
		}

		public function textdomain() {
			load_plugin_textdomain( 'wovax', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
		}

	  function wovax_add_custom_nav_fields( $menu_item ) {
			$menu_item->twitterpage = get_post_meta( $menu_item->ID, '_menu_item_twitterpage', true );
			return $menu_item;
		}

		function wovax_update_custom_nav_fields( $menu_id, $menu_item_db_id, $args ) {
		  // Check if element is properly sent
			if ( is_array( $_REQUEST['menu-item-twitterpage']) ) {
				$subtitle_value = $_REQUEST['menu-item-twitterpage'][$menu_item_db_id];
				update_post_meta( $menu_item_db_id, '_menu_item_twitterpage', $subtitle_value );
			}
		}

		function wovax_edit_walker($walker,$menu_id) {
			return 'Wovax_Walker_Nav_Menu_Edit_Custom';
		}
	}

	class wovax_app_twitter_hash_tag {
	  function __construct() {
			add_action( 'init', array( $this, 'textdomain' ) );
		  add_filter( 'wp_setup_nav_menu_item', array( $this, 'wovax_add_custom_nav_fields' ) );
			add_action( 'wp_update_nav_menu_item', array( $this, 'wovax_update_custom_nav_fields'), 10, 3 );
			add_filter( 'wp_edit_nav_menu_walker', array( $this, 'wovax_edit_walker'), 10, 2 );
		}

		public function textdomain() {
			load_plugin_textdomain( 'wovax', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
		}

		function wovax_add_custom_nav_fields( $menu_item ) {
	    $menu_item->twitterhashtag = get_post_meta( $menu_item->ID, '_menu_item_twitterhashtag', true );
	    return $menu_item;
		}

		function wovax_update_custom_nav_fields( $menu_id, $menu_item_db_id, $args ) {
		    // Check if element is properly sent
		    if ( is_array( $_REQUEST['menu-item-twitterhashtag']) ) {
		        $subtitle_value = $_REQUEST['menu-item-twitterhashtag'][$menu_item_db_id];
		        update_post_meta( $menu_item_db_id, '_menu_item_twitterhashtag', $subtitle_value );
		    }
		}

		function wovax_edit_walker($walker,$menu_id) {
		    return 'Wovax_Walker_Nav_Menu_Edit_Custom';
		}
	}

	class wovax_app_twitter_catType {
		function __construct() {
			add_action( 'init', array( $this, 'textdomain' ) );
			add_filter( 'wp_setup_nav_menu_item', array( $this, 'wovax_add_custom_nav_fields' ) );
			add_action( 'wp_update_nav_menu_item', array( $this, 'wovax_update_custom_nav_fields'), 10, 3 );
			add_filter( 'wp_edit_nav_menu_walker', array( $this, 'wovax_edit_walker'), 10, 2 );
		}

		public function textdomain() {
			load_plugin_textdomain( 'wovax', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
		}

		function wovax_add_custom_nav_fields( $menu_item ) {
		    $menu_item->catType = get_post_meta( $menu_item->ID, '_menu_item_catType', true );
		    return $menu_item;
		}

		function wovax_update_custom_nav_fields( $menu_id, $menu_item_db_id, $args ) {
		    // Check if element is properly sent
		    if ( is_array( $_REQUEST['menu-item-catType']) ) {
		        $subtitle_value = $_REQUEST['menu-item-catType'][$menu_item_db_id];
		        update_post_meta( $menu_item_db_id, '_menu_item_catType', $subtitle_value );
		    }
		}

		function wovax_edit_walker($walker,$menu_id) {
		    return 'Wovax_Walker_Nav_Menu_Edit_Custom';
		}
	}

	class wovax_app_oneclick_call {
		function __construct() {
			add_action( 'init', array( $this, 'textdomain' ) );
			add_filter( 'wp_setup_nav_menu_item', array( $this, 'wovax_add_custom_nav_fields' ) );
			add_action( 'wp_update_nav_menu_item', array( $this, 'wovax_update_custom_nav_fields'), 10, 3 );
			add_filter( 'wp_edit_nav_menu_walker', array( $this, 'wovax_edit_walker'), 10, 2 );
		}

		public function textdomain() {
			load_plugin_textdomain( 'wovax', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
		}

		function wovax_add_custom_nav_fields( $menu_item ) {
		    $menu_item->oneclickcall = get_post_meta( $menu_item->ID, '_menu_item_oneclickcall', true );
		    return $menu_item;
		}

		function wovax_update_custom_nav_fields( $menu_id, $menu_item_db_id, $args ) {
		    // Check if element is properly sent
		    if ( is_array( $_REQUEST['menu-item-oneclickcall']) ) {
		        $subtitle_value = $_REQUEST['menu-item-oneclickcall'][$menu_item_db_id];
		        update_post_meta( $menu_item_db_id, '_menu_item_oneclickcall', $subtitle_value );
		    }
		}

		function wovax_edit_walker($walker,$menu_id) {
		    return 'Wovax_Walker_Nav_Menu_Edit_Custom';
		}
	}

	class wovax_app_oneclick_mail {
		function __construct() {
			add_action( 'init', array( $this, 'textdomain' ) );
			add_filter( 'wp_setup_nav_menu_item', array( $this, 'wovax_add_custom_nav_fields' ) );
			add_action( 'wp_update_nav_menu_item', array( $this, 'wovax_update_custom_nav_fields'), 10, 3 );
			add_filter( 'wp_edit_nav_menu_walker', array( $this, 'wovax_edit_walker'), 10, 2 );
		}

		public function textdomain() {
			load_plugin_textdomain( 'wovax', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
		}

		function wovax_add_custom_nav_fields( $menu_item ) {
	    $menu_item->oneclickmail = get_post_meta( $menu_item->ID, '_menu_item_oneclickmail', true );
	    return $menu_item;
		}

		function wovax_update_custom_nav_fields( $menu_id, $menu_item_db_id, $args ) {
		    // Check if element is properly sent
		    if ( is_array( $_REQUEST['menu-item-oneclickmail']) ) {
		        $subtitle_value = $_REQUEST['menu-item-oneclickmail'][$menu_item_db_id];
		        update_post_meta( $menu_item_db_id, '_menu_item_oneclickmail', $subtitle_value );
		    }
		}

		function wovax_edit_walker($walker,$menu_id) {
		    return 'Wovax_Walker_Nav_Menu_Edit_Custom';
		}
	}

	class wovax_app_company_name {
		function __construct() {
			add_action( 'init', array( $this, 'textdomain' ) );
			add_filter( 'wp_setup_nav_menu_item', array( $this, 'wovax_add_custom_nav_fields' ) );
			add_action( 'wp_update_nav_menu_item', array( $this, 'wovax_update_custom_nav_fields'), 10, 3 );
			add_filter( 'wp_edit_nav_menu_walker', array( $this, 'wovax_edit_walker'), 10, 2 );
		}

		public function textdomain() {
			load_plugin_textdomain( 'wovax', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
		}

		function wovax_add_custom_nav_fields( $menu_item ) {
		    $menu_item->companyname = get_post_meta( $menu_item->ID, '_menu_item_companyname', true );
		    return $menu_item;
		}

		function wovax_update_custom_nav_fields( $menu_id, $menu_item_db_id, $args ) {
		    // Check if element is properly sent
		    if ( is_array( $_REQUEST['menu-item-companyname']) ) {
		        $subtitle_value = $_REQUEST['menu-item-companyname'][$menu_item_db_id];
		        update_post_meta( $menu_item_db_id, '_menu_item_companyname', $subtitle_value );
		    }
		}

		function wovax_edit_walker($walker,$menu_id) {
		    return 'Wovax_Walker_Nav_Menu_Edit_Custom';
		}
	}

	class wovax_app_company_address {
		function __construct() {
			add_action( 'init', array( $this, 'textdomain' ) );
			add_filter( 'wp_setup_nav_menu_item', array( $this, 'wovax_add_custom_nav_fields' ) );
			add_action( 'wp_update_nav_menu_item', array( $this, 'wovax_update_custom_nav_fields'), 10, 3 );
			add_filter( 'wp_edit_nav_menu_walker', array( $this, 'wovax_edit_walker'), 10, 2 );
		}

		public function textdomain() {
			load_plugin_textdomain( 'wovax', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
		}

		function wovax_add_custom_nav_fields( $menu_item ) {
		    $menu_item->companyaddress = get_post_meta( $menu_item->ID, '_menu_item_companyaddress', true );
		    return $menu_item;
		}

		function wovax_update_custom_nav_fields( $menu_id, $menu_item_db_id, $args ) {
		    // Check if element is properly sent
		    if ( is_array( $_REQUEST['menu-item-companyaddress']) ) {
		        $subtitle_value = $_REQUEST['menu-item-companyaddress'][$menu_item_db_id];
		        update_post_meta( $menu_item_db_id, '_menu_item_companyaddress', $subtitle_value );
		    }
		}

		function wovax_edit_walker($walker,$menu_id) {
		    return 'Wovax_Walker_Nav_Menu_Edit_Custom';
		}
	}

	class wovax_app_company_latlong {
		function __construct() {
			add_action( 'init', array( $this, 'textdomain' ) );
			add_filter( 'wp_setup_nav_menu_item', array( $this, 'wovax_add_custom_nav_fields' ) );
			add_action( 'wp_update_nav_menu_item', array( $this, 'wovax_update_custom_nav_fields'), 10, 3 );
			add_filter( 'wp_edit_nav_menu_walker', array( $this, 'wovax_edit_walker'), 10, 2 );
		}

		public function textdomain() {
			load_plugin_textdomain( 'wovax', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
		}

		function wovax_add_custom_nav_fields( $menu_item ) {
		    $menu_item->companylatlong = get_post_meta( $menu_item->ID, '_menu_item_companylatlong', true );
		    return $menu_item;
		}

		function wovax_update_custom_nav_fields( $menu_id, $menu_item_db_id, $args ) {
		    // Check if element is properly sent
		    if ( is_array( $_REQUEST['menu-item-companylatlong']) ) {
		        $subtitle_value = $_REQUEST['menu-item-companylatlong'][$menu_item_db_id];
		        update_post_meta( $menu_item_db_id, '_menu_item_companylatlong', $subtitle_value );
		    }
		}

		function wovax_edit_walker($walker,$menu_id) {
		    return 'Wovax_Walker_Nav_Menu_Edit_Custom';
		}
	}

	class wovax_app_company_phonenumber {
		function __construct() {
			add_action( 'init', array( $this, 'textdomain' ) );
			add_filter( 'wp_setup_nav_menu_item', array( $this, 'wovax_add_custom_nav_fields' ) );
			add_action( 'wp_update_nav_menu_item', array( $this, 'wovax_update_custom_nav_fields'), 10, 3 );
			add_filter( 'wp_edit_nav_menu_walker', array( $this, 'wovax_edit_walker'), 10, 2 );
		}

		public function textdomain() {
			load_plugin_textdomain( 'wovax', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
		}

		function wovax_add_custom_nav_fields( $menu_item ) {
		    $menu_item->companyphonenumber = get_post_meta( $menu_item->ID, '_menu_item_companyphonenumber', true );
		    return $menu_item;
		}

		function wovax_update_custom_nav_fields( $menu_id, $menu_item_db_id, $args ) {
		    // Check if element is properly sent
		    if ( is_array( $_REQUEST['menu-item-companyphonenumber']) ) {
		        $subtitle_value = $_REQUEST['menu-item-companyphonenumber'][$menu_item_db_id];
		        update_post_meta( $menu_item_db_id, '_menu_item_companyphonenumber', $subtitle_value );
		    }
		}

		function wovax_edit_walker($walker,$menu_id) {
		    return 'Wovax_Walker_Nav_Menu_Edit_Custom';
		}
	}

	class wovax_app_company_emailaddress {
		function __construct() {
			add_action( 'init', array( $this, 'textdomain' ) );
			add_filter( 'wp_setup_nav_menu_item', array( $this, 'wovax_add_custom_nav_fields' ) );
			add_action( 'wp_update_nav_menu_item', array( $this, 'wovax_update_custom_nav_fields'), 10, 3 );
			add_filter( 'wp_edit_nav_menu_walker', array( $this, 'wovax_edit_walker'), 10, 2 );
		}

		public function textdomain() {
			load_plugin_textdomain( 'wovax', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
		}

		function wovax_add_custom_nav_fields( $menu_item ) {
		    $menu_item->companyemailaddress = get_post_meta( $menu_item->ID, '_menu_item_companyemailaddress', true );
		    return $menu_item;
		}

		function wovax_update_custom_nav_fields( $menu_id, $menu_item_db_id, $args ) {
		    // Check if element is properly sent
		    if ( is_array( $_REQUEST['menu-item-companyemailaddress']) ) {
		        $subtitle_value = $_REQUEST['menu-item-companyemailaddress'][$menu_item_db_id];
		        update_post_meta( $menu_item_db_id, '_menu_item_companyemailaddress', $subtitle_value );
		    }
		}

		function wovax_edit_walker($walker,$menu_id) {
		    return 'Wovax_Walker_Nav_Menu_Edit_Custom';
		}
	}
