<?php
	header('Access-Control-Allow-Origin: *');
	$comments = get_comments('post_id='.$_GET['id'].'&order=ASC');
	$count=0;

	foreach($comments as $comment) {
		if($comment->comment_approved){
			?>
			<body style='margin: 0px;padding: 0px;border: 0px;'>
			<div style='border-top:1px solid #cfcfcf; margin:0px; padding:0px;'>
			<div style='font-family:HelveticaNeue, sans-serif; margin:5px 5% 5px 5%;'>
			<?php
				if($comment->comment_author_url) {
					echo("<div style='font-weight:normal;'><b><a href='".$comment->comment_author_url."'></b></div>");
				}
			?>
			<div style='font-weight:normal;'><b><?php echo $comment->comment_author; ?></b></a> <i>says:</i></div>
			<div style='padding-top:5px;'><?php echo $comment->comment_content; ?></div>
			<div style='margin-top:5px;'>On <?php echo $comment->comment_date; ?></div>
			<?php
			if($comment->comment_parent) {
				echo("<div style='margin-top:5px;'>In reply to".$comment->comment_parent."</div>");
			}
			echo '</div>';
			$count++;
		}
	}
?>
<div style='height:10px;'></div>
