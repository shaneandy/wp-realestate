<?php

  wp_add_dashboard_widget(
    'wovax_stats_dashboard_widget', // Widget slug.
    'Your Wovax Statistics', // Widget title.
    'wovax_stats_dashboard_widget_function' // Display function.
  );

  function wovax_stats_dashboard_widget_function() {
    global $wpdb;
    $sql = "SELECT * FROM `".$wpdb->prefix."wovaxapp_apns_device`";
    $apns = $wpdb->get_results($sql);
    $sql = "SELECT * FROM `".$wpdb->prefix."wovaxapp_gcm_device`";
    $gcm = $wpdb->get_results($sql);
    $count = sizeof($apns) + sizeof($gcm);

    echo "<p>You have ".$count." push notification subscribers.</p>";
    echo "<p>Your Wovax software license is ".get_option('wovaxapp_activated')."</p>";
    echo "View your <a href='https://wovax.com/my-account/' target='_blank'>Wovax account or manage your payments</a>.";
  }

?>
