<?php
	register_widget("Wovax_Search_Form");

/**
 * The Wovax Recent Posts Widget
 *
 */

 class Wovax_Search_Form extends WP_Widget {

	 //Register widget with WordPress
	 function __construct() {
		 parent::__construct(
			 'Wovax_Search_Form', //Base ID
			 __( 'Wovax Search Form', 'text_domain' ), //Name
			 array( 'description' => __('Displays the Wovax Search Form.', 'text_domain'), ) //Args
		 );
	 }

	 /**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	 function widget( $args, $instance ) {
		$title = esc_attr($instance['title']);
		$column_override = esc_attr($instance['column_override']);
		$all_filters = esc_attr($instance['all_filters']);
		$search_sections = $instance['search_sections'];
		if(isset($instance['search_button'])) {
			$search_button = esc_attr($instance['search_button']);
		} else {
			$search_button = 'Search';
		}

		$filters = wovax_get_search_filters();
		$filter_size = count($filters['filter']);
		// search_sections is the ids of the search filters that can be passed in.
		// if you don't pass in anything, all available search filters are used.
		if ($all_filters != 'no') {
			$search = array();
			for ($i =0;$i<$filter_size;$i++) {
				$search[] = $filters['filter'][$i];
			}
		}else {
			$search_sections = array_values(array_unique($search_sections));
			$arg_size = count($search_sections);
			$search = array();
			for ($i=0;$i<$arg_size;$i++) {
				for ($j=0;$j<$filter_size;$j++) {
					if ((int) $filters['filter'][$j]['no'] === (int) $search_sections[$i]) { //Cast both to integers to avoid funny business.
						$search[] = $filters['filter'][$j];
					}
				}
			}
		}
		//col now autogens and ignores the passed in value.
		if(isset($column_override) && is_numeric($column_override)) {
			$col = $column_override;
		} else {
			$col = count($search)+1; //The one accounts for the search button.
		}
		if  ((int)$col > 6) {
			$col = 6; //There should never be more than six columns for readability.
		}
		$output = "<h4 class='widget-title widgettitle'>".$instance['title']."</h4>";
		$output .= '<form class="wovax-search-form col-'.$col.'" action="'.site_url("search-results").'" method="GET">';
		for ($i=0;$i<sizeof($search);$i++) {
		$custom_field = stripslashes($search[$i]["custom_field"]);
		$search_filter = $search[$i]['no'];
		if(isset($_GET[$search_filter])) {
			$current_search = $_GET[$search_filter];
		} else {
			$current_search = "";
		}
		$output .= '<div class="wovax-search-section">';
		$output .= '<span class="wovax-search-label">' . $search[$i]["label"] . '</span>';
		switch ($search[$i]["type"]) {
			case "text":
			$output .= ' <input class="wovax-search-filter wovax-text-input wovaxPlaceholder" type="text" name="'.$search_filter.'" placeholder="'.$search[$i]["default_value"].'" value="'.$current_search.'">';
			break;
		case "numeric":
			$output .= ' <input class="wovax-search-filter" type="number" pattern="[0-9]*" name="'.$search_filter.'" placeholder="'.$search[$i]["default_value"].'" value="'.( isset($_GET[$search_filter]) ? $_GET[$search_filter] : $search[$i]["label"]).'">';
			break;
		case "numeric_min":
			$output .= ' <input class="wovax-search-filter" type="number" pattern="[0-9]*" name="'.$search_filter.'" placeholder="'.$search[$i]["default_value"].'" value="'.( isset($_GET[$search_filter]) ? $_GET[$search_filter] : $search[$i]["label"]).'">';
			break;
		case "numeric_max":
			$output .= ' <input class="wovax-search-filter" type="number" pattern="[0-9]*" name="'.$search_filter.'" placeholder="'.$search[$i]["default_value"].'" value="'.( isset($_GET[$search_filter]) ? $_GET[$search_filter] : $search[$i]["label"]).'">';
			break;
		case "existing":
			$output .= ' <select class="wovax-search-filter" name="'.$search_filter.'">
			<option value="All">
			'.$search[$i]["default_value"].'
			</option> ';
			$options = $search[$i]['options'];
			foreach($options as $option){
				if($option === $current_search){
					$option_selected = ' selected';
				} else {
					$option_selected = '';
				}
				$output .= '<option value="'.$option.'"'.$option_selected.' >'.$option.'</option>';
			}
			$output .= ' </select> ';
			break;
		case "binary":
			if( $current_search === 'yes' ) {
				$yes_selected = ' selected';
				$no_selected = '';
			} else if( $current_search === 'no' ) {
				$yes_selected = '';
				$no_selected = ' selected';
			} else {
				$yes_selected = '';
				$no_selected = '';
			}
			$output .= '<select class="wovax-search-filter" name="'.$search_filter.'"> ' . '<option value="All">'.$search[$i]["default_value"].'</option><option value="yes"'.$yes_selected.'>Yes</option><option value="no"'.$no_selected.'>No</option> ' . '</select>';
			break;
		case "range":
			$ranges = wovax_get_search_ranges($search_filter);
			$output .= '<select class="wovax-search-filter" name="'.$search_filter.'">' . '<option value="All">'.$search[$i]["default_value"].'</option> ';;
			foreach($ranges as $range){
				if($range['value'] === $current_search) {
					$range_selected = ' selected';
				} else {
					$range_selected = '';
				}
				$output .= '<option value="'.$range['value'].'" '.$range_selected.' >'.$range['display_name'].'</option>';
			}
			$output .= '</select>';
			break;
		}
		$output .= '</div>';
	}
		$output .= "<div class='wovax-search-section'>";
		$output .= "<input type='hidden' name='cols' id='shortcodesearch' value='".$col."' />";
		$output .= " <input type='submit' class='bbutton-primary' value='".$search_button."'>";
		$output .= "</div>";
		$output .= "</form>";
		if ( array_key_exists('before_widget', $args) ) echo $args['before_widget'];
		echo $output;
		if ( array_key_exists('after_widget', $args) ) echo $args['after_widget'];
	 }

	 /**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	 function form( $instance ) {
		$title = esc_attr($instance['title']);
		$column_override = esc_attr($instance['column_override']);
		$all_filters = esc_attr($instance['all_filters']);
		$search_sections = $instance['search_sections'];
		$search_button = $instance['search_button'];
		$search_filters = wovax_get_search_filters();
		$filters = $search_filters['filter'];
		//TODO: In order to get the checkboxes to save, had to remove some value sanitizing that was designed for text input, not checked value input or arrays. Replace with actual sanitization.
		?>
		<div>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title: </label>
				<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
			</p>
			<div>
				<p>
					<label for="<?php echo $this->get_field_id( 'column_override' ); ?>">Column Override: </label>
					<input id="<?php echo $this->get_field_id( 'column_override' ); ?>" name="<?php echo $this->get_field_name( 'column_override' ); ?>" type="number" value="<?php echo esc_attr( $column_override ); ?>"> <br />
					<small>The number of columns will be auto-generated based on the number of search filters plus the submit button unless overridden here, and cannot be overridden to be more than six.</small>
				</p>
			</div>
			<div>
				<p>
					<label for="<?php echo $this->get_field_id( 'all_filters' ); ?>">Use All Filters: </label>
					<select id="<?php echo $this->get_field_id( 'all_filters' ); ?>" name="<?php echo $this->get_field_name( 'all_filters' ); ?>" class="wx-search-form-all-filters">
						<option value='yes'<?php if($all_filters === 'yes') { echo 'selected'; } ?>>Yes</option>
						<option value='no'<?php if($all_filters === 'no') { echo 'selected'; } ?>>No</option>
					</select>
				</p>
			</div>
			<div class='wx-search-sections-div' <?php if($all_filters === 'yes' || !$all_filters ){ echo ' hidden';}?>>
				<p>
					<label for="<?php echo $this->get_field_id( 'search_sections' ); ?>">Filters Included: </label> <br />
					<?php
					foreach ($filters as $id => $array ) { ?>
						<input id="<?php echo $this->get_field_id( 'search_sections' ).$id; ?>" name="<?php echo $this->get_field_name('search_sections');?>[]" value="<?php echo $array['no'] ?>" <?php if(isset($search_sections)) { if(in_array($array['no'], $search_sections)) { echo ' checked'; }} else { echo ' checked'; } ?> type="checkbox"> <?php
						echo $array['label']; ?> <br />

					<?php } ?>
				</p>
			</div>
			<div>
				<p>
					<label for="<?php echo $this->get_field_id( 'search_button' ); ?>">Search Button Text: </label>
					<input id="<?php echo $this->get_field_id( 'search_button' ); ?>" name="<?php echo $this->get_field_name( 'search_button' ); ?>" value="<?php if(isset($search_button)) { echo esc_attr( $search_button ); } else { echo 'Search'; } ?>">
				</p>
			</div>

		</div>
		<?php
	 }

	 /**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	 function update( $new_instance, $old_instance ) {
		 $instance = $old_instance;
		 $instance['title'] = strip_tags($new_instance['title']);
		 $instance['column_override'] = strip_tags($new_instance['column_override']);
		 $instance['all_filters'] = strip_tags($new_instance['all_filters']);
		 $instance['search_sections'] = array_map('absint', $new_instance['search_sections']); //Since the search sections saves filter ids, there should only ever be integers here.
		 $instance['search_button'] = strip_tags($new_instance['search_button']);
		 return $instance;
	 }
 }
