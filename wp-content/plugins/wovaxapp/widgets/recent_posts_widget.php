<?php
	
register_widget("Wovax_Recent_Posts");
	
/**
 * The Wovax Recent Posts Widget
 *
 */

 class Wovax_Recent_Posts extends WP_Widget {
	 
	 //Register widget with WordPress
	 function __construct() {
		 parent::__construct(
			 'Wovax_Recent_Posts', //Base ID
			 __( 'Wovax Recent Posts', 'text_domain' ), //Name
			 array( 'description' => __('Displays a set of recent posts.', 'text_domain'), ) //Args
		 );
		 
	 }
	 
	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	 function widget( $args, $instance ) {
		// use a template for the output so that it can easily be overridden by theme or for easy format changes.

		// get the selected template type
		$template = $instance['template'];

		if ( $template === 'property' ) { //display real estate listings
			$template = locate_template(array('wx_property_template.php'));
			if ($template === '' ) {
				if(file_exists(WX_CUSTOM_TEMPLATE_PATH.'/wx_property_template.php')){
					$template = WX_CUSTOM_TEMPLATE_PATH.'/wx_property_template.php';
				} else {
					$template = WOVAX_APP_DIR.'/templates/wx_property_template.php';
				}
			}
		} else if ( $template === 'post' ) { //display regular posts
			$template = locate_template(array('wx_posts_template.php'));
			if ($template === '') {
				if(file_exists(WX_CUSTOM_TEMPLATE_PATH.'/wx_posts_template.php')){
					$template = WX_CUSTOM_TEMPLATE_PATH.'/wx_posts_template.php';
				} else {
					$template = WOVAX_APP_DIR.'/templates/wx_posts_template.php';
				}
			}
		} else if ( $template === 'toolset' ) { //Only Dabco uses this for now - if we use the toolset plugins for any other site, this is the template for it.
			$template = locate_template(array('wx_toolset_template.php'));
			if ($template === ''){
				if(file_exists(WX_CUSTOM_TEMPLATE_PATH.'/wx_toolset_template.php')){
					$template = WX_CUSTOM_TEMPLATE_PATH.'/wx_toolset_template.php';
				} else {
					$template = WOVAX_APP_DIR.'/templates/wx_toolset_template.php';
				}
			}
		} else { // in all other cases use the default template
			$template = WOVAX_APP_DIR.'/templates/wx_posts_template.php';
		} 
		
		if ( array_key_exists('before_widget', $args) ) echo $args['before_widget'];
		$post_type = $instance['post_type'];
		$number_of_posts = $instance['number_of_posts'];
		$taxonomy = $instance['taxonomy'];
		$term = $instance['term'];
		$post_args = array(
			'numberposts'      => $number_of_posts,
			'orderby'          => 'date',
			'order'            => 'DESC',
			'post_type'        => $post_type,
			'post_status'      => 'publish',
			'suppress_filters' => true,
		);
		if (isset($taxonomy) && isset($term)) {
			if($taxonomy == 'category'){
				$taxonomy = 'category_name'; //special handling due to 'category' requiring an ID instead of a slug.
			}
			$post_args[$taxonomy] = $term;
		}
		$counter = 0;
		$wovax_posts = get_posts($post_args);
		include ( $template );
		if ( array_key_exists('after_widget', $args) ) echo $args['after_widget'];
	 }
	 
	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	function form( $instance ) {
		$title = esc_attr($instance['title']);
		$post_type_saved = esc_attr($instance['post_type']);
		$number = esc_attr($instance['number_of_posts']);
		$display = esc_attr($instance['display']);
		$taxonomy = esc_attr($instance['taxonomy']);
		$term = esc_attr($instance['term']);
		$template = esc_attr($instance['template']);
		$pt_args = array();
		$post_types = get_post_types($pt_args, 'object');
		$type_array = array();
		foreach ($post_types as $post_type) {			
			if (isset($post_type->label)) {
				$type_array[$post_type->label] = $post_type->name; 
			} else if (isset($post_type->labels)) {
				$type_array[$post_type->labels->name] = $post_type->name;
			}
		}
		$taxonomy_objects = get_object_taxonomies($post_type_saved, 'objects');
		$term_objects = get_terms($taxonomy);
		?>
		<div>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title: </label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
			</p>
			<div>
				<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>">Post Type: </label>
				<select id="<?php echo $this->get_field_id( 'post_type' ); ?>" name="<?php echo $this->get_field_name( 'post_type' ); ?>" class="wx-recent-post-type-select">
				<?php foreach ($type_array as $label => $value) { ?>
					<option value="<?php echo $value; ?>" <?php if ($value == $instance['post_type']) echo 'selected'; ?>><?php echo $label; ?></option>
				<?php } ?>
				</select>
				</p>
			</div>
			<div class='wx-recent-number'>
				<p>
				<label for="<?php echo $this->get_field_id( 'number_of_posts' ); ?>">Number of Recent Posts: </label>
				<input id="<?php echo $this->get_field_id( 'number_of_posts' ); ?>" name="<?php echo $this->get_field_name( 'number_of_posts' ); ?>" type="number" value="<?php echo esc_attr( $number ); ?>">
				</p>
			</div>
			<div class='wx-display-type'>
				<p>
				<label for="<?php echo $this->get_field_id( 'display' ); ?>">Display Type: </label>
				<select id="<?php echo $this->get_field_id( 'display' ); ?>" name="<?php echo $this->get_field_name( 'display' ); ?>" class="wx-recent-form-display">
					<option value='recent'<?php if($display === 'recent') { echo 'selected'; } ?>>Recent Posts</option>
					<option value='taxonomy_display' <?php if($display === 'taxonomy_display') { echo 'selected'; } ?>>Posts from Taxonomy</option>
				</select>
				</p>
			</div>
			<div class='wx-taxonomy-select' <?php if ($display != 'taxonomy_display') { echo 'hidden'; }?>>
				<p>
				<label for="<?php echo $this->get_field_id( 'taxonomy' ); ?>">Taxonomy: </label>
				<select id="<?php echo $this->get_field_id( 'taxonomy' ); ?>" name="<?php echo $this->get_field_name( 'taxonomy' ); ?>" class="wx-recent-form-taxonomy">
					<option value="select_taxonomy" <?php if( $taxonomy == 'select_taxonomy') { echo 'selected'; } ?>>Select Taxonomy</option>
					<?php foreach ($taxonomy_objects as $object) {
						if($taxonomy == $object->name) {
							$selected = ' selected';
						} else {
							$selected = '';
						}
						echo "<option value='".$object->name."'".$selected.">".$object->label."</option>";
					} ?>
				</select>
				</p>
			</div>
			<div class='wx-term-select' <?php if ($display != 'taxonomy_display' || $taxonomy == 'select_taxonomy') { echo 'hidden'; }?>>
				<p>
				<label for="<?php echo $this->get_field_id( 'term' ); ?>">Term: </label>
				<select id="<?php echo $this->get_field_id( 'term' ); ?>" name="<?php echo $this->get_field_name( 'term' ); ?>" class="wx-recent-form-term" <?php if (! isset($taxonomy)) { echo 'disabled';}?>>
					<?php foreach ($term_objects as $terms) {
						if ($term == $terms->slug ) {
							$selected = ' selected';
						} else {
							$selected = '';
						}
						echo "<option value='".$terms->slug."'".$selected.">".$terms->name."</option>";
					} ?>
				</select>
				</p>
			</div>
			<div class='wx-template-select'>
				<p>
				<label for="<?php echo $this->get_field_id( 'template' ); ?>">Display Template: </label>
				<select id="<?php echo $this->get_field_id( 'template' ); ?>" name="<?php echo $this->get_field_name( 'template' ); ?>" class="wx-recent-form-template">
					<option value='post'<?php if($template === 'post') { echo 'selected'; } ?>>Post Template</option>
					<option value='property'<?php if($template === 'property') { echo 'selected'; } ?>>Property Template</option>
					<option value='toolset'<?php if($template === 'toolset') { echo 'selected'; } ?>>Toolset Template</option>
				</select>
				</p>
			</div>
		</div>
		<?php 
	 }
	 
	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	 function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['post_type'] = strip_tags($new_instance['post_type']);
   		$instance['number_of_posts'] = strip_tags($new_instance['number_of_posts']);
    	$instance['display'] = strip_tags($new_instance['display']);
		$instance['taxonomy'] = strip_tags($new_instance['taxonomy']);
		$instance['term'] = strip_tags($new_instance['term']);
		$instance['template'] = strip_tags($new_instance['template']);
	   	return $instance;
	 }
 }