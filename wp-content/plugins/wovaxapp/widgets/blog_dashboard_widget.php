<?php

  global $wp_meta_boxes;
  wp_add_dashboard_widget(
    'wovax_blog_dashboard_widget', //Widget slug.
    'Wovax News', // Widget title.
    'wovax_blog_dashboard_widget_output' // Display function.
  ); //Add new RSS feed output

  function wovax_blog_dashboard_widget_output() {
    echo '<div class="rss-widget">';
    wp_widget_rss_output(array(
    'url' => 'https://wovax.com/wp-rss2.php', // Feed URL
    'title' => 'Wovax News', // Widget title
    'items' => 2, // Number of posts to show
    'show_summary' => 1, // 1 = Show excerpt
    'show_author' => 1, // 1 = Show author
    'show_date' => 1, // 1 = Show date
    ));
    echo "</div>";
  }
?>
