<?php

	class wovaxapi_wovax_search_controller {

		private $from_cache = 0;

		public function advanced_search() {
			$start = microtime();
			global $wpdb;
			$offset = 0;
			if (isset($_GET['numberposts'])) {
				$numberposts = $_GET['numberposts'];
				unset($_GET['numberposts']);
			}else {
				$numberposts = get_option("posts_per_page");
			}
			if (isset($_GET['page'])) {
				$paged = $_GET['page'];
				if($paged > 0) {
					$offset = ($paged - 1)*($numberposts);	
				}
				unset($_GET['page']);
			} else {
				$paged = 1;
			}
			if(isset($_GET['offset'])){
				$offset = $_GET['offset'];
				$paged = ($_GET['offset']/$numberposts) + 1;
				unset($_GET['offset']);
			}
			$request = $_GET;
			$order = $request['orderby'];
  			unset($_GET['orderby']);
			$filters = wovax_get_search_filters();
			$search = array();
			$size = count($filters['filter']);
			//This loop takes the filters array and the array sent by the search form and merges the necessary information into a third array.
			//To search properly, we need the custom field being searched in, the type of search, and the value being searched.
			foreach($request as $name => $value) {
				for ($j=0;$j<$size;$j++) {
					if ($filters['filter'][$j]['no'] == $name && $value != 'All' && $value != '') {
						$search[$name]['custom_field'] = $filters['filter'][$j]['custom_field'];
						$search[$name]['type'] = $filters['filter'][$j]['type'];
						$search[$name]['value'] = $value;
					}
				}
			}
			//This loop prepares the meta query based on the type of search being performed.
			foreach($search as $info) {
				if($info['type'] == 'text' ) {
				if ($info['custom_field'] != 'search__content'){
					$meta[] = array('key' => $info['custom_field'], 'value' => $info['value'], 'compare' => 'LIKE');
				} else {
					$keyword = $info['value']; //Special handling for searches of the content, since the content isn't postmeta, and content searches can only be text.
				}
				} else if ( $info['type'] == 'numeric' || $info['type'] == 'existing' ) {
					$meta[] = array('key' => $info['custom_field'], 'value' => stripslashes($info['value']), 'compare' => 'LIKE');
				} else if ($info['type'] == 'binary') {
					if( trim($info['value']) == 'yes' ) {
						$meta[] = array('key' => $info['custom_field'], 'value' => 1, 'compare' => '=');
					} else if( trim($info['value']) == 'no' ) {
						$meta[] = array('key' => $info['custom_field'], 'value' => 0, 'compare' => '=');
					};
				} else if ($info['type'] == 'numeric_min') {
					$meta[] = array('key' => $info['custom_field'], 'value' => $info['value'], 'compare' => '>=', 'type' => 'NUMERIC');
				} else if ($info['type'] == 'numeric_max') {
					$meta[] = array('key' => $info['custom_field'], 'value' => $info['value'], 'compare' => '<=', 'type' => 'NUMERIC');
				} else if ($info["type"] == "range") {
                    $range = explode("-",$info["value"]);
                    $meta[] = array("key" => $info["custom_field"], "value" => $range[0], "compare" => ">=", "type" => "NUMERIC");
                    $meta[] = array("key" => $info["custom_field"], "value" => $range[1], "compare" => "<=", "type" => "NUMERIC");
                }
			}
			//This section prepares the orderby values if any are passed in.
			$order = explode("-",$order);
			if($order[0] == 'date'){
				$direction = strtoupper($order[1]);
				$orderby = 'date';
				$meta_key = '';
			} else if ($order[1] == 'numeric'){
				$direction = strtoupper($order[2]);
				$orderby = 'meta_value_num';
				$meta_key = $order[0];
			} else if ($order[1] == 'alphabetical'){
				$direction = strtoupper($order[2]);
				$orderby = 'meta_value';
				$meta_key = $order[0];
			} else {
				$direction = 'DESC';
				$orderby = 'date';
				$meta_key = '';
			}
			$post_type = json_decode(get_option('wovax_filter_names'), true); //This reference may need to change if we move towards multiple searches per site.
			$post_type = $post_type[0]['type']; //Mostly because it does in two lines what could totally be done in one.
			$args = array(
				'post_type' => $post_type, 
				's' => $keyword, 
				'posts_per_page' => $numberposts, 
				'paged' => $paged, 
				'meta_query' => $meta,
				'meta_key' => $meta_key,
				'orderby' => $orderby, 
				'order' => $direction, 
				);
			$wovax_posts = new WP_Query($args); //Get an object containing all the relevant posts.
			require_once WOVAX_APP_DIR."/library/wovaxapp_post.php";
			$wx_post = new wovaxapp_post;
			
			$posts_total = $wovax_posts->posts; //For getting a count of the total number of posts
			//$posts_paged = array_slice($posts_total,$offset,$numberposts); //We only want to format the number of posts we're going to display at a time.
			$posts_formatted = $wx_post->format_posts($posts_total); //Format the posts into json for app display.
			$filters = $this->get_queried_filters($_GET); //Get the filters used in the query
			$end = microtime();
			$time = $end - $start . " ms";
			$results = array(
				"filters" => $filters,
				"memory_usage" => memory_get_usage() / 1024 / 1024 . " MB",
				"count" => count($posts_formatted['posts']),//($numberposts < sizeof($posts) ? $numberposts : sizeof($posts)),
				"count_total" => $wovax_posts->found_posts,
				"offset" => $offset,
				"posts" => $posts_formatted['posts'],
				"page" => $paged,
				"pages" => $wovax_posts->max_num_pages,
			);

			return $results;
		}

		public function basic_search() {
			$wxapi = new wovaxapi;
			$wxapi->check_variables(array("query"));

			$query = new WP_Query(array('s'=>$_GET['query']));
			$raw = $query->posts;

			require_once WOVAX_APP_DIR."/library/wovaxapp_post.php";
			$wx_post = new wovaxapp_post;

			$posts = $wx_post->format_posts($raw);

			return array(
				"query" => $_GET['query'],
				"posts" => $posts['posts']
			);
		}

		public function get_advanced_filters() {
			// function can be found in wovaxapp.php
			$filters = wovax_get_search_filters();
			$results = array();
			$count = 0;
			foreach ($filters['filter'] as $filter) {
                if($filter['type'] == 'range'){
                    $ranges = wovax_get_search_ranges($filter['no']);
					$i = 0;
					if(is_array($ranges)) {
						foreach($ranges as $range){
							$filter['options'][] = $range['value'];
							$filter['options_label'][] = $range['display_name'];
							$filter['options_v2'][$i]['value'] = $range['value'];
							$filter['options_v2'][$i]['label'] = $range['display_name'];
							$i++;
						}
					}
                } else if($filter['type'] == 'existing'){
					$options = wovax_get_existing_option_values($filter['no']);
					//$filter['options'] = array();
					$i = 0;
					if(is_array($options)) {
						foreach($options as $option){
							$filter['options_v2'][$i]['value'] = $option;
							$filter['options_v2'][$i]['label'] = $option;
							$i++;
						}
					} 
                }
				if ($filter['in_app'] == "YES"){
					unset($filter['in_app']);
					unset($filter['item_order']);
					$results[] = $filter;
					$count++;
				}
			}
			unset($filters['filter']);
			unset($filters['count']);
			$filters['count'] = $count;
			$filters['filter'] = $results;
			return $filters;
		}
		
		public function get_search_sort_fields() {
			$sort_options = get_option('wovax_sort_fields');
			$sorts = array();
			$i = 0;
			foreach($sort_options as $sort_option){
				$sorts['sort_options'][$i]['title'] = $sort_option['display_name'];
				$sorts['sort_options'][$i]['value'] = $sort_option['value'];
				$i++;
			}
			return $sorts;
		}

		public function add_saved_search() {
			$wovax = new wovax_functions();
			$wovaxapi = new wovaxapi;
			if(is_multisite() && is_null($_GET['site_id'])) {
				$site_id = get_current_blog_id();
			} else if(!is_multisite()) {
				$site_id = 1;
			} else {
				$site_id = $_GET['site_id'];
			}
			$user_id = intval($_GET['user_id']);
			$search_name = $_GET['name'];
			$search_query = $_GET['query'];
			$add = $wovax->add_saved_search($search_name,$search_query,$user_id,$site_id);
			return $add;
		}

		public function remove_saved_search() {
			$wovax = new wovax_functions();
			$wovaxapi = new wovaxapi;
			if(is_multisite() && is_null($_GET['site_id'])) {
				$site_id = get_current_blog_id();
			} else if(!is_multisite()) {
				$site_id = 1;
			} else {
				$site_id = $_GET['site_id'];
			}
			$user_id = intval($_GET['user_id']);
			$search_name = $_GET['name'];
			$search_query = $_GET['query'];
			$remove = $wovax->remove_saved_search($search_name,$search_query,$user_id,$site_id);
			return $remove;
		}

		public function get_saved_searches() {
			$wovax = new wovax_functions();
			$wovaxapi = new wovaxapi;
			$user_id = intval($_GET['user_id']);
			if(is_multisite() && is_null($_GET['site_id'])) {
				$site_id = get_current_blog_id();
			} else if(!is_multisite()) {
				$site_id = 1;
			} else {
				$site_id = $_GET['site_id'];
			}
			if(is_null($user_id)) {
				return $wovaxapi->return_error(
					"'user_id' variable not set",
					"Try again using the id of the desired user in the 'user_id' variable"
				);
			}
			$saved_searches = $wovax->get_saved_searches($user_id,$site_id);
			return array(
				'searches' => $saved_searches
			);
		}

		private function get_queried_filters($query) {
			global $wpdb;
			$filters = array();
			foreach ($query as $index => $needle) {
				if (!is_numeric($index)) {
					return array("error" => "$index is not a valid variable type");
				}else {
					$sql = "SELECT `no`,`post_type`,`custom_field`,`display_name`,`type` FROM " . $wpdb->prefix . "wovaxapp_search_filter WHERE no=".$index;
					$filter = $wpdb->get_results($sql,ARRAY_A);
					$filter[0]['query'] = $needle;
					$filters[] = $filter[0];
				}
			}
			return $filters;
		}
	}
