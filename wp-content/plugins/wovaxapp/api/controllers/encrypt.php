<?php

  class wovaxapi_encrypt_controller {

    public function encrypt_data() {
      $wovaxapi = new wovaxapi;

      if (isset($_GET['data'])) {
        return array(
          "data_to_encrypt" => $_GET['data'],
          "encrypted_data" => $wovaxapi->encrypt($_GET['data'],$_GET['secret']),
        );
      }else {
        $wovaxapi->return_error(
          "'data' variable not set",
          "Try again using the data variable"
        );
      }
    }

    public function decrypt_data() {
      $wovaxapi = new wovaxapi;

      if (isset($_GET['data'])) {
        return array(
          "data_to_decrypt" => $_GET['data'],
          "decrypted_data" => $wovaxapi->decrypt($_GET['data'],$_GET['secret']),
        );
      }else {
        $wovaxapi->return_error(
          "'data' variable not set",
          "Try again using the data variable"
        );
      }
    }

  }

?>
