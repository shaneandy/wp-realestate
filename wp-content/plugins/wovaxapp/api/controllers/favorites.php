<?php

use SimpleFavorites\Entities\Favorite\SyncSingleFavorite;
use SimpleFavorites\Entities\User\UserFavorites;
use SimpleFavorites\Entities\User\UserRepository;

class wovaxapi_favorites_controller {

	public function add() {
		$wovaxapi = new wovaxapi;
		$user_id = intval($_GET['user_id']);
		$post_id = intval($_GET['post_id']);
		if(is_multisite() && is_null($_GET['site_id'])) {
			$site_id = get_current_blog_id();
		} else if(!is_multisite()) {
			$site_id = 1;
		} else {
			$site_id = $_GET['site_id'];
		}
		$user = new UserRepository;
		if(empty($user_id)) {
			$favorites = $user->getAllFavorites($site_id);
			$user_id = get_current_user_id();
		} else {
			$favorites = $user->getFavorites($user_id);
		}
		foreach($favorites as $key => $site_favorites){
			if ( $site_favorites['site_id'] !== $site_id ) continue;
			if ( in_array($post_id,$site_favorites['posts']) ) return true;
			$favorites[$key]['posts'][] = $post_id;
		}
		$add = update_user_meta( $user_id, 'simplefavorites', $favorites );
		return $add;
	}

	public function remove() {
		$wovaxapi = new wovaxapi;
		$user_id = intval($_GET['user_id']);
		$post_id = intval($_GET['post_id']);
		if(is_multisite() && is_null($_GET['site_id'])) {
			$site_id = get_current_blog_id();
		} else if(!is_multisite()) {
			$site_id = 1;
		} else {
			$site_id = $_GET['site_id'];
		}
		$user = new UserRepository;
		if(empty($user_id)) {
			$favorites = $user->getAllFavorites($site_id);
			$user_id = get_current_user_id();
		} else {
			$favorites = $user->getFavorites($user_id);
		}
		foreach($favorites as $key => $site_favorites){
			if ( $site_favorites['site_id'] !== $site_id ) continue;
			foreach($site_favorites['posts'] as $k => $fav){
				if ( $fav == $post_id ) unset($favorites[$key]['posts'][$k]);
			}
		}
		$remove = update_user_meta( $user_id, 'simplefavorites', $favorites );
		if($remove !== true) {
			return 'Post ID was not a favorite';
		}
		return $remove;
	}


	public function get_user_favorites() {
		$wovaxapi = new wovaxapi;
		$user_id = $_GET['user_id'];
		if(is_null($user_id)) {
			return $wovaxapi->return_error(
				"'user_id' variable not set",
          		"Try again using the id of the desired user in the 'user_id' variable"
			);
		}
		if(is_multisite() && is_null($_GET['site_id'])) {
			$site_id = get_current_blog_id();
		} else if(!is_multisite()) {
			$site_id = 1;
		} else {
			$site_id = $_GET['site_id'];
		}
		$favorites = new UserFavorites($user_id, $site_id, $links = false);
		//get post titles
		$post_ids = $favorites->getFavoritesArray();
		$favorites_array = array();
		if(!is_array($post_ids[0])) {
			foreach($post_ids as $post_id) {
				$array = array(
					'title' => get_the_title($post_id),
					'post_id' => $post_id
				);
				$favorites_array[] = $array;
			}
		}
		return array(
			'favorites' => $favorites_array
		);
	}

}