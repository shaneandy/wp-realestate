<?php

class wovaxapi_nearby_post_map_controller {
	
	public function get_posts(){
		$lat 		= $this->safeRequest('lat', 0);
		$long 		= $this->safeRequest('long', 0);
		$post_type 	= $this->safeRequest('type', 'post');
		$limit 		= $this->safeRequest('limit', 100);
		return $this->get_nearest_posts($lat, $long, $post_type, $limit);
	}
	
	private function get_nearest_posts($lat, $long, $post_type, $limit){
		global $wpdb;
		$sql = "SELECT `ID` AS `id`, `post_title` AS `title`, `a`.`meta_value` AS `geo_latitude`, `b`.`meta_value` AS `geo_longitude` FROM `".$wpdb->posts."` ";
		$sql .= "LEFT JOIN `".$wpdb->postmeta."`  AS `a` ON `a`.`post_id` = `id` AND `a`.`meta_key` = 'geo_latitude' ";
		$sql .= "LEFT JOIN `".$wpdb->postmeta."`  AS `b` ON `b`.`post_id` = `id` AND `b`.`meta_key` = 'geo_longitude' ";
		$sql .= "LEFT JOIN `".$wpdb->postmeta."`  AS `c` ON `c`.`post_id` = `id` AND `c`.`meta_key` = 'geo_enabled' ";
		$sql .= "WHERE `a`.`meta_key` IS NOT NULL ";
		$sql .= "AND `b`.`meta_key` IS NOT NULL ";
		$sql .= "AND `c`.`meta_key` IS NOT NULL ";
		$sql .= "AND `c`.`meta_value` = 1 ";
		$sql .= "AND `post_type` = '%s';";
		//$sql .= "LIMIT %d; ";
		$sql = $wpdb->prepare($sql, $post_type);
		$posts = $wpdb->get_results($sql, ARRAY_A);
		$posts_with_dist = array();
		foreach($posts as $index => $post){
			$post['distance_km'] = $this->haversine_distance($lat, $long, $post['geo_latitude'], $post['geo_longitude']);
			$posts_with_dist[] = $post;
			unset($posts[$index]);
		}
		unset($posts);
		usort($posts_with_dist, function($a, $b){
			return intval(round($a['distance_km'] - $b['distance_km'], 0));
		});
		return array_slice($posts_with_dist, 0, $limit);
	}

	private function haversine_distance($lat, $long, $lat2, $long2, $decimals = 5) {
		// Calculate the distance in degrees
		$degrees = rad2deg(acos((sin(deg2rad($lat)) * sin(deg2rad($lat2))) + (cos(deg2rad($lat)) * cos(deg2rad($lat2)) * cos(deg2rad($long-$long2)))));
		$distance = $degrees * 111.13384; // 1 degree ~ 111.13384 km
		return round($distance, 5);
	}

	private function getAllPostWithLatLongOnly(){
		$wovaxapi = new wovaxapi;
		$allArray = array();
		$catID = $_GET['cat'];
		$post_type = array_key_exists('type',$_GET) ? $_GET['type'] : 'post';
	
		if(!array_key_exists('limit',$_GET) || $_GET['limit'] < 1){
			$number_posts = 1000; //A good baseline to keep the function from bogging down.
		} else {
			$number_posts = $_GET['limit'];
		}
		
		if($catID == "") {
			$args = array(
				'numberposts'      => 1000, //$number_of_posts,
				'post_type'        => $post_type,
				'post_status'      => 'publish',
				'suppress_filters' => true,
			);
		} else {
			$args = array(
				'numberposts'      	=> 1000, //$number_of_posts,
				'post_type'        	=> $post_type,
				'cat_id'			=> $catID,
				'post_status'      	=> 'publish',
				'suppress_filters' 	=> true,
			);
		}
		$wx_post_ids = $this->getPostIds($post_type, $number_posts);
		if(!empty($wx_post_ids)){
			foreach($wx_post_ids as $post_id){
				$location = get_post_meta($post_id, 'geo_enabled', TRUE);
				if($location == 1) {
					$allArray[] = array(
						"id"			=>	$post_id,
						"title"			=>	get_the_title($post_id),
						"geo_latitude"	=>	get_post_meta($post_id, 'geo_latitude', TRUE),
						"geo_longitude"	=>	get_post_meta($post_id, 'geo_longitude', TRUE)
					);
				}
			}
			return array("status"=>"ok","posts"=>$allArray);
		} else {
			return array("status"=>"NO POSTS","posts"=>$allArray);
		}
		
		/*
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();
				$postid = get_the_ID();
				$location = get_post_meta($postid, 'postMapEnable', TRUE);
				if($location == 1) {
					array_push($allArray,array(
					"id"=>$postid,
					"title"=>get_the_title(),
					"geo_latitude"=>get_post_meta($postid, 'geo_latitude', TRUE),
					"geo_longitude"=>get_post_meta($postid, 'geo_longitude', TRUE)
					));
				}
			endwhile;

			return array("status"=>"ok","posts"=>$allArray);

		else :
			return array("status"=>"NO POSTS","posts"=>$allArray);
		endif;
		*/
	}

	private function findNearMe($currentLat,$currentLong,$radius,$catID){
		$wovaxapi = new wovaxapi;
		$allArray = array();
		if(empty($catID) || !$catID) {
			query_posts( 'posts_per_page=1000&');
		} else {
			query_posts( 'posts_per_page=1000&cat='.$catID.'');
		}
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();
				$postid = get_the_ID();
				$location = get_post_meta($postid, 'postMapEnable', TRUE);
				if($location == 1) {
					if($this->compareDataWithGoogle($currentLat,$currentLong,get_post_meta($postid, 'geo_latitude', TRUE),get_post_meta($postid, 'geo_longitude', TRUE),$radius) == TRUE) {

						$decoded = $this->googleApifindDistanceByLatLong($currentLat,$currentLong,get_post_meta($postid, 'geo_latitude', TRUE),get_post_meta($postid, 'geo_longitude', TRUE));

						array_push($allArray,array(
							"id"=>$postid,
							"title"=>get_the_title(),
							"geo_latitude"=>get_post_meta($postid, 'geo_latitude', TRUE),
							"geo_longitude"=>get_post_meta($postid, 'geo_longitude', TRUE),
							"distance"=>$decoded['rows'][0]['elements'][0]['distance']['text']
						));
					}
				}
			endwhile;

			return array("status"=>"ok","posts"=>$allArray);

		else :
			return array("status"=>"NO POST","posts"=>$allArray);
		endif;

	}

	private function googleApifindDistanceByLatLong($currentLat,$currentLong,$desLat,$desLog) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://maps.googleapis.com/maps/api/distancematrix/json?origins=".$currentLat.",".$currentLong."&destinations=".$desLat.",".$desLog."&mode=car&sensor=false");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);


		$decoded = json_decode($output,true);

		return $decoded;

	}


	private function compareDataWithGoogle($currentLat,$currentLong,$desLat,$desLog,$radius) {
		$decoded = $this->googleApifindDistanceByLatLong($currentLat,$currentLong,$desLat,$desLog);
		if($decoded['rows'][0]['elements'][0]['status'] == "ok") {
			if($decoded['rows'][0]['elements'][0]['distance']['value'] <= $radius) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	//returns an array of post ids - note that this just gets the first $number_posts number of posts, so you won't see everything.
	private function getPostIds($post_type,$number_posts){
		global $wpdb;
		if($number_posts < 1){
			return array();
		}
		$sql = "Select `ID` from `".$wpdb->posts."` where `post_type` = '%s' LIMIT %d;";
		$sql = $wpdb->prepare($sql,$post_type,$number_posts);
		$all_posts = $wpdb->get_results($sql, ARRAY_A);
		$post_ids	= [];
		foreach($all_posts as $post){
			$post_ids[] = $post['ID'];
		}
		return $post_ids;
	}
	
	//
	private function safeRequest($arg, $def = ''){
		return array_key_exists($arg,$_REQUEST) ? $_REQUEST[$arg] : $def;
	}	
}
?>