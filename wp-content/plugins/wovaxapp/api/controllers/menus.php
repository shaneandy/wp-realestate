<?php

  class wovaxapi_menus_controller {

    public function get_menu() {
      if (isset($_GET['menu_id'])) {
        $menu = wp_get_nav_menu_items($_GET['menu_id']);
        $return = array(
          "count" => sizeof($menu),
          "menu_id" => $_GET['menu_id'],
          "menu" => $menu,
        );
        return $return;
      }else {
        $wovaxapi = new wovaxapi;
        $wovaxapi->return_error(
          "'menu_id' variable not set",
          "Try again using the id, name, or slug of the desired post in the 'menu_id' variable"
        );
      }
    }

  }

?>
