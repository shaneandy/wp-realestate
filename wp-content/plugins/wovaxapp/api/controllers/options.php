<?php

class wovaxapi_options_controller {

	// Get WordPress option settings from the options table, e.g. the blog name, see http://codex.wordpress.org/Option_Reference
    public function get_options() {
        if ($_GET['wp_options'] != "") {
          $options = explode(",",$_GET['wp_options']);
          $return = array();
          if (isset($_GET['json']) && $_GET['json'] == "true") {
            foreach ($options as $option) {
              $return[$option] = json_decode(get_option($option));
            }
          }else {
            foreach ($options as $option) {
              $return[$option] = get_option($option);
            }
          }
        }else {
          $return = array(
            "error" => "Include 'options' var in your request, options comma separated. See for a list of options: http://codex.wordpress.org/Option_Reference"
          );
        }
        return $return;
    }

}

?>
