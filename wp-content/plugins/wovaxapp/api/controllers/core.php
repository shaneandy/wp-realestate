<?php

class wovaxapi_core_controller {

	public function info() {
		$wovaxapi = new wovaxapi;
		$controllers = $wovaxapi->list_controllers();
		return array(
			'Wovax API Version' => WOVAX_APP_VERSION,
			'controllers' => $controllers
		);
	}

	public function get_nonce() {
		global $json_api;
		extract($json_api->query->get(array('controller', 'method')));
		if ($controller && $method) {
			$controller = strtolower($controller);
			if (!in_array($controller, $json_api->get_controllers())) {
				$json_api->error("Unknown controller '$controller'.");
			}
			require_once $json_api->controller_path($controller);
			if (!method_exists($json_api->controller_class($controller), $method)) {
				$json_api->error("Unknown method '$method'.");
			}
			$nonce_id = $json_api->get_nonce_id($controller, $method);
			return array(
				'controller' => $controller,
				'method' => $method,
				'nonce' => wp_create_nonce($nonce_id)
			);
		} else {
			$json_api->error("Include 'controller' and 'method' vars in your request.");
		}
	}
	
	public function get_recent_posts() {
		require_once WOVAX_APP_DIR."/library/wovaxapp_post.php";
		$wovaxapp_post = new wovaxapp_post;
		$args = $wovaxapp_post->get_query_data();
		$posts = wp_get_recent_posts($args);
		return $wovaxapp_post->format_posts($posts,$args);
	}

	public function get_posts() {
		require_once WOVAX_APP_DIR."/library/wovaxapp_post.php";
		$wovaxapp_post = new wovaxapp_post;
		$args = $wovaxapp_post->get_query_data();
		//$posts = get_posts($args);
		$posts = new WP_Query($args);
		$args['count_total'] = $posts->found_posts;
		$args['page_total'] = $posts->max_num_pages;
		return $wovaxapp_post->format_posts($posts->posts,$args);
	}

	public function get_post() {
		if (isset($_GET['id'])) {
			require_once WOVAX_APP_DIR."/library/wovaxapp_post.php";
			$wovaxapp_post = new wovaxapp_post;
			return $wovaxapp_post->format_posts(array(get_post($_GET['id'])));
		}else {
			$wovaxapi = new wovaxapi;
			$wovaxapi->return_error("'id' variable not set","Try again using the ID if the desired post in the 'id' variable");
		}
	}

	public function get_page() {
		return $this->get_post();
	}

	public function get_pages() {
		require_once WOVAX_APP_DIR."/library/wovaxapp_post.php";
		$wovaxapp_post = new wovaxapp_post;
		$args = $wovaxapp_post->get_query_data(array("post_type"=>"page"));
		$posts = get_pages($args);
		return $wovaxapp_post->format_posts($posts,$args);
	}

	public function get_categories() {
		return array(
			"categories" => array_values(get_categories()),
		);
	}

	public function multi_query() {
		$wxapi = new wovaxapi;
		{ // checks input
			if (empty($_GET)) {
				$wxapi->return_error(
					"empty variable array",
					array(
						"please structure your variables in a  fashon",
						'/?controller1=method1,method2&controller2=method3'
					)
				);
			}else {
				if (isset($_GET['core'])) { // did user try to call itself?
					if (in_array("multi_query",explode(",",$_GET['core']))) {
						$wxapi->return_error(
							"stop that",
							"you know what you did..."
						);
					}
				}
				if (isset($_GET['args'])) {
					$args = json_decode(stripslashes($_GET['args']),true);
					unset($_GET['args']);
				}
			}
		}

		$results = array();
		foreach ($_GET as $class => $methods) {
			$class_info = $wxapi->check_controller($class);
			$methods = explode(",",$methods);
			if ($class_info["controller_exists"]) {
				foreach ($methods as $method) {
					$path = $class_info["path"];
					$name = $class_info["class_name"];
					if (isset($args[$class][$method])) {
						foreach ($args[$class][$method] as $k => $v) {
							$_GET[$k] = $v;
						}
						$results[$class][$method] = $wxapi->execute($path,$method,$name);
						foreach ($args[$class][$method] as $k => $v) {
							unset($_GET[$k]);
						}
					}else {
						$results[$class][$method] = $wxapi->execute($path,$method,$name);
					}
				}
			}else {
				$results[$class][$method] = "class not found";
			}
		}
		return $results;
	}

}

?>
