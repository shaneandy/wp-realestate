<?php

class wovaxapi_posts_controller {

  public function create_post() {
    $wovaxapi = new wovaxapi;
    $auth = $wovaxapi->return_class("auth");
    $auth->validate_session($auth);
    $session = $auth->get_current_session();

    $wovaxapi->check_variables(array(
      "post_title",
      "post_content",
      "post_status",
    ));

    $args = array(
      "post_title" => $_GET['post_title'],
      "post_content" => $_GET['post_content'],
      "post_status" => $_GET['post_status'],
      "post_author" => $session["user_id"]
    );

    $result = wp_insert_post($args);

    if ($result == 0) {
      $wovaxapi->return_error(
        "there was an error in creating the post",
        "please try again later"
      );
    }

    return array(
      'post' => $result
    );
  }

  public function update_post() {
    $wovaxapi = new wovaxapi;
    $auth = $wovaxapi->return_class("auth");
    $auth->validate_session($auth);

    $wovaxapi->check_variables(array(
      "ID",
      "post_title",
      "post_content",
      "post_status"
    ));

    $args = array(
      'ID' => $_GET['ID'],
      'post_title' => $_GET['post_title'],
      'post_content' => $_GET['post_content'],
      'post_status' => $_GET['post_status']
    );

    $status = wp_update_post($args);
    if ($status == 0) {
      $wovaxapi->return_error(
        "an error occured when trying to update that post",
        "try again later"
      );
    }else {
      return array(
        "post" => $status,
      );
    }
  }

  public function delete_post() {
    $wovaxapi = new wovaxapi;
    $auth = $wovaxapi->return_class("auth");
    $auth->validate_session($auth);

    $wovaxapi->check_variables(array(
      "ID",
      "force_delete"
    ));

    $status = wp_delete_post($_GET['ID'],$_GET['force_delete']);

    if ($status != false) {
      return array();
    }else {
      $wovaxapi->return_error(
        "deleting the post failed",
        "an unknown error has occured, double check the post ID"
      );
    }
  }

  public function search_meta() {
    global $wpdb;

    $all_sql = array();
    $result = array();
    foreach ($_GET as $name => $value) {
      $sql = "SELECT * FROM ".$wpdb->prefix."postmeta WHERE `meta_key` = '".$name."' AND `meta_value` != 'null'";
      $output = $wpdb->get_results($sql,ARRAY_A);
    }

    $post_id = array();
    for ($i=0;$i<sizeof($output);$i++) {
      if (!in_array($output[$i]['post_id'],$post_id)) {
        $post_id[] = $output[$i]['post_id'];
      }
    }

    $post_keep = array();
    for ($i=0;$i<sizeof($post_id);$i++) {
      $sql = "SELECT * FROM ".$wpdb->prefix."postmeta WHERE `post_id` = '".$post_id[$i]."'";
      $post_meta = $wpdb->get_results($sql,ARRAY_A);
      for ($j=0;$j<sizeof($post_meta);$j++) {
        foreach ($_GET as $name => $value) {
          $haystack = json_decode($post_meta[$j]['meta_value']);
          if ($post_meta[$j]['meta_key'] == $name && in_array($value,$haystack)) {
            $post_keep[] = $post_meta[$j]['post_id'];
          }
        }
      }
    }

    $post_keep = array_unique($post_keep);
    $posts = array();
    for ($i=0;$i<sizeof($post_keep);$i++) {
      $sql = "SELECT post_type FROM ".$wpdb->prefix."posts WHERE `ID` = '".$post_keep[$i]."'";
      $type = $wpdb->get_results($sql,ARRAY_A);
      $url = get_option('siteurl')."?json=get_post&post_id=".$post_keep[$i]."&post_type=".$type[0]['post_type'];
      $json = json_decode(file_get_contents($url));
      $posts[] = $json->post;
    }

    $url = explode("?",$_SERVER['REQUEST_URI']);
    $query = array(
      "path" => $url[0],
      "query" => $url[1],
    );
    $result = array(
      "count" => sizeof($posts),
      "count_total" => sizeof($posts), // implement pages later
      "pages" => 1,
      "posts" => $posts,
      "url" => $query
    );
    return $result;
  }

}

?>
