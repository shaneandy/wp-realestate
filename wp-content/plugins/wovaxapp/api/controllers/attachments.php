<?php

class wovaxapi_attachments_controller {

  public function get_attachments() {
    if (isset($_GET["parent"])) {
      $attachments = get_attached_media('',$_GET["parent"]);
      $return = array();
      foreach ($attachments as $attachment) {
        $attachment->meta = wp_get_attachment_metadata($attachment->ID);
        $attachment->guid = wp_get_attachment_url($attachment->ID);
        $return[] = $attachment;
      }
      return array(
        "count" => sizeof($attachments),
        "attachments" => $return
      );
    }else {
      $wovaxapi = new wovaxapi;
      $wovaxapi->return_error(
        "The 'parent' variable is not set",
        "Set the 'parent' variable to a valid post ID and try again"
      );
    }
  }

}

?>
