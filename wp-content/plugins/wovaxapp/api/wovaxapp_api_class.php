<?php

	class wovaxapi {

		public function api_called() {
			$uri = $this->getUri();
			if (substr($uri,0,1) == "/") {
				$uri = substr($uri,1);
			}
			$uri_exploded = explode("/",$uri);
			foreach($uri_exploded as $uri) {
				if ($uri == "wovaxapi"){
					return true;
				}
			}
			return false;
		}

		public function check_controller($class) {
			if (empty($class)) {
				return array(
					"controller_exists" => false
				);
			}

			$controller_paths = array_values(array_filter(explode(",",get_option("wovax_api_controller_paths"))));
			$class_name = "wovaxapi_".$class."_controller";
			$filename = $class.".php";
			$controller_exists = false;

			for ($i=0;$i<sizeof($controller_paths);$i++) {
				$files = scandir($controller_paths[$i]);
				if (in_array($filename,$files)) {
					$controller_exists = true;
					include_once WOVAX_APP_DIR."/api/controllers/".$filename;
					$class = new $class_name;
					$methods = get_class_methods($class);
					$path = WOVAX_APP_DIR."/api/controllers/".$filename;
				}
			}

			if ($controller_exists && $methods != NULL) {
				return array(
					"controller_exists" => true,
					"class_name" => $class_name,
					"path" => $path,
					"methods" => $methods,
				);
			}else {
				return array(
					"controller_exists" => false
				);
			}
		}

		public function check_variables($vars) {
			$missing = array();
			foreach ($vars as $var) {
				if (!isset($_GET[$var])) {
					$missing[] = $var;
				}
			}
			if (!empty($missing)) {
				$this->return_error(
					"please set all required variables",
					array(
						"required" => $vars,
						"missing" => $missing
					)
				);
			}
		}

		public function controller_exists($controller) {
			$controller = strtolower($controller);
			if (in_array($controller,$this->list_controllers())) {
				return true;
			} else {
				return false;
			}
		}

		public function decrypt($data, $secret) {
			$data = str_replace(" ","+",$data);
			//Generate a key from a hash
			$key = md5(utf8_encode($secret), true);
			//Take first 8 bytes of $key and append them to the end of $key.
			$key .= substr($key, 0, 8);
			$data = base64_decode($data);
			$data = mcrypt_decrypt('tripledes', $key, $data, 'ecb');
			$block = mcrypt_get_block_size('tripledes', 'ecb');
			$len = strlen($data);
			$pad = ord($data[$len-1]);
			return substr($data, 0, strlen($data) - $pad);
		}

		public function encrypt($data, $secret) {
			//Generate a key from a hash
			$key = md5(utf8_encode($secret), true);
			//Take first 8 bytes of $key and append them to the end of $key.
			$key .= substr($key, 0, 8);
			//Pad for PKCS7
			$blockSize = mcrypt_get_block_size('tripledes', 'ecb');
			$len = strlen($data);
			$pad = $blockSize - ($len % $blockSize);
			$data .= str_repeat(chr($pad), $pad);
			//Encrypt data
			$encData = mcrypt_encrypt('tripledes', $key, $data, 'ecb');
			return base64_encode($encData);
		}

		public function execute($path,$function,$class_name) {
			include_once $path;
			$class = new $class_name;
			if (in_array($function,$this->list_methods($class))) {
				return $class->$function();
			}else {
				return "method not found";
			}
		}

		public function getUri() {
			//$parsed_url = parse_url($this->getUrl());
			//return $parsed_url['path'];
			return $_SERVER["REQUEST_URI"];
		}

		public function getUrl() {
			if (isset($_SERVER["HTTPS"])) {
				if ($_SERVER["HTTPS"] != "on") {
					$url = 'http://'.$_SERVER["SERVER_NAME"];
				}else {
					$url = 'https://'.$_SERVER["SERVER_NAME"];
				}
			}else {
				$url = 'http://'.$_SERVER["SERVER_NAME"];
			}
			if (!(($_SERVER["SERVER_PORT"] == 80) || ($_SERVER["SERVER_PORT"] == 443))) {
				$url .= ":".$_SERVER["SERVER_PORT"];
			}
			$url .= $_SERVER["REQUEST_URI"];
			return $url;
		}

		public function return_class($class) {
			if ($this->controller_exists($class)) {
				$this->include_class($class);
				$class_name = "wovaxapi_".$class."_controller";
				$return = new $class_name;
				return $return;
			} else {
				$this->return_error(
					"Controller not found",
					array(
						"Make sure there is an '".$class.".php' file in a valid controller path",
						"Make sure said controller has a class named 'wovaxapi_".$class."_controller'"
					)
				);
			}
		}

		public function list_controllers() {
			$paths = explode(",",get_option("wovax_api_controller_paths"));
			$controllers = array();
			foreach ($paths as $path) {
				if($path != ''){
					$controllers= array_merge($controllers,scandir($path));
				}   
			}
			$controllers = array_values($controllers);
			$list = array();
			foreach ($controllers as $controller) {
				if ($controller != "." && $controller != "..") {
					$list[] = str_replace(".php","",$controller);
				}
			}
			return $list;
		}

		public function list_methods($class) {
			return get_class_methods($class);
		}

		public function include_class($class) {
			if ($this->controller_exists($class)) {
				$controller_info = $this->check_controller($class);
				include_once $controller_info["path"];
			}
		}

		public function return_content($content,$kill = true) {
			if (!empty($content)) {
				if (is_array($content) || is_object($content)) {
					echo json_encode(
						array_merge(array("status"=>"ok"),(array)$content)
					);
				}else {
					echo json_encode(
						array(
							"status" => "ok",
							"content" => $content
						)
					);
				}
			}
			/*if ($kill) {
				$this->kill_conn();
			}*/
		}

		public function return_error($reason,$explain = array()) {
			echo json_encode(
				array(
					"status" => "error",
					"error" => $reason,
					"suggestions" => $explain
				)
			);
			die();
		}

		public function setup_conn() {
			status_header( 200 );
			global $wp_query;
			$wp_query->is_404 = false;
			header("Content-Type: application/json");
			/*ob_end_clean();
			header("Connection: close");
			ignore_user_abort();
			ob_start();*/
		}

		public function kill_conn() {
			$size = ob_get_length();
			header("Content-Length: $size");
			ob_end_flush(); //needed
			flush(); //needed
		}

	}

?>
