=== Wovax Universal Plugin ===
Contributors: Wovax
Tags: API, Wovax, App, 
Requires at least: 3.3
Tested up to: 4.3.1

== Description ==
This plugin creates a link between your WordPress website and a mobile app 
(available on both Android and Apple app stores) that Wovax will create for you.
It also adds a custom search functionality, and several widgets.

== Installation ==
To install  the plugin, follow these steps:
 * Purchase the plugin from wovax.com
 * Download the plugins *.zip file from the provided link
 * From the plugins menu on your website, upload the *.zip file
 * Activate the plugin
 * On the license activation page, enter your email address and the license key that was provided upon purchase.

== Frequently Asked Questions ==
None yet.

== Changelog ==
* 2.4.4
	1. Fixes some minor css styling issues.
	2. Adds several design options.
	3. Adds several app menu options.
	4. Changes default json api base from 'api' to 'json_api' for compatibility.
	5. Adds a featured image column to all post types except media.
* 2.4.3
	1. Adds date_query modifier 'days_ago' to the recent posts shortcode.
	2. Fixes an issue where custom ranges were not saving correctly.
* 2.4.2
	1. Updating the plugin updating script to allow the update modal to display correctly.
	2. Changes to the search sort JSON values again.
	3. Adjusts mobile CSS.
	4. Adds nearby post map controller.
* 2.4.1
	1. Changes to sort field JSON return values.
	2. Fixes an issue with the wovaxrecent shortcode.
* 2.4.0
	1. Adds the ability to change default search sorting
	2. Adds the ability to have external templates for some display code, 
		which allows per-client modifications outside the update cycle.
	3. Removes non-numeric variable unsetting in some display code.
* 2.3.11
	1. Fixes a minor syntax error.
* 2.3.10
	1. Fixes wovaxrecent shortcode meta query functionality.
	2. Fixes duplicate entries in 'existing' dropdowns.
	3. Improvements to speed and efficiency.
* 2.3.9
	1. Adds a custom posts per page option for search result display.
	2. Adds tinymce support to textarea box in translations page.
	3. Updates default terms and conditions.
	4. Fixes bugs in json display for api.
* 2.3.8
	1. Adds range options to the search controller for the app.
	2. Fixes several display bugs.
	3. Fixes a bug with push notification deletion.
* 2.3.7
	1. Fixes an issue with table creation on activation.
* 2.3.6
	1. Adds a Range dropdown option in the search filters.
	2. Fixes for Android gallery and comments features.
	3. Visual tweaks to admin pages.
	4. Clarifications for admin UI elements.
* 2.3.5
	1. Fixes push notification api requests from the app
* 2.3.4
	1. Fixes issues with wovaxapi and directory sites.
	2. Adds a section for Android Maps API Key
	3. Fixes an issue with Android category subscription.
* 2.3.3
	1. Fixes issues with wovaxapi and SSL.
* 2.3.2
	1. Visual changes to admin pages.
	2. Reintegrates comment api into the wovaxapi structure.
	3. Fixes a bug.
* 2.3.1.1
	1. Reverting changes that caused unexpected errors in some circumstances.
* 2.3.1
	1. Changes and Fixes to search module
	2. Tweaks to visual styling.
	3. Fixes to push notifications.
	4. Removes outdated code references.
* 2.3.0.1
	1. Minor patch with superficial updates.
* 2.3.0
	1. Added Search Sorting capability.
* 2.2.1.3
	1. Fixed an issue with ‘existing’ type searches.
* 2.2.1.2
	1. Fixed thumbnail output in JSON API
 * 2.2.1.1
 	1. Tweaks to JSON output to improve speed
	2. Added ability to get categories, tags, and arbitrary custom taxonomies via the JSON API.
 * 2.2.1
 	1. Added this readme file
	2. Tweaks to JSON API functionality.
	3. Speed increases.
	4. Efficiency increases.
 * 2.2.0.2
 	1. Bug fixes
 * 2.2.0.1
 	1. Bug Fixes
 * 2.2.0
 	1. Rewritten JSON API
	2. Faster and more accurate search results returned to app.
	3. Tweaks to Wovax admin menu UX.
	4. Speed increases.
 * 2.1.1
 	1. Bug fixes
 * 2.1
 	1. Fixed bug in search function
 * 2.0
 	1. Added custom search functionality
	2. Revised interface.
	3. Speed increases.
 * 1.2
 	1. Bug fixes.
	2. Added licence activation and updating.
 * 1.0
	1.  Initial release