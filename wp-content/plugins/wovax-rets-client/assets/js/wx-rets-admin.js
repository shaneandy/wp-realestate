jQuery(document).ready(function($) {
	//Function for uploading the IDX logo on the Settings page
	$(document).on('click', '.wx-idx-upload', function(event) {
        event.preventDefault();
        console.log('idx');
        var correct = $(this).parent().children('.wovax-image-url');
        var image = wp.media({
            title: 'Upload IDX Logo',
            // mutiple: true if you want to upload multiple files at once
            multiple: false
        }).open()
        .on('select', function(e){
            // This will return the selected image from the Media Uploader, the result is an object
            var uploaded_image = image.state().get('selection').first();
            // Output to the console uploaded_image
            console.log(uploaded_image);
            // We convert uploaded_image to a JSON object to make accessing it easier
           	var image_url = uploaded_image.toJSON().url;
            // Let's assign the url value to the input field
            $(correct).val(image_url);
        });
    });
    
    //function for carousel widget form.
	$(document).on('change', '.wx-carousel-form-display', function() {
        recent = $('.wx-carousel-recent');
		category = $('.wx-carousel-category');
		if ($(this).val() === 'recent') {
			category.hide('slow');
			recent.show('slow');
		} else if ($(this).val() === 'category_display') {
			recent.hide('slow');
			category.show('slow');
		}
    });
	
	//Function for saving property gallery type
	$('#wovax-property-gallery-type').change(function(){
		var type = $('#wovax-property-gallery-type').val();
		
		var data = {
			action: 'wx_rets_client_property_gallery_save',
			data: type
		}
		
		$.post(ajaxurl, data, function(response) {
      		console.log(response);
    	});
	});
	
	//Function for making field display sortable
    $('#wx-above-content-sortable, #wx-below-content-sortable, #wx-hidden-content-sortable').sortable({
       connectWith: ".wx-sortable-fields",
       placeholder: "wx-rets-placeholder",
       stop: function(event, ui){
           saveDisplayFields();
       }
    }).disableSelection();
    
    function saveDisplayFields(){
        var above = $('#wx-above-content-sortable li').map(function(){
			return $(this).attr('data-field')	
		}).get();
        var below = $('#wx-below-content-sortable li').map(function(){
			return $(this).attr('data-field')	
		}).get();
        var hidden = $('#wx-hidden-content-sortable li').map(function(){
			return $(this).attr('data-field')	
		}).get();
        
        console.log(above);
        console.log(below);
        
        data = {
            action: 'wx_field_order_save',
            data: {
                above: above,
                below: below,
                hidden: hidden
            }
        }
        
         $.post(ajaxurl, data, function(response) {
            console.log(response);
          });    
        
    }
    
    //Function for title and image overlay field sortable
    $('#wx-unused-sortable, #wx-rets-title-modifier, #wx-rets-featured-field').sortable({
       cancel: ".wx-rets-existing-title",
       connectWith: ".wx-sortable-fields",
       placeholder: "wx-rets-placeholder",
       stop: function(event, ui){
           saveTitleImageOverlay();
       }
    }).disableSelection();
    
    function saveTitleImageOverlay(){
        var title = $('#wx-rets-title-modifier li').map(function(){
			return $(this).attr('data-field')	
		}).get();
        var featured = $('#wx-rets-featured-field li').map(function(){
			return $(this).attr('data-field')	
		}).get();
        var unused = $('#wx-unused-sortable li').map(function(){
			return $(this).attr('data-field')	
		}).get();
        
        data = {
            action: 'wx_rets_title_image_save',
            data: {
                title: title,
                featured: featured,
                unused: unused
            }
        }
        
         $.post(ajaxurl, data, function(response) {
            console.log(response);
          });
    }
	
	/*
	*
	* THE IMPORT STATUS PAGE
	*
	*/
	
	//Triggers the Halt function
	$(document).on('click','#wovax-rets-halt-button.wovax-halt', function(event){
		event.preventDefault();
		$('#wovax-rets-halt-button').prop('disabled', true);
		
		$('#wovax-rets-halt-section .spinner').addClass('is-active');
		data = {
            action: 'wx_rets_activate_halt',
            data: 'halt'
        }
        
        $.post(ajaxurl, data, function(response) {
            console.log(response);
			if(response === 'Success!'){
				$('#wovax-rets-halt-button').addClass('wovax-unhalt');
				$('#wovax-rets-halt-button').removeClass('wovax-halt');
				$('#wovax-rets-halt-button').val('Resume Importing');
			} else {
				alert('The operation could not be halted. Please try again.');
			}
			$('#wovax-rets-halt-section .spinner').removeClass('is-active');
			$('#wovax-rets-halt-button').prop('disabled', false);
        });
	});
	
	//Triggers the UnHalt function
	$(document).on('click','#wovax-rets-halt-button.wovax-unhalt', function(event){
		event.preventDefault();
		$('#wovax-rets-halt-button').prop('disabled', true);
		
		$('#wovax-rets-halt-section .spinner').addClass('is-active');
		data = {
            action: 'wx_rets_deactivate_halt',
            data: 'unhalt'
        }
        
        $.post(ajaxurl, data, function(response) {
            console.log(response);
			if(response === 'Success!'){
				$('#wovax-rets-halt-button').addClass('wovax-halt');
				$('#wovax-rets-halt-button').removeClass('wovax-unhalt');
				$('#wovax-rets-halt-button').val('Halt Importing');
			} else {
				alert('The operation could not be resumed. Please try again.');
			}
			$('#wovax-rets-halt-section .spinner').removeClass('is-active');
			$('#wovax-rets-halt-button').prop('disabled', false);
        });
	});
	
	/*
	*
	* THE ADVANCED SETTINGS TAB
	*	ON THE RETS SETTINGS PAGE
	*/
	
	//Turns the preserve featured function on
	$(document).on('click','#wovax-rets-preserve-button.wovax-preserve-on', function(event){
		event.preventDefault();
		$('#wovax-rets-preserve-button').prop('disabled', true);
		
		$('#wovax-rets-preserve-featured-section .spinner').addClass('is-active');
		data = {
            action: 'wx_rets_activate_preserve_featured',
            data: true
        }
        
        $.post(ajaxurl, data, function(response) {
            console.log(response);
			if(response === 'Success!'){
				$('#wovax-rets-preserve-button').addClass('wovax-preserve-off');
				$('#wovax-rets-preserve-button').removeClass('wovax-preserve-on');
				$('#wovax-rets-preserve-button').val('Turn Preserve Featured Off');
			} else {
				alert('The operation could not be completed. Please try again.');
			}
			$('#wovax-rets-preserve-featured-section .spinner').removeClass('is-active');
			$('#wovax-rets-preserve-button').prop('disabled', false);
        });
	});
	
	//Turns the preserve featured function 
	$(document).on('click','#wovax-rets-preserve-button.wovax-preserve-off', function(event){
		event.preventDefault();
		$('#wovax-rets-preserve-button').prop('disabled', true);
		
		$('#wovax-rets-preserve-featured-section .spinner').addClass('is-active');
		data = {
            action: 'wx_rets_deactivate_preserve_featured',
            data: false
        }
        
        $.post(ajaxurl, data, function(response) {
            console.log(response);
			if(response === 'Success!'){
				$('#wovax-rets-preserve-button').addClass('wovax-preserve-on');
				$('#wovax-rets-preserve-button').removeClass('wovax-preserve-off');
				$('#wovax-rets-preserve-button').val('Turn Preserve Featured On');
			} else {
				alert('The operation could not be completed. Please try again.');
			}
			$('#wovax-rets-preserve-featured-section .spinner').removeClass('is-active');
			$('#wovax-rets-preserve-button').prop('disabled', false);
        });
	});
	
	// Default Image upload modal
	$(document).on('click', '.wx-rets-default-upload', function(event) {
        event.preventDefault();
        var correct = $(this).parent().children('.wx-rets-default-image-url');
        var image = wp.media({
            title: 'Upload IDX Logo',
            // mutiple: true if you want to upload multiple files at once
            multiple: false
        }).open()
        .on('select', function(e){
            // This will return the selected image from the Media Uploader, the result is an object
            var uploaded_image = image.state().get('selection').first();
            // Output to the console uploaded_image
            console.log(uploaded_image);
            // We convert uploaded_image to a JSON object to make accessing it easier
           	var image_url = uploaded_image.toJSON().url;
            // Let's assign the url value to the input field
            $(correct).val(image_url);
        });
    });
	
	// Default image save
	$('#wovax-rets-save-default-image-url').click(function(event){
		event.preventDefault();
		$('#wovax-rets-save-default-image-url').prop('disabled', true);
		var image = $('#wovax_rets_default_property_image').val();
		$('#wovax-rets-default-image-section .spinner').addClass('is-active');
		
		data = {
            action: 'wx_rets_save_default_image',
            data: image
        }
        
        $.post(ajaxurl, data, function(response) {
            console.log(response);
			if(response === 'Success!'){
				console.log('Image URL Saved');
			} else {
				alert('The operation could not be completed. Please try again.');
			}
			$('#wovax-rets-default-image-section .spinner').removeClass('is-active');
			$('#wovax-rets-save-default-image-url').prop('disabled', false);
        });
	});
	
	// Save server key
	$('#wovax-rets-save-server-key').click(function(event){
		event.preventDefault();
		$('#wovax-rets-save-server-key').prop('disabled', true);
		var key = $('#wovax-rets-server-access-key').val();
		$('#wovax-rets-server-key-section .spinner').addClass('is-active');
		
		data = {
            action: 'wx_rets_save_server_key',
            data: key
        }
        
        $.post(ajaxurl, data, function(response) {
            console.log(response);
			if(response === 'Success!'){
				console.log('Server Key Saved');
			} else {
				alert('The operation could not be completed. Please try again.');
			}
			$('#wovax-rets-server-key-section .spinner').removeClass('is-active');
			$('#wovax-rets-save-server-key').prop('disabled', false);
        });
	});
	
	// Save server URL
	$('#wovax-rets-save-server-url').click(function(event){
		event.preventDefault();
		$('#wovax-rets-save-server-url').prop('disabled', true);
		var url = $('#wovax-rets-server-url').val();
		$('#wovax-rets-server-url-section .spinner').addClass('is-active');
		
		data = {
            action: 'wx_rets_save_server_url',
            data: url
        }
        
        $.post(ajaxurl, data, function(response) {
            console.log(response);
			if(response === 'Success!'){
				console.log('Server URL saved.');
			} else {
				alert('The operation could not be completed. Please try again.');
			}
			$('#wovax-rets-server-url-section .spinner').removeClass('is-active');
			$('#wovax-rets-save-server-url').prop('disabled', false);
        });
	});
	
	//Save Property User
	$('#wovax-rets-property-user-select').change(function(){
		var user = $('#wovax-rets-property-user-select').val();
		$('#wovax-rets-property-user-select').prop('disabled', true);
		$('#wovax-rets-user-section .spinner').addClass('is-active');
		
		data = {
			action: 'wx_rets_save_property_user',
			data: user
		}
		
		$.post(ajaxurl, data, function(response) {
			if(response === 'Success!'){
				console.log(response);
			} else {
				console.log(response);
				alert('The operation could not be completed. Please try again.');
			}
			$('#wovax-rets-user-section .spinner').removeClass('is-active');
			$('#wovax-rets-property-user-select').prop('disabled', false);
		});
	});

	//Turn On White Border Removal 
	$(document).on('click','#wovax-rets-remove-borders-button.wx-rets-remove-turn-on',function(event) {
		event.preventDefault();
		$('#wovax-rets-remove-borders-button').prop('disabled', true);
		
		$('#wovax-rets-remove-borders-section .spinner').addClass('is-active');
		data = {
            action: 'wx_rets_borders_on',
            data: 'on'
        }
        
        $.post(ajaxurl, data, function(response) {
            console.log(response);
			if(response === 'Success!'){
				$('#wovax-rets-remove-borders-button').addClass('wx-rets-remove-turn-off');
				$('#wovax-rets-remove-borders-button').removeClass('wx-rets-remove-turn-on');
				$('#wovax-rets-remove-borders-button').val('Turn Off');
			} else {
				alert('The operation could not be completed. Please try again.');
			}
			$('#wovax-rets-remove-borders-section .spinner').removeClass('is-active');
			$('#wovax-rets-remove-borders-button').prop('disabled', false);
        });
	});
	
	//Turn Off White Border Removal
	$(document).on('click','#wovax-rets-remove-borders-button.wx-rets-remove-turn-off',function(event){
		event.preventDefault();
		$('#wovax-rets-remove-borders-button').prop('disabled', true);
		
		$('#wovax-rets-remove-borders-section .spinner').addClass('is-active');
		data = {
            action: 'wx_rets_borders_off',
            data: 'off'
        }
        
        $.post(ajaxurl, data, function(response) {
            console.log(response);
			if(response === 'Success!'){
				$('#wovax-rets-remove-borders-button').addClass('wx-rets-remove-turn-on');
				$('#wovax-rets-remove-borders-button').removeClass('wx-rets-remove-turn-off');
				$('#wovax-rets-remove-borders-button').val('Turn On');
			} else {
				alert('The operation could not be completed. Please try again.');
			}
			$('#wovax-rets-remove-borders-section .spinner').removeClass('is-active');
			$('#wovax-rets-remove-borders-button').prop('disabled', false);
        });
	});
	
	/*
	*
	* THE ALIASED FIELDS TAB
	*	ON THE RETS SETTINGS PAGE
	*/
	
	//SAVE THE FIELDS
	$('#wovax-rets-save-aliased-field-names').click(function(event){
		event.preventDefault();
		$('#wovax-rets-save-aliased-field-names').prop('disabled', true);
		$('#wovax-rets-save-aliased-fields-section .spinner').addClass('is-active');
		var fields = $('#wovax-rets-field-alias-table > tbody > tr > th').map(function(){
			return $(this).text();
		}).get();
		var aliases = $('#wovax-rets-field-alias-table > tbody > tr > td > .wx-rets-field-aliases').map(function(){
			return $(this).val();
		}).get();
		console.log(fields);
		console.log(aliases);
		
		data = {
			action: 'wx_rets_save_field_aliases',
			data: {
				fields: fields,
				aliases: aliases
			}
		}
		
		$.post(ajaxurl, data, function(response) {
			console.log(response);
			$('#wovax-rets-save-aliased-fields-section .spinner').removeClass('is-active');
			$('#wovax-rets-save-aliased-field-names').prop('disabled', false);
		});
	});
});