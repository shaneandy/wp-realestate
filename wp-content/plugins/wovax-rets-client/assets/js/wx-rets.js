jQuery(document).ready(function($) {
	//Instantiates instances of the Preview Gallery carousel.
	$("#preview-gallery").touchCarousel({
        /* carousel options go here see Javascript Options section for more info */
		itemsPerPage: 1,
		itemsPerMove: 4,
      	scrollbar: false,
		scrollbarAutoHide: true,
		scrollbarTheme: "dark",
		pagingNav: false,
		snapToItems: false,
		scrollToLast: false,
		useWebkit3d: false,
		loopItems: true,
		directionNav: true	
    });
	
	$('.property-gallery').click(function() {
		var src = $(this).children().attr('data-src');
        var srcset = $(this).children().attr('data-srcset');
		$('.wp-post-image').attr('src',src);
        $('.wp-post-image').attr('srcset',srcset);
	});
	
	//New Slick carousel
	$('.fullsize-gallery').slick({
		adaptiveHeight: false,
		arrows: true,
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		speed: 300,
	});
	
	//Instantiates instances of the Featured gallery carousel.
	$("#featured-carousel").touchCarousel({
        /* carousel options go here see Javascript Options section for more info */
		itemsPerPage: 1,
		itemsPerMove: 1,
      	scrollbar: false,
		scrollbarAutoHide: true,
		scrollbarTheme: "dark",
		pagingNav: false,
		snapToItems: false,
		scrollToLast: false,
		useWebkit3d: false,
		loopItems: true,
		directionNav: true	
    });
});