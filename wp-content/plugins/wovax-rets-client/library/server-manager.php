<?php

Class RetsServer{
	private $key = "";
	private $domain = "";
	private $key_opt = 'wovax_rets_key';
	private $domain_opt = 'wovax_rets_server_domain';
	private $halt_opt = 'wovax_rets_halted';
	private $cat_opt = 'wovax_rets_categories';
	private $field_cache_opt = 'wovax_rets_field_cache';
	private $categories = array();
	private $max_attempts = 5;
	private static $instance = NULL;//The current instance of the manager.
	/**
	* The constructor prepares this class for use.
	*/
	public function  __construct (){
		$key = get_option($this->key_opt);
		$domain = get_option($this->domain_opt);
		$halted = get_option($this->halt_opt);
		$categories = get_option($this->cat_opt);
		if($key === FALSE) {
			add_option($this->key_opt, get_rand_str(), '', 'no');
		}
		if($domain === FALSE){
			add_option($this->domain_opt, 'rets.wovax.io', '', 'no');
			$domain = 'rets.wovax.io';
		}
		if($halted === FALSE){
			add_option($this->halt_opt, 'no', '', 'no');
		}
		if($categories === FALSE){
			$categories = array();
			add_option($this->cat_opt, $categories, '', 'no');
		}
		if(!is_array($categories)){
			$categories = array();
			update_option($this->cat_opt, $categories, 'no');
		}
		$this->key			= $key;
		$this->domain		= $domain;
		$this->categories	= $categories;
	}
	/**
	* Tells us if we are halted or not
	*
	* @since 2.0.0b
	*
	* @return bool		True for halted and false for running.
	*/
	public function getHaltStatus(){
		$GLOBALS['wp_object_cache']->delete($this->halt_opt, 'options');//Remove it from options cache.
		 return (get_option($this->halt_opt) === 'yes');
	}
	/**
	* Halts long running operations
	*
	* @since 2.0.0b
	*
	* @return bool		True if we successfully halted. False otherwise.
	*/
	public function halt(){
		return update_option($this->halt_opt, 'yes', 'no');
	}
	/**
	* Unhalts long running operations
	*
	* @since 2.0.0b
	*
	* @return bool		True if we successfully unhalted. False otherwise.
	*/
	public function unhalt(){
		return update_option($this->halt_opt, 'no', 'no');
	}
	/**
	* Get's the current instance of the Server Mananager. This class is a singleton.
	*
	* @since 2.0.0b
	*
	* @return ClientManager The current client manager instance.
	*/
	public static function getInstance() {
		if (is_null(self::$instance)) {
			self::$instance = new self;
		}
		return self::$instance;
	}
	/**
	* Gets the current domain for the server.
	*
	* @since 2.0.0b
	*
	* @return string	The domain of the api server.
	*/
	public function getDomain(){
		return $this->domain;
	}
	/**
	* Changes the current server domain.
	*
	* @since 2.0.0b
	*
	* @param string		$domain		The domain of the server.
	* @return bool 					True if option domain has changed, false if not or if update failed. 
	*/
	public function changeDomain($domain){
		if(!is_string($domain)){
			return false;
		}
		$ret = update_option($this->domain_opt, $domain, 'no');
		if($ret){
			$this->domain = $domain;
		}
		return $ret;
	}
	/**
	* Gets the current key.
	*
	* @since 2.0.0b
	*
	* @return  string		The current key.
	*/
	public function getKey(){
		return $this->key;
	}
	/**
	* Changes the current key.
	*
	* @since 2.0.0b
	*
	* @param string		$key		The the new key we wish to use.
	* @return bool 					True if option key has changed, false if not or if update failed. 
	*/
	public function changeKey($key){
		if(!is_string($key)){
			return false;
		}
		$ret = update_option($this->key_opt, $key, 'no');
		if($ret){
			$this->key= $key;
		}
		return $ret;
	}
	/**
	* Gets info about the requested IDs.
	*
	* @since 2.0.0b
	*
	* @param array(str, str,...)		$mls_ids	The list of MLS IDs we want info from.
	* @return array(array(), array()) 				An array propertie arrays.
	*/
	public function getMlsIdInfo($mls_ids, $formatted = FALSE, $retain = FALSE){
		if(empty($mls_ids)){
			return array();
		}
		$ids = implode(',', $mls_ids);
		$data = array(
			'mls_ids'	=> $ids
		);
		$categories = array();
		if($formatted){
			foreach($this->getCategories() as $slug => $field){
				$info = get_term_by('slug', $slug, 'wovax_property_category', ARRAY_A);
				$categories[$field] = array('slug' => $slug, 'id' => intval($info['term_taxonomy_id']));
			}
		}
		$info = $this->callMethod('rets', 'mls_id_info', $data, FALSE);
		if(empty($info['listings']) OR $info === FALSE){
			error_log('Failed to return listing data in '.__FILE__.' on line '.__LINE__);
			return array();
		}
		$listings = array();
		foreach($info['listings'] as $property){
			$mod_prop = array();
			if($formatted){
				$mod_prop['Categories'] = array();
			}
			foreach($property as $field => $val){
				$value = is_string($val) ? trim($val) : $val;
				$field = $this->convert_server_field($field);
				if(wx_empty($value)){
					switch($field){
						case 'Address':
							$value = 'No Address Provided.';
							break;
						case 'Description':
							$value = 'A description has yet to be provided.';
							break;
						case 'Posting_Date':
						case 'Modify_Date':
						case 'Photo_Modify_Date':
							$value = date('Y-m-d H:i:s');
							break;
						default:
							continue 2;//changed to continue 2 since a switch is considered
							//a loop structure in php. 1 to exit the switch and another 1
							//to skip an interation of the for loop.			
							break;
					}
				}
				$mod_prop[$field] = $value;
				if($formatted AND array_key_exists($field, $categories)){
					unset($mod_prop[$field]);
					$str_val = strtolower(strval($value));
					switch($str_val){
						case '1':
						case 'y':
						case 'yes':
						case 'true':
							$slug = $categories[$field]['slug'];
							$id = $categories[$field]['id'];
							$mod_prop['Categories'][$slug] = $id;
							if($slug === 'wx_featured' AND $retain){
								$mod_prop['Categories']['wx_retained'] = $categories['Retained']['id'];
							}
							break;
						default:
							break;
					}
				}
			}
			$mod_prop['geo_latitude'] = floatval($mod_prop['geo_latitude']);
			$mod_prop['geo_longitude'] = floatval($mod_prop['geo_longitude']);
			if($mod_prop['geo_latitude'] === 0.0 AND $mod_prop['geo_longitude'] === 0.0){
				unset($mod_prop['geo_latitude']);
				unset($mod_prop['geo_longitude']);
			} else {
				$mod_prop['geo_public'] = 1;
				$mod_prop['geo_enabled'] = 1;
			}
			$mod_prop['Posting_Date']		= substr(str_replace('T', ' ', $mod_prop['Posting_Date']), 0, 19);
			$mod_prop['Modify_Date']		= substr(str_replace('T', ' ', $mod_prop['Modify_Date']), 0, 19);
			$mod_prop['Photo_Modify_Date']	= substr(str_replace('T', ' ', $mod_prop['Photo_Modify_Date']), 0, 19);
			$mod_prop['wovaxListViewTemplate'] = 'WT-RealEstate';
			$listings[] = $mod_prop;
		}
		return $listings;
	}
	
	public function getModifiedAfter($timestamp){
		$data = array(
			'timestamp' => $timestamp
		);
		$ids = $this->callMethod('rets', 'get_modified_after', $data);
		if($ids === FALSE){
			return array();
		}
		return $ids['ids'];
	}
	
	public function getModifiedPhotosAfter($timestamp){
		$data = array(
			'timestamp' => $timestamp
		);
		$ids = $this->callMethod('rets', 'get_modified_photos_after', $data);
		if($ids === FALSE){
			return array();
		}
		return $ids['ids'];
	}
	/**
	* Gets the number of active properties on the server. 
	*
	* @since 2.0.0b
	*
	* @return int/false				The number of active properties or false for error. 
	*/
	public function getActiveCount(){
		$count = $this->callMethod('rets', 'get_active_count');
		if($count === FALSE){
			return FALSE;
		}
		return intval($count['count']);
	}
	/**
	* Gets the list of active mls_ids on the server. 
	*
	* @since 2.0.0b
	*
	* @return array(str, str, ...)/false				The list of mls_ids or false for error. 
	*/
	public function getActiveMlsIds(){
		$mls_ids = $this->callMethod('rets', 'get_active_mls_ids');
		if($mls_ids === FALSE){
			return array();
		}
		return $mls_ids['ids'];
	}
	/**
	* Gets all the fields this client is using.
	*
	* @since 2.0.0b
	*
	* @return array(str, str, ...) 		An array of field names.
	*/
	public function getAllFields(){
		$fields = $this->callMethod('rets', 'get_fields');
		if($fields === FALSE) {
			$fields = get_option($this->field_cache_opt);
			if($fields === FALSE) {
				return array();
			}
		} else {
			add_option($this->field_cache_opt, $fields, '', 'no');
		}
		//format the fields. 
		$new_info = array();
		foreach($fields as $field){
			$field = $this->convert_server_field($field);
			switch($field){
				case 'Featured':
				case 'Description':
					continue;
					break;
				default:
					$new_info[] = $field;
					break;
			}
		}
		$fields = $new_info;
		sort($fields);
		return $fields;
	}
	/**
	* Gets all the dynamic categories that a client has. 
	*
	* @since 2.0.0b
	*
	* @return array(field => slug, ...) 		A list of the categories.
	*/
	public function getCategories(){
		$categories = $this->callMethod('rets', 'getcategories');
		if($categories === FALSE){
			error_log('Failed to return categories in '.__FILE__.' on line '.__LINE__);
			$categories = array();
		}
		//Add default categories
		if(!array_key_exists('Featured', $categories)){
			$categories['Featured'] = array(
				"field" => "featured",
				"description" => "Give your property prominence."
			);
		}
		if(!array_key_exists('Retained', $categories)){
			$categories['Retained'] = array(
				"field" => "retained",
				"description" => "Retain off market or sold listings on your site."
			);
		}
		//END default categories
		$rets_categories = $this->categories;
		foreach($categories as $title => $info){
			$field = $this->convert_server_field($info['field']);
			$slug = 'wx_'.strtolower($title);
			$description = empty($info['description']) ? '' : $info['description'];
			//check if catergorie exists if not build creature it.
			if(!term_exists($title, 'wovax_property_category')){
				wp_insert_term($title, 'wovax_property_category', array(
						'description'	=> $description,
						'slug'			=> $slug
				));
			}
			$rets_categories[$slug] = $field;
		}
		//@todo remove terms that are not longer in the category list.
		//@todoo then make the releship table is nice and clean.
		$ret = update_option($this->cat_opt, $rets_categories, 'no');
		if($ret !== FALSE){
			$this->categories = $rets_categories;
		}
		return $this->categories;
	}
	
	public function reportError($title, $info){
		$data = array(
			"title" => rawurlencode($title),
			"msg" => rawurlencode($info)
		);
		$info = $this->callMethod('rets', 'get_modified_after', $data);
		if($info === FALSE){
			return array();
		}
	}
	
	/**
	* Get a new nonce.
	*
	* @since 2.0.0b
	*
	* @return str/bool 		The nonce value as a hex string or FALSE for error.
	*/
	public function getNonce(){
		$nonce = '';
		$sleep = (rand() % 5) + 1;
		for($i = 0; $i < $this->max_attempts; $i++){
			$tmp = $this->callUrl("http://".$this->domain."/api/wx_nonce/nonce/");
			if($tmp !== FALSE){
				$nonce = $tmp;
				break;
			}
			sleep($sleep);
		}
		$nonce = json_decode($nonce, TRUE);
		if($nonce['status'] !== 'ok'){
			return FALSE;
		}
		$nonce = $nonce['nonce'];
		if(empty($nonce)){
			return FALSE;
		}
		return $nonce;
	}
	
	private function convert_server_field($field){
		$ret = '';
		switch ($field){
			case 'mls_id':
				$ret = 'MLS_No';
				break;
			case 'latitude':
			case 'longitude':
				$ret = 'geo_'.$field;
				break;
			case 'geo_latitude':
			case 'geo_longitude':
				break; 
			default:
				$ret = ucwords(trim($field), " \t\r\n\f\v_");
				break;
		}
		return $ret;
	}
	
	private function callMethod($api, $method, $data=array(), $public = TRUE){
		$sleep = (rand() % 5) + 1;
		$ret_data = FALSE;
		for($i = 0; $i < $this->max_attempts; $i++){
			$nonce = '';
			if(!$public){
				$tmp = $this->getNonce();
				if($tmp === FALSE){
					sleep($sleep);
					continue;
				}
				$nonce = $tmp;
			}
			$url = $this->buildUrl($api, $method, $data, $nonce);
			$tmp_data = $this->callUrl($url);
			if($tmp_data === FALSE){
				sleep($sleep);
				continue;
			}
			$tmp_data = json_decode($tmp_data, TRUE);
			if($tmp_data['status'] !== 'ok'){
				sleep($sleep);
				continue;
			}
			unset($tmp_data['status']);
			$ret_data = $tmp_data;
			break;
		}
		return $ret_data;
	}
	
	private function callUrl($url, $time_out = 80){
		$old = ini_set('default_socket_timeout', $time_out);
		$return = FALSE;
		$info = file_get_contents($url);
		if($info !== FALSE){
			$return = $info;
		}
		if($old !== FALSE){
			ini_set('default_socket_timeout', $old);
		}
		return $return;
	}
	
	private function buildUrl($api, $method, $data = array(), $nonce = ''){
		$url = "http://".$this->domain;
		$url .= '/api/'.$api;
		$url .= '/'.$method.'/';
		$url_prt = parse_url(site_url());
		$data['identity'] = $url_prt['host'];
		if(!empty($url_prt['path']) AND $url_prt['path'] !== '/'){
			$data['identity'] .= $url_prt['path'];
		}
		if(!empty($nonce)){
			$data['api']		= $api;
			$data['method']		= $method;
			$hash_info = message_sign_assoc_array($this->key, $data, $nonce);
			$data['hash'] = $hash_info['hash'];
			$data['time'] = $hash_info['time'];
			$data['nonce'] = $nonce;
			unset($data['api']);
			unset($data['method']);
		}
		$url_part = "";
		foreach($data as $param => $value){
			$url_part .= empty($url_part) ? '?' : '&';
			$url_part .= $param.'='.$value;
		}
		return $url.$url_part;
	}
}
?>