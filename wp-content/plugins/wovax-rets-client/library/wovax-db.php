<?php
//A simple database wrapper to avoid wordpress core cacheing
//Wordpress leaks memory for long running scripts. 
class WovaxDB {
	private $db_conn = NULL;
	private $mysqli = TRUE;
	private $last_error = 'none';
	public function  __construct($db_handle = ''){
		if(($db_handle instanceof mysqli) OR (get_resource_type($db_handle) === 'mysql link')){
			$this->mysqli = ($db_handle instanceof mysqli);
			$this->db_conn = $db_handle;
		} else {
			throw new Exception('Improper database handle type!', 1);
		}
	}
	
	public function getColumn($query, $column = 0){
		$data = $this->getResults($query);
		if(!is_array($data)){
			return $data;
		}
		if(empty($data)){
			return array();
		}
		$column = array_key_exists($column, $data[0]) ? $column : 0;
		$index = FALSE;
		if(is_int($column)){
			$index = TRUE;
		}
		$column_dat = array();
		foreach($data as $row){
			$value = $index ? current(array_slice($row, $column, 1)) : $row[$column];
			$column_dat[] = $value;
		}
		return $column_dat;
	}
	
	public function getResults($query){
		return $this->mysqli ? $this->mysqli_query($query) : $this->mysql_query($query);
	}
	
	public function getVar($query){
		$data = $this->getResults($query);
		if(!is_array($data)){
			return $data;
		}
		if(empty($data)){
			return NULL;
		}
		$data = current(array_slice($data[0], 0, 1));
		return $data;
	}
	
	public function getRow($query){
		$data = $this->getResults($query);
		if(!is_array($data)){
			return $data;
		}
		if(empty($data)){
			return NULL;
		}
		return $data[0];
	}

	public function escapeString($string){
		if($this->mysqli){
			return $this->db_conn->escape_string($string);
		} 
		return mysql_real_escape_string($string, $this->db_conn);
	}
	
	public function getLastError(){
		return $this->last_error;
	}

	public function selectedDb($db_name){
		$ret = FALSE;
		if($this->mysqli){
			$ret = $this->db_handle->select_db($db_name);
		} else {
			$ret = mysql_select_db($db_name, $this->db_handle);
		}
		return $ret;
	}

	public function getLastErrorMsg(){
		if($this->mysqli){
			return $this->db_handle->error;
		} else {
			return mysql_error($this->db_handle);
		}
		return FALSE;
	}

	public function getLastErrorCode(){
		if($this->mysqli){
			return $this->db_handle->errno;
		} else {
			return mysql_errno($this->db_handle);
		}
		return FALSE;
	}
	
	private function mysqli_query($query){
		$results = $this->db_conn->query($query);
		if($results === FALSE){
			$this->last_error = 'Error description: '.mysqli_error($this->db_conn);
			return FALSE;
		}
		$this->last_error = 'No Errors!';
		$data = array();
		if($results instanceof mysqli_result){
			while($tmp = $results->fetch_array(MYSQLI_ASSOC)){
				$data[] = $tmp;
			}
		} else {
			$data = $results;
		}
		$this->last_error = 'No Errors!';
		return $data;
	}
	private function mysql_query($query){
		//deprecated in 5.5 but a needed as a fall back for older
		//versions.
		$results = mysql_query($query, $this->db_conn);
		if($results === FALSE){
			$this->last_error = 'Error description: '.mysql_error($this->db_conn);
			return FALSE;
		}
		$data = array();
		if(is_resource($results)){	
			while($tmp = mysql_fetch_assoc($results)){
				$data[] = $tmp;
			}
		} else {
			$data = $results;
		}
		$this->last_error = 'No Errors!';
		return $data;
	}
}