<?php

  /**
   * Register a property post type.
   *
   * @link http://codex.wordpress.org/Function_Reference/register_post_type
   *
  **/
  $labels = array(
    'name'               	=> _x( 'Properties', 'post type general name' ),
    'singular_name'      	=> _x( 'Property', 'post type singular name'),
    'menu_name'          	=> _x( 'Properties', 'admin menu'),
    'name_admin_bar'     	=> _x( 'Property', 'add new on admin bar'),
    'add_new'            	=> _x( 'Add New', 'property'),
    'add_new_item'       	=> __( 'Add New Property'),
    'new_item'           	=> __( 'New Property'),
    'edit_item'          	=> __( 'Edit Property'),
    'view_item'          	=> __( 'View Property'),
    'all_items'          	=> __( 'All Properties'),
    'search_items'       	=> __( 'Search Properties'),
    'parent_item_colon'  	=> __( 'Parent Properties:'),
    'not_found'          	=> __( 'No properties found.'),
    'not_found_in_trash' 	=> __( 'No properties found in Trash.')
  );

  $args = array(
    'labels'             	=> $labels,
    'public'             	=> true,
    'publicly_queryable' 	=> true,
    'show_ui'            	=> true,
    'show_in_menu'       	=> true,
    'menu_icon'				=> 'dashicons-admin-multisite',
    'query_var'          	=> true,
    'rewrite'            	=> array( 'slug' => 'wovaxproperty' ),
    'capability_type'    	=> 'post',
    'has_archive'        	=> true,
    'hierarchical'       	=> false,
    'menu_position'      	=> 43,
    'supports'           	=> array( 'title', 'editor', 'thumbnail', 'custom-fields', 'excerpt' ),
    'taxonomies'         	=> array('wovax_property_category')
  );

  register_post_type( 'wovaxproperty', $args );
?>