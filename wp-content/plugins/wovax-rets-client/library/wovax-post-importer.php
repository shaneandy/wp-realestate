<?php
//todo make into a more generic importer
//I also coded this in a hurry more tweaks may nessarcy.
//I also would like to create addtional folders for each property.
Class WovaxPostImporter{
	private $user_id			= 0;
	private $last_import_opt	= 'wovax_rets_last_import';
	private $def_img_url		= '';
	private $post_type			= '';
	//database stuff
	private $post_tbl			= '';
	private $meta_tbl			= '';
	private $term_relations_tbl	= '';
	private $term_tax_tbl		= '';
	private $wxdb				= NULL;
	//where post info is stored
	private $posts				= array();
	private $default_img		= NULL;
	private $remove_white		= FALSE;
	public function  __construct ($post_type, $post_user, $def_img_url = '', $remove_white = false){
		if(!is_bool($remove_white)){
			throw new Exception('Remove white is not a bool '.$remove_white);
		}
		$this->remove_white = $remove_white;
		//check and intilize the import option.
		if(get_option($this->last_import_opt) === FALSE){
			add_option($this->last_import_opt, 0, '', 'no');
		}
		//check for bad data
		if(username_exists($post_user) === FALSE){
			throw new Exception('Bad Username: '.$post_user);
		}
		if(post_type_exists($post_type) === FALSE){
			throw new Exception('Bad Post Type: '.$post_type);
		}
		$this->user_id		= get_user_by('login', $post_user)->ID;
		$this->def_img_url	= $def_img_url;
		$this->post_type	= $post_type;
		//Next set up the database variables
		global $wpdb;
		$this->wxdb					= new WovaxDB($wpdb->dbh);
		$this->post_tbl				= $wpdb->posts;
		$this->meta_tbl				= $wpdb->postmeta;
		$this->term_relations_tbl	= $wpdb->term_relationships;
		$this->term_tax_tbl			= $wpdb->term_taxonomy;
		//set up default image
		$def_img = file_get_contents($this->def_img_url);
		if($def_img === FALSE){
			$this->clean_images();
			throw new Exception('Unabled to download default image at: '.$this->def_img_url);
		}
		$def_img = imagecreatefromstring($def_img);
		$this->default_img = $def_img;
	}
	public function __destruct(){
		if(is_resource($this->default_image)){
			imagedestroy($this->default_image);
		}
	}
	//create a new post that addtionally information can be added too.
	public function enqueuePost($uuid_name, $title, $post_date, $modify_date, $description){
		$uuid 						= wx_get_wovax_UUID($uuid_name);
		$post_data					= array();
		$post_data['uuid_name']		= $uuid_name;
		$post_data['uuid']			= $uuid;
		$post_data['post_id'] 		= 0;
		$post_data['images']		= array();
		$post_data['meta']			= array('postGalleryEnable' => 0);
		$post_data['categories']	= array();
		$post_data['title']			= $this->wxdb->escapeString($title);
		$post_data['description']	= $this->wxdb->escapeString($description);
		$post_data['guid']			= $uuid;
		$post_data['post_date']		= $this->wxdb->escapeString($post_date);
		$post_data['modify_date']	= $this->wxdb->escapeString($modify_date);
		$this->posts[$uuid]			= $post_data;
		return $uuid;
	}
	//adds a meta_field to the specified post with the matching guid
	public function enqueueMeta($uuid, $meta_key, $meta_value){
		if(!array_key_exists($uuid, $this->posts)){
			return FALSE;
		}
		$this->posts[$uuid]['meta'][$meta_key] = $this->wxdb->escapeString($meta_value); 
		return TRUE;
	}
	//adds a category to the specified post with the matching guid
	public function enqueueCategory($uuid, $cat_id){
		if(!array_key_exists($uuid, $this->posts) OR !is_int($cat_id)){
			return FALSE;
		}
		$this->posts[$uuid]['categories'][] = $cat_id;
		return TRUE;
	}
	//takes the guid of and enqued propery and attaches the image
	public function enqueueImage($uuid, $url){
		if(!array_key_exists($uuid, $this->posts)){
			return FALSE;
		}
		$index = count($this->posts[$uuid]['images']);
		if($index >= 1){
			$this->posts[$uuid]['meta']['postGalleryEnable'] = 1;
		}
		$this->posts[$uuid]['images'][] = array('post_id' => 0, 'meta' => array(), 'url' => $url);
		return TRUE;
	}
	//run each phase of insertion
	public function processQueue(){
		$this->insert_posts();
		$this->save_images();
		$this->insert_images();
		$this->insert_meta();
		$this->insert_categories();
		$this->undraft();
		update_option($this->last_import_opt, time(), 'no');
	}
	//add posts to the post table
	private function insert_posts(){
		if(empty($this->posts)){
			return FALSE;
		}
		$sql = 'INSERT INTO `'.$this->post_tbl.'` ';
		$sql .= '(';
		$sql .= '`post_type`,';//set to wovaxproperty
		$sql .= '`post_author`,';
		$sql .= '`post_title`,';
		$sql .= '`post_name`,';//title without spaces
		$sql .= '`post_content`,';
		$sql .= '`post_date`,';
		$sql .= '`post_date_gmt`,';
		$sql .= '`post_modified`,';
		$sql .= '`post_modified_gmt`,';
		$sql .= '`comment_status`,';//set to closed
		$sql .= '`ping_status`,';//set to closed
		$sql .= '`guid`,';//set to mls_id
		$sql .= '`post_status`';//set to draft
		$sql .= ') VALUES ';
		$query_parts = array();
		$uuids = array();
		foreach($this->posts as $uuid => $data){
			$query_str = sprintf(
				"('%s', %d, '%s', '%s', '%s', '%s', '%s', '%s', '%s', 'closed', 'closed', 'urn:uuid:%s', 'draft')",
				$this->post_type,
				$this->user_id,
				$data['title'],
				$this->generate_post_name($data['title'].' '.$data['uuid_name']),
				$data['description'],
				$data['post_date'],
				$data['post_date'],//@todo acount for timezones
				$data['modify_date'],
				$data['modify_date'],//@todo acount for timezones
				$uuid
			);
			$query_parts[] = $query_str;
			$uuids[] = 'urn:uuid:'.$uuid;
		}
		if(empty($query_parts)){
			return array();
		}
		$sql .= implode(', ', $query_parts);
		$ret = $this->wxdb->getResults($sql);
		if($ret === FALSE){
			throw new Exception('Unable to insert posts! uuid_name: '.$data['uuid_name'].' '.$this->wxdb->getLastError());
		}
		$sql = "SELECT `ID` AS `post_id`, `guid` AS `uuid` FROM `".$this->post_tbl."` WHERE `guid` IN ('".implode("','", $uuids)."')";
		$ret = $this->wxdb->getResults($sql);
		if($ret === FALSE){
			throw new Exception('Unable to get post ids! '.$this->wxdb->getLastError());
		}
		foreach($ret as $ids){
			$uuid = str_replace('urn:uuid:', '', $ids['uuid']);
			$this->posts[$uuid]['post_id'] = intval($ids['post_id']);
		}
		return $ret;
	}
	//save the images to disk
	private function save_images(){
		foreach($this->posts as $uuid => $data){
			//then go through image and add it to the query parts array.
			foreach($data['images'] as $index => $img_data){
				$file_name = $this->posts[$uuid]['uuid_name'].'-'.$index;
				$ret = wx_save_all_images($this->post_type.'-images'.'/'.$this->posts[$uuid]['uuid_name'], $file_name, $img_data['url'], 97, $this->remove_white);
				if($ret === FALSE){
					unset($this->posts[$uuid]['images'][$index]);
					//we were unable to save this image
					continue;
				}
				$this->posts[$uuid]['images'][$index]['meta'] = $ret;
			}
			if(empty($data['images'])){
				$file_name = $this->posts[$uuid]['uuid_name'].'-default';
				$ret = wx_save_all_images($this->post_type.'-images'.'/'.$this->posts[$uuid]['uuid_name'], $file_name, $this->default_img, 97, $this->remove_white);
				if($ret === FALSE){
					error_log('Unable to add default image for uuid_name: '.$data['uuid_name']);
					continue;
				}
				$this->posts[$uuid]['images']['default'] = array('post_id' => 0, 'meta' => $ret, 'url' => $this->def_img_url);
			}
		}
	}
	//insert the image post into the database
	private function insert_images(){
		if(empty($this->posts)){
			return FALSE;
		}
		$sql = 'REPLACE INTO `'.$this->post_tbl.'` ';
		$sql .= '(';
		$sql .= '`post_type`,';//set to attachment
		$sql .= '`post_author`,';
		$sql .= '`post_parent`,';
		$sql .= '`post_title`,';//set image {number} for {address}
		$sql .= '`post_name`,';//title without spaces
		$sql .= '`post_date`,';
		$sql .= '`post_date_gmt`,';
		$sql .= '`post_modified`,';
		$sql .= '`post_modified_gmt`,';
		$sql .= '`comment_status`,';//set to closed
		$sql .= '`ping_status`,';//set to closed
		$sql .= '`guid`,';//set to mls_id-{image number}
		$sql .= '`post_status`,';//set to inherit
		$sql .= '`post_mime_type`';//set to image/jpeg
		$sql .= ') VALUES ';
		$query_parts	= array();
		$uuids			= array();
		$uuid_lookup	= array();
		foreach($this->posts as $uuid => $data){
			$post_id	= $data['post_id'];
			$title		= $data['title'];
			$post_date	= $data['post_date'];
			$modify_date= $data['modify_date'];
			//then go through image and add it to the query parts array.
			foreach($data['images'] as $index => $img_data){
				$img_uuid 	= wx_get_wovax_UUID($data['uuid_name'].' img: '.$index);
				$query_str	= sprintf(
					"('attachment', %d, %d, '%s', '%s', '%s', '%s', '%s', '%s', 'closed', 'closed', 'urn:uuid:%s', 'inherit', 'image/jpeg')",
					$this->user_id,
					$post_id,
					'Image '.$index.' for '.$title,
					$this->generate_post_name('Image '.$index.' for '.$title),		
					$post_date,
					$post_date,//@todo acount for timezones
					$modify_date,
					$modify_date,//@todo acount for timezones 
					$img_uuid
				);
				$query_parts[]			= $query_str;
				$uuids[]				= 'urn:uuid:'.$img_uuid;
				$uuid_lookup[$img_uuid]	= array('post_uuid' => $uuid, 'img_index' => $index);
			}
		}
		if(empty($query_parts)){
			error_log('No images to insert');
			return false;
		}
		$sql .= implode(', ', $query_parts);
		$ret = $this->wxdb->getResults($sql);
		if($ret === FALSE){
			$this->clean_images();
			throw new Exception('Unable to insert images! Error: '.$this->wxdb->getLastError());
		}
		$sql = "SELECT `ID` AS `post_id`, `guid` AS `uuid` FROM `".$this->post_tbl."` WHERE `guid` IN ('".implode("','", $uuids)."')";
		$ret = $this->wxdb->getResults($sql);
		if($ret === FALSE){
			$this->clean_images();
			throw new Exception('Unable to get image post ids! Error: '.$this->wxdb->getLastError());
		}
		foreach($ret as $index => $ids){
			$img_post_id	= intval($ids['post_id']);
			$img_uuid 		= str_replace('urn:uuid:', '', $ids['uuid']);
			$post_uuid		= $uuid_lookup[$img_uuid]['post_uuid'];
			$image_index	= $uuid_lookup[$img_uuid]['img_index'];
			//set the default thumbnail image for post
			if(empty($this->posts[$post_uuid]['meta']['_thumbnail_id'])){
				if(($image_index === 0 || $image_index === 'default')){
					$this->enqueueMeta($post_uuid, '_thumbnail_id', $img_post_id);
				}
				//for whatever reason the image does not have an index 0 or a default index so just add one this one
				if((array_key_exists(0, $this->posts[$post_uuid]['images']) === FALSE) && (array_key_exists('default', $this->posts[$post_uuid]['images']) === FALSE)){
					$this->enqueueMeta($post_uuid, '_thumbnail_id', $img_post_id);
				}
			}
			$this->posts[$post_uuid]['images'][$image_index]['post_id'] = $img_post_id;
		}
		return true;
	}
	//start inserting the meta data for the posts and images
	private function insert_meta(){
		if(empty($this->posts)){
			return FALSE;
		}
		$sql = 'INSERT INTO `'.$this->meta_tbl.'` ';
		$sql .= '(';
		$sql .= '`post_id`,';
		$sql .= '`meta_key`,';
		$sql .= '`meta_value`';
		$sql .= ') VALUES ';
		$query_parts = array();
		foreach($this->posts as $uuid => $data){
			if($data['post_id'] === 0){
				//@todo probably log this or maybe should throw exception
				continue;
			}
			//add this property's meta
			foreach($data['meta'] as $key => $value){
				$query_parts[] = sprintf(
					"(%d, '%s', '%s')",
					$data['post_id'],
					$key,
					$value
				);
			}
			//add this property's images
			foreach($data['images'] as $img_data){
				if($img_data['post_id'] === 0){
					//@todo probably log this or maybe should throw exception
					continue;
				}
				$post_id	= $img_data['post_id'];
				$path		= $img_data['meta']['file'];
				$meta		= serialize($img_data['meta']);
				$query_parts[] = sprintf(
					"(%d, '_wp_attached_file', '%s')",
					$post_id,
					$path
				);
				$query_parts[] = sprintf(
					"(%d, '_wp_attachment_metadata', '%s')",
					$post_id,
					$meta
				);
			}		
		}
		if(empty($query_parts)){
			return FALSE;
		}
		$sql .= implode(', ', $query_parts);
		$ret = $this->wxdb->getResults($sql);
		if($ret === FALSE){
			$this->clean_images();
			throw new Exception('Unable to insert meta! Error: '.$this->wxdb->getLastError());
		}
		return TRUE;
	}
	private function insert_categories(){
		if(empty($this->posts)){
			return FALSE;
		}
		$sql = 'INSERT INTO `'.$this->term_relations_tbl.'` ';
		$sql .= '(';
		$sql .= '`object_id`,';
		$sql .= '`term_taxonomy_id` ';
		$sql .= ') VALUES ';
		$query_parts = array();
		$term_tax_ids = array();
		foreach($this->posts as $uuid => $data){
			if($data['post_id'] === 0){
				//@todo probably log this or maybe should throw exception
				continue;
			}
			foreach($data['categories'] as $category_id){
				$query_parts[] = sprintf(
					"(%d, %d)",
					$data['post_id'],
					$category_id
				);
				$term_tax_ids[$category_id] = $category_id;
			}
		}
		if(empty($query_parts)){
			//no categories to insert
			return TRUE;
		}
		$sql .= implode(', ', $query_parts);
		$ret = $this->wxdb->getResults($sql);
		if($ret === FALSE){
			$this->clean_images();
			throw new Exception('Unable to insert term categories! '.$this->wxdb->getLastError());
		}
		//update the term count
		//I tried using wp_update_term_count_now but I could not get it working
		//using this for now.
		foreach ($term_tax_ids as $id) {
			$count = $this->wxdb->getVar('SELECT COUNT(*) FROM `'.$this->term_relations_tbl.'` WHERE term_taxonomy_id = '.$id);
			$sql = "UPDATE `".$this->term_tax_tbl."` SET `count` = ".intval($count)." WHERE `term_taxonomy_id` = ".$id." AND `taxonomy` = 'wovax_property_category'";
			$ret = $this->wxdb->getResults($sql);
			if($ret === FALSE){
				throw new Exception('Failed to update term count! '.$this->wxdb->getLastError());
			}
		}
		return TRUE;
	}
	private function undraft(){
		$sql = "UPDATE ".$this->post_tbl." SET `post_status` = 'publish' WHERE `ID` IN ";
		$query_parts = array();
		foreach($this->posts as $mls_id => $data){
			if($data['post_id'] === 0){
				//@todo probably log this or maybe should throw exception
				continue;
			}
			$query_parts[] = $data['post_id'];
		}
		if(empty($query_parts)){
			return FALSE;
		}
		$sql .= '('.implode(', ', $query_parts).')';
		$ret = $this->wxdb->getResults($sql);
		if($ret === FALSE){
			throw new Exception('Unable to update draft status! '.$this->wxdb->getLastError());
		}
		return TRUE;
	}
	private function clean_images(){
		//@todo implement this
	}
	
	private function generate_post_name($title){
		$name = preg_replace('/[^0-9a-z ]/', '', strtolower($title));
		//                  space here ^
		$name = preg_replace('/\s+/', ' ', trim($name));// makes any double spaces one space
		$name = str_replace(' ', '-', $name);		// replace all spaces with a dash
		return $name;
	}
};

add_filter('wx_on_the_fly_dirs', function ($dir){
	$dir[] = 'wovaxproperty-images';
	return $dir;
});