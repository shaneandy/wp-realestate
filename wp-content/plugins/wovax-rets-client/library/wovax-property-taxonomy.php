<?php
    $labels = array(
        'name'              => _x( 'Property Categories', 'taxonomy general name' ),
		'singular_name'     => _x( 'Property Category', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Categories' ),
		'all_items'         => __( 'All Categories' ),
		'parent_item'       => __( 'Parent Category' ),
		'parent_item_colon' => __( 'Parent Category:' ),
		'edit_item'         => __( 'Edit Category' ),
		'update_item'       => __( 'Update Category' ),
		'add_new_item'      => __( 'Add New Category' ),
		'new_item_name'     => __( 'New Category Name' ),
		'menu_name'         => __( 'Property Category' ),
    );
    $args = array(
        'hierarchical'  => true,
        'labels'        => $labels,  
        'query_var'     => true,
        'rewrite'       => array(
            'slug'          => 'wovax_property_category', // This controls the base slug that will display before each term
            'with_front'    => false // Don't display the category base before 
        )
    );
    
register_taxonomy(  
	'wovax_property_category',//The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces). 
	'wovaxproperty',//post type name
	$args
);
wp_insert_term('Customized', 'wovax_property_category', array(
		'description'	=> 'Property listings that are manually maintained.',
		'slug'			=> 'wx_custom'
));
?>