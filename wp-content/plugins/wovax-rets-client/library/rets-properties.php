<?php
Class RetsProperties{
	private $user = "";
	private $retain = false;
	private $def_image = "";
	private $user_opt = 'wovax_rets_post_user';
	private $retain_opt = 'wovax_rets_auto_retain';
	private $def_img_url_opt = 'wovax_rets_default_image_url';
	private $last_import_opt = 'wovax_rets_last_import';
	private $last_update_opt = 'wovax_rets_last_update';
	private $last_photo_update_opt = 'wovax_rets_last_photo_update';
	private $remove_white_opt = 'wovax_rets_remove_white';
	private static $instance = NULL;//The current instance of the manager.
	private $wxdb;
	/**
	* The constructor prepares this class for use.
	*/
	public function  __construct (){
		global $wpdb;
		$user = get_option($this->user_opt);
		$image = get_option($this->def_img_url_opt);
		$retain = get_option($this->retain_opt);
		$update = get_option($this->last_update_opt);
		$photo_up = get_option($this->last_photo_update_opt);
		if($user === FALSE){
			$user = 'wovax';
			add_option($this->user_opt,	$user, '', 'no');
		}
		if($image === FALSE){
			$image = WX_PLUGIN_FOLDER_URL.'assets/images/HousePlaceholder.png';
			add_option($this->def_img_url_opt, $image, '', 'no');
		}
		if($retain === FALSE){
			$retain = 'no';
			add_option($this->retain_opt, $retain, '', 'no');
		}
		if($update === FALSE){
			add_option($this->last_update_opt, time(), '', 'no');
		}
		if($photo_up === FALSE){
			add_option($this->last_photo_update_opt, time(), '', 'no');
		}
		if(get_option($this->last_import_opt) === FALSE){
			add_option($this->last_import_opt, 0, '', 'no');
		}
		if(get_option($this->remove_white_opt) === FALSE){
			add_option($this->remove_white_opt, 'no', '', 'no');
		}
		//filter var will pass on things like http://localhost so urls without a domain
		//extension may get past. It always excepts urls like ftp://example.com/myfile.txt
		// or ssh://example.com/myproject.proj so lets check the protocol as well.
		//@todo check domain extension
		if((filter_var($image, FILTER_VALIDATE_URL) === FALSE) OR (preg_match('/^https?:\/\//i', $image) !== 1)){
			throw new Exception('Bad default image url!');
		}
		$this->user	= $user;
		$this->def_img = $image;
		$this->retain = ($retain === 'yes');
		$this->wxdb = new WovaxDB($wpdb->dbh);
	}
	/**
	* Get's the current instance of the Server Mananager. This class is a singleton.
	*
	* @since 2.0.0b
	*
	* @return ClientManager The current client manager instance.
	*/
	public static function getInstance() {
		if (is_null(self::$instance)) {
			self::$instance = new self;
		}
		return self::$instance;
	}
	/**
	* Changes post user
	*
	* @since 2.0.0b
	*
	* @param string		$user		The usename of the new user. 
	* @return bool 					True if option deafult post user has changed, false if not or if update failed. 
	*/
	public function changeDefaultUser($user){
		if(!is_string($user)){
			return false;
		}
		if(username_exists($user) === false){
			return false;
		}
		$ret = update_option($this->user_opt, $user, 'no');
		if($ret){
			$this->user = $user;
		}
		return $ret;
	}
	/**
	* Gets the post user.
	*
	* @since 2.0.0b
	*
	* @return  string		The post user.
	*/
	public function getPostUser(){
		return $this->user;
	}
	/**
	* Gets the current deafult image url.
	*
	* @since 2.0.0b
	*
	* @return string	The url of the default image.
	*/
	public function getDefaultImageUrl(){
		return $this->def_img;
	}
	/**
	* Changes deafult image
	*
	* @since 2.0.0b
	*
	* @param string		$url		Url to the new default image
	* @return bool 					True if option deafult iamge url has changed, false if not or if update failed. 
	*/
	public function changeDefaultImageUrl($url){
		if(!is_string($url)){
			return false;
		}
		$ret = update_option($this->def_img_url_opt, $url, 'no');
		if($ret){
			$this->def_img = $url;
		}
		return $ret;
	}
	/**
	* tells us if we want to mark featured properties as retained as well?
	*
	* @since 2.0.0b
	*
	* @return	bool		FALSE if we will not. True if we will.
	*/	
	public function autoRetain(){
		return $this->retain;
	}
	/**
	* Sets the retain value
	*
	* @since 2.0.0b
	*
	* @return	bool		True if we changed the value. False otherwise.
	*/
	public function setRetain($bool){
		global $wpdb;
		if(!is_bool($bool)){
			return false;
		}
		$new_val = $bool ? 'yes' : 'no';
		$ret = update_option($this->retain_opt, $new_val, 'no');
		if($ret){
			$this->retain = $bool;
			if($bool){
				foreach($this->getFeaturedIds() as $post_id){
				wp_set_object_terms($post_id, 'wx_retained', 'wovax_property_category', true);
				}
			} else {     
				foreach($this->getFeaturedAndRetainedIds() as $post_id){
					wp_remove_object_terms($post_id, 'wx_retained', 'wovax_property_category');
				}
			}
		}
		return $ret;
	}
	
	public function setUpdateTimeNow(){
		return update_option($this->last_update_opt, time(), 'no');
	}
	public function getLastUpdateTime(){
		return get_option($this->last_update_opt, 0);
	}
	
	public function setPhotoUpdateTimeNow(){
		return update_option($this->last_photo_update_opt, time(), 'no');
	}
	public function getLastPhotoUpdateTime(){
		return get_option($this->last_photo_update_opt, 0);
	}
	public function removeWhite(){
		return (get_option($this->remove_white_opt) === 'yes');
	}
	public function setRemoveWhite($remove){
		if(!is_bool($remove)){
			return FALSE;
		}
		$val = $remove ? 'yes' : 'no';
		return update_option($this->remove_white_opt, $val, 'no');
	}
	/**
	* Takes an MLS ID and tells you if it exists. If more than one how many exist.
	*
	* @since 2.0.0b
	*
	* @param	str    		$mls_id		The mls id we want to see if a property post exists for.
	* @return	bool/int				FALSE if the property does not exist true if it does. Int if more than one.
	*/	
	public function propertyExists($mls_id){
		global $wpdb;
		$sql = "SELECT COUNT(*) FROM `".$wpdb->postmeta."` LEFT JOIN `".$wpdb->posts."` ON `ID` = `post_id` WHERE `meta_key` = 'MLS_No' AND `meta_value` = %s;";
		$sql = $wpdb->prepare($sql, $mls_id);
		$count = intval($this->wxdb->getVar($sql));
		if($count === 0){
			return FALSE;
		}
		if($count === 1){
			return TRUE;
		}
		return $count;
	}
	/**
	* Gets all ids for objects of the given terms.
	*
	* @since 2.0.0b
	* 
	* $terms	array/string					The terms we wish to grab ids for.
	* $taxonomy string							The taxonomy those terms should belong. Blank means any taxonomy.
	* $junction	string							The valuse should be AND and OR. If the string is somthing else it uses AND.
	* @return	array(post_id, post_id, ...) 	An array of of post_ids
	*/
	function getTermIds($terms, $taxonomy = '', $junction = 'AND'){
		global $wpdb;
		if(!is_string($junction) OR !is_string($taxonomy)){
			return array();
		}
		if(is_string($terms)){
			$terms = array($terms);
		}
		if(!is_array($terms)){
			return array();
		}
		$sql = "SELECT `object_id` FROM `".$wpdb->term_taxonomy."` ";
		$sql .= "LEFT JOIN  `".$wpdb->terms."` ON `".$wpdb->term_taxonomy."`.`term_id` = `".$wpdb->terms."`.`term_id` ";
		$sql .= "LEFT JOIN `".$wpdb->term_relationships."` ON `".$wpdb->term_taxonomy."`.`term_taxonomy_id` = `".$wpdb->term_relationships."`.`term_taxonomy_id` ";
		$sql .= "WHERE `object_id` IS NOT NULL ";
		if(!empty($taxonomy)){
			$sql .= "AND `taxonomy` = '".$taxonomy."' ";
		}
		if(count($terms > 1)){
			$sql .= "GROUP BY `object_id` HAVING sum(`slug` in ('".implode("','", $terms)."')) ";
			$sql .= ($junction === 'OR') ? ">= 1;" : "= ".count($terms).";";
		} else {
			$sql .= "AND `slug` = '".$terms[0]."' GROUP BY `object_id`;";
		}
		$object_ids = $this->wxdb->getColumn($sql);
		if(empty($object_ids)){
			return array();
		}
		return $object_ids;
	}
	/**
	* Gets all featured properties post_ids.
	*
	* @since 2.0.0b
	* 
	* @return	array(post_id, post_id, ...) 		An array of of post_ids
	*/
	function getFeaturedIds(){
		return $this->getTermIds('wx_featured', 'wovax_property_category');
	}
	/**
	* Gets all featured and retained property post_ids.
	*
	* @since 2.0.0b
	* 
	* @return	array(post_id, post_id, ...) 		An array of of post_ids
	*/
	function getFeaturedAndRetainedIds(){
		return $this->getTermIds(array('wx_featured', 'wx_retained'), 'wovax_property_category');
	}
	/**
	* Gets all property post_ids. As long as they have an mls number.
	*
	* @since 2.0.0b
	* 
	* @return	array(post_id, post_id, ...) 		An array of post_ids
	*/
	public function getPostIds(){
		global $wpdb;
		$sql = "SELECT `ID` FROM `".$wpdb->posts."` LEFT JOIN `".$wpdb->postmeta."` ON `post_id` = `ID` WHERE `meta_key` = 'MLS_No' AND `post_type` = 'wovaxproperty';";
		return $this->wxdb->getColumn($sql);
	}
	/**
	* Gets all property mls_ids.
	*
	* @since 2.0.0b
	* 
	* @return	array(mls_id, mls_id, ...) 		An array of mls_ids
	*/
	public function getMlsIds(){
		global $wpdb;
		$sql = "SELECT `meta_value` FROM `".$wpdb->posts."` LEFT JOIN `".$wpdb->postmeta."` ON `post_id` = `ID` WHERE `meta_key` = 'MLS_No' AND `post_type` = 'wovaxproperty';";
		return $this->wxdb->getColumn($sql);
	}
	/**
	* Gets all property post_ids with mls_ids and their mls id
	*
	* @since 2.0.0b
	* 
	* @return	array(array(post_id, mls_id), array(post_id, mls_id), ...) 		An array of ids
	*/
	public function getMlsAndPostIds(){
		global $wpdb;
		$sql = "SELECT `ID` AS `post_id`, `meta_value` AS `mls_id` FROM `".$wpdb->posts."` LEFT JOIN `".$wpdb->postmeta."` ON `post_id` = `ID` WHERE `meta_key` = 'MLS_No' AND `post_type` = 'wovaxproperty';";
		return $this->wxdb->getResults($sql, ARRAY_A);
	}
	/**
	* Gets all property post_ids that do not have mls # postmeta
	*
	* @since 2.0.0b
	* 
	* @return	array(array('ID'=>post_id),array('ID'=>post_id),...) 		An array of ids
	*/
	public function getBadPostIds(){
		global $wpdb;
		$sql =
		"SELECT `a`.`ID` FROM `{$wpdb->posts}`  AS `a`
		LEFT JOIN `{$wpdb->postmeta}`           AS `b` ON `a`.`ID` = `b`.`post_id` AND `b`.`meta_key` = 'MLS_No'
		LEFT JOIN `{$wpdb->term_relationships}` AS `c` ON `a`.`ID` = `c`.`object_id`
		LEFT JOIN `{$wpdb->term_taxonomy}`      AS `d` ON `c`.`term_taxonomy_id` = `d`.`term_taxonomy_id`
		LEFT JOIN `{$wpdb->terms}`              AS `e` ON `d`.`term_id` = `e`.`term_id`
		WHERE `a`.`post_type` = 'wovaxproperty' AND `a`.`post_status` = 'publish' AND `b`.`meta_key` IS NULL AND (`e`.`slug` IS NULL OR `e`.`slug` NOT IN('wx_custom', 'wx_retained'));";
		return $this->wxdb->getResults($sql, ARRAY_A);
	}
	/**
	* Gets all published property post_ids that do not have images attached
	*
	* @since 2.0.0b
	* 
	* @return	array(array('ID'=>post_id),array('ID'=>post_id),...) 		An array of ids
	*/
	public function getNoImagePostIds(){
		global $wpdb;
		$sql = 
			"SELECT `a`.`ID` FROM `{$wpdb->posts}` AS `a`
			LEFT JOIN `{$wpdb->posts}` AS `b` ON `a`.`ID` = `b`.`post_parent`
			AND `b`.`post_type` = 'attachment'
			AND `b`.`post_mime_type` REGEXP '^image/'
			WHERE `b`.`ID` IS NULL AND `a`.`post_type` = 'wovaxproperty' and `a`.`post_status` = 'publish';";
		return $this->wxdb->getResults($sql, ARRAY_A);
	}
	/**
	* Gets the post id tied to an mls number. 0 for no post.
	*
	* @since 2.0.0b
	*
	* @return int		The post id. 0 for no posts.
	*/
	public function getPostId($mls_id){
		global $wpdb;
		$sql = "SELECT `ID` FROM `".$wpdb->posts."` LEFT JOIN `".$wpdb->postmeta."` ON `post_id` = `ID` WHERE `meta_key` = 'MLS_No' AND `post_type` = 'wovaxproperty' AND `meta_value` = '%s';";
		$sql = $wpdb->prepare($sql, $mls_id);
		return intval($this->wxdb->getVar($sql));
	}
	
	function publishProperty($post_id){
		$post_update = array(	
			"ID"			=> $post_id,
			"post_status"	=> "publish"
		);
		// Update the post into the database
		$result = wp_update_post($post_update);
		return ($result !== 0);
	}
	
	public function updateProperty($post_id, $title, $description, $meta = array(), $terms = array(), $modify_date = ''){
		$post = array(
			'ID'           		=> intval($post_id),
			'post_author'		=> get_user_by('login', $this->user)->ID,
			'post_title'		=> $title,
			'post_name' 		=> $title,
			'post_content'		=> $description,
			'meta_input'		=> $this->format_meta($meta, $terms)
		);
		if(!empty($modify_date)){
			$post['post_modified']		= $modify_date;
			$post['post_modified_gmt']	= $modify_date;
		}
		$post_id = wp_update_post($post, TRUE);
		if($post_id instanceof WP_Error){
			return array('status' => FALSE, 'msg' => 'Can not create the post somthing went wrong.', 'data' => $post_id);
		}
		$slugs = $this->get_slugs($meta, $terms);
		wp_set_object_terms($post_id, $slugs, 'wovax_property_category', true);
		return $post_id;
	}
	
	/**
	* Creates a wovax property post.
	*
	* @since 2.0.0b
	*
	* @param string 				$title			The title of the property.
	* @param string 				$description	Optional. The image description.
	* @param array(field => data)	$meta			The meta we wish to add to these post.
	* @param array(term_slug => field, ...)	$terms	The terms and the fields to check to add said term.
	* @return int|array(status, msg, data) 		On success the attachmets id, otherwise array with info.
	*/
	public function createProperty($title, $description, $meta = array(), $terms = array(), $post_date = ''){
		$post = array(	
			'post_author'		=> get_user_by('login', $this->user)->ID,
			'post_status'		=> 'draft',
			'comment_status'	=> 'closed',
			'ping_status' 		=> 'closed',
			'menu_order' 		=> 0,
			'post_title'		=> $title,
			'post_name' 		=> $title,
			'post_content'		=> $description,
			'post_type' 		=> 'wovaxproperty',
			'meta_input'		=> $this->format_meta($meta, $terms)
		);
		if(!empty($post_date)){
			$post['post_date']			= $post_date;
			$post['post_modified']		= $post_date;
			$post['post_date_gmt']		= $post_date;
			$post['post_modified_gmt']	= $post_date;
		}
		$post_id = wp_insert_post($post, TRUE);
		if($post_id instanceof WP_Error){
			return array('status' => FALSE, 'msg' => 'Can not create the post somthing went wrong.', 'data' => $post_id);
		}
		//Add taxonomy info
		$slugs = $this->get_slugs($meta, $terms);
		wp_set_object_terms($post_id, $slugs, 'wovax_property_category', true);
		update_option($this->last_import_opt, time(), 'no');
		return $post_id;
	}
	/**
	* Returns the number of images attached to this property.
	*
	* @since 2.0.0b
	*
	* @return int 			The number of images.
	*/
	public function imageCount($post_id){
		global $wpdb;
		$sql = "SELECT COUNT(*) FROM `".$wpdb->posts."` WHERE `post_type` = 'attachment' AND `post_mime_type` LIKE 'image%' AND `post_parent` = ".$wpdb->prepare('%d', $post_id).";";
		return intval($this->wxdb->getVar($sql));
	
	}
	/**
	* Attaches and array of image urls to the 
	**/
	public function attachImages($post_id, $mls_id, $images = array()){
		if(empty($images)){
			$images = array($this->getDefaultImageUrl());
		}
		$counter = 0;
		$failure = 0;
		foreach($images as $image_url){
			$ret = $this->attachImage($post_id, $image_url, 'Image '.$counter.' for '.get_the_title($post_id), array('image_mls_id' => $mls_id));
			if(is_array($ret)){
				$failure++;
			}
			$counter++;
		}
		return $failure;
	}
	/**
	* Download an image from a URL, and attaches it to the property, and enable gallery if more than one image.
	*
	* @since 2.0.0b
	*
	* @param string 			$url			The URL of the image that needs downloaded.
	* @param string 			$description	Optional. The image description.
	* @param array(feild, data)	$meta			The meta we wish to add to these post.
	* @return int|array(status, msg, data) 		On success the attachmets id, otherwise array with info.
	*/
	public function attachImage($post_id, $url, $description = null, $meta = array()) {
		if (!empty($url) AND (filter_var($url, FILTER_VALIDATE_URL) !== FALSE)) {
			require_once(ABSPATH . 'wp-admin/includes/media.php');
			require_once(ABSPATH . 'wp-admin/includes/file.php');
			require_once(ABSPATH . 'wp-admin/includes/image.php');
			$file_array = array();
			$file_array['name'] = sanitize_file_name(basename($url));
			//check file extension
			$file_parts = pathinfo($file_array['name']);
			if(!in_array(strtolower($file_parts['extension']), array("bmp", "gif", "jpeg", "jpg", "png"))){
				return array('status' => FALSE, 'msg' => '$url does not have a valid image extension.', 'data' => $url);
			}
			$file_array['tmp_name'] = download_url($url);//download the file
			if (is_wp_error($file_array['tmp_name'])){// If failed downloading
				return array('status' => FALSE, 'msg' => 'Failed to download the image.', 'data' => $url);
			}
			$id = media_handle_sideload($file_array, $post_id, $description);//attach image.
			if (is_wp_error($id)) {
				unlink($file_array['tmp_name']);//delete the file somthing happend.
				return array('status' => FALSE, 'msg' => "Failed to attach the image:".$url."to post id:".$post_id, 'data' => $id);
			}
			wp_update_post(array("ID" => $id, "post_author" => get_user_by('login', $this->user)->ID), TRUE);
			foreach($meta as $field => $data){
				add_post_meta($id, $field, $data);
			}
			if ($this->imageCount($post_id) === 1){//if this the first image set it as the thumbnail.
				set_post_thumbnail($post_id, $id);
			}
			if($this->imageCount($post_id) > 1){//if we have more than one image enable the gallery.
				update_post_meta($post_id, 'postGalleryEnable', 1);
			}
			return $id;
		}
		//Umm no URL return error.
		return array('status' => FALSE, 'msg' => 'Bad url', 'data' => $url);
	}
	/**
	* Sets all properties without an MLS_No as custom
	*
	* @since 2.0.0b
	*
	* @return 	bool	Did the fetch query succeed.
	*/
	function markCustom(){
		global $wpdb;
		$sql = "SELECT `ID` FROM `".$wpdb->posts."` WHERE `post_type` = 'wovaxproperty' AND (SELECT SUM(IF(`meta_key` = 'MLS_No',1, 0)) AS `MLS_No` FROM `".$wpdb->postmeta."` WHERE `post_id` = `ID`) = 0;";
		$results = $this->wxdb->getColumn($sql);
		$custom_post_ids = $this->getTermIds('wx_custom', 'wovax_property_category');
		$results = array_diff($results, $custom_post_ids);
		foreach($results as $post_id){
			if(!has_term('wx_custom', 'wovax_property_category', $post_id)){
				$ret = wp_set_object_terms($post_id, 'wx_custom', 'wovax_property_category', true);
			}
		}
		return ($results === FALSE) ? FALSE : TRUE;
	}
	/**
	* Gets the linux time stamp of the last property creation.
	*
	* @since 2.0.0b
	*
	* @return int		The linux time stamp of the last created property.
	*/
	public function lastImport(){
		$GLOBALS['wp_object_cache']->delete($this->last_import_opt, 'options');//Remove it from options cache.
		return intval(get_option($this->last_import_opt));
	}
	/**
	* Gets total numbers for various property states.
	*
	* @since 2.0.0b
	*
	* @return int  The count of 
	*/
	public function totals(){
		global $wpdb;
		$sql = "SELECT count(*) AS `total`,";
		$sql .= "SUM(IF(`post_status` = 'publish',1, 0)) AS `published`,";
		$sql .= "SUM(IF(`post_status` = 'draft',1, 0))	AS `drafted`,";
		$sql .= "SUM(IF(`post_status` = 'trash',1, 0))	AS `trashed`,";
		$sql .= "(SELECT `count` FROM `".$wpdb->terms."` LEFT JOIN `".$wpdb->term_taxonomy."` ON `".$wpdb->term_taxonomy."`.`term_id` = `".$wpdb->terms."`.`term_id` WHERE `slug` = 'wx_featured') AS `featured`,";
		$sql .= "(SELECT `count` FROM `".$wpdb->terms."` LEFT JOIN `".$wpdb->term_taxonomy."` ON `".$wpdb->term_taxonomy."`.`term_id` = `".$wpdb->terms."`.`term_id` WHERE `slug` = 'wx_retained') AS `retained`,";
		$sql .= "(SELECT `count` FROM `".$wpdb->terms."` LEFT JOIN `".$wpdb->term_taxonomy."` ON `".$wpdb->term_taxonomy."`.`term_id` = `".$wpdb->terms."`.`term_id` WHERE `slug` = 'wx_custom') AS `custom`,";
		$sql .= "(SELECT COUNT(*) FROM `".$wpdb->term_relationships."` ";
		$sql .= "LEFT JOIN `".$wpdb->posts."` ON `ID` = `object_id` ";
		$sql .= "LEFT JOIN `".$wpdb->postmeta."` ON `post_id` = `object_id` ";
		$sql .= "WHERE `post_type` = 'wovaxproperty' AND ";
		$sql .= "`term_taxonomy_id` = (SELECT `term_taxonomy_id` FROM `".$wpdb->term_taxonomy."` LEFT JOIN `".$wpdb->terms."` ON `".$wpdb->terms."`.`term_id` = `".$wpdb->term_taxonomy."`.`term_id` WHERE `slug` = 'wx_retained') AND ";
		$sql .= "`meta_key` = 'Status' AND `meta_value` = 'Off Market') AS `retained_inactive` ";		
		$sql .= "FROM `".$wpdb->posts."` WHERE `post_type` = 'wovaxproperty';";
		$totals = $wpdb->get_row($sql, ARRAY_A);
		foreach($totals as $key => $val){
			$totals[$key] = intval($val);
		}
		ksort($totals);
		return $totals;
	}
	//this function takes a list of meta and terms array and tells you what slugs the property should
	//be marked with
	private function get_slugs($meta, $terms){
		$slugs = array();
		foreach($terms as $slug => $field){
			if(!array_key_exists($field, $meta)){
				continue;
			}
			$str_val = strtolower(strval($meta[$field]));
			switch($str_val){
				case '1':
				case 'y':
				case 'yes':
				case 'true':
					$add = true;
					break;
				default:
					break;
			}
			if($add){
				$slugs[] = $slug;
				if($slug === 'wx_featured' AND $this->autoRetain()){
					$slugs[] = 'wx_retained';
				}
				
			}
		}	
		return $slugs;
	}
	//creates the proper meta array for a property using terms array and proper defaults.
	private function format_meta($meta = array(), $terms = array()){
		$new_meta = $meta;
		//remove term fields from the meta.
		foreach($terms as $slug => $field){
			if(array_key_exists($field, $new_meta)){
				unset($new_meta[$field]);
			}
		}
		//clean the meta up. 
		foreach($new_meta as $field => $val){
			$value = trim($val);
			if(wx_empty($value)){
				unset($new_meta[$field]);
			}
			$new_meta[$field] = $value;
		}
		//add default fields
		$new_meta['wovaxListViewTemplate']	= 'WT-RealEstate';
		if($this->imageCount($post_id) > 1){
			$new_meta['postGalleryEnable'] 		= 1;
		} else {
			$new_meta['postGalleryEnable'] 		= 0;
		}
		//check geo data	
		if(array_key_exists('geo_latitude', $new_meta) AND array_key_exists('geo_longitude', $new_meta)){
			$new_meta['geo_public']		= 1;
			$new_meta['geo_enabled']	= 1;
		} else {
			$new_meta['geo_public']		= 0;
			$new_meta['geo_enabled']	= 0;
		}
		return $new_meta;
	}
}