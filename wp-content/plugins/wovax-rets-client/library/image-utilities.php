<?php
function wx_crop_image($image, $width, $height, $top_y = 0, $left_x = 0){
	$cropped = imagecreatetruecolor($width, $height);
	imagecopy($cropped, $image, 0, 0, $left_x, $top_y, $width+$left_x, $height+$top_y);
	return $cropped;
}
function wx_clone_image($image){
    $w = imagesx($image);
    $h = imagesy($image);
    $copy = imagecreatetruecolor($w, $h);
    imagecopy($copy, $image, 0, 0, 0, 0, $w, $h);
    return $copy;
}
function wx_crop_white($org_image){
	$image = wx_clone_image($org_image);
	$max_x	= imagesx($image);
	$max_y 	= imagesy($image);
	$left_x = 0;
	$top_y 	= 0;
	$right_x = 0;
	$bot_y 	= 0;
	//imagefilter($image, IMG_FILTER_GRAYSCALE);
	imagefilter($image, IMG_FILTER_CONTRAST, -150);
	//USE THE higly constrasted image to find the edge of white border.
	//find the top y
	for($y = 0; $y < $max_y; $y++){
		for($x = 0; $x < $max_x; $x++){
			$color = imagecolorat($image, $x, $y);
			if($color !== 16777215){
				$top_y = $y;
				break 2;
			}
		}
	}
	//find the bottom y
	for($y = ($max_y-1); $y >= 0; $y--){
		for($x = 0; $x < $max_x; $x++){
			$color = imagecolorat($image, $x, $y);
			if($color !== 16777215){
				$bot_y = $y;
				break 2;
			}
		}
	}
	//find the left x
	for($x = 0; $x < $max_x; $x++){
		for($y = 0; $y < $max_y; $y++){
			$color = imagecolorat($image, $x, $y);
			if($color !== 16777215){
				$left_x = $x;
				break 2;
			}
		}
	}
	//find right x
	for($x = ($max_x-1); $x >= 0; $x--){
		for($y = 0; $y < $max_y; $y++){
			$color = imagecolorat($image, $x, $y);
			if($color !== 16777215){
				$right_x = $x;
				break 2;
			}
		}
	}
	$crop_height	= $bot_y - $top_y;
	$crop_width		= $right_x - $left_x;
	$cropped		= imagecreatetruecolor($crop_width, $crop_height);
	imagecopy($cropped, $org_image, 0, 0, $left_x, $top_y, $max_x, $max_y);
	imagedestroy($image);
	return $cropped;
}

function wx_save_image($image, $path, $height = -1, $width = -1, $crop = FALSE, $return_scaled_img = FALSE, $quality = 97, $always_save = FALSE){
	$org_width		= imagesx($image);
	$org_height		= imagesy($image);
	//there must be diffence in dimensions and height and width can't be both less than zero
	if(($org_height !== $height OR $width !== $org_width) AND ($width > 0 OR $height > 0)){
		$crop = !($width < 1 OR $height < 1);//if a one dimension is not specified it will result
		//in the same image as just scaling. Therefore lets overide the crop value to false.
		if($crop){
			$crop_height	= $org_height;
			$crop_width		= $org_width;
			$crop_ar		= $width/$height;
			if($crop_ar > 1 or $crop_ar < 1){
				$possible_height = intval(round($org_width/$crop_ar));
				$possible_width = intval(round($crop_ar*$org_height));
				if($org_height >= $org_width){
					$cmp_1 = $possible_height;
					$cmp_2 = $org_height;
				} else {
					$cmp_1 = $org_width;
					$cmp_2 = $possible_width;
				}
				if($cmp_1 > $cmp_2){
					$crop_width = $possible_width;
				} else {
					$crop_height = $possible_height;
				}
			} else {
				if($org_height >= $org_width){
					$crop_height = $org_width;
				} else {
					$crop_width = $org_height;
				}
			}
			//center the crop
			$top_y		= intval(round(($org_height-$crop_height)/2));
			$left_x		= intval(round(($org_width-$crop_width)/2));
			$image	 	= wx_crop_image($image, $crop_width, $crop_height, $top_y, $left_x);
			$org_width	= imagesx($image);
			$org_height	= imagesy($image);
		}
		$height_scale	= 0;
		$width_scale	= 0;
		//I checked exhaustively each case here to ensure no 0/dimension for scale
		if (($org_width >= $org_height AND $width > 0) OR $height < 1){
			$width_scale = $width/$org_width ;
			$height_scale = $width_scale;
		} else {
			$height_scale = $height/$org_height;
			$width_scale = $height_scale;
		}
		//We are not down sizing the image so lets do nothing
		if($height_scale >= 1){
			if($crop){
				imagedestroy($image);
			}
			return 'larger';
		}
		$new_height	= intval(round($org_height	* $height_scale));
		$new_width	= intval(round($org_width	* $width_scale));
		$scaled_img = imagescale($image, $new_width, $new_height, IMG_BILINEAR_FIXED);
		$ret = imagejpeg($scaled_img, $path, $quality);
		if($crop){
			imagedestroy($image);
		}
		if(!$return_scaled_img){
			imagedestroy($scaled_img);
		}
		if($ret === FALSE){
			imagedestroy($scaled_img);
			return FALSE;
		}
		return $return_scaled_img ? $scaled_img : $ret;
	}
	if($always_save){
		$copy = wx_clone_image($image);
		$ret = imagejpeg($copy, $path, $quality);
		if($ret === FALSE){
			imagedestroy($copy);
			return FALSE;
		}
		return $copy;
	}
	return 'same';
}
function wx_get_image_sizes(){
	global $_wp_additional_image_sizes;
	$sizes = array();
	foreach (get_intermediate_image_sizes() as $_size) {
		if (in_array($_size, array('thumbnail', 'medium', 'medium_large', 'large'))) {
			$sizes[$_size]['width']  = get_option("{$_size}_size_w");
			$sizes[$_size]['height'] = get_option("{$_size}_size_h");
			$sizes[$_size]['crop']   = (bool) get_option( "{$_size}_crop" );
		} elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
			$sizes[ $_size ] = array(
				'width'  => $_wp_additional_image_sizes[ $_size ]['width'],
				'height' => $_wp_additional_image_sizes[ $_size ]['height'],
				'crop'   => $_wp_additional_image_sizes[ $_size ]['crop'],
			);
		}
	}
	return $sizes;
}
function wx_save_all_images($upload_dir, $file_name, $image, $quality = 97, $remove_white = false){
	$image_data 		= NULL;
	//strip various characters out of the file_name and directory path
	$file_name			= preg_replace('/[^0-9a-z\-]/', '', strtolower($file_name));
	$upload_dir			= preg_replace('/[^0-9a-z\/\-\_]/', '', strtolower($upload_dir));
	//strip any leading or trailing slash
	$upload_dir			= preg_replace('/^\//', '', $upload_dir);
	$upload_dir 		= preg_replace('/\/$/', '', $upload_dir);
	//then make the directory a child of the default upload directory
	$wp_upload_dir		= wp_upload_dir()['basedir'];
	$full_upload_dir	= $wp_upload_dir.'/'.$upload_dir;
	$fly_dirs			= apply_filters('wx_on_the_fly_dirs', $dir = array());
	$on_the_fly			= FALSE;
	foreach($fly_dirs as $directory){
		//strip any ending or leading forward slashes
		$directory = preg_replace('/^\//', '', $directory);
		$directory = preg_replace('/\/$/', '', $directory);
		//check if we are in a directory that has on the fly image resizing
		if(preg_match('/^'.preg_quote($wp_upload_dir.'/'.$directory.'/', '/').'/', $full_upload_dir)){
			$on_the_fly = TRUE;
			break;
		}
	}
	//check if our directory exists
	if(!is_dir($full_upload_dir)){
		//sadly it does not so lets make it.
		if(!mkdir($full_upload_dir, 0755, TRUE)){//create and check if created.
			//probably a permission issue
			throw new Exception('Failed to create upload directory: '.$full_upload_dir);
			return FALSE;
		}
	}
	if(!is_resource($image)){
		$img = file_get_contents($image);
		if($img === FALSE){
			//don't throw since this may happen bad url
			error_log('Unable to download image: '.$image);
			return FALSE;
		}
		$image_data = imagecreatefromstring($img);
		if($image_data === FALSE){
			//don't throw since this may happen such as a non-image file
			error_log('Unable to make use of image: '.$image);
			return FALSE;
		}
	} else {
		$image_data = $image;
	}
	$new_file_name		= $file_name.'.jpg';
	$new_file_sav_loc	= $full_upload_dir.'/'.$new_file_name;
	if($remove_white){
		$new_img = wx_crop_white($image_data);
		if(is_resource($new_img)){
			imagedestroy($image_data);
			$image_data = $new_img;
		}
	}
	$ret = imagejpeg($image_data, $new_file_sav_loc, 97);
	if($ret === FALSE){
		throw new Exception('Unable to save image: '.$this->upload_dir.'/'.$new_file_name);
	}
	//@todo pull image meta from the image.
	//@todo for SEO I beleive wordpress uses _wp_attachment_image_alt as way to store alt text for for images.
	$meta = array(
		'file' 			=> $upload_dir.'/'.$new_file_name, 
		'height'		=> imagesy($image_data),
		'width'			=> imagesx($image_data),
		'sizes' 		=> array(),
		'image_meta' 	=> array (
			'aperture'			=> '0',
			'credit'			=> '',
			'camera'			=> '',
			'caption'			=> '',
			'created_timestamp'	=> '0',
			'copyright'			=> '',
			'focal_length'		=> '0',
			'iso'				=> '0',
			'shutter_speed'		=> '0',
			'title'				=> '',
			'orientation' 		=> '0',
			'keywords'			=> array()//does this have the SEO tags that joe was talking about
		)
	);
	foreach(wx_get_image_sizes() as $sz_name => $size_info){
		$height			= $size_info['height'];
		$width			= $size_info['width'];
		$crop			= $size_info['crop'];
		$sz_file_name	= $file_name.'__'.$sz_name.'.jpg';
		$sz_file_sav_loc= $full_upload_dir.'/'.$sz_file_name;
		//image is too small don't bother resizing it. 
		if($meta['height'] <= $height or $meta['width'] <= $width){
			continue;
		}
		//if we are not on the fly let's resize it now.
		//otherwise the on fly hook will handle sizing the image on request
		if(!$on_the_fly){
			$ret = wx_save_image($image_data, $sz_file_sav_loc, $height, $width, $crop);
			if($ret === FALSE){
				throw new Exception('Unable to save image: '.$sz_file_sav_loc);
			}
			if($ret === 'same' or $ret === 'larger'){
				continue;
			}
		}
		$meta['sizes'][$sz_name] = array('file' => $sz_file_name, 'height' => $height, 'width' => $width, 'mime-type' => 'image/jpeg');
	}
	if(!is_resource($image)){
		imagedestroy($image_data);
	}
	return $meta;
}
/*
** This function checks to see if the results of the 404 was a wovax property image
** If what was see if we have the orginal unresized image. Then we resize the image
** set the header to be a jpeg header, and print the image out. Then save the image
** so we don't 404 again and don't have to resize it again. However, if there is no
** default image we or the size does not match one of the existing wordpress sizes
** we just return false and let wordpress handle the 404 logic.
*/
function wx_on_the_fly_images($arg1, $arg2){
	$registerd_dir		= FALSE;
	$directories 		= apply_filters('wx_on_the_fly_dirs', $dir = array());
	$upload_url_base	= parse_url(wp_upload_dir()['baseurl'])['path'];
	$upload_dir_base	= wp_upload_dir()['basedir'];
	$request_uri		= $_SERVER['REQUEST_URI'];
	$upload_url_part	= '';
	$upload_dir_path	= '';
	foreach($directories as $directory){
		//strip any ending or leading forward slashes
		$directory = preg_replace('/^\//', '', $directory);
		$directory = preg_replace('/\/$/', '', $directory);
		//check if we are in a directory that has on the fly image resizing
		if(preg_match('/^'.preg_quote($upload_url_base.'/'.$directory.'/', '/').'/', $request_uri)){
			$upload_url_part = $upload_url_base.'/'.$directory.'/';
			$upload_dir_path = $upload_dir_base.'/'.$directory.'/';
			$registerd_dir = TRUE;
			break;
		}
	}
	if($registerd_dir){
		$sizes		= wx_get_image_sizes();
		$path_parts = pathinfo($request_uri);
		$file_info  = explode('__', $path_parts['filename']);
		$org_name	= strtolower($file_info[0].'.'.$path_parts['extension']);
		$size		= strtolower($file_info[1]);
		$save_loc	= $upload_dir_path.str_replace($upload_url_part, '', $path_parts['dirname'].'/');
		if(array_key_exists($size, $sizes) and file_exists($save_loc.$org_name)){
			$org_img = file_get_contents($save_loc.$org_name);
			if($org_img === FALSE){
				return FALSE;
			}
			$org_img = $image_data = imagecreatefromstring($org_img);
			if($org_img === FALSE){
				return FALSE;
			}
			$width = $sizes[$size]['width'];
			$height = $sizes[$size]['height'];
			$crop = $sizes[$size]['crop'];
			$scaled_img = wx_save_image($org_img, $save_loc.$file_info[0].'__'.$size.'.jpg', $height, $width, $crop, TRUE, 97, TRUE);
			if(!is_resource($scaled_img)){
				return FALSE;
			}
			//var_dump($org_img, $save_loc, $size, $save_loc.$org_name);
			header("Content-Type: image/jpeg");
			imagejpeg($scaled_img, NULL, 97);
			imagedestroy($org_img);
			imagedestroy($scaled_img);
			exit;
		}
	}
	//Let wordpress handle the 404 from here.
	return FALSE;
}
//hook the on the fly image resizing into the pre 404 handle
//basically if all other filer and wordpress post checks failed.
//then we check if we have on the fly resize of an image.
add_filter('pre_handle_404', 'wx_on_the_fly_images', 10, 2);