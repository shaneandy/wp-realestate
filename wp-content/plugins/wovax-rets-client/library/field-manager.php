<?php

Class FieldManager{
	private $field_data_opt = 'wovax_rets_field_data';
	private static $instance = NULL;//The current instance of the manager.
	/**
	* The constructor prepares this class for use.
	*/
	public function  __construct (){
		$this->get_data();
	}
	/**
	* Get's the current instance of the Field Manage. This class is a singleton.
	*
	* @since 2.0.0b
	*
	* @return FieldManager The current field manager instance.
	*/
	public static function getInstance() {
		if (is_null(self::$instance)) {
			self::$instance = new self;
		}
		return self::$instance;
	}	
	
	public function addAbove($field){
		return $this->add_field_case('above', $field);
	}
	
	public function addBelow($field){
		return $this->add_field_case('below', $field);
	}
	
	public function addTitle($field){
		return $this->add_field_case('title', $field);
	}
	
	public function addFeatured($field){
		return $this->add_field_case('featured', $field);
	}
	
	public function getAbove(){
		return $this->get_field_case('above');
	}
	
	public function getBelow(){
		return $this->get_field_case('below');
	}
	
	public function getTitle(){
		return  $this->get_field_case('title');
	}
	
	public function getFeatured(){
		return  $this->get_field_case('featured');
	}
	
	public function getUnusedFields(){
		return $this->get_unused_case('above');
	}
	
	public function getUnusedTitleFields(){
		return $this->get_unused_case('title');
	}
	
	public function getAllFields(){
		$new_data = $this->get_data();
		return $new_data['fields'];
	}
	
	public function unuseField($field){
		return $this->remove_field_case('above', $field);
	}
	
	public function unuseTitleAndFeatured($field){
		return $this->remove_field_case('title', $field);
	}
	
	public function getFieldAlias($field){
		$data = $this->get_data();
		if(!array_key_exists($field, $data['fields'])){
			return '';
		}			
		return  $data['fields'][$field];
	}
	
	public function massSetAbove($fields){
		return $this->mass_field_case('above', $fields);
	}
	
	public function massSetBelow($fields){
		return $this->mass_field_case('below', $fields);
	}
	
	public function massSetTitle($fields){
		return $this->mass_field_case('title', $fields);
	}
	
	public function massSetFeatured($fields){
		return $this->mass_field_case('featured', $fields);
	}
	
	public function massSetFieldAliases($alias_info){
		$new_data = $this->get_data();
		foreach($alias_info as $field => $alias){
			if(!array_key_exists($field, $new_data['fields'])){
				continue;
			}
			$alias = trim($alias);
			if(empty($alias)){
				$alias = $this->converter($field);
			}
			$new_data['fields'][$field] = $alias;
		}
		return update_option($this->field_data_opt, $new_data);
	}
	
	public function setFieldAlias($field, $alias = ''){
		$alias = trim($alias);
		$new_data = $this->get_data();
		if(!array_key_exists($field, $new_data['fields'])){
			return FALSE;
		}
		return update_option($this->field_data_opt, $new_data);
	}
	
	public function loadFields($fields){
		if(!is_array($fields)){
			return array('Status' => FALSE, 'Message' => 'Fields is not an array!');
		}
		if(empty($fields)){
			return array('Status' => TRUE, 'Message' => 'Fields is an empty array!');
		}
		$old_data = $this->get_data();
		$new_data = array(
			'fields' => array(),
			'above' => array(),
			'below' => array(),
			'title' => array(),
			'featured' => array()
		);
		foreach($fields as $field){
			if(!is_string($field)){
				return array('Status' => FALSE, 'Message' => 'An item in the fields array is not a string!');
			}
			if(array_key_exists($field, $old_data['fields']) AND !empty($old_data['fields'][$field])){
				$new_data['fields'][$field] = $old_data['fields'][$field];
			} else {
				$new_data['fields'][$field] = $this->converter($field);
			}
		}
		//now add only existing fields to back to the settings
		foreach($old_data['above'] as $field){
			if(array_key_exists($field, $new_data['fields'])){
				$new_data['above'][] = $field;
			}
		}
		foreach($old_data['below'] as $field){
			if(array_key_exists($field, $new_data['fields'])){
				$new_data['below'][] = $field;
			}
		}
		foreach($old_data['featured'] as $field){
			if(array_key_exists($field, $new_data['fields'])){
				$new_data['featured'][] = $field;
			}
		}
		foreach($old_data['title'] as $field){
			if(array_key_exists($field, $new_data['fields']) OR $field === '__title__'){
				$new_data['title'][] = $field;
			}
		}
		
		if(update_option($this->field_data_opt, $new_data) === FALSE){
			error_log('Failed to update the fields option');
			return array('Status' => FALSE, 'Message' => 'Failed to update the fields option');
		}
		return array('Status' => TRUE, 'Message' => 'Success');
	}
	
	public function getFieldValuesFormatted($post_id, $type = 'above', $formatter = ''){
		if(!is_string($type)){
			return array();
		}
		$type = strtolower($type);
		if(!in_array($type, array('above', 'below', 'title', 'featured'))){
			return array();
		}
		$fields = $this->get_field_case($type);
		$ret = array();
		foreach($fields as $field => $alias){
			$value = '';
			if($type === 'title' AND $field === '__title__'){
				$value = get_the_title($post_id);
			} else {
				$value = get_post_meta($post_id, $field, TRUE);
			}
			if($value === FALSE){
				continue;
			}
			$value = is_string($value) ? trim($value) : $value;
			if(wx_empty($value)){
				continue;
			}
			switch($field){
				case 'Price':
					if(is_numeric($value)){
						$value = "$".number_format($value);
					}
					break;
				case 'MLS_No':
					$value = "#".$value;
				default:
					break;
			} 
			if(!empty($formatter) AND is_callable($formatter)){
				$ret[$alias] = call_user_func($formatter, $field, $alias, $value);
			} else {
				$ret[$alias] = $value;
			}
		}
		return $ret;
	}

	private function load_option($option, $default = '',  $auto = 'yes'){
		$value = get_option($this->field_data_opt);
		if($value === FALSE){
			$value = $default;
			add_option($option, $value, '', $auto);
		}
		return $value;
	}
	
	private function converter($field){
		switch($field){
			case 'MLS_No':
				return 'MLS ID';
				break;
			case 'geo_longitude':
			case 'geo_latitude':
				return ucwords(substr($field,4));
				break;
			default:
				return str_replace("_"," ",$field);
				break;
		}
	}
	
	private function get_data(){
		$default = array(
			'fields' => array(),
			'above' => array(),
			'below' => array(),
			'title' => array('__title__'),
			'featured' => array()
		);
		return $this->load_option($this->field_data_opt, $default);
	}
	
	private function get_field_case($pos){
		$data = $this->get_data();
		$ret = array();
		foreach($data[$pos] as $field){
			if(($pos === 'title') AND ($field === '__title__')){
				$ret[$field] = 'Post Title';
				continue;
			}
			$ret[$field] = $data['fields'][$field];
		}
		return $ret;
	}
	
	private function add_field_case($pos, $field){
		$first = $this->get_pair($pos)['first'];
		$second = $this->get_pair($pos)['second'];
		$new_data = $this->get_data();
		if(!array_key_exists($field, $data['fields'])){
			return FALSE;
		}
		if(in_array($field, $new_data[$second])){
			return FALSE;
		}
		if(in_array($field, $new_data[$first])){
			return TRUE;
		}
		$new_data[$first][] = $field;
		return update_option($this->field_data_opt, $new_data);
	}
	
	private function mass_field_case($pos, $fields){
		$first = $this->get_pair($pos)['first'];
		$second = $this->get_pair($pos)['second'];
		$new_data = $this->get_data();
		$new_first = array();
		$new_second = array();
		foreach($fields as $field){
			if(!array_key_exists($field, $new_data['fields'])){
				if(($field !== '__title__') AND ($pos !== 'title')){//title exception
					continue;
				}
			}
			$new_first[] = $field;
		}
		foreach($new_data[$second] as $field){
			if(in_array($field, $new_first)){
				continue;
			}
			$new_second[] = $field;
		}
		if($pos === 'title' AND !in_array('__title__', $new_first)){
			$new_first[] = '__title__';
		}
		$new_data[$first] = $new_first;
		$new_data[$second] = $new_second;
		return update_option($this->field_data_opt, $new_data);
	}
	
	private function remove_field_case($pos, $remove_field){
		$first = $this->get_pair($pos)['first'];
		$second = $this->get_pair($pos)['second'];
		if(($first === 'title' OR $second === 'title') AND ($remove_field === '__title__')){
			return FALSE;
		}
		$data = $this->get_data();
		$new_first = array();
		$new_second = array();
		foreach($data[$first] as $field){
			if($field === $remove_field){
				continue;
			}
			$new_first[] = $field;
		}
		foreach($data[$second] as $field){
			if($field === $remove_field){
				continue;
			}
			$new_second[] = $field;
		}
		$data[$first] = $new_first;
		$data[$second] = $new_second;
		return update_option($this->field_data_opt, $data);
	}
	
	private function get_unused_case($pos){
		$first = $this->get_pair($pos)['first'];
		$second = $this->get_pair($pos)['second'];
		$data = $this->get_data();
		$ret = array();
		foreach($data['fields'] as $field => $alias){
			if(in_array($field, $data[$first]) OR in_array($field, $data[$second])){
				continue;
			}
			$ret[$field] = $alias;
		}
		return $ret;
	}
	
	private function get_pair($pos){
		$first = 'above';
		$second = 'below';
		switch($pos){
			case 'above':
				$first = 'above';
				$second = 'below';
				break;
			case 'below':
				$first = 'below';
				$second = 'above';
				break;
			case 'title':
				$first = 'title';
				$second = 'featured';
				break;
			case 'featured':
				$first = 'featured';
				$second = 'title';
				break;
			default:
				break;
		}
		return array('first' => $first, 'second' => $second);
	}
}