<?php

  add_submenu_page('edit.php?post_type=wovaxproperty', "RETS Settings", "RETS Settings", 'manage_options', 'retsSettings', 'retsSettings');
  add_submenu_page('edit.php?post_type=wovaxproperty', "Import Status", "Import Status", "administrator", "importStatus", 'importStatus');
  //add_menu_page('Wovax Property', 'Wovax Property', 'administrator', 'wovaxRETSclient', 'wovaxRETSclient', 'dashicons-admin-home', '48' );
  //add_submenu_page('wovaxRETSclient', 'Dashboard', 'Dashboard', 'administrator', 'wovaxRETSclient', 'wovaxRETSclient');

  function importStatus() {
  	require WOVAX_PROPERTY_DIR."/pages/import_status.php";
  }

  function wovaxRETSclient() {
  	require WOVAX_PROPERTY_DIR."/pages/dashboard.php";
  }

  function retsSettings() {
  	require WOVAX_PROPERTY_DIR."/pages/settings.php";
  }

?>
