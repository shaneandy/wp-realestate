<?php

 // database initialization
    global $wpdb;
	
	//Setting up initial options
	$serverManager = RetsServer::getInstance();
	$retsProperties = RetsProperties::getInstance();
	$fields = $serverManager->getAllFields();
	$aliased_fields = array();
	foreach($fields as $original_field){
		switch($original_field){
			case 'MLS_No':
				$aliased_fields[$original_field] = 'MLS ID';
				break;
			case 'geo_longitude':
			case 'geo_latitude':
				$aliased_fields[$original_field] = ucwords(substr($original_field,4));
				break;
			default:
				$aliased_fields[$original_field] = str_replace("_"," ",$original_field);
				break;
		}
	}
	$empty_array = json_encode(array());
    
    add_option("wovax_rets_processing","FALSE");
    add_option("wovax_rets_last_import","0");
    add_option("wovax_rets_legal_notice");
    add_option("wovax_rets_idx_logo");
    add_option("wovax_rets_contact_email");
    add_option("wovax_rets_realtor_name");
    add_option("wovax_rets_realtor_address1");
    add_option("wovax_rets_realtor_address2");
    add_option("wovax_rets_realtor_phone");
	add_option("wovax_rets_field_aliases",$aliased_fields);
	add_option("wovax_rets_property_gallery_type","preview");
	add_option("wovax_rets_fields_above",$empty_array);
	add_option("wovax_rets_fields_below",$empty_array);
	add_option("wovax_rets_fields_hidden",$fields);
	add_option("wovax_rets_unused_fields",$fields);
	add_option("wovax_rets_featured_fields",$empty_array);
	add_option("wovax_rets_modified_title",array("Title"));
	
// create user to assign posts to
if(!username_exists('wovax')){
	$ret = wp_create_user('wovax', get_rand_str(), 'developer@wovax.com');
	if(is_wp_error($ret)){
		exit('Failed to make wovax user: '.$ret->get_error_message());
	}
}

require WOVAX_PROPERTY_DIR."/library/wovax-property-post-type.php";
require WOVAX_PROPERTY_DIR."/library/wovax-property-taxonomy.php";

//wx_property_init();
flush_rewrite_rules();

?>