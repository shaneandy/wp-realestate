<?php
/*
File: utilities.php
Description: This file contains small helper functions.
@author	     Keith Cancel
@since     	 2.0.0b
*/


/**
* Checks if a value is an int. This differ from php's is_int() as it check value not
* variable type.
*
* @since 2.0.0b
*
* @return bool    Returns true if the value is an int False otherwise
*/
function is_val_int($var) {
  $tmp = (int) $var;
  return ($tmp == $var) ? TRUE : FALSE;
}


/**
* Gets a random str generated from openssl_random_pseudo_bytes
* 
* @since 2.0.0b
*
* @param int		The length of the random str. Default 32
* @param array		An array of characters to use.
* @return bool		Returns true if the value is an int False otherwise
*/
function get_rand_str($length=32, $char_list = array('-','_','?','@','!','0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z')){
	$list_sz = count($char_list);
	$rand_str = "";
	while(strlen($rand_str) < $length){
		$rand_val = hexdec(bin2hex(openssl_random_pseudo_bytes(1)));
		if($rand_val < $list_sz){
			$rand_str.= $char_list[$rand_val];
		}	
	}
	return $rand_str;
}
//works just like get get_post_meta, but does not cause the memory usage to explode.
function wx_get_post_meta($post_id, $key='', $single=FALSE){
	global $wpdb;
	$single_val	= '';
	$all		= array();
	$sql		= "Select `meta_key`, `meta_value` From `".$wpdb->postmeta."` where `post_id` = '".$post_id."';";
	if(!empty($key)){
		$sql = "Select `meta_key`, `meta_value` From `".$wpdb->postmeta."` where `meta_key` = '".$key."' AND `post_id` = ".$post_id.";";
	}
	if(is_numeric($post_id)){
		$all_meta = $wpdb->get_results($sql, ARRAY_A);
		if(count($all_meta) > 0){
			$single_val = $all_meta[0]['meta_value'];
			$formatted = array();
			foreach($all_meta as $meta){
				if(empty($key)){
					$formatted[$meta['meta_key']] = array();
					$formatted[$meta['meta_key']][] = $meta['meta_value'];
				}
				else{
					$formatted[] = $meta['meta_value'];
				}
			}
			$all = $formatted;
		}
	}
	if($single){
		return $single_val;
	}
	return $all;
}
/**
* Deletes all the images for the chosen post id
* 
* @since 2.0.0b
*
* @param int		$post_id	The post to delete all images from
* @return array 				A list of undeleted image ids. Should return an empty array().
*/
function wx_delete_post_images($post_id){
	global $wpdb;
	$sql = "Select `ID` FROM `".$wpdb->posts."` WHERE `post_type` = 'attachment' AND `post_parent` = '".$post_id."' AND `post_mime_type` LIKE '%image%';";
	$image_ids = $wpdb->get_col($sql);
	$ret = array();
	foreach ($image_ids as $img_id) {
		if(false === wp_delete_attachment($img_id)){
			$ret[] = $img_id;
		}
	}
	return $ret;
}
/**
* Works like empty, but does not consider 0, 0.0, and '0' as empty;
* 
* @since 2.0.0b
*
* @param any		$var	the variable we want to check if it's empty. 
* @return bool 				TRUE = EMPTY : FALSE = not empty
*/
function wx_empty($var){
	return ($var !== '0') and ($var !== 0) and ($var !== 0.0) and empty($var);
}

function wx_get_memory_usage(){
    $unit=array('B','KB','MB','GB','TB','PB');
	$size = memory_get_usage(true);
   	return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}