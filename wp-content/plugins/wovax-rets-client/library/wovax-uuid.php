<?php

define('WOVAX_UUID_NAME_SPACE', '9eb82041-0d3e-49d5-1992-0707b60728b2');

//add urn as protocol for the filters that are applied to feilds.
add_filter('kses_allowed_protocols', function ($protocols){
		$protocols[] = 'urn';
		return $protocols;
});
//take control of guid generation and replace with uuid
add_filter('wp_insert_post_data', function($data){
	if(empty($data['guid'])){
		$data['guid'] = wp_slash(sprintf('urn:uuid:%s', wx_UUID_get_v4()));
	}
	return $data;
});
//creates a determistic UUID based off the wovax UUID
function wx_get_wovax_UUID($name){
	return wx_UUID_get_v5(WOVAX_UUID_NAME_SPACE, $name);
}
/**
* Creates a v4 uuid.
*
* Found @http://php.net/manual/en/function.uniqid.php#94959
* It looks like it uses the mersene twister random number generator and
* and does some bitwise oring with the result.
* I like mersene twister a decent non-cryptographic psuedo-random number generetor.
*
* @return string  A new uuid
*/
function wx_UUID_get_v4(){
	mt_srand();
	return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
		// 32 bits for "time_low"
		mt_rand(0, 0xffff), mt_rand(0, 0xffff),
		// 16 bits for "time_mid"
		mt_rand(0, 0xffff),
		// 16 bits for "time_hi_and_version",
		// four most significant bits holds version number 4
		mt_rand(0, 0x0fff) | 0x4000,
		// 16 bits, 8 bits for "clk_seq_hi_res",
		// 8 bits for "clk_seq_low",
		// two most significant bits holds zero and one for variant DCE1.1
		mt_rand(0, 0x3fff) | 0x8000,
		// 48 bits for "node"
		mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
	);
}
// Found @http://php.net/manual/en/function.uniqid.php#94959
function wx_UUID_get_v5($namespace, $name) {
	if(!wx_UUID_valid($namespace)) return false;
	// Get hexadecimal components of namespace
	$nhex = str_replace(array('-','{','}'), '', $namespace);
	// Binary Value
	$nstr = '';
	// Convert Namespace UUID to bits
	for($i = 0; $i < strlen($nhex); $i+=2) {
		$nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
	}
	// Calculate hash value
	$hash = sha1($nstr.$name);
	return sprintf('%08s-%04s-%04x-%04x-%12s',
		// 32 bits for "time_low"
		substr($hash, 0, 8),
		// 16 bits for "time_mid"
		substr($hash, 8, 4),
		// 16 bits for "time_hi_and_version",
		// four most significant bits holds version number 5
		(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,
		// 16 bits, 8 bits for "clk_seq_hi_res",
		// 8 bits for "clk_seq_low",
		// two most significant bits holds zero and one for variant DCE1.1
		(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,
		// 48 bits for "node"
		substr($hash, 20, 12)
	);
}
// Found @http://php.net/manual/en/function.uniqid.php#94959
function wx_UUID_valid($uuid) {
	return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
					'[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
}