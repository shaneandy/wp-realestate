<?php

//@ todo automatically add posts without an mls_id to customized term.
//this query finds those. 
//SELECT * FROM `wp_posts` LEFT JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` AND `wp_postmeta`.`meta_key` = 'MLS_No' WHERE `wp_postmeta`.`post_id` IS NULL AND  `wp_posts`.`post_type` = 'wovaxproperty';

class wovax_api__rets_client{
	public function add_properties($mls_ids){
		set_time_limit(0);//Keep running till done.
		header("Content-Type: application/json");
		ignore_user_abort(true);//Prevents client disconnect from aborting script.
		echo '  ';
		$server				= RetsServer::getInstance();
		if($server->getHaltStatus()){
			echo json_encode(array('msg' => 'Halted' ,'status' => 'ok'));
			exit();
		}
		$prop_man			= RetsProperties::getInstance();
		$importer			= new WovaxPostImporter('wovaxproperty', $prop_man->getPostUser(), $prop_man->getDefaultImageUrl(), $prop_man->removeWhite());
		$existing_ids		= $prop_man->getMlsIds();
		$mls_ids			= explode(',', $mls_ids);
		$new_ids			= array();
		$terms 				= $server->getCategories();
		//remove any already existing mls ids
		foreach($mls_ids as $id){
			if(!in_array($id, $existing_ids)){
				$new_ids[] = $id;
			}
		}
		//get property information. 
		$properties = $server->getMlsIdInfo($new_ids, TRUE, $prop_man->autoRetain());
		$return_data = array(
			'requested'	=> count($mls_ids),
			'adding'	=> count($properties),
			'properties'=> array()
		);
		foreach($properties as $prop_info){
			$mls_id			= $prop_info['MLS_No'];
			$title			= $prop_info['Address'];
			$post_date		= $prop_info['Posting_Date'];
			$modify_date	= $prop_info['Modify_Date'];
			$description	= $prop_info['Description'];
			$images			= array();
			$categories		= $prop_info['Categories'];
			$status			= array('images' => array());
			unset($prop_info['Categories']);
			unset($prop_info['Description']);
			if(!empty($prop_info['Images'])){
				$images = $prop_info['Images'];
				unset($prop_info['Images']);
			}		
			$uuid = $importer->enqueuePost($mls_id, $title, $post_date, $modify_date, $description);
			$status['uuid'] = $uuid;
			foreach($images as $url){
				$status['images'][] = $importer->enqueueImage($uuid, $url);
			}
			foreach($prop_info as $meta_key => $meta_value){
				$importer->enqueueMeta($uuid, $meta_key, $meta_value);
			}
			foreach($categories as $category_id){
				$importer->enqueueCategory($uuid, $category_id);
			}
			$return_data[$mls_id] = $status;
		}
		$importer->processQueue();
		echo json_encode($return_data);
		exit();
	}
	
	public function remove_inactive(){
		header("Content-Type: application/json");
		set_time_limit(0);//Keep running till done.
		ignore_user_abort(true);//Prevents client disconnect from aborting script.
		echo "  ";
		$server = RetsServer::getInstance();
		$prop_man = RetsProperties::getInstance();
		//get various ids
		$retained_posts	= $prop_man->getTermIds('wx_retained', 'wovax_property_category');
		$custom_posts 	= $prop_man->getTermIds('wx_custom', 'wovax_property_category');
		$post_ids		= $prop_man->getMlsAndPostIds();
		$mls_ids		= $server->getActiveMlsIds();
		if(count($mls_ids) <= 0){
			return array('msg' => 'Old Property removal aborted zero active properties returned.');
		}
		$posts = array();
		foreach($post_ids as $post){
			$posts[$post['post_id']] = $post['mls_id'];
		}
		unset($posts_ids);
		$posts = array_diff($posts, $mls_ids);
		$posts = array_flip($posts);
		$old = array_diff($posts, $retained_posts);
		$old = array_diff($old, $custom_posts);
		//done getting various ids
		$deleted = array();
		$deleted_count = 0;
		foreach($old as $mls_id => $post_id){
			if($server->getHaltStatus()){
				echo json_encode(array('msg' => 'Halted' ,'status' => 'ok'));
				exit();
			}
			$ret = wp_delete_post($post_id, true);
			$deleted[$mls_id] = ($ret === false) ? 'failure' : 'success';
			if($ret === false){
				$server->reportError('Failed to delete Off Market property.', 'MLS_ID: '.$old_mls_id.' Post_ID: '.$post_id);
			} else {
				$deleted_count++;
			}
		}
		echo json_encode(array('old_count' => count($old), 'deleted' => $deleted_count, 'properties' => $deleted));
		exit();
	}

	public function remove_corrupted() {
		header("Content-Type: application/json");
		set_time_limit(0);//Keep running till done.
		ignore_user_abort(true);//Prevents client disconnect from aborting script.
		echo "  ";
		$prop_man = RetsProperties::getInstance();
		$post_ids = $prop_man->getBadPostIds();
		$deleted = array();
		$failed = array();
		$deleted_count = 0;
		foreach($post_ids as $id_array) {
			$id = $id_array['ID'];
			$ret = wp_delete_post($id, true);
			$deleted[$id] = ($ret === false) ? 'failure' : 'success';
			if($ret === false){
				$failed[] = 'Failed to delete property post. Post_ID: '.$id;
			} else {
				$deleted_count++;
			}
		}
		$no_image_ids = $prop_man->getNoImagePostIds();
		foreach($no_image_ids as $id_array) {
			$id = $id_array['ID'];
			$ret = wp_delete_post($id, true);
			$deleted[$id] = ($ret === false) ? 'failure' : 'success';
			if($ret === false){
				$failed[] = 'Failed to delete property post. Post_ID: '.$id;
			} else {
				$deleted_count++;
			}
		}
		if(empty($failed)) {
			$failed[] = 'No Failures';
		}
		echo json_encode(array('deleted' => $deleted_count, 'properties' => $deleted, 'failed' => $failed));
		exit();
	}
	
	public function update_modified_photos(){
		header("Content-Type: application/json");
		set_time_limit(0);//Keep running till done.
		ignore_user_abort(true);//Prevents client disconnect from aborting script.
		echo "  ";
		$server			= RetsServer::getInstance();
		$prop_man		= RetsProperties::getInstance();
		$last_update 	= $prop_man->getLastPhotoUpdateTime();
		$ids			= $server->getModifiedPhotosAfter($last_update);
		$custom_posts	= $prop_man->getTermIds('wx_custom', 'wovax_property_category');
		$prop_man->setPhotoUpdateTimeNow();
		$update			= array();
		$post_ids		= array();
		foreach($ids as $mls_id){
			$post_id = $prop_man->getPostId($mls_id);
			if($this->check_update($post_id, $last_update, 'Photo')){
				continue;
			}
			if(in_array($post_id, $custom_posts)){
				continue;
			}
			$update[] = $mls_id;
			$post_ids[$mls_id] = $post_id;
		}
		unset($ids);
		$id_chunks = array_chunk($update, 150);
		//only do a 150 at a time since that is that max we can request from the server
		$updated = array();
		foreach($id_chunks as $ids){
			$properties = $server->getMlsIdInfo($ids);
			foreach($properties as $property){
				if($server->getHaltStatus()){
					$return_data[] = array('msg' => 'Halted' ,'status' => 'ok');
					break;
				}
				$mls_id		= $property['MLS_No'];
				$post_id	= intval($post_ids[$mls_id]);
				$images		= empty($property['Images']) ? array() : $property['Images'];
				if($post_id === 0){
					continue;
				}
				wx_delete_post_images($post_id);
				$ret = $prop_man->attachImages($post_id, $mls_id, $images);
				if($ret !== 0){
					$updated[$mls_id] = 'Failed'; 
					$server->reportError('Failed to update images for property.', 'MLS_ID: '.$mls_id.' Post_ID: '.$post_id.' Failure_Count: '.$ret.' Image_Total: '.count($images));
				} else {
					update_post_meta($post_id, 'Photo_Modify_Date', $property['Photo_Modify_Date']);
					$updated[$mls_id] = $post_id;
				}
				if($prop_man->imageCount($post_id) === 0){
					$prop_man->attachImages($post_id, $mls_id);
				}
			}
		}
		echo json_encode(array('update_count' => count($updated),'updated' => $updated));
		exit();
	}
	
	public function update_modified_properties(){
		set_time_limit(0);//Keep running till done.
		ignore_user_abort(true);//Prevents client disconnect from aborting script.
		echo "  ";
		$server			= RetsServer::getInstance();
		$prop_man		= RetsProperties::getInstance();
		//make the script alway check the past 24 hours.
		//only temporary till we find a better solution to deal with
		//timestamps in different timezone. 
		$last_update 	= $prop_man->getLastUpdateTime()-(24*60*60);
		if($last_update < 0){
			$last_update = 0;
		}
		$ids			= $server->getModifiedAfter($last_update);
		//$prop_man->markCustom(); //Commented out due to race condition with wp_insert_post
		$custom_posts	= $prop_man->getTermIds('wx_custom', 'wovax_property_category');
		$pairs			= array();
		foreach($ids as $mls_id){
			$post_id = $prop_man->getPostId($mls_id);
			if($this->check_update($post_id, $last_update)){
				continue;
			}
			if(in_array($post_id, $custom_posts)){
				continue;
			}
			$pairs[] = $mls_id.'|'.$post_id;
		}
		$chunks 	= array_chunk($pairs, 100);
		$i 			= 1;
		foreach($chunks as $chunk){
			$url = site_url();
			if(substr($url, -1) !== '/'){
				$url .= '/';
			}
			$url .= 'api/rets_client/update_chosen_ids/';
			$url .= '?ids='.implode(',', $chunk);
			echo "Batch Number: ".$i++."</br>";
			echo "Memory Useage ".wx_get_memory_usage()."</br>";
			echo file_get_contents($url);
		}
		$prop_man->setUpdateTimeNow();
		exit();
	}
	
	
	
	//A local and sequential update all
	public function update_all(){
		set_time_limit(0);//Keep running till done.
		//ignore_user_abort(true);//Prevents client disconnect from aborting script.
		$prop_man 	= RetsProperties::getInstance();
		$ids 		= $prop_man->getMlsAndPostIds();
		$chunks 	= array_chunk($ids, 100);
		$i 			= 1;
		foreach($chunks as $chunk){
			$pairs = array();
			foreach($chunk as $pair){
				$pairs[] = $pair['mls_id'].'|'.$pair['post_id'];
			}
			$url = site_url();
			if(substr($url, -1) !== '/'){
				$url .= '/';
			}
			$url .= 'api/rets_client/update_chosen_ids/';
			$url .= '?ids='.implode(',', $pairs);
			echo "Batch Number: ".$i++."</br>";
			echo "Memory Useage ".wx_get_memory_usage()."</br>";
			echo file_get_contents($url);
		}
		exit();
	}
	
	
	public function update_chosen_ids($ids){
		set_time_limit(0);//Keep running till done.
		ignore_user_abort(true);//Prevents client disconnect from aborting script.
		echo "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=ISO-8859-1\">";
		echo "<pre>";
		echo "<strong>Starting Update Process</strong>\n";
		$server		= RetsServer::getInstance();
		$prop_man	= RetsProperties::getInstance();
		$custom_ids = $prop_man->getTermIds('wx_custom', 'wovax_property_category');
		$terms 		= $server->getCategories();
		$pairs 		= explode(',', $ids);
		$keyed_ids	= array();
		$mls_ids	= array();
		$properties = array();
		//starting updating propeties
		foreach($pairs as $pair){
			$id_pairs	= explode('|', $pair);
			$mls_id		= $id_pairs[0];
			$post_id 	= $id_pairs[1];
			if(in_array($post_id, $custom_ids)){
				continue;
			}
			$mls_ids[]	= $mls_id;
			$keyed_ids[$mls_id] = intval($post_id);
		}
		echo "Getting MLS ID information.\n";
		if(!empty($mls_ids)){
			$properties = $server->getMlsIdInfo($mls_ids);
		}
		foreach($properties as $property){
			if($server->getHaltStatus()){
				$return_data[] = array('msg' => 'Halted' ,'status' => 'ok');
				break;
			}
			$mls_id		= $property['MLS_No'];
			$post_id	= $keyed_ids[$mls_id];
			$description= $property['Description'];
			unset($property['Description']);
			unset($property['Images']);
			echo "Updating ".$mls_id;
			$meta = wx_get_post_meta($post_id, 'Photo_Modify_Date', TRUE);
			if(!empty($meta)){
				unset($property['Photo_Modify_Date']);
			}
			$ret = $prop_man->updateProperty($post_id, $property['Address'], $description, $property, $terms, $property['Modify_Date']);
			if(intval($ret) !== $post_id){
				$server->reportError('Failed to update property.', ' MLS_ID: '.$mls_id.' Post_ID: '.$post_id.' Data: '.$ret);
				$updated[$mls_id] = 'Failed';
				echo ": <strong><font color=\"FF0000\">Failed!</font></strong>\n";
				continue;
			}
			echo ": <strong><font color=\"00E600\">Success!</font></strong>\n";
			$updated[$mls_id] = $post_id; 
		}
		echo "<strong>DONE!</strong></pre>";
		exit();
	}
	
	private function check_update($post_id, $timestamp, $type = 'Post'){
		if($post_id === 0){
			return TRUE;
		}
		$meta_key = ($type === 'Photo') ? 'Photo_Modify_Date' : 'Modify_Date';
		$time = wx_get_post_meta($post_id, $meta_key, TRUE);
		$time = empty($time) ? $timestamp : strtotime($time);
		if($time <= $timestamp){
			return FALSE;
		}
		return TRUE;
	}

	public function get_ids(){
		$ids = RetsProperties::getInstance()->getMlsIds();
		return array('count' => count($ids), 'properties' => $ids);
	}
	
	public function get_post_and_mls_ids(){
		$prop_man = RetsProperties::getInstance();
		$ids = $prop_man->getMlsAndPostIds();
		return array('count' => count($ids), 'properties' => $ids);
	}
	
	public function get_featured_ids(){
		$ids = rets_get_featured_posts(FALSE);
		return array('count' => count($ids), 'properties' => $ids);
	}
	
	public function numbers(){
		$prop_man = RetsProperties::getInstance();
		$numbers = $prop_man->totals();
		$numbers['last_import'] = $prop_man->lastImport();
		return $numbers;
	}
	//helper functions
	private function get_and_unset(&$array, $key){
		$value = $array[$key];
		unset($array[$key]);
		return $value;
	}
	
	public function delete_duplicates(){
		ob_implicit_flush(true);
		echo "<pre>";
		global $wpdb;
		$sql = "SELECT `post_id`, `meta_value` FROM `".$wpdb->postmeta."` WHERE `meta_key` = 'MLS_No'";
		$ids = $wpdb->get_results($sql, ARRAY_A);
		$keyed = array();
		foreach($ids as $id_pair){
			$mls_id = $id_pair['meta_value'];
			$post_id = $id_pair['post_id'];
			if(!array_key_exists($mls_id, $keyed)){
				$keyed[$mls_id] = array();
			}
			$keyed[$mls_id][] = $post_id;
		}
		unset($ids);
		$counter = 0;
		foreach($keyed as $mls_id => $post_ids){
			$count = count($post_ids);
			if($count > 1){
				asort($post_ids);
				for($i = ($count-1); $i > 0; $i--){
					echo $mls_id.", ".$post_ids[$i];
					$ret = wp_delete_post($post_ids[$i], TRUE);
					if($ret === FALSE){
						echo ": <strong><font color=\"FF0000\">Failed!</font></strong>\n";
					} else {
						echo ": <strong><font color=\"00E600\">Success!</font></strong>\n";
					}
					flush();
				}
				if($counter >= 9){
					break;
				} else {
					$counter++;
				}
			}
		}
		echo "done!\n</pre>";
		echo '<META http-equiv="refresh" content="1;URL=./delete_duplicates">';
		exit();
	}
	// This function is really a hack.
	// It's fixes a problem that I am not sure of the cause
	// at the moment. However, clients where upset, so a hack it shall be.
	public function fix_thumbnails() {
		set_time_limit(0);//Keep running till done.
		ignore_user_abort(true);
		global $wpdb;
		$post_tbl = $wpdb->posts;
		$meta_tbl = $wpdb->postmeta;
		// Get listings without a the thumbnail meta.
		$sql =
		"SELECT `ID` FROM `$post_tbl` AS `a`
		LEFT JOIN `$meta_tbl` AS `b` ON `a`.`ID` = `b`.`post_id` AND `b`.`meta_key` = '_thumbnail_id'
		WHERE `post_type` = 'wovaxproperty' AND `b`.`post_id` IS NULL;";
		$listings = $wpdb->get_col($sql);
		echo "Missing thumbnails: ".count($listings)."</br>\n";
		if(is_array($listings)) {
			foreach($listings as $post_id) {
				$this->choose_listing_thumbnail(intval($post_id));
				echo "post_id: $post_id</br>\n";
			}
		}
		exit();
	}

	private function choose_listing_thumbnail($post_id) {
		$images  = $this->get_images($post_id);
		if(empty($images)) {
			return FALSE;
		}
		echo "Images: ".count($images)." ";
		// Default to this one incase there is none with a valid title.
		$thumb_id = $images[0]['ID'];
		// Find the image with the the proper image for the thumbnail.
		foreach($images as $info) {
			if(preg_match('/^Image 0 for /', $info['post_title']) === 1) {
				$thumb_id = $info['ID'];
				break;
			}
		}
		return set_post_thumbnail($post_id, $thumb_id);
	}

	private function get_images($post_id) {
		global $wpdb;
		$post_id  = intval($post_id);
		$post_tbl = $wpdb->posts;
		$sql = 
		"SELECT `ID`, `post_title` FROM `$post_tbl`
		WHERE `post_parent` = $post_id AND `post_type` = 'attachment' AND `post_mime_type` LIKE 'image/%'
		ORDER BY `post_title` ASC";
		echo "$sql ";
		$data = $wpdb->get_results($sql, ARRAY_A);
		if(is_array($data) === FALSE) {
			return array();
		}
		foreach($data as $index => $info) {
			$data[$index]['ID'] = intval($info['ID']); 
		}
		return $data;
	}
	
}