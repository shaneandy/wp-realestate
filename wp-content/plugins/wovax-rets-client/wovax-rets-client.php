<?php
/*
Plugin Name: Wovax IDX
Plugin URI: https://www.wovax.com/
Description: The Wovax MLS Property Import Plugin
Version: 2.3.0
Author: Wovax, LLC.
Author URI: https://www.wovax.com/
*/

 // Exit if accessed directly
if (!defined('ABSPATH')) {
  exit;
}

if(!class_exists('WovaxError')){	
	return 'Missing Wovax Error Class';
}

if(!class_exists('wovaxApiLoader') OR !class_exists('WovaxApiAuth')){	
	return 'Missing Wovax API Plugin';
}

/**
 * WooCommerce API Manager Integration - disabled for now
 */
/*
if (!defined('WOVAX_PROPERTY_UPGRADE_URL')) {
    define('WOVAX_PROPERTY_UPGRADE_URL', 'https://wovax.com/' );
}

if (!defined('WOVAX_PROPERTY_RENEW_LICENSE_URL')) {
    define('WOVAX_PROPERTY_RENEW_LICENSE_URL', 'https://wovax.com/my-account/' );
}

if (!defined('WOVAX_PROPERTY_SETTINGS_MENU_TITLE')) {
    define('WOVAX_PROPERTY_SETTINGS_MENU_TITLE', 'Wovax Property License Activation' );
}

if (!defined('WOVAX_PROPERTY_SETTINGS_TITLE')) {
    define('WOVAX_PROPERTY_SETTINGS_TITLE', 'Wovax Property Activation' );
}

if (!defined('WOVAX_PROPERTY_TEXT_DOMAIN')) {
    define('WOVAX_PROPERTY_TEXT_DOMAIN', 'wovax-property' );
}

if (!defined('WOVAX_PROPERTY_FILE')) {
    define('WOVAX_PROPERTY_FILE', __FILE__ );
}
*/

if (!defined('WOVAX_PROPERTY_DIR')) {
  define('WOVAX_PROPERTY_DIR', plugin_dir_path(__FILE__));
}

if ( ! defined( 'WX_CUSTOM_TEMPLATE_PATH' ) ) {
	/** @var string WX_CUSTOM_TEMPLATE_PATH */
	define( 'WX_CUSTOM_TEMPLATE_PATH', WP_CONTENT_DIR . '/wovax_templates' );
}

if ( ! defined( 'WX_CUSTOM_TEMPLATE_URL' ) ) {
	/** @var string WX_CUSTOM_TEMPLATE_URL */
	define( 'WX_CUSTOM_TEMPLATE_URL', content_url() . '/wovax_templates' );
}

if ( ! defined( 'WX_PLUGIN_FOLDER_URL' ) ){
	define( 'WX_PLUGIN_FOLDER_URL',  plugin_dir_url( __FILE__ ));	
}
/**
 * Is Wordpress Enhanced Media Library active?
 */
 if (!defined('WOVAX_WEML')) {
   if (!function_exists("is_plugin_active")) {
     include_once(ABSPATH.'wp-admin/includes/plugin.php');
   }
   define('WOVAX_WEML', is_plugin_active("enhanced-media-library/enhanced-media-library.php"));
 }

 /**
 * Integrating the plugin update checker
 */
require WOVAX_PROPERTY_DIR.'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = PucFactory::buildUpdateChecker(
    'http://wovax-plugin-releases.s3.amazonaws.com/wovax-rets-client/metadata.json',
    __FILE__
);

//require_once( 'am/am.php' );

require_once(WOVAX_PROPERTY_DIR."ajax/ajax.php");

require_once(WOVAX_PROPERTY_DIR."controllers/rets-client.php");

require_once(WOVAX_PROPERTY_DIR."filters/content-filters.php");

require_once(WOVAX_PROPERTY_DIR."pages/no-images.php");
require_once(WOVAX_PROPERTY_DIR."pages/duplicate-guids.php");
require_once(WOVAX_PROPERTY_DIR."pages/duplicate-posts-names.php");

require_once(WOVAX_PROPERTY_DIR."library/wovax-db.php");
require_once(WOVAX_PROPERTY_DIR."library/utilities.php");
require_once(WOVAX_PROPERTY_DIR."library/wovax-uuid.php");
require_once(WOVAX_PROPERTY_DIR."library/field-manager.php");
require_once(WOVAX_PROPERTY_DIR."library/server-manager.php");
require_once(WOVAX_PROPERTY_DIR."library/image-utilities.php");
require_once(WOVAX_PROPERTY_DIR."library/rets-properties.php");
require_once(WOVAX_PROPERTY_DIR."library/wovax-post-importer.php");

register_activation_hook(__FILE__, 'wovaxrets_activation');
function wovaxrets_activation() {
  require WOVAX_PROPERTY_DIR."/library/wovax-property-activation.php";
}

register_deactivation_hook(__FILE__,'wovaxrets_deactivate');
function wovaxrets_deactivate() {
  require WOVAX_PROPERTY_DIR."/library/wovax-property-deactivation.php";
}

add_action('admin_menu', 'wovax_property_menu', 15);

function wovax_property_menu() {
  require WOVAX_PROPERTY_DIR."/library/wovax-property-menu.php";
}

add_action('init', function(){
	$auth = WovaxApiAuth::getInstance();
	$auth->setGetKeyFunc(function ($id){
		if(intval($id) !== 1){
			return FALSE;
		}
		$server = RetsServer::getInstance();
		return $server->getKey();
	});
	$auth->setGetKeyIds(function (){
		return array('1' => site_url());
	});
	$auth->addKeyToClass('rets_client', '1');
	require WOVAX_PROPERTY_DIR."/library/wovax-property-post-type.php";
	require WOVAX_PROPERTY_DIR."/library/wovax-property-taxonomy.php";
	add_shortcode('wxretslastupdate', 'wovax_rets_last_update');
});

function wovax_rets_last_update($atts){
	// The list of accepted names can be found here: http://php.net/manual/en/timezones.america.php
	// Eastern ........... America/New_York
	// Central ........... America/Chicago
	// Mountain .......... America/Denver
	// Mountain no DST ... America/Phoenix
	// Pacific ........... America/Los_Angeles
	// Alaska ............ America/Anchorage
	// Hawaii ............ America/Adak
	// Hawaii no DST ..... Pacific/Honolulu
	$atts = shortcode_atts(
		array(
			'timezone' => 'America/Los_Angeles'
		), $atts, 'wxretslastupdate');
	$rets_prop = RetsProperties::getInstance();
	$timezone = $atts['timezone'];
	$last_import = $rets_prop->lastImport();
	$last_update = $rets_prop->getLastUpdateTime();
	$most_recent = max($last_import,$last_update);
	//$last_update = get_option('wx_rets_last_run', time());
	$date = new DateTime(null, new DateTimeZone($timezone));
	$date->setTimestamp($most_recent);
	$last_update_date = $date->format('m/d/Y h:i A');
	return $last_update_date;
}

add_action('wp_loaded', function(){
	$auth = WovaxApiAuth::getInstance();
	$auth->setClassAccess('rets_client', TRUE, TRUE);
	$auth->updateMethod('rets_client', 'add_properties', FALSE, TRUE);
	/*$auth->updateMethod('rets_client', 'delete_all', FALSE, TRUE);*/
	//need to find a better way to auto load these
	$auth->addKeyToClass('rets_client', '1');
});

//Register and Enqueue all relevant styles and scripts

//Register Slick Carousel
add_action('wp_enqueue_scripts', 'wx_register_slick');

function wx_register_slick(){
	wp_register_style('slick-theme', plugins_url('wovax-rets-client/assets/slick/slick-theme.css'));
	wp_register_style('slick', plugins_url('wovax-rets-client/assets/slick/slick.css'), array('slick-theme'));
	wp_enqueue_style('slick');
	wp_register_script('slickjs', plugins_url('wovax-rets-client/assets/slick/slick.js'), array('jquery'));
	wp_enqueue_script('slickjs');
}


//Register style sheets
add_action('wp_enqueue_scripts', 'wx_register_styles');

function wx_register_styles() {
	wp_register_style('black', plugins_url('wovax-rets-client/assets/css/black-and-white-skin/black-and-white-skin.css'));
	wp_register_style('blue', plugins_url('wovax-rets-client/assets/css/grey-blue-skin/grey-blue-skin.css'));
	wp_register_style('minimal', plugins_url('wovax-rets-client/assets/css/minimal-light-skin/minimal-light-skin.css'));
	wp_register_style('touchcarousel', plugins_url('wovax-rets-client/assets/css/touchcarousel.css'), array('black','blue','minimal'));
	wp_register_style('wxretsdisplay', plugins_url('wovax-rets-client/assets/css/wx-rets.css'));
	wp_enqueue_style('touchcarousel');
	wp_enqueue_style('wxretsdisplay');
}

//Register javascripty things
add_action('wp_enqueue_scripts', 'wx_enqueue_scripts');
function wx_enqueue_scripts() {
	wp_register_script('touchcarouseljs', plugins_url('wovax-rets-client/assets/js/jquery.touchcarousel-1.2.min.js'), array('jquery'));
	wp_register_script('wxrets', plugins_url('wovax-rets-client/assets/js/wx-rets.js'), array('jquery','touchcarouseljs','jquery-ui-sortable'));
	wp_enqueue_script('wxrets');
}

add_action('admin_enqueue_scripts', 'wx_enqueue_admin_scripts');
function wx_enqueue_admin_scripts() {
	wp_register_script('wx_rets_admin_js', plugins_url('wovax-rets-client/assets/js/wx-rets-admin.js'), array('jquery'));
	wp_enqueue_script('wx_rets_admin_js');
	wp_register_style('wx_rets_admin_css', plugins_url('wovax-rets-client/assets/css/wx-rets-admin.css'));
	wp_enqueue_style('wx_rets_admin_css');
}

//Adding the Wovax Carousel Widget
add_action('widgets_init', 'wovax_register_widgets');

function wovax_register_widgets() {
	require WOVAX_PROPERTY_DIR."widgets/wovax-property-carousel-widget.php";
}

//Add Proper hook for post_delete of a custom post type
add_action('before_delete_post', 'wovax_delete_post_meta');
add_action('before_delete_post', 'wovax_delete_post_attachments');
function wovax_delete_post_attachments($post_id) {
	//Check if the post is of the type wovaxproperty if not return
	if (get_post_type($post_id) != 'wovaxproperty'){
		return;
	}
	$attachments = get_posts(array(
	'post_type'      => 'attachment',
	'post_status'    => 'any',
	'post_parent'    => $post_id,
	'posts_per_page' => -1));
	foreach ($attachments as $attachment) {
		if (false === wp_delete_attachment($attachment->ID)) {
			//@todo log/report this			
		}
	}
}
function wovax_delete_post_meta($post_id) {
	//Check if the post is of the type wovaxproperty if not return
	if (get_post_type($post_id) != 'wovaxproperty'){
		return;
	}
	$meta_data = get_post_meta($post_id);
	foreach ($meta_data as $meta_key => $meta_val) {
		if (false === delete_post_meta($post_id, $meta_key)) {
			//@todo log/report this
		}
	}
}