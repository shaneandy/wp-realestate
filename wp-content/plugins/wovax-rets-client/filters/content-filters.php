<?php

function wovax_rets_filter($content) {
	if (is_singular("wovaxproperty") || is_post_type_archive("wovaxproperty") || (get_post_type() == 'wovaxproperty') ) {
		$fieldManager = FieldManager::getInstance();
		$above = $fieldManager->getAbove();
		$below = $fieldManager->getBelow();
		ob_start();
		if(file_exists(WX_CUSTOM_TEMPLATE_PATH.'/wx_content_filter_template.php')){
			include(WX_CUSTOM_TEMPLATE_PATH.'/wx_content_filter_template.php');
		} else {
			include(WOVAX_PROPERTY_DIR.'/templates/wx_content_filter_template.php');
		}
		$content = ob_get_clean();
		return $content;
	} else {
		return $content;
	}
}

add_filter('the_content', 'wovax_rets_filter');

add_filter('the_title', 'wovax_rets_title_filter',10,2);

function wovax_rets_title_filter($title, $id = null) {
    if ((is_singular("wovaxproperty") || is_post_type_archive("wovaxproperty") || (get_post_type($id) == 'wovaxproperty')) && in_the_loop() ) {
		$fieldManager = FieldManager::getInstance();
        $modified_title = $fieldManager->getTitle();
        foreach($modified_title as $field => $alias){
            if($field === "__title__") {
                $new_title[] = $title;
            } else {
                $meta = get_post_meta($id,str_replace(" ","_",$field),true);
                if($field === "Price") {
                    if(is_numeric($meta)){
                        $meta = "$".number_format($meta);
                    }
                } else if ($field === "MLS_No") {
                    $meta = "#".$meta;
                }
                $new_title[] = $meta;
            }         
        }
        $new_title = array_filter($new_title);
        $title = implode(" - ", $new_title);
        return $title;
    } else {
        return $title;
    }
}

add_filter('the_content','wovax_gallery_display');

function wovax_gallery_display($content) {
	global $wpdb;
	$gallery_enabled = get_post_meta(get_the_id(), 'postGalleryEnable',true);
	$gallery_type = get_option('wovax_rets_property_gallery_type','preview');
	if (is_singular('wovaxproperty') && $gallery_enabled == 1 ) {
		if($gallery_type === 'preview'){
			if(file_exists(WX_CUSTOM_TEMPLATE_PATH.'/wx_preview_gallery_template.php')){
				include(WX_CUSTOM_TEMPLATE_PATH.'/wx_preview_gallery_template.php');
			} else {
				include(WOVAX_PROPERTY_DIR.'/templates/wx_preview_gallery_template.php');
			}
			return $gallery;
		} else if ($gallery_type === 'fullsize'){
			if(file_exists(WX_CUSTOM_TEMPLATE_PATH.'/wx_fullsize_gallery_template.php')){
				include(WX_CUSTOM_TEMPLATE_PATH.'/wx_fullsize_gallery_template.php');
			} else {
				include(WOVAX_PROPERTY_DIR.'/templates/wx_fullsize_gallery_template.php');
			}
			return $gallery;
		} else {
			return $content;
		}
	} else {
		return $content;
	}
}

function elementFormatter($field,$alias,$value){
	if($field == "Virtual_Tour_Url"){
		if(!filter_var($value, FILTER_VALIDATE_URL) === FALSE){
			$value = "<a href='".$value."' target='_blank'>Click Here!</a>";
		}
	}
	$element = "<span class='wx-field-label'>".$alias.":</span> <span class='wx-field-meta'>".$value."</span>";
	return $element;
}
//****CUSTOM COLUMN STUFF****/
//add columns titles to wovax property page
function wx_prop_col_headers($defaults){
	$new	= array();
	$titles	= array(
		'wx_city_col'	=> 'City',
		'wx_price_col'	=> 'Price',
		'wx_mls_col'	=> 'MLS ID'
	);
	if(!array_key_exists('title', $defaults)){
		$defaults = array_merge($defaults, $titles);
		return $defaults;
	}
	foreach($defaults as $key => $val){
		$new[$key] = $val;
		if($key === 'title'){//insert after title
			$new = array_merge($new, $titles);
		}
	}
	return $new;
}
//Starting wovax property specfic columns. 
function wx_prop_col_content($column_name, $post_id){
	switch($column_name){
		case 'wx_city_col':
			echo get_post_meta($post_id, 'City', TRUE);
			break;
		case 'wx_price_col':
			$value = get_post_meta($post_id, 'Price', TRUE);
			if(is_numeric($value)){
				$value = "$".number_format($value);
			}
			echo $value;
			break;
		case 'wx_mls_col':
			$value = get_post_meta($post_id, 'MLS_No', TRUE);
			if($value === FALSE){
				echo '<strong>No MLS ID!</strong>';
				break;
			}
			echo "#".$value;
			break;
		default:
			break;
	}
}
//make this columns sortable
function wx_prop_col_sort($columns){
	$titles	= array(
		'wx_city_col'	=> 'wx_city_col',
		'wx_price_col'	=> 'wx_price_col',
		'wx_mls_col'	=> 'wx_mls_col'
	);
	$columns = array_merge($columns, $titles);
	return $columns;
}

function wx_prop_sort_load($request_vars) {
	//check if the request is for wovaxproperty
	if(!array_key_exists('post_type', $request_vars) OR 'wovaxproperty' !== $request_vars['post_type']){
		return $request_vars;
	}
	if(!array_key_exists('orderby', $request_vars)){
		return $request_vars;
	}
	$sort_by = array();
	switch($request_vars['orderby']){
		case 'wx_city_col':
			$sort_by = array(
				'meta_key' => 'City',
				'orderby' => 'meta_value'
			);
			break;
		case 'wx_price_col':
			$sort_by = array(
				'meta_key' => 'Price',
				'orderby' => 'meta_value_num'
			);
			break;
		case 'wx_mls_col':
			$sort_by = array(
				'meta_key' => 'MLS_No',
				'orderby' => 'meta_value'
			);
			break;
		default:
			return $request_vars;
			break;
	}
	$vars = array_merge($request_vars, $sort_by);
	return $vars;
}
//add columns to the wovax property page
add_filter('manage_wovaxproperty_posts_columns', 'wx_prop_col_headers');
add_filter('manage_edit-wovaxproperty_sortable_columns', 'wx_prop_col_sort');
add_action('manage_wovaxproperty_posts_custom_column', 'wx_prop_col_content', 10, 2);
//make that sort only loads on the admin load edit page
add_action('load-edit.php', function() {
	add_filter('request', 'wx_prop_sort_load');
});
