<?php

register_widget('Wovax_Carousel');
	
/**
 * The Wovax Carousel Widget
 *
 */

 class Wovax_Carousel extends WP_Widget {

	 //Register widget with WordPress
	 function __construct() {
		 parent::__construct(
			 'Wovax_Carousel', //Base ID
			 __( 'Wovax Property Carousel', 'text_domain' ), //Name
			 array( 'description' => __('Displays a carousel of properties.', 'text_domain'), ) //Args
		 );

		 add_action('sidebar_admin_setup', array($this, 'wovax_carousel_admin_setup'));
	 }
	 //Enqueue jquery necessary for widget form magic.
	 function wovax_carousel_admin_setup() {
			 wp_register_script('wx-admin-js', plugins_url('wovax-rets-client/assets/js/wx-rets-admin.js'), array('jquery', 'jquery-ui-sortable', 'jquery-ui-autocomplete'));
			 wp_enqueue_script('wx-admin-js');
	 }

	 /**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	 function widget( $args, $instance ) {
		// use a template for the output so that it can easily be overridden by theme
		if ( array_key_exists('before_widget', $args) ) echo $args['before_widget'];
		if(file_exists(WX_CUSTOM_TEMPLATE_PATH.'/wx_carousel_template.php')){
			include(WX_CUSTOM_TEMPLATE_PATH.'/wx_carousel_template.php');
		} else {
			include(WOVAX_PROPERTY_DIR.'/templates/wx_carousel_template.php');
		}
		if ( array_key_exists('after_widget', $args) ) echo $args['after_widget'];
	 }

	 /**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	 function form( $instance ) {

		$title = esc_attr($instance['title']);

		$number = esc_attr($instance['number-of-recent-posts']);

		$display = esc_attr($instance['display']);

		$property_category = esc_attr($instance['category']);

		if ($display == 'category_display') {
			$recent = ' hidden';
			$category_display = '';
			$recent_selected = '';
			$category_selected = ' selected';
		} else if ($display == 'recent') {
			$recent = '';
			$category_display = ' hidden';
			$recent_selected = ' selected';
			$category_selected = '';
		} else {
			$recent = '';
			$category_display = ' hidden';
			//$recent_selected = ' selected';
			//$category_selected = '';
		}
		?>
		<div>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title: </label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
			</p>
			<div>
				<select id="<?php echo $this->get_field_id( 'display' ); ?>" name="<?php echo $this->get_field_name( 'display' ); ?>" class="wx-carousel-form-display">
					<option value='recent'<?php echo $recent_selected ?>>Recent Properties</option>
					<option value='category_display'<?php echo $category_selected ?>>Properties in a Category</option>
				</select>
			</div>
			<div class='wx-carousel-recent'<?php echo $recent ?>>
				<label for="<?php echo $this->get_field_id( 'number-of-recent-posts' ); ?>">Number of Recent Posts: </label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'number-of-recent-posts' ); ?>" name="<?php echo $this->get_field_name( 'number-of-recent-posts' ); ?>" type="number" value="<?php echo esc_attr( $number ); ?>">
			</div>
			<div class='wx-carousel-category'<?php echo $category_display ?>>
				<label for="<?php echo $this->get_field_id( 'category' ); ?>">Category: </label>
				<select id="<?php echo $this->get_field_id( 'category' ); ?>" name="<?php echo $this->get_field_name( 'category' ); ?>">
					<option value="none">Select Category</option>
					<?php
						$tax_args = array(
							'orderby'           => 'name', 
							'order'             => 'ASC',
							'hide_empty'        => true, 
							'fields'            => 'all', 
							'hierarchical'      => true,
							'pad_counts'        => false,
							'cache_domain'      => 'core'
						); 
						$categories = get_terms('wovax_property_category', $tax_args);
						foreach ($categories as $category) {
							if ($property_category == $category->slug) {
								$select = ' selected';
							} else {
								$select = '';
							}
							$option = '<option value="'.$category->slug.'"'.$select.'>';
							$option .= $category->name;
							$option .= '</option>';
							echo $option;
						}
					?>
				</select>
			</div>
		</div>
		<?php
	 }

	 /**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	 function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
   		$instance['number-of-recent-posts'] = strip_tags($new_instance['number-of-recent-posts']);
    	$instance['display'] = strip_tags($new_instance['display']);
		$instance['category'] = strip_tags($new_instance['category']);
	   	return $instance;
	 }
 }
