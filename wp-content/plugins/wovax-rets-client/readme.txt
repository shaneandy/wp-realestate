=== Wovax RETS Client ===
Contributors: Wovax, LLC.
Tags: Real Estate, RETS, MLS, API, Wovax
Requires at least: 4.0
Tested up to: 4.4

== Description ==
This plugin will help bring the MLS and the WordPress CMS together.

This is accomplished by having the MLS data imported into our proprietary
software hosted at mls.wovax.io. From there, it is translated into
a format that is easily read by WordPress, and is temporarily hosted
at api.wovax.io.

Once an hour, every hour, your website will send a request to
api.wovax.io. Our server will then reply with all the MLS
Numbers of the active listings on your MLS board. Your website
will analyze this, and depending on a few factors do one of a
few things:
 1.  Delete listings no-longer needed
	a. This will delete that listings post, postmeta, and all associated images
 2.  Import new listings
	a. This is done in 3 steps.
		1.  Load base-post as a draft
		2.  Import post-meta information
		3.  Download associated images
	b. This process will only run if another instance is not already running
 3.  Re-Index local listings
	a. This has 2 main functions
		1.  Deletes listings from local index that are no longer in
		    the properties index on api.wovax.io.
		2.  Deletes properties not in the main post index. This protects
		    against corrupted imported posts and removes old posts.
	b. This process will only run if another instance is not already running

Should for any reason the process stall or crash, you have a few options.
 1.  Manually set the "wovax_rets_processing" option to "FALSE" in the options table.
 2.  Poke The Bear. This can be done by following these steps:
	a. Navigate to Tools -> Poke The Bear
	b. Double check the on-screen info. If you are currently importing data,
	   don't interrupt the process. You will get duplicate listings that
	   will not go away until the next hourly cron job.
	c. If it looks like something went wrong and you are currently not importing
	   data, then you may "Poke The Bear"

The indexing process does NOT check the contents of each post. It only checks the
post status (publish || draft). This means you are free to do any of the following:
 1.  Change the featured image
	a. By default, the first image in the MLS listing is the featured image.
	   If you find yourself looking at a poor picture, you may change that.
 2.  Upload new pictures
 3.  Delete pictures
 4.  Change the meta, or "custom fields," information
 5.  Update the content of the post
	a. For things like spelling or capitalization errors
 6.  Update the title of the post
	a. By default it is the address of the listing

== Installation ==
To install  the plugin, follow these steps:
 * Purchase the plugin from wovax.com
 * Download the plugins *.zip file from the provided link
 * A Wovax, LLC Employee will get in touch for your MLS login information
 * From the plugins menu on your website, install the *.zip file
 * If properties do not start importing within a few hours, contact a Wovax Employee
 * You may have to reset your permalinks if you get 404 errors when viewing posts


== Frequently Asked Questions ==
Nobody has asked questions, as this plugin was just released. Ask questions and
you might get a featured question here!

== Changelog ==
 * 2.0 
	1. Complete overhaul of the import system.
	2. Visual changes.
	 
 * 1.2
	1. Speed and accuracy improvements
	2. Adds custom templates - display code that can be put outside the update loop.
	
 * 1.1 
 	1. Added property carousel widget
	2. Added a property category taxonomy for featuring listings.

 * 1.0
	1.  Initial release