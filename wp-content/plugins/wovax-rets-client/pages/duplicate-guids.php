<?php
namespace WovaxGUIDFixes;
use Exception;
use mysqli;
add_action('admin_menu', function(){
	//NULL(defaults to options.php I belevie) or options.php does not have an admin menu.
	//However you can access the page by going to options.php?page={slug}
	add_submenu_page('options.php', 'Fix Duplicate GUIDs', 'Fix Duplicate GUIDs', 'edit_plugins', 'wx-duplicate-guids', __NAMESPACE__ .'\\fix_guids_page');
	add_submenu_page('options.php', 'Fix Character Set', 'Fix Character Set', 'edit_plugins', 'wx-chg-char-set', __NAMESPACE__ .'\\change_char_set_page');
});

add_action('wp_ajax_wx_fix_dup_guids_action', __NAMESPACE__ .'\\fix_duplicate_guids');
add_action('wp_ajax_wx_get_dup_guids_action', __NAMESPACE__ .'\\get_duplicate_guids');

function fix_guids_page(){
	?>
		<div id='wx-dup-guid-data' style='visibility:hidden;'>
			<h4>Fixing Duplicate GUIDS</h4>
			<i>This may take a bit.</i></br>
			Duplicate GUIDs: <strong id='wx-dup-count'>NaN</strong></br>
			<div id='wx-dup-guid-stat'></div>
		</div>
		<div id='wx-dup-guid-loading'>
			<div class='loader'>Loading...</div>
			<center>Getting Duplicate GUIDS.</br>Please Wait.</center>
		</div>
		<style>
			/*using https://github.com/lukehaas/css-loaders for spinner
			//MIT LICENSE*/
			.loader,
			.loader:before,
			.loader:after {
				background: #ffffff;
				-webkit-animation: load1 1s infinite ease-in-out;
				animation: load1 1s infinite ease-in-out;
				width: 1em;
				height: 4em;
			}
			.loader:before,
			.loader:after {
				position: absolute;
				top: 0;
				content: '';
			}
			.loader:before {
				left: -1.5em;
				-webkit-animation-delay: -0.32s;
				animation-delay: -0.32s;
			}
			.loader {
				color: #ffffff;
				text-indent: -9999em;
				margin: 88px auto;
				position: relative;
				font-size: 11px;
				-webkit-transform: translateZ(0);
				-ms-transform: translateZ(0);
				transform: translateZ(0);
				-webkit-animation-delay: -0.16s;
				animation-delay: -0.16s;
			}
			.loader:after {
				left: 1.5em;
			}
			@-webkit-keyframes load1 {
				0%, 80%, 100% {
					box-shadow: 0 0;
					height: 4em;
				}
				40% {
					box-shadow: 0 -2em;
					height: 5em;
				}
			}
			@keyframes load1 {
				0%, 80%, 100% {
					box-shadow: 0 0;
					height: 4em;
				}
				40% {
					box-shadow: 0 -2em;
					height: 5em;
				}
			}
			#progressbar {
				background-color: black;
				border-radius: 13px;
				padding: 3px;
			}

			#progressbar > div {
				background-color: RoyalBlue;
				width:	1%;
				height: 20px;
				border-radius: 10px;
			}
			
		</style>
		<script>
			function ajax_call(action, args, call_back){
				var url_args = new Array();
				for (key in args) {
					url_args.push(key+ '='+encodeURIComponent(args[key]));
				}
				url_args = url_args.join('&');
				var xhttp = new XMLHttpRequest();
				//call this function when http request state changes.
				xhttp.onreadystatechange = function() {
					//only do somthing when the request is done.
					if(xhttp.readyState === XMLHttpRequest.DONE){
						if(xhttp.status !== 200) {
							console.log('Wovax Ajax: Ajax Conection Error!');
							return false;
						}
						var data = JSON.parse(xhttp.responseText);
						if(data.status !== 'success'){
							console.log('Wovax Ajax Status Error: '+data.status);
							return false;
						}
						//success now set the thumbnail
						call_back(data.data)
						return true;
					}
				};
				var request_url = 'admin-ajax.php/?action='+action;
				if(!(!url_args || 0 === url_args.length)){
					request_url += '&'+url_args;
				}
				console.log(request_url);
				xhttp.open('GET', request_url, true);
				xhttp.send();
				return true;
			}
			var status_box = document.getElementById('wx-dup-guid-stat');
			var loader = document.getElementById('wx-dup-guid-loading');
			function recursive_fix_guids(index, guids, req_sz){
				if(index >= guids.length){
					status_box.innerHTML += '<h3>Done!</h3></br>';
					return true;
				}
				var args = new Array();
				var new_index = index;
				for(i = index; (i < (index+req_sz) && i < guids.length); i++){
					args.push(guids[i]);
					new_index = i;
				}
				new_index += 1;
				var guid_str = args.join('-|-');
				var args = new Array();
				args['guids'] = guid_str;
				ajax_call('wx_fix_dup_guids_action', args, function(data){
					status_box.innerHTML += 'Count: '+(new_index)+'</br>';
					status_box.innerHTML += '<pre>'+data+'</pre></br>';
					loader.scrollIntoView();
					recursive_fix_guids(new_index, guids, req_sz);
				});
				return true;
			}
			var args = new Array();
			ajax_call('wx_get_dup_guids_action', args, function(guids){	
				data_box = document.getElementById('wx-dup-guid-data');
				count = document.getElementById('wx-dup-count');
				loader.style.visibility = 'hidden';
				loader.innerHTML = '';
				data_box.style.visibility = 'visible';
 				count.innerHTML = guids.length;
				recursive_fix_guids(0, guids, 15);
			});	
		</script>
	<?php
}

function get_duplicate_guids(){
	global $wpdb;
	$dup_guids = $wpdb->get_col('SELECT `guid` FROM `'.$wpdb->posts.'` GROUP BY `guid` HAVING COUNT(*) > 1');
	$data = array(
		'status' => 'success',
		'data' => $dup_guids
	);
	echo json_encode($data);
	wp_die();
}

function fix_duplicate_guids(){
	global $wpdb;
	$guids = array_key_exists('guids', $_GET) ? $_GET['guids'] : '__NA__';
	if($guid === '__NA__'){
		wp_die('Bad guid');
	}
	$guids = explode('-|-', $guids);
	$results = array();
	foreach($guids as $guid){
		$sql = 'SELECT `ID` FROM `'.$wpdb->posts.'` WHERE `guid` = \''.$guid.'\';';
		$post_ids = $wpdb->get_col($sql);
		$results[] =  'Duplicate GUID: '.$guid."\n";
		foreach($post_ids as $post_id){
			$new_guid = wp_slash(sprintf('urn:uuid:%s', wx_UUID_get_v4()));
			$sql = 'UPDATE `'.$wpdb->posts.'` SET `guid` = \''.$new_guid.'\' WHERE `ID` = '.$post_id.';';
			$ret = $wpdb->get_results($sql);
			$success_str = ($ret === FALSE) ? "<strong><font color=\"FF0000\">Failed!</font></strong>" : "<strong><font color=\"00E600\">Success!</font></strong>";
			$results[] =  'Setting GUID for '.$post_id.' to: '.$new_guid.' - '.$success_str."\n";
		}
		$results[] = "\n";
	}
	$data = array(
		'status' => 'success',
		'data' => implode('', $results)
	);
	echo json_encode($data);
	wp_die();
}

function change_char_set_page(){
	global $wpdb;
	$sql = 'ALTER TABLE `'.$wpdb->posts.'` MODIFY `guid` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci;';
	$changed = $wpdb->query($sql);
	if($changed === FALSE){
		throw new Exception('Failed to change table default charset.'.$wpdb->last_error);
	}
	$sql = 'ALTER TABLE `'.$wpdb->posts.'` CHARACTER SET utf8 COLLATE utf8_general_ci;';
	$changed = $wpdb->query($sql);
	if($changed === FALSE){
		throw new Exception('Failed to change guid charset.'.$wpdb->last_error);
	}
	echo '<h3>Character set change done!<h3>';
}

if(is_admin()){
	add_action('init', function(){
		global $wpdb;
		//lastly the unique constraint exists on the guid in in the posts table
		$selected_db = $wpdb->get_var('SELECT DATABASE()');
		if($selected_db === NULL OR $selected_db === FALSE){
			//umm if this returns nothings the handle is not setup right
			throw new Exception('Can not query select database! '.$wpdb->last_error);
		}
		$sql = "SELECT count(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_NAME = 'uc_guid' AND CONSTRAINT_TYPE = 'UNIQUE' AND TABLE_SCHEMA = '".$selected_db."' AND TABLE_NAME = '".$wpdb->posts."'";
		$ret = $wpdb->get_var($sql);
		if($ret === FALSE){
			throw new Exception('Unable to check if unique constraint exists! '.$wpdb->last_error);
		}
		$constraint_exists = boolval($ret);
		if($constraint_exists === FALSE){
			//the unique constraint does not exist so lets add it
			$sql = "ALTER TABLE `".$wpdb->posts."` ADD CONSTRAINT UNIQUE `uc_guid` (`guid`)";
			$added = $wpdb->query($sql);
			if($added === FALSE AND current_user_can('edit_plugins')){
				$db_handle = $wpdb->dbh;
				$last_error = $wpdb->last_error;
				$error_code = 0;
				if($db_handle instanceof mysqli){
					$error_code = $db_handle->errno;
				}else if(is_resource($db_handle) AND get_resource_type($db_handle) === 'mysql link'){
					$error_code = mysql_errno($db_handle);
				} else {
					throw new Exception('Improper database handle type! '.var_export($db_handle, true));
				}
				add_action('admin_notices', function() use ($last_error, $error_code){
					global $wpdb;
					$fixes[] = array();
					$fixes['unkown_code'] = 'Could not identify the error code: '.$error_code.' Please try the following';
					$fixes['1586'] = 'Fixing duplicate guid entrys <strong><a href="'.menu_page_url('wx-duplicate-guids', false).'">Here</a></strong>';
					$fixes['1071'] = 'Try changing the table character set <strong><a href="'.menu_page_url('wx-chg-char-set', false).'">Here</a></strong>';
					switch($error_code){
						case 1071://column charset too long
							$fixes = array('1071' => $fixes['1071']);
							break;
						case 1586://duplicate entry
							$fixes = array('1586' => $fixes['1586']);
							break;
						default:
							break;
					}
					?>
					<div class="error notice">
						<p>
							<?php echo '<strong>Error</strong>: Could not add unique constraint to guid column! <strong>Error Message</strong>: '.$last_error?></br>
							<?php echo implode('</br>', $fixes) ?>;
						</p>	
					</div>
					<?php
				});
			}
		}
	});
}