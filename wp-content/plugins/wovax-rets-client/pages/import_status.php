<?php
$serverManager = RetsServer::getInstance();
$retsProperties = RetsProperties::getInstance();

if($serverManager->getHaltStatus() === true){
	$halt_class = 'wovax-unhalt';
	$halt_text = 'Resume Importing';
} else {
	$halt_class = 'wovax-halt';
	$halt_text = 'Halt Importing!';
}
?>
<h2>MLS Property Import Information</h2>

<table class="form-table">
	<tr>
		<th>Halt Importing</th>
		<td id="wovax-rets-halt-section">
			<input type="submit" class="button button-primary <?php echo $halt_class;?>" id="wovax-rets-halt-button" value="<?php echo $halt_text;?>"/>
			<div class="spinner"></div>
		</td>
	</tr>
	<tr>
		<td colspan="2">
  			Stops all of this plugin's long-running scripts, like the property importer.
		</td>
	</tr>
</table>

<h3>When was the last property imported?</h3>
<?php
  $seconds_ago = time() - $retsProperties->lastImport();
  $seconds = ($seconds_ago % 60)." Seconds ";
  $minutes = floor(($seconds_ago/60)%60)." Minutes ";
  $hours = floor(($seconds_ago/60/60)%24)." Hours ";
  $days = floor(($seconds_ago/60/60/24)%7)." Days ";
  $weeks = floor(($seconds_ago/60/60/24/7))." Weeks ";
  echo "<b>".$weeks.$days.$hours.$minutes.$seconds."Ago</b>";
?>
<p>
  During the initial loading process, properties will come in fairly often.
  Once this finishes though, new ones will only be added when they are available
  to the server.
<br><br>

<h3>How many properties do I have?</h3>
<?php
  $active_ids = $serverManager->getActiveCount();
  $local_totals = $retsProperties->totals();
  $active_total = abs($local_totals['total'] - $local_totals['custom'] - $local_totals['retained_inactive']);
  if ($active_ids == 0) {
    echo "<b>N/A %</b>";
  }else {
    echo "<b>". ceil(($active_total / $active_ids) * 100) .'%</b><br>';
  }
  echo "<p>There are currently ".$active_ids." properties registered on the server with your site.<br>";
  echo "Of those, your site shows as having ".$active_total.".<br>";
  echo "If the number is way off, reload the page. If it changes then our server is reindexing your data.<br>";
  echo "Don't worry if you have too many or too few, it usually sorts itself out.</p>";
?>