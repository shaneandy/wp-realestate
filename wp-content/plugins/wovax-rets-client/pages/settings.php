<?php
	// Setup for the Field tabs
	$serverManager = RetsServer::getInstance();
	$retsProperties = RetsProperties::getInstance();
	$fieldManager = FieldManager::getInstance();
	$fields = $serverManager->getAllFields();
	//Update field list with any new fields
	$load_fields = $fieldManager->loadFields($fields);
	//Grab the saved fields for the Field Display Settings tab
	$fields_above = $fieldManager->getAbove();
	$fields_below = $fieldManager->getBelow();
	$fields_hidden = $fieldManager->getUnusedFields();
	//Grab the saved fields for the Title and Status Settings tab
	$featured_fields = $fieldManager->getFeatured();
	$fields_unused = $fieldManager->getUnusedTitleFields();
	$title_modified = $fieldManager->getTitle();
	//Grab the saved fields for the Field Aliases tab
	$aliased_fields = $fieldManager->getAllFields();//Subject to change.
	
	// Setup for the advanced settings tab.
	// Gets Default User variable.
	$default_post_user = $retsProperties->getPostUser();
	
	$preserved = $retsProperties->autoRetain();
	if($preserved === true){
		$preserved_class = 'wovax-preserve-off';
		$preserved_text = 'Turn Preserve Featured Off';
	} else {
		$preserved_class = 'wovax-preserve-on';
		$preserved_text = 'Turn Preserve Featured On';
	}

	if($retsProperties->removeWhite() === TRUE) {
		$remove_class = 'wx-rets-remove-turn-off';
		$remove_text = 'Turn Off';
	} else {
		$remove_class = 'wx-rets-remove-turn-on';
		$remove_text = 'Turn On';
	}
	
	// Setup for the gallery settings tab.
	$gallery_type = get_option('wovax_rets_property_gallery_type','preview');
    
	//Saves the realtor info.
	if (sizeof($_POST) != 0) {
		if ($_POST['legal'] != "") {
			$legal = $_POST['legal'];
			update_option("wovax_rets_legal_notice",stripslashes($legal));
			unset($_POST['legal']);
		}
		if ($_POST['wx_idx_logo'] != "") {
			$logo = $_POST['wx_idx_logo'];
			update_option("wovax_rets_idx_logo", $logo);
			unset($_POST['wx_idx_logo']);
		}
		if ($_POST['wx_idx_contact'] != "") {
			$contact = $_POST['wx_idx_contact'];
			update_option("wovax_rets_contact_email", $contact);
			unset($_POST['wx_idx_contact']);
		}
		if ($_POST['wx_idx_realtor_name'] != "") {
			$realtor_name = $_POST['wx_idx_realtor_name'];
			update_option("wovax_rets_realtor_name", $realtor_name);
			unset($_POST['wx_idx_realtor_name']);
		}
		if ($_POST['wx_idx_realtor_address1'] != "") {
			$realtor_address1 = $_POST['wx_idx_realtor_address1'];
			update_option("wovax_rets_realtor_address1", $realtor_address1);
			unset($_POST['wx_idx_realtor_address1']);
		}
		if ($_POST['wx_idx_realtor_address2'] != "") {
			$realtor_address2 = $_POST['wx_idx_realtor_address2'];
			update_option("wovax_rets_realtor_address2", $realtor_address2);
			unset($_POST['wx_idx_realtor_address2']);
		}
		if ($_POST['wx_idx_realtor_phone'] != "") {
			$realtor_phone = $_POST['wx_idx_realtor_phone'];
			update_option("wovax_rets_realtor_phone", $realtor_phone);
			unset($_POST['wx_idx_realtor_phone']);
		}
		//$settings = json_encode($_POST);
		//update_option('wovax_rets_post_layout',$settings);
	}

	function wx_selected($compare,$against) {
		if ($compare == $against) {
			return " selected ";
		}else {
			return "";
		}
	}
?>
<div class="wrap">
	<h2>Settings</h2>
	<h2 class="wovax-nav-wrapper nav-tab-wrapper">
		<a class="nav-tab nav-tab-active" href="#">Field Display Settings</a>
		<a class="nav-tab" href="#">Title and Status Settings</a>
		<a class="nav-tab" href="#">Field Aliases</a>
		<a class="nav-tab" href="#">Realtor Customization Settings</a>
		<a class="nav-tab" href="#">Gallery Settings</a>
		<a class="nav-tab" href="#">Advanced Settings</a>
	</h2>
	<form enctype="multipart/form-data" action="" method="POST">
		 <div id="wovax-rets-settings-sections">
            <section id="wovax-field-display-settings">
                <h3>Locations</h3>
                <br />
                <div class="wx-rets-admin-hidden-div">
                    <h3>Hidden Fields</h3>
                    <ul id="wx-hidden-content-sortable" class="wx-sortable-fields">
                        <?php foreach($fields_hidden as $field => $alias) { ?>
                            <li class="wx-rets-admin-fields" data-field="<?php echo $field; ?>"><?php echo $alias; ?></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="wx-rets-admin-above-div">
                    <h3>Above Content</h3>
                    <ul id="wx-above-content-sortable" class="wx-sortable-fields">
                        <?php foreach($fields_above as $field => $alias) { ?>
                            <li class="wx-rets-admin-fields" data-field="<?php echo $field; ?>"><?php echo $alias; ?></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="wx-content-display">
                    POST CONTENT
                </div>
                <div class="wx-rets-admin-below-div">
                    <h3>Below Content</h3>
                    <ul id="wx-below-content-sortable" class="wx-sortable-fields">
                        <?php foreach($fields_below as $field => $alias) { ?>
                            <li class="wx-rets-admin-fields" data-field="<?php echo $field; ?>"><?php echo $alias; ?></li>
                        <?php } ?>
                    </ul>
                </div>                
                <br />
			</section>
			<section id="wovax-rets-title-status" hidden>
                <h3>Title and Image Display Settings</h3>
                <div class="wx-rets-admin-unused-div">
                    <h3>Unused Fields</h3>
                    <ul id="wx-unused-sortable" class="wx-sortable-fields">
                        <?php foreach($fields_unused as $field => $alias) { ?>
                            <li class="wx-rets-admin-fields" data-field="<?php echo $field; ?>"><?php echo $alias; ?></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="wx-rets-admin-title-div">
                    <h4>Post Title Modifiers</h4>
                    <ul id="wx-rets-title-modifier" class="wx-sortable-fields">
                        <?php foreach($title_modified as $field => $alias) {
                            if($field === '__title__' || $field === "Existing Title"){ ?>
                                <li class="wx-rets-existing-title" data-field="__title__">Existing Title</li>
                            
                        <?php } else { ?>  
                            <li class="wx-rets-admin-fields" data-field="<?php echo $field; ?>"><?php echo $alias; ?></li>
                        <?php }
                        }  ?>                
                    </ul>
                </div>
                <div class="wx-rets-admin-featured-field-div">
                    <h4>Featured Field Box</h4>
                    <ul id="wx-rets-featured-field" class="wx-sortable-fields">
                        <?php foreach($featured_fields as $field => $alias) { ?>
                            <li class="wx-rets-admin-fields" data-field="<?php echo $field; ?>"><?php echo $alias; ?></li>
                        <?php } ?>
                    </ul>
                </div>
            </section>
			<section id="wovax-rets-field-aliases" hidden>
				<h3>Field Aliases</h3>
				<table id="wovax-rets-field-alias-table" class="form-table">
					<?php
					foreach($aliased_fields as $field => $alias){
						?>
						<tr class="wx-rets-aliased-field-rows">
							<th class="wx-rets-field-names"><?php echo $field;?></th>
							<td><input type="text" class="wx-rets-field-aliases" value="<?php echo $alias;?>"/></td>
						</tr>
						<?php
					}
					?>
				</table>
				<div id="wovax-rets-save-aliased-fields-section">
					<a id="wovax-rets-save-aliased-field-names" class='button button-primary'>Save</a><div class="wx-rets spinner"></div>
				</div>
			</section>
			<section id="wovax-rets-realtor-information" hidden>
				<h3>Legal Notice and Information</h3>
				<table class='form-table'>
					<tr>
						<td colspan='2'>
							<textarea  name="legal" cols="100" rows="10"><?php echo get_option("wovax_rets_legal_notice");?></textarea>
						</td>
					</tr>
					<tr>
						<th>
							<strong>IDX Logo</strong>
						</th>
						<td>
							<input
								type='text'
								name='wx_idx_logo'
								id='wx_idx_logo'
								class='wovax-image-url'
								value='<?php echo "'" . get_option("wovax_rets_idx_logo") . "'"; ?>'
							>
							<input type='button' class='wx-idx-upload button-secondary' value='Upload IDX Logo'>
							<img src=<?php echo "'" . get_option("wovax_rets_idx_logo") . "'"; ?>>
						</td>
					</tr>
					<tr>
						<th>
							<strong>Contact Form Email Address</strong>
						</th>
						<td>
							<input type='email' name='wx_idx_contact' id='wx_idx_contact' value=<?php echo "'" . get_option("wovax_rets_contact_email") . "'"; ?>>
						</td>
					</tr>
					<tr>
						<th>
							<strong>Realtor Name</strong>
						</th>
						<td>
							<input type='text' name='wx_idx_realtor_name' id='wx_idx_realtor_name' value=<?php echo "'" . get_option("wovax_rets_realtor_name") . "'"; ?> >
						</td>
					</tr>
					<tr>
						<th>
							<strong>Address Line 1</strong>
						</th>
						<td>
							<input type='text' name='wx_idx_realtor_address1' id='wx_idx_realtor_address1' value=<?php echo "'" . get_option("wovax_rets_realtor_address1") . "'"; ?> >
						</td>
					</tr>
					<tr>
						<th>
							<strong>Address Line 2</strong>
						</th>
						<td>
							<input type='text' name='wx_idx_realtor_address2' id='wx_idx_realtor_address2' value=<?php echo "'" . get_option("wovax_rets_realtor_address2") . "'"; ?> > <br />
						</td>
					</tr>
					<tr>
						<th>
							<strong>Phone Number</strong>
						</th>
						<td>
							<input type='text' name='wx_idx_realtor_phone' id='wx_idx_realtor_phone' value=<?php echo "'" . get_option("wovax_rets_realtor_phone") . "'"; ?> > <br />
						</td>
					</tr>
				</table>
				<input type='submit' class='button button-primary' value='Save'>
			</section>
			<section id="wovax-rets-gallery-settings" hidden>
				<h3>Gallery Settings</h3>
				<table class='form-table'>
					<tr>
						<th>
							<strong>Property Gallery Style</strong>
						</th>
						<td>
							<select name="wx_property_gallery_type" id="wovax-property-gallery-type">
								<option value="preview" <?php echo ($gallery_type === 'preview' ? ' selected' : ''); ?>>Preview Gallery (Default)</option>
								<option value="fullsize" <?php echo ($gallery_type === 'fullsize' ? ' selected' : ''); ?>>Full-Size Gallery</option>
								<option value="off" <?php echo ($gallery_type === 'off' ? ' selected' : ''); ?>>Off</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>
								Preview Gallery (Default)
						</td>
						<td>
							<img width="500px" src="<?php echo plugins_url( '/assets/images/PreviewGallery.jpg', dirname(__FILE__) );?>"/>
						</td>
					</tr>
					<tr>
						<td>
								Full Size Gallery Image
						</td>
						<td>
								<img width="500px" src="<?php echo plugins_url( '/assets/images/FullSizeGallery.jpg', dirname(__FILE__) );?>"/>
						</td>
					</tr>
				</table>
			</section>
			<section id="wovax-rets-advanced-settings" hidden>
				<h3>Advanced Settings</h3>
				<table class="form-table">
					<tr>
						<th>Preserve Featured Properties</th>
						<td id="wovax-rets-preserve-featured-section"><input type="submit" class="button button-primary <?php echo $preserved_class;?>" id="wovax-rets-preserve-button" value="<?php echo $preserved_text;?>"/><div class="wx-rets spinner"></div></td>
					</tr>
					<tr>
						<td colspan="2">
							<small>When on, Featured properties are no longer removed from the system when they are sold or go off market. This feature may only be used with the express
							permission and consent of your MLS board. Failure to heed this notice may result in penalties from your board, up to and including loss of access to their data.</small>
						</td>
					</tr>
					<tr>
						<th>Set Default Image URL</th>
						<td id="wovax-rets-default-image-section"><input
								type='text'
								id='wovax_rets_default_property_image'
								class='wx-rets-default-image-url'
								value="<?php echo $retsProperties->getDefaultImageUrl(); ?>"
							>
							<input type='button' class='wx-rets-default-upload button-secondary' value='Upload Default Property Image'>
							<input type='submit' class='button button-primary' id="wovax-rets-save-default-image-url" value='Save'>
							<div class="wx-rets spinner"></div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<small>This is the default image that is used whenever a property doesn't have any images set in the system.</small>
						</td>
					</tr>
					<tr>
						<th>Set Property User</th>
						<td id="wovax-rets-user-section">
							<select id="wovax-rets-property-user-select">
								<?php
								$users = get_users();
								foreach($users as $user){
									if($user->user_login === $default_post_user){
										$default_user_option = ' selected';
									} else {
										$default_user_option = '';
									}
									?>
									<option value="<?php echo $user->user_login;?>" <?php echo $default_user_option;?>><?php echo $user->user_login;?></option>
									<?php
								}
								?>
							</select>
							<div class="wx-rets spinner"></div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<small>This is the user that will be used to post properties. By default, a wovax user is created with Subscriber capabilities.
							This setting should not be changed without Wovax supervision.</small>
						</td>
					</tr>
					<tr>
						<th>Set Server Access Key</th>
						<td id="wovax-rets-server-key-section">
							<input 
								type="text" 
								id="wovax-rets-server-access-key"
								value="<?php echo $serverManager->getKey(); ?>"
							/>
							<input type='submit' class='button button-primary' id="wovax-rets-save-server-key" value='Save'>
							<div class="wx-rets spinner"></div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<small>This is the key that allows this site to communicate with the main Wovax server and import properties. This setting should not be changed without Wovax supervision.</small>
						</td>
					</tr>
					<tr>
						<th>Set Server URL</th>
						<td id="wovax-rets-server-url-section">
							<input 
								type="text" 
								id="wovax-rets-server-url"
								value="<?php echo $serverManager->getDomain(); ?>"
							/>
							<input type='submit' class='button button-primary' id="wovax-rets-save-server-url" value='Save'>
							<div class="wx-rets spinner"></div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<small>This is the location that the plugin will import properties from. This setting should not be changed without Wovax supervision.</small>
						</td>
					</tr>
					<tr>
						<th>Remove White Borders</th>
						<td id="wovax-rets-remove-borders-section">
							<input type="submit" class="button button-primary <?php echo $remove_class;?>" id="wovax-rets-remove-borders-button" value="<?php echo $remove_text;?>"/>
							<div class="wx-rets spinner"></div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<small>If your board provides images that have white borders around them, this option removes those borders.</small>
						</td>
					</tr>
				</table>
			</section>
		</div>
	</form>
</div>