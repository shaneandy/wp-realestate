<?php

add_action('admin_menu', function(){
	//NULL(defaults to options.php I belevie) or options.php does not have an admin menu.
	//However you can access the page by going to options.php?page={slug}
	add_submenu_page('options.php', 'Fix Posts with No Images', 'Fix Posts with No Images', 'edit_plugins', 'wx-no-images', 'wx_fix_no_images_page');
});

add_action('wp_ajax_wx_fix_no_img_action', 'wx_delete_post_no_images');

function wx_fix_no_images_page(){
	?>
		<div id='wx-no-img'>
			Starting to delete woxaxproperty posts with no images.</br>
		</div>
		<div id='wx-scroll-point'></div>
		<script>
			function ajax_call(action, args, call_back){
				var url_args = new Array();
				for (key in args) {
					url_args.push(key+ '='+encodeURIComponent(args[key]));
				}
				url_args = url_args.join('&');
				var xhttp = new XMLHttpRequest();
				//call this function when http request state changes.
				xhttp.onreadystatechange = function() {
					//only do somthing when the request is done.
					if(xhttp.readyState === XMLHttpRequest.DONE){
						if(xhttp.status !== 200) {
							console.log('Wovax Ajax: Ajax Conection Error!');
							return false;
						}
						var data = JSON.parse(xhttp.responseText);
						if(data.status !== 'success'){
							console.log('Wovax Ajax Status Error: '+data.status);
							return false;
						}
						//success now set the thumbnail
						call_back(data.data)
						return true;
					}
				};
				var request_url = 'admin-ajax.php/?action='+action;
				if(!(!url_args || 0 === url_args.length)){
					request_url += '&'+url_args;
				}
				console.log(request_url);
				xhttp.open('GET', request_url, true);
				xhttp.send();
				return true;
			}
			var status_box = document.getElementById('wx-no-img');
			var scroll_point = document.getElementById('wx-scroll-point');
			var args = new Array();
			function recursive_fix_no_img(){
				ajax_call('wx_fix_no_img_action', args, function(data){
					if(data.complete === 'done'){
						status_box.innerHTML += '<strong>Done!</strong></br>';
						console.log(data);
						return true;
					}
					status_box.innerHTML += 'Deleted post id: '+data.id+'</br>';
					for(data.status in status){
						if(status === false){
							status_box.innerHTML += 'A failure occured!</br>';
						}
					}
					scroll_point.scrollIntoView();
					recursive_fix_no_img();
				});
				return true;
			}
			recursive_fix_no_img();
		</script>
	<?php
}

function wx_delete_post_no_images(){
	$data = array(
		'complete' => 'running',
		'id' => 'N/A',
		'status' => true
	);
	global $wpdb;
	$sql = "SELECT a.ID FROM `".$wpdb->posts."` a LEFT JOIN `".$wpdb->posts."` b ON (b.post_parent = a.ID AND b.post_type = 'attachment') WHERE a.post_type =  'wovaxproperty' AND b.post_parent IS NULL LIMIT 1";
	$post_id = intval($wpdb->get_var($sql));
	if($post_id === 0){
		$data['complete'] = 'done';
	}
	$data['id'] = $post_id;
	$data['status'] = wp_delete_post($post_id, TRUE);
	$ret_data = array(
		'status' => 'success',
		'data' => $data
	);
	echo json_encode($ret_data);
	wp_die();
}