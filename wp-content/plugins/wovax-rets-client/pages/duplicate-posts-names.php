<?php

add_action('admin_menu', function(){
	//NULL(defaults to options.php I belevie) or options.php does not have an admin menu.
	//However you can access the page by going to options.php?page={slug}
	add_submenu_page('options.php', 'Fix Duplicate Post Names', 'Fix Duplicate Post Names', 'edit_plugins', 'wx-duplicate-post-names', 'wx_fix_post_names_page');
});

add_action('wp_ajax_wx_fix_dup_pname_action', 'wx_fix_duplicate_post_names');

function wx_fix_post_names_page(){
	?>
		<div id='wx-dup-pname-stat'>
			Starting Duplicate Post Name Fix</br>
		</div>
		<div id='wx-scroll-point'></div>
		<script>
			function ajax_call(action, args, call_back){
				var url_args = new Array();
				for (key in args) {
					url_args.push(key+ '='+encodeURIComponent(args[key]));
				}
				url_args = url_args.join('&');
				var xhttp = new XMLHttpRequest();
				//call this function when http request state changes.
				xhttp.onreadystatechange = function() {
					//only do somthing when the request is done.
					if(xhttp.readyState === XMLHttpRequest.DONE){
						if(xhttp.status !== 200) {
							console.log('Wovax Ajax: Ajax Conection Error!');
							return false;
						}
						var data = JSON.parse(xhttp.responseText);
						if(data.status !== 'success'){
							console.log('Wovax Ajax Status Error: '+data.status);
							return false;
						}
						//success now set the thumbnail
						call_back(data.data)
						return true;
					}
				};
				var request_url = 'admin-ajax.php/?action='+action;
				if(!(!url_args || 0 === url_args.length)){
					request_url += '&'+url_args;
				}
				console.log(request_url);
				xhttp.open('GET', request_url, true);
				xhttp.send();
				return true;
			}
			var status_box = document.getElementById('wx-dup-pname-stat');
			var scroll_point = document.getElementById('wx-scroll-point');
			var args = new Array();
			function recursive_fix_pname(){
				ajax_call('wx_fix_dup_pname_action', args, function(data){
					if(data.complete === 'done'){
						status_box.innerHTML += '<strong>Done!</strong></br>';
						console.log(data);
						return true;
					}
					status_box.innerHTML += 'Fixed post name '+data.name+' had '+data.count+' duplicates</br>';
					for(data.status in status){
						if(status === false){
							status_box.innerHTML += 'A failure occured!</br>';
						}
					}
					scroll_point.scrollIntoView();
					recursive_fix_pname();
				});
				return true;
			}
			recursive_fix_pname();
		</script>
	<?php
}

function wx_fix_duplicate_post_names(){
	global $wpdb;
	$sql = "SELECT `post_name`, count(*) AS `count` FROM `".$wpdb->posts."` WHERE `post_type` = 'wovaxproperty' GROUP BY `post_name` HAVING count(*) > 1 LIMIT 1";
	$duplicate = $wpdb->get_row($sql, ARRAY_A);
	$data = array(
		'complete' => 'running',
		'name' => 'N/A',
		'count' => 0,
		'status' => array()
	);
	if(empty($duplicate) or $duplicate === FALSE){
		$data['complete'] = 'done';
		$data['status'] = var_export($duplicate, true);
	} else {
		$data['count'] = $duplicate['count'];
		$data['name'] = $duplicate['post_name'];
		$sql = "SELECT `ID` as `post_id` FROM `".$wpdb->posts."` WHERE `post_name` = '%s'";
		$sql = $wpdb->prepare($sql, $duplicate['post_name']);
		$duplicates = $wpdb->get_col($sql);
		$is_first = TRUE;
		foreach($duplicates as $index => $post_id){
			$new_name = $duplicate['post_name'].'-'.$index;
			if($is_first){
				$is_first = FALSE;
				continue;
			}
			$sql = 'UPDATE `'.$wpdb->posts."` SET `post_name` = '".$new_name."' WHERE `ID` = ".$post_id.';';
			$data['status'][$post_id] = boolval($wpdb->query($sql));
		}
	}
	$ret_data = array(
		'status' => 'success',
		'data' => $data
	);
	echo json_encode($ret_data);
	wp_die();
}