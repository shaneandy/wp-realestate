<?php
	$fields = file_get_contents("http://api.wovax.io/api_key/rets/list_custom_fields/");
	$fields = json_decode($fields);
	$fields = $fields->ids;
	
	$fields_above = json_decode(get_option('wovax_rets_fields_above'));
	$fields_below = json_decode(get_option('wovax_rets_fields_below'));
	$fields_hidden = json_decode(get_option('wovax_rets_fields_hidden'));
	
	if(!get_option('wovax_rets_fields_above')) {
		$fields_above = array();
	}
	if(!get_option('wovax_rets_fields_below')) {
		$fields_below = array();
	}
	if(!get_option('wovax_rets_fields_hidden')) {
		$fields_hidden = array();
	}
	
	$fields_new = array_diff($fields, $fields_above,$fields_below,$fields_hidden);
	
	if(isset($fields_new)){
		foreach($fields_new as $field) {
			$fields_hidden[] = $field;
		}
	}
?>
<div class="wx-rets-admin-hidden-div">
	<h3>Hidden Fields</h3>
	<ul id="wx-hidden-content-sortable" class="wx-sortable-fields">
		<?php foreach($fields_hidden as $field) { ?>
			<li class="wx-rets-admin-fields"><?php echo $field; ?></li>
		<?php } ?>
	</ul>
</div>
<div clas="wx-rets-admin-abov-div">
	<h3>Above Content</h3>
	<ul id="wx-above-content-sortable" class="wx-sortable-fields">
		<?php foreach($fields_above as $field) { ?>
			<li class="wx-rets-admin-fields"><?php echo $field; ?></li>
		<?php } ?>
	</ul>
</div>
<div class="wx-content-display">
	POST CONTENT
</div>
<div class="wx-rets-admin-below-div">
	<h3>Below Content</h3>
	<ul id="wx-below-content-sortable" class="wx-sortable-fields">
		<?php foreach($fields_below as $field) { ?>
			<li class="wx-rets-admin-fields"><?php echo $field; ?></li>
		<?php } ?>
	</ul>
</div>


