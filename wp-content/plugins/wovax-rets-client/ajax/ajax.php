<?php
add_action('wp_ajax_wx_field_order_save', 'wx_field_order_save');

function wx_field_order_save(){
	$data = $_POST['data'];
	$fieldManager = FieldManager::getInstance();
	$above = $data['above'];
	$below = $data['below'];
	$hidden = $data['hidden'];
	
	$fieldManager->massSetAbove($above);
	$fieldManager->massSetBelow($below);
	//update_option('wovax_rets_fields_above', $above);
	//update_option('wovax_rets_fields_below', $below);
	//update_option('wovax_rets_fields_hidden', $hidden);
	
	echo "Order Saved";
	die();
}

add_action('wp_ajax_wx_rets_title_image_save', 'wx_rets_title_image_save');

function wx_rets_title_image_save(){
	$data = $_POST['data'];
	$fieldManager = FieldManager::getInstance();
	$unused = $data['unused'];
	$featured = $data['featured'];
	$title_modified = $data['title'];
	if(is_null($unused) || empty($unused) || !isset($unused)){
		$unused = array();
	}
	if(is_null($featured) || empty($featured) || !isset($featured)){
		$featured = array();
	}
	if(is_null($title_modified) || empty($title_modified) || !isset($title_modified)){
		$title_modified = array("__title__");
	}
	$fieldManager->massSetTitle($title_modified);
	$fieldManager->massSetFeatured($featured);
	//update_option('wovax_rets_modified_title',$title_modified);
	//update_option('wovax_rets_featured_fields',$featured);
	//update_option('wovax_rets_unused_fields',$unused);
	
	echo "Options Saved";
	die();
}

add_action('wp_ajax_wx_rets_client_property_gallery_save','wx_rets_client_property_gallery_save');
function wx_rets_client_property_gallery_save(){
	$gallery_type = $_POST['data'];
	$success = update_option('wovax_rets_property_gallery_type',$gallery_type);
	if($success == FALSE){
		die('Something went wrong.');
	} else {
		die('Success');
	}
}

add_action('wp_ajax_wx_rets_activate_halt','wx_rets_activate_halt');
function wx_rets_activate_halt(){
	$serverManager = RetsServer::getInstance();
	$halt = $serverManager->halt();
	if($halt === true){
		echo "Success!";
		die();
	} else {
		echo "Failure";
		die();
	}
}

add_action('wp_ajax_wx_rets_deactivate_halt','wx_rets_deactivate_halt');
function wx_rets_deactivate_halt(){
	$serverManager = RetsServer::getInstance();
	$halt = $serverManager->unhalt();
	if($halt === true){
		echo "Success!";
		die();
	} else {
		echo "Failure";
		die();
	}
}

add_action('wp_ajax_wx_rets_activate_preserve_featured','wx_rets_activate_preserve_featured');
function wx_rets_activate_preserve_featured(){
	$properties = RetsProperties::getInstance();
	$preserve = $properties->setRetain(true);
	if($preserve === true){
		echo "Success!";
		die();
	} else {
		echo "Failure";
		die();
	}
}

add_action('wp_ajax_wx_rets_deactivate_preserve_featured','wx_rets_deactivate_preserve_featured');
function wx_rets_deactivate_preserve_featured(){
	$properties = RetsProperties::getInstance();
	$preserve = $properties->setRetain(false);
	if($preserve === true){
		echo "Success!";
		die();
	} else {
		echo "Failure";
		die();
	}
}

add_action('wp_ajax_wx_rets_save_default_image','wx_rets_save_default_image');
function wx_rets_save_default_image(){
	$image = $_POST['data'];
	$properties = RetsProperties::getInstance();
	$image_save = $properties->changeDefaultImageUrl($image);
	if($image_save === true){
		echo "Success!";
		die();
	} else {
		echo "Failure";
		die();
	}
}

add_action('wp_ajax_wx_rets_save_server_key','wx_rets_save_server_key');
function wx_rets_save_server_key(){
	$key = $_POST['data'];
	$serverManager = RetsServer::getInstance();
	$key_save = $serverManager->changeKey($key);
	if($key_save === true){
		echo "Success!";
		die();
	} else {
		echo "Failure";
		die();
	}
}

add_action('wp_ajax_wx_rets_save_server_url','wx_rets_save_server_url');
function wx_rets_save_server_url(){
	$url = $_POST['data'];
	$serverManager = RetsServer::getInstance();
	$url_save = $serverManager->changeDomain($url);
	if($url_save === true){
		echo "Success!";
		die();
	} else {
		echo "Failure";
		die();
	}
}

add_action('wp_ajax_wx_rets_save_field_aliases','wx_rets_save_field_aliases');
function wx_rets_save_field_aliases(){
	$fieldManager = FieldManager::getInstance();
	$data = $_POST['data'];
	$fields = $data['fields'];
	$aliases = $data['aliases'];
	$aliased_fields = array_combine($fields,$aliases);
	$fieldManager->massSetFieldAliases($aliased_fields);
	//update_option('wovax_rets_field_aliases',$aliased_fields);
	echo "Done.";
	die();
}

add_action('wp_ajax_wx_rets_save_property_user','wx_rets_save_property_user');
function wx_rets_save_property_user(){
	$user = $_POST['data'];
	$properties = RetsProperties::getInstance();
	$user_save = $properties->changeDefaultUser($user);
	if($user_save === true){
		echo "Success!";
		die();
	} else {
		echo "Failure";
		die();
	}
}

add_action('wp_ajax_wx_rets_borders_on','wx_rets_borders_on');
function wx_rets_borders_on(){
	$properties = RetsProperties::getInstance();
	$success = $properties->setRemoveWhite(TRUE);
	if($success){
		echo 'Success!';
		die();
	} else {
		echo 'Failure';
		die();
	}
}

add_action('wp_ajax_wx_rets_borders_off','wx_rets_borders_off');
function wx_rets_borders_off(){
	$properties = RetsProperties::getInstance();
	$success = $properties->setRemoveWhite(FALSE);
	if($success){
		echo 'Success!';
		die();
	} else {
		echo 'Failure';
		die();
	}
}