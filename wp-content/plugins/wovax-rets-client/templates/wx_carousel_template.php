<?php
/*
 * Template for the output of the Wovax Carousel Widget
 * Override by placing a file called wx_carousel_template.php in wp-content/wovax_templates
 */
 
 	if ($instance['display'] == 'recent') {
		$number = $instance['number-of-recent-posts'];
		$gp_args = array(
				'numberposts'		=> $number,
				'posts_per_page'	=> $number,
				'post_type' 		=> 'wovaxproperty',
				'orderby'			=> 'post_date',
				'order' 			=> 'desc',
				'post_status' 		=> 'publish'
		);
		$properties = get_posts($gp_args);	 
	} else if ($instance['display'] == 'category_display') {
		$gp_args = array(
				'showposts'		=>	-1,
				'post_type' 	=> 	'wovaxproperty',
				'orderby' 		=> 	'post_date',
				'order' 		=> 	'desc',
				'post_status' 	=> 	'publish',
				'tax_query'		=> 	array(
					array(
						'taxonomy'	=>	'wovax_property_category',
						'field'		=>	'slug',
						'terms'		=>	$instance['category'] 
					)
				)
		);
		$properties = get_posts($gp_args); 	
	}
	
	
	
	if ($properties) {
		//Puts the output in a buffer and then outputs the buffer when done.
  		ob_start(); ?>
		<h4 class="widget-title widgettitle"><?php echo $instance['title']; ?></h4>
		<div id='featured-carousel' class='touchcarousel black-and-white'>
			<ul class='touchcarousel-container featured-carousel-container'>
		<?php	  
		foreach ($properties as $property) :
			$price = get_post_meta( $property->ID, 'Price', true );
			$title = get_the_title( $property->ID );
			$city = get_post_meta( $property->ID, 'City', true );
			$state = get_post_meta( $property->ID, 'State', true );
			$image = get_the_post_thumbnail( $property->ID, 'thumbnail');
			$image = str_replace('wp-post-image','',$image); 
			?>
			<li class='touchcarousel-item featured-carousel-item'>
				<a href=' <?php echo esc_url( get_permalink($property->ID) ); ?>' title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'grauke' ), $title ) ); ?>" rel="bookmark">
					<div class="wx-carousel-image">
						<?php echo $image ?>
					</div>
					<div class='wx-carousel-address'>
						<?php echo $title ?>
					</div>
				</a>
				<div class='wx-carousel-citystate'>
					<?php echo $city ?>, <?php echo $state ?>
				</div>
				<div class='wx-carousel-price'>
					$<?php echo number_format($price) ?>
				</div>
			</li>				
		<?php endforeach; ?>
		</ul></div>
		<?php
		$output = ob_get_clean();
		echo $output;
	} else {
		echo __( 'No properties found.', 'text_domain' );
	}
	
	