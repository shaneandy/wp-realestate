<?php 
/*
 * Template for the output of the Preview Image Gallery Carousel
 * Override by placing a file called wx_preview_gallery_template.php in wp-content/wovax_templates
 */
$gallery = "<div id='preview-gallery' class='touchcarousel black-and-white'><ul class='touchcarousel-container'>";
$images = get_attached_media('image',get_the_id());
$ordered_images = [];
$custom_images = [];
foreach ($images as $image) {
	$title_words = explode(' ', $image->post_title);
	if(count($title_words) > 1) {
		$ordered_images[intval($title_words[1])] = $image;
	} else {
		$custom_images[] = $image;
	}	
}
ksort($ordered_images, SORT_NUMERIC);
$ordered_images = array_merge($ordered_images,$custom_images);
foreach ($ordered_images as $image) {
	//we can also use wp_get_attachment_thumb_url instead of wp_get_attachment_image_src
	$thumbnail_object = wp_get_attachment_image_src($image->ID, 'thumbnail');
	$featured_object = wp_get_attachment_image_src($image->ID, 'large');
	$thumbnail = $thumbnail_object[0];
	//$featured could also be set to the value of wp_get_attachment_url($image->ID) if we don't want a resized image.
	//and of course if we do that we don't need to use wp_get_attachment_image_src.
	$featured = $featured_object[0];
	$featured_srcset = wp_get_attachment_image_srcset( $image->ID, 'post-thumbnail' );
	/*$sql = "SELECT `s3_url` FROM `".$wpdb->prefix."rtamazon_s3_media` WHERE wp_url = '".$image->guid."'";
	$s3url = $wpdb->get_results($sql,ARRAY_A);*/
	$gallery .= "<li class='touchcarousel-item property-gallery'>";
	$gallery .= "<img src='".$thumbnail."' data-src='".$featured."' data-srcset='".$featured_srcset."' width='75px' height='75px' />";
	$gallery .= "</li>";
}
$gallery .= "</ul></div>";
$gallery .= $content;
return $gallery;