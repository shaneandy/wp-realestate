<?php
/*
 * Template for the output of the Fullsize Image Gallery Carousel
 * Override by placing a file called wx_fullsize_gallery_template.php in wp-content/wovax_templates
 */
 
$gallery = "<div id='fullsize-gallery' class='fullsize-gallery'>";
$images = get_attached_media('image',get_the_id());
$ordered_images = [];
$custom_images = [];
foreach ($images as $image) {
	$title_words = explode(' ', $image->post_title);
	if(count($title_words) > 1) {
		$ordered_images[intval($title_words[1])] = $image;
	} else {
		$custom_images[] = $image;
	}	
}
ksort($ordered_images, SORT_NUMERIC);
$ordered_images = array_merge($ordered_images,$custom_images);
$image_total = count($ordered_images);
$i = 1;
foreach ($ordered_images as $image) {
	$featured_object = wp_get_attachment_image_src( $image->ID, 'large');
	$featured = $featured_object[0];
	$featured_srcset = wp_get_attachment_image_srcset( $image->ID, 'post-thumbnail' );
	/*$sql = "SELECT `s3_url` FROM `".$wpdb->prefix."rtamazon_s3_media` WHERE wp_url = '".$image->guid."'";
	$s3url = $wpdb->get_results($sql,ARRAY_A);*/
	$gallery .= "<div><div class='wx-rets-image-container'>";
	$gallery .= "<img src='".$featured."' srcset='".$featured_srcset."' />";
	$gallery .= "<div class='wx-rets-image-numbers'><small><em>".$i." of ".$image_total."</small></em></div>";
	$gallery .= "</div></div>";
	$i++;
}
$gallery .= "</div>";
$gallery .= $content;
return $gallery;