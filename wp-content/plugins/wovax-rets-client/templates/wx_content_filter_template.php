<?php
/*
 * Template for the output of the wovaxproperty content filter
 * Override by placing a file called wx_content_filter_template.php in wp-content/wovax_templates
 */
		?>
		<div class="wovax-above">
			<?php
			$post_id = get_the_id();
			$formatted_fields = $fieldManager->getFieldValuesFormatted($post_id,'above','elementFormatter');
			echo implode('<hr class="wovax-field-separator"/>',$formatted_fields);
		?>
		</div>
		<br />
		<?php
		echo $content;
		?>
		<p class='wovax-legal-listing-attribution'>Listing courtesy of <?php echo get_post_meta(get_the_ID(),'Listing_Agency', true);?></p>
		<div class="wovax-below">
			<?php
			$formatted_fields = $fieldManager->getFieldValuesFormatted($post_id,'below','elementFormatter');
			echo implode('<hr class="wovax-field-separator"/>',$formatted_fields);
		?>
		</div>
		<div class='wovax-legal'>
			<img class='wovax-legal-idx-logo' src='<?php echo get_option('wovax_rets_idx_logo'); ?>'>
			<small><?php echo get_option('wovax_rets_legal_notice'); ?></small>
		</div>
		<br />
		<div class='wovax-contact-form'>
			<span class='wovax-contact'>Contact Us!</span>
			<br />
			[contact-form to='<?php echo get_option('wovax_rets_contact_email'); ?>' subject='IDX Contact']
				[contact-field label='Name' type='name' required='1'/]
				[contact-field label='Email' type='email' required='1'/]
				[contact-field label='Message' type='textarea' required='1'/]
			[/contact-form]
		</div>
		<br />
		<div class='wovax-realtor-info'>
			<strong><?php echo get_option('wovax_rets_realtor_name');?></strong>
			<br />
			<span class='wovax-realtor-address'><?php echo get_option('wovax_rets_realtor_address1');?>
			<br> <?php echo get_option('wovax_rets_realtor_address2');?></span>
			<br>
			<span class='wovax-realtor-phone'>Phone: <?php echo get_option('wovax_rets_realtor_phone');?></span>
		</div>