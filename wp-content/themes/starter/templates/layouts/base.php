<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('partials/head'); ?>

  <body <?php body_class(); ?>>
    <?php
      if (have_posts()) : the_post();

        do_action('get_header');
        get_template_part('partials/header');
        include App\template()->main();

      endif;

      do_action('get_footer');
      get_template_part('partials/footer');
      wp_footer();
    ?>
  </body>
</html>
