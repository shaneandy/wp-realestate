<?php
/**
 * Template Name: Listings Template
 */

  $data = \Realty\PageGetters::getHomepageTemplateData();
?>

  <?php \Realty\Template::render('carousel/hero'); ?>

  <div class="container content-container">
    <h3 class="text-uppercase">Results for: 3 Bedroom</h3>

    <?php for ($i = 0; $i <= 4; $i++) : ?>
      <?php \Realty\Template::render('realty/property/preview'); ?>
    <?php endfor; ?>

    <nav aria-label="Page navigation" class="text-center">
      <ul class="pagination">
        <li>
          <a href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
          </a>
        </li>
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li>
          <a href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
          </a>
        </li>
      </ul>
    </nav>
  </div>

<?php
