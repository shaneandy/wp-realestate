<?php
/**
 * Template Name: Home Template
 */

  $data = \Realty\PageGetters::getHomepageTemplateData();
  \Realty\Debug::log($data);
?>

  <?php \Realty\Template::render('carousel/hero'); ?>

  <?php if (isset($data['two_column_block_with_image'])) : ?>
    <div class="container content-container">
      <?php \Realty\Template::render('content/two-col-with-image', [
        'data' => $data['two_column_block_with_image']
      ]); ?>
    </div>
  <?php endif; ?>

  <?php if (isset($data['featured_testimonials'])) : ?>
    <div class="container content-container">
      <?php \Realty\Template::render('carousel/testimonials', [
        'data' => $data['featured_testimonials']
      ]); ?>
    </div>
  <?php endif; ?>

  <div class="container content-container">
    <h4 class="text-center text-uppercase section-header">Featured Properties</h4>

    <?php for ($i = 0; $i <= 2; $i++) : ?>
      <?php \Realty\Template::render('realty/property/preview'); ?>
    <?php endfor; ?>

    <p class="text-center">
      <a href="#" class="btn btn-default btn-large cta">See all Properties</a>
    </p>
  </div>

  <?php if (isset($data['banner_block'])) : ?>
    <div class="content-container">
      <?php \Realty\Template::render('content/jumbotron'); ?>
    </div>
  <?php endif; ?>

  <div class="container content-container padded-container box-shadow">
    <h4 class="text-center text-uppercase section-header">Featured Neighbourhoods</h4>

    <div class="row">
      <?php for ($i = 0; $i <= 3; $i++) : ?>
        <?php \Realty\Template::render('realty/neighbourhood/featured'); ?>
      <?php endfor; ?>
    </div>
  </div>

<?php

