<?php
/**
 * Template Name: Form Template
 */

// Fetch date for the form template
$data = \Realty\PageGetters::getFormTemplateData();
\Realty\Debug::log( $data );

if ($data) :
?>

<?php if (!empty($data['hero_area'])) {
  \Realty\Template::render('hero/standard', [
    'title' => $data['hero_area']['title'],
    'image' => $data['hero_area']['image'],
  ]);
} ?>

<div class="container content-container">
  <?php
    if (!empty($data['content'])) {
      \Realty\Template::render('content/standard', ['data' => $data['content']]);
    }

    if (!empty($data['form'])) {
      \Realty\Template::render('forms/standard', ['data' => $data['form']]);
    }
  ?>
</div>

<?php endif;
