<?php
/**
 * Template Name: Single Listing Template
 */

  if (have_posts()) : while (have_posts()) : the_post();
    $data = \Realty\PageGetters::getHomepageTemplateData();
?>

  <?php \Realty\Template::render('carousel/hero'); ?>

  <div class="container content-container padded-container box-shadow">
    <div class="col-sm-8">
      <span class="badge badge-sale">
        <i class="fa fa-tag" aria-hidden="true"></i>
        For Sale
      </span>

      <h2 class="title">Excellent Single Home for Sale in North Delta</h3>
      <small class="text-uppercase border-bottom location">1093 Royal Avenue | Surrey, BC</small>
      <small class="description">11.613 ACRES! Zones Business office park. Property borders on proposed major arterial intersection. Nice 2bedrm, 2bath rancher.</small>
    </div>
  </div>

<?php endwhile;
  endif;
