<?php
/**
 * Template Name: Video Gallery Template
 */

  // Fetch date for the video gallery template
  $data = \Realty\PageGetters::getVideoGalleryTemplateData();
  \Realty\Debug::log( $data );

  if ($data) :
?>

<?php if (!empty($data['hero_area'])) {
  \Realty\Template::render('hero/standard', [
    'title' => $data['hero_area']['title'],
    'image' => $data['hero_area']['image'],
  ]);
} ?>

<div class="container content-container">
  <?php if (!empty($data['video_gallery'])) : ?>

  <?php endif; ?>
</div>

<?php endif;
