<div class="row">
  <div class="col-md-12">
    <div class="property box-shadow">
      <div class="row">
        <div class="col-lg-4 col-md-5 col-sm-12 carousel carousel-fade slide" id="property-carousel-0" data-ride="carousel" data-interval="false">
          <ol class="carousel-indicators">
            <li data-target="#property-carousel-0" data-slide-to="0" class="active"></li>
            <li data-target="#property-carousel-0" data-slide-to="1"></li>
            <li data-target="#property-carousel-0" data-slide-to="2"></li>
          </ol>

          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="http://aios3-staging.agentimage.com/c/charlesvivash_pending.com/htdocs/wp-content/uploads/2016/12/slide8-compressor.jpg" alt="house" />
            </div>

            <div class="item">
              <img src="http://aios3-staging.agentimage.com/c/charlesvivash_pending.com/htdocs/wp-content/uploads/2016/11/slide6.jpg" alt="house" />
            </div>

            <div class="item">
              <img src="http://aios3-staging.agentimage.com/c/charlesvivash_pending.com/htdocs/wp-content/uploads/2016/12/slide7-compressor.jpg" alt="house" />
            </div>
          </div>

          <span class="badge badge-sale">
            <i class="fa fa-tag" aria-hidden="true"></i>
            For Sale
          </span>
        </div>

        <div class="col-lg-8 col-md-7 col-sm-12 content">
          <small class="text-uppercase status">For Sale</small>
          <h2 class="title">Excellent Single Home for Sale in North Delta</h3>

          <small class="text-uppercase border-bottom location">Surrey, BC</small>
          <small class="description">11.613 ACRES! Zones Business office park. Property borders on proposed major arterial intersection. Nice 2bedrm, 2bath rancher.</small>
          <h2 class="price">$12,000,000</h4>

          <ul class="list-inline attributes hidden-xs">
            <li><span class="icon house">House</span> <span>House</span></li>
            <li><span class="icon bedroom">Bedroom</span> <span>3 Bedroom</span></li>
            <li><span class="icon bathroom">Bathroom</span> <span>2.5 Bathrooms</span></li>
            <li><span class="icon ruler">Ruler</span> <span>980 Square Feet</span></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
