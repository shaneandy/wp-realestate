<?php
  /**
   * Testimonials archive view
   */

  $data = !empty($app_vars['data']) ? $app_vars['data'] : null;

  if ($data) :
?>
  <div class="post-list testimonials-list wrapper">
    <?php
      foreach ($data as $key => $value) :
        $alternate = $key % 2 === 0 ? true : false;
    ?>
      <?php \Realty\Template::render('list/items/testimonial', [
        'data' => $value,
        'alternate' => $alternate,
      ]); ?>
    <?php endforeach; ?>
  </div>
<?php
  endif;
