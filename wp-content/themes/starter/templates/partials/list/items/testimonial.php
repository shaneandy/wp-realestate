<?php
/**
 * Individual testimonial
 */

$data = !empty($app_vars['data']) ? $app_vars['data'] : null;
$alternate = !empty($app_vars['alternate']) ? $app_vars['alternate'] : null;

if ($data) : ?>

<div class="row feature testimonial-item">

 <?php if ($alternate) : ?>

    <div class="col-sm-12 col-md-7 background-white box-shadow post-content">
     <h4 class="text-uppercase quiet testimonial-item-title">
        <?php
          $title = !empty($data['title']) ? $data['title'] : '';
          if (!empty($data['speaker_title'])) {
            $title .= ' | ' . $data['speaker_title'];
          }
          echo $title;
        ?>
      </h4>

      <?php if (!empty($data['content'])) : ?>
        <?php echo apply_filters('the_content', $data['content']); ?>
      <?php endif; ?>
    </div>

    <div class="hidden-xs hidden-sm col-md-4 col-md-offset-1">
      <?php if (!empty($data['image'])) : ?>
        <div class="image-background" style="background-image: url(<?php echo $data['image']['url']; ?>)"></div>
      <?php endif; ?>
    </div>
  </div>

  <?php else : ?>

    <div class="hidden-xs hidden-sm col-md-4 col-md-offset-1">
      <?php if (!empty($data['image'])) : ?>
        <div class="image-background" style="background-image: url(<?php echo $data['image']['url']; ?>)"></div>
      <?php endif; ?>
    </div>

    <div class="col-sm-12 col-md-7 background-white box-shadow pull-right post-content">
      <h4 class="text-uppercase quiet testimonial-item-title">
        <?php
          $title = !empty($data['title']) ? $data['title'] : '';
          if (!empty($data['speaker_title'])) {
            $title .= ' | ' . $data['speaker_title'];
          }
          echo $title;
        ?>
      </h4>

      <?php if (!empty($data['content'])) : ?>
        <?php echo apply_filters('the_content', $data['content']); ?>
      <?php endif; ?>
    </div>

  <?php endif; ?>

</div>

<?php endif;
