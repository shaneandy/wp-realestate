<?php
/**
 * Standard content template
 */

$content = !empty($app_vars['data']) ? $app_vars['data'] : null;

if ($content) :
?>

<div class="standard-content">
  <?php echo apply_filters('the_content', $content); ?>
</div>

<?php endif;
