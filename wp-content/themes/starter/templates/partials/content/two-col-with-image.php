<?php
/**
 * Two col with image template
 */

$content = !empty($app_vars['data']) ? $app_vars['data'] : null;

if ($content) :
?>
  <div class="row vertical-center two-column-with-image">
    <div class="col-lg-5 col-md-6 col-sm-6 hidden-xs">
      <img src="<?php echo $content['two_column_text_block_image'] ?>" />
    </div>

    <div class="col-md-6 col-sm-6 padding-mobile">
      <h1 class="border-bottom">
        <?php echo $content['two_column_text_block_headline'] ?>
      </h1>

      <?php echo $content['two_column_text_block_copy'] ?>
    </div>
  </div>
<?php endif;
