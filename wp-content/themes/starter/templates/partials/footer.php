<?php
  // Fetch global contact form and address info
  $data = \Realty\Post::getFooterData();
  \Realty\Debug::log($data);
?>


<footer class="content-info">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-3">
        <h2 class="logo-white"><?php bloginfo('name'); ?></h2>
      </div>

      <div class="col-md-3 col-sm-4">
        <h3>Our Location</h3>
        <ul>
          <?php if (!empty($data['address']['address_line_1'])) : ?>
            <li><?php echo $data['address']['address_line_1']; ?></li>
          <?php endif; ?>
          <?php if (!empty($data['address']['address_line_2'])) : ?>
            <li><?php echo $data['address']['address_line_2']; ?></li>
          <?php endif; ?>
          <?php if (!empty($data['address']['phone_number'])) : ?>
            <li><?php echo $data['address']['phone_number']; ?></li>
          <?php endif; ?>
        </ul>

        <ul class="list-inline social-icons">
          <li><a href="#" class="icon icon-white facebook">Facebook</a></li>
          <li><a href="#" class="icon icon-white twitter">Twitter</a></li>
        </ul>
      </div>

      <div class="col-md-4 col-md-offset-3">
        <?php if (!empty($data['contact_form'])) {
          \Realty\Template::render('forms/standard.php', [
            'data'       => $data['contact_form'],
            'show_title' => true
          ]);
        } ?>
      </div>
    </div>

    <div class="row">
      <div class="col-md-7 col-md-offset-3">
        <small class="quiet">&copy; Best Coast Realtor. All rights reserved. <a href="#">Sitemap</a> | Website Design by <a href="#">Some Company</a></small>
      </div>
    </div>
  </div>
</footer>
