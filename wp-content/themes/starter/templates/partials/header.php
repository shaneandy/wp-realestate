<nav class="navbar sub-nav hidden-xs<?php if (!is_front_page()) echo ' navbar-fixed-top'; ?>">
  <div class="container">
    <div id="navbar" class="collapse navbar-collapse">
      <p class="navbar-text navbar-right">
        <span class="icon phone">Phone</span>
        604-123-4567
      </p>

      <p class="navbar-text navbar-right">
        <span class="icon mail">Mail</span>
        sales@realtor.com
      </p>
    </div>
  </div>
</nav>

<?php if (is_front_page()) : ?>
  <nav class="navbar primary-nav hidden-xs clearfix">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

        <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>">
          <?php bloginfo('name'); ?>
        </a>
      </div>

      <div id="navbar" class="collapse navbar-collapse">
        <ul class="nav navbar-nav navbar-right social-icons">
          <li><a href="#" class="icon icon-white facebook">Facebook</a></li>
          <li><a href="#" class="icon icon-white twitter">Twitter</a></li>
        </ul>

        <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu([
              'container'      => false,
              'theme_location' => 'primary_navigation',
              'menu_class'     => 'nav navbar-nav navbar-right',
              'walker'         => new wp_bootstrap_navwalker()
            ]);
          endif;
        ?>
      </div>
    </div>
  </nav>
<?php endif; ?>

<nav class="navbar navbar-default mobile-nav navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="collapsed navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a href="<?php echo home_url(); ?>" class="navbar-brand"><?php bloginfo('name'); ?></a>
    </div>

    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav navbar-right social-icons">
        <li><a href="#" class="icon facebook">Facebook</a></li>
        <li><a href="#" class="icon twitter">Twitter</a></li>
      </ul>

      <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu([
            'container'      => false,
            'theme_location' => 'primary_navigation',
            'menu_class'     => 'nav navbar-nav navbar-right',
            'walker'         => new wp_bootstrap_navwalker()
          ]);
        endif;
      ?>
    </div>
  </div>
</nav>

