<?php
/**
 * Standard form template
 *
 * outputs the form based on the gravity form Id that is passed in
 */

$form = !empty($app_vars['data']) ? $app_vars['data'] : null;
$showTitle = !empty($app_vars['show_title']) ? $app_vars['show_title'] : null;

if ($form) :
?>

<div class="standard-form">
  <?php if ($showTitle) : ?>
    <h4><?php echo $form['title']; ?></h4>
  <?php endif; ?>

  <?php
    if (!empty($form['id'])) {
      if (!function_exists('gravity_form')) {
        \Realty\Debug::log('Looks like Gravity Forms isn\'t active');
      } else {
        gravity_form( $form['id'], false, false, false, false, true, 4, 1 );
      }
    }
   ?>
</div>

<?php endif;
