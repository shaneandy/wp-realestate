<?php
/**
 * Testimonial submission form template
 *
 * outputs the form based on the gravity form Id that is passed in
 */

$form = !empty($app_vars['data']) ? $app_vars['data'] : null;

if ($form) :
?>

<div class="testimonial-form background-white hidden-xs hidden-sm">
  <div class="container">
    <div class="col-md-5">
      <h4>Have you worked with Charles? Let us know how it went!</h4>
    </div>

    <div class="col-md-6 pull-right">
      <?php
        if (!empty($form['id'])) {
          if (!function_exists('gravity_form')) {
            \Realty\Debug::log('Looks like Gravity Forms isn\'t active');
          } else {
            gravity_form( $form['id'], false, false, false, false, true, 4, 1 );
          }
        }
       ?>
    </div>
  </div>
</div>

<?php endif;
