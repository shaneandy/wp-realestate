<?php
  /**
   * Standard hero partial to be used across standard pages/posts
   */

  $title = !empty($app_vars['title']) ? $app_vars['title'] : false;
  $image = !empty($app_vars['image']) ? $app_vars['image'] : false;

  if ($title && $image) :
?>

<div class="hero image-background" style="background-image: url('<?php echo $image; ?>')">
  <div class="hero-container container">
    <h1><?php echo $title; ?></h1>
  </div>
</div>

<?php endif;
