<?php
/**
 * Testimonials carousel template
 */

$content = !empty($app_vars['data']) ? $app_vars['data'] : null;

if ($content) :
?>
  <div id="testimonial-carousel" class="carousel carousel-fade testimonial-carousel slide" data-ride="carousel">
    <div class="row box-shadow">
      <div class="col-xs-12">
        <h4 class="text-center text-uppercase section-header">Testimonial</h4>

        <div class="carousel-inner">
          <?php foreach ($content['posts'] as $index => $testimonial) : ?>
            <div class="item <?php echo ($index == 0) ? 'active' : '' ?>">
              <div class="carousel-content">
                <div class="col-md-8 col-md-offset-2 text-center">
                  <?php if (!empty($testimonial['excerpt'])) {
                    echo apply_filters( 'the_content', $testimonial['excerpt'] );
                  } ?>
                  <small class="text-uppercase quiet">
                    <?php
                      $title = !empty($testimonial['title']) ? $testimonial['title'] : '';
                      if (!empty($testimonial['speaker_title'])) {
                        $title .= ' | ' . $testimonial['speaker_title'];
                      }

                      echo $title;
                    ?>
                  </small>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>

        <a class="left carousel-control hidden-sm hidden-xs" href="#testimonial-carousel" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>

        <a class="right carousel-control hidden-sm hidden-xs" href="#testimonial-carousel" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
      </div>
    </div>
  </div>
<?php endif;
