<div id="hero-carousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
  <div class="carousel-sub">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <ul class="list-inline attributes active pull-left hidden-xs">
            <li class="type"><span class="icon house">House</span> <span class="value">House</span></li>
            <li class="bedrooms"><span class="icon bedroom">Bedroom</span> <span class="value">3 Bedroom</span></li>
            <li class="bathrooms"><span class="icon bathroom">Bathroom</span> <span class="value">2.5 Bathrooms</span></li>
            <li class="size"><span class="icon ruler">Ruler</span> <span class="value">980 Square Feet</span></li>
          </ul>

          <ul class="list-inline attributes pull-left hidden-xs">
            <li class="type"><span class="icon house">House</span> <span class="value">Apartment</span></li>
            <li class="bedrooms"><span class="icon bedroom">Bedroom</span> <span class="value">3 Bedroom</span></li>
            <li class="bathrooms"><span class="icon bathroom">Bathroom</span> <span class="value">2.5 Bathrooms</span></li>
            <li class="size"><span class="icon ruler">Ruler</span> <span class="value">980 Square Feet</span></li>
          </ul>

          <ul class="list-inline attributes pull-left hidden-xs">
            <li class="type"><span class="icon house">House</span> <span class="value">Trailor</span></li>
            <li class="bedrooms"><span class="icon bedroom">Bedroom</span> <span class="value">3 Bedroom</span></li>
            <li class="bathrooms"><span class="icon bathroom">Bathroom</span> <span class="value">2.5 Bathrooms</span></li>
            <li class="size"><span class="icon ruler">Ruler</span> <span class="value">980 Square Feet</span></li>
          </ul>

          <ol class="carousel-indicators pull-right">
            <li data-target="#hero-carousel" data-slide-to="0" class="active"></li>
            <li data-target="#hero-carousel" data-slide-to="1"></li>
            <li data-target="#hero-carousel" data-slide-to="2"></li>
          </ol>
        </div>
      </div>
    </div>
  </div>

  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="http://aios3-staging.agentimage.com/c/charlesvivash_pending.com/htdocs/wp-content/uploads/2016/11/slide6.jpg" alt="image" />

      <div class="carousel-caption container">
        <div class="row">
          <div class="col-sm-12">
            <span class="badge badge-sale hidden-xs">
              <i class="fa fa-tag" aria-hidden="true"></i>
              For Sale
            </span>

            <h1>Beautiful Cozy 2-Beedroom Townhouse in the Heart of White Rock</h1>
            <h4 class="text-uppercase hidden-xs">White Rock, BC</h4>
          </div>
        </div>
      </div>
    </div>

    <div class="item">
      <img src="http://aios3-staging.agentimage.com/c/charlesvivash_pending.com/htdocs/wp-content/uploads/2016/12/slide8-compressor.jpg" alt="image" />

      <div class="carousel-caption container">
        <div class="row">
          <div class="col-sm-12">
            <span class="badge badge-rent hidden-xs">
              <i class="fa fa-key" aria-hidden="true"></i>
              For Rent
            </span>

            <h1>Beautiful2222 Cozy 2-Beedroom Townhouse in the Heart of White Rock</h1>
            <h4 class="text-uppercase hidden-xs">White Rock, BC</h4>
          </div>
        </div>
      </div>
    </div>

    <div class="item">
      <img src="http://aios3-staging.agentimage.com/c/charlesvivash_pending.com/htdocs/wp-content/uploads/2016/12/slide7-compressor.jpg" alt="image" />

      <div class="carousel-caption container">
        <div class="row">
          <div class="col-sm-12">
            <span class="badge badge-sale hidden-xs">
              <i class="fa fa-tag" aria-hidden="true"></i>
              For Sale
            </span>

            <h1>Beautiful333333 Cozy 2-Beedroom Townhouse in the Heart of White Rock</h1>
            <h4 class="text-uppercase hidden-xs">White Rock, BC</h4>
          </div>
        </div>
      </div>
    </div>
  </div>

  <a class="left carousel-control hidden-xs" href="#hero-carousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>

  <a class="right carousel-control hidden-xs" href="#hero-carousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
