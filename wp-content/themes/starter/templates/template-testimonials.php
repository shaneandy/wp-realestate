<?php
/**
 * Template Name: Testimonials Template
 */

  // Fetch date for the contact template
  $data = \Realty\PageGetters::getTestimonialsTemplateData();
  \Realty\Debug::log( $data );

  if ($data) :
?>

<?php if (!empty($data['hero_area'])) {
  \Realty\Template::render('hero/standard', [
    'title' => $data['hero_area']['title'],
    'image' => $data['hero_area']['image'],
  ]);
} ?>

<div class="container content-container">
  <?php if (!empty($data['testimonials']['posts'])) : ?>
    <?php \Realty\Template::render('list/testimonials', ['data' => $data['testimonials']['posts']]); ?>
  <?php endif; ?>

  <?php if (!empty($data['form'])) : ?>
    <?php \Realty\Template::render('forms/testimonial-submission', ['data' => $data['form']]); ?>
  <?php endif; ?>
</div>

<?php endif;
