<?php
/**
 * Template Name: About Template
 */

  // Fetch date for the about page
  $data = \Realty\PageGetters::getAboutTemplateData();
  \Realty\Debug::log( $data );
?>

<?php if (!empty($data['hero_area'])) {
  \Realty\Template::render('hero/standard', [
    'title' => $data['hero_area']['title'],
    'image' => $data['hero_area']['image'],
  ]);
} ?>

<div class="container content-container">
  <div class="row vertical-center">
    <div class="col-md-5 col-sm-6 hidden-xs">
      <img src="http://placehold.it/420x420" class="img-responsive" />
    </div>

    <div class="col-sm-6">
      <h3 class="border-bottom">Helping you achieve the highest selling price from your home in the shortest amount of time to finding your next perfect property.</h3>

      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel ullamcorper augue. Suspendisse tempor, est et vulputate tincidunt, nulla neque maximus metus, a ultrices eros nibh eget ante. Nulla lobortis lectus ac metus pulvinar, non pulvinar neque facilisis. Praesent posuere.</p>
    </div>
  </div>
</div>

<?php
