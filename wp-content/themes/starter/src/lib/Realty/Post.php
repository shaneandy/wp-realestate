<?php

  namespace Realty;

  class Post
  {

    /**
     * Place actions and hooks inside this constructor function
     */
    function __construct() {} /* __construct() */


    /**
     * Manage Post query
     *
     * @author Andrew Vivash
     * @since 1.0
     * @param $fields = array()
     * @return $data
     */

    static public function fetchPosts( $fields = array(), $perPage = false, $postIDs = false )
    {

      $query = static::buildPostsQuery( $perPage, $postIDs );
      $posts = static::getPostsData( $query, $fields );

      return $posts;

    }/* fetchPosts() */


   /**
     * build the query for Posts
     *
     * @author Andrew Vivash
     * @package Post.php
     * @since 1.0
     * @param $queryType
     * @return $data
     */

    static public function buildPostsQuery( $perPage = false, $postIDs = false )
    {

      $category = !empty( $_REQUEST[ 'category' ] ) ? $_REQUEST[ 'category' ] : false;
      $perPage = !$perPage ? 6 : $perPage;
      $perPage = !empty($_REQUEST['perPage']) ? $_REQUEST['perPage'] : $perPage;
      $pageNum = !empty($_REQUEST['pagenum']) ? intval($_REQUEST['pagenum']) : 1;

      $body = array(
        'perPage' => $perPage,
        'pagenum' => $pageNum,
      );

      $offset = Utils::getOffsetFromBody( $body );

      $args = array(
        'post_type' => 'post',
        'posts_per_page' => $perPage,
        'offset' => $offset,
        'paged' => $pageNum,
        'orderby' => 'date',
        'order' => 'ASC',
      );

      // Only query for specific posts if specified
      if ($postIDs) {
        $args['post__in'] = $postIDs;
      }

      // Only query for specific categories if specified
      if ($category) {
        $args['category_name'] = $category;
      }

      // Exclude the featured post for a given page
      $featuredPostID = get_field('featured_article', get_the_ID());
      if ($featuredPostID) {
        $args['post__not_in'] = [$featuredPostID];
      }

      $query = new \WP_Query( $args );

      return $query;

    }/* buildPostsQuery() */


    /**
     * This is a sample function to show an example
     * of how to fetch post data from a template
     *
     * $data = \App\Post::getDataForPost();
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     */

    static public function getDataForPost( $postID = false, $fields = array() )
    {

      $postID = !$postID ? get_the_ID() : $postID;

      if (empty($fields))
      {
        $fields = [
          'id',
          'title',
          'content',
          'excerpt',
          'permalink',
          'image',
          'photo_gallery',
        ];
      }

      // Empty array to store our data
      $data = array();

      // Make sure the current post context is associated with the postID
      if ($postID !== get_the_ID()) {
        $post = get_post($postID);
        setup_postdata($post);
      }

      $title = get_the_title();


      if ( !empty( $fields ) )
      {

        foreach ( $fields as $key => $value )
        {

          switch ( $value )
          {

            case 'id':

              $data[ $value ] = get_the_ID();

              break;

            case 'title':

              $data[ $value ] = isset($post) && is_object($post) ? $post->post_title : get_the_title();

              break;

            case 'excerpt':

              $content = isset($post) && is_object($post) ? $post->post_content : get_the_content();
              $excerpt = $content ? wp_trim_words( $content, 25, '...' ) : false;

              $data[ $value ] = $excerpt ? apply_filters('the_excerpt', $excerpt) : null;

              break;

            case 'content':

              if (is_single()) {
                global $post;
                $data[ $value ] = $post->post_content;
              } else {
                $data[ $value ] = isset($post) && is_object($post) ? $post->post_content : get_the_content();
              }

              break;

            case 'permalink':

              $data[ $value ] = get_permalink( $postID );

              break;

            case "author":
            case "auth":
              // Fetch the author's nicename
              // $data[$value] = get_the_author_meta('user_nicename', $post->post_author);
              $data[ $value ] = get_the_author();
              // $data[ $value ] = get_field( 'author', $postID );
              break; // case "author"

            case "date":
              // Fetch the date of the post and format like Jan 1, 1970
              $data[$value] = get_post_time("M j, Y", false, $postID);
              break; // case "date"

            case "categories":
            case "cats":
              // Fetch the category IDs
              $data[ $value ] = static::getTaxonomyForPost( $postID, 'category', true );
              break; // case "categories"

            case 'image':

              $data[$value] = self::getFeatureImageData($postID, 'full');

              break;

            case 'hero_image':

              $data[$value] = self::getFeatureImageData($postID, 'full');

              break;

            case 'hero_area':

              $imageID = get_field('hero_image', $postID);
              if ($imageID) {
                $item = array();
                $imageDataArray = wp_get_attachment_image_src( $imageID, 'full' );

                $item['image'] = $imageDataArray[0];
                $item['title'] = $title;

                $data[$value] = $item;
              }

              break;

            case 'text_block':

              $data[$value] = self::getTextBlock($postID, 'text');

              break;

            case 'intro_content':

              $imageID    = get_field('intro_block_image', $postID);
              $data[$value] = self::getTextBlock($postID, 'intro');

              if ($imageID) {
                $imageDataArray = wp_get_attachment_image_src( $imageID, 'full' );
                $data[$value]['image'] = $imageDataArray[0];
              }

              break;

            case 'video_block':

              $imageID    = get_field('video_block_image', $postID);
              $data[$value] = self::getVideoBlock($postID, 'video');

              if ($imageID) {
                $imageDataArray = wp_get_attachment_image_src( $imageID, 'full' );
                $data[$value]['image'] = $imageDataArray[0];
              }

              break;

            case 'two_column_text_block_image':

              $imageID = get_field($value, $postID);
              if ($imageID) {
                $imageDataArray = wp_get_attachment_image_src( $imageID, 'full' );
                $data[$value] = $imageDataArray[0];
              }

              break;

            case 'intro_block_image':

              $imageID = get_field('intro_block_image', $postID);
              if ($imageID) {
                $imageDataArray = wp_get_attachment_image_src( $imageID, 'full' );
                $data[$value] = $imageDataArray[0];
              }

              break;

            case 'two_column_block':

              $data[$value] = self::getTwoColumnTextBlock($postID);

              break;

            case 'banner_image':

              $imageID = get_field($value, $postID);
              if ($imageID) {
                $imageDataArray = wp_get_attachment_image_src( $imageID, 'full' );
                $data[$value] = $imageDataArray[0];
              }

              break;

            case 'banner_block':

              $data[$value] = self::getBannerImageBlock($postID);

              break;

            case 'two_column_block_with_image':

              $data[$value] = self::getTwoColumnTextBlockWithImage($postID);

              break;

            case 'featured_article':

              $featuredArticleID = get_field($value, $postID);
              $data[$value] = $featuredArticleID ? self::getDataForPost($featuredArticleID) : null;

              break;

            case 'most_recent_article':

              $mostRecent = wp_get_recent_posts(['numberposts' => 1]);
              $fields = [
                'title',
                'excerpt',
                'image',
                'permalink',
              ];

              $data[$value] = $mostRecent && !empty($mostRecent[0]) ?
                self::getDataForPost($mostRecent[0]['ID'], $fields) : null;

              break;

            case 'featured_event':
              $fields = [
                'id',
                'title',
                'featured_excerpt',
                'permalink',
                'date',
                'address',
                'image',
                'feature_terms',
                'type_terms',
              ];

              $featuredEventID = get_field($value, $postID);
              $data[$value] = $featuredEventID ? Event::getDataForEvent($featuredEventID, $fields) : null;

              break;

            case 'event_archive':

              $data[$value] = Event::fetchEvents();

              break;

            case 'recent_posts':

              $data[$value] = self::fetchPosts([], 3);

              break;

            case 'blog_archive':

              $data[$value] = self::fetchPosts();

              break;

            case 'testimonials':

              $data[$value] = Testimonial::fetchTestimonials();

              break;

            case 'hero_featured_events':

              $eventIDs = get_field('featured_events', get_the_ID() );
              if (!empty($eventIDs)) {
                $data[$value] = Event::fetchEvents([], 3, $eventIDs);
              } else {
                $data[$value] = Event::fetchEvents([], 3);
              }

              break;

            case 'business_directory_categories':

              $data[$value] = Business::getCategories();

              break;

            case 'blog_categories':

              $data[$value] = self::getCategories();

              break;

            case 'board_of_directors':

              $data[$value] = BoardOfDirectors::fetchBoardOfDirectors();

              break;

            case 'author_block':

              $author = get_field( $value, $postID );;
              $data[ $value ] = $author && !empty($author[0]) ?
                Team::getSingleTeam( $author[0]->ID ) : null;

              break;

            case "next_post":

              $data[ $value ] = self::getSiblingPost($postID, 'next');

              break;

            case "previous_post":

              $data[ $value ] = self::getSiblingPost($postID, 'previous');

              break;

            case "video_block_video_id":

              $videoURL = get_field('video_block_video_url', $postID);
              $data[ $value ] = $videoURL ? Media::getVideoIDFromURL($videoURL) : null;
              $data[ 'video_block_video_service' ] = $videoURL ? Media::getVideoService($videoURL) : null;

              break;

            case 'event_categories' :

              $data[$value] = Event::getCategories();

              break;

            case 'social_grid' :

              $data[$value] = SocialPost::fetchSocialPosts();

              break;

            case 'featured_testimonials' :

              $testimonialIDs = get_field($value, $postID);
              $data[$value] = $testimonialIDs ? Testimonial::fetchTestimonials([
                'title',
                'speaker_title',
                'excerpt',
              ], 5, $testimonialIDs) : null;

              break;

            case 'featured_properties' :

              $propertyIDs = get_field($value, $postID);
              $data[$value] = $propertyIDs ? Property::fetchProperties([], 5, $propertyIDs) : null;

              break;

            case 'form' :

              $form = get_field($value, $postID);
              $data[$value] = $form ? ['title' => $form->title, 'id' => $form->id] : null;

              break;

            case 'video_gallery' :

              $data[$value] = self::getVideoGallery($postID);

              break;

            default:

              $data[ $value ] = get_field( $value, $postID );

              break;

          }

        }

      }

      // Send our data back
      return $data;

    } /* getDataForPost() */




    /**
     * Iterate through and get posts
     *
     * @author Andrew Vivash
     * @package Post.php
     * @since 1.0
     * @param $query = array(), $fields = array()
     * @return $data
     */

    static public function getPostsData( $query = array(), $fields = array() )
    {

      if (empty($query)) {
        return;
      }

      $data = array();
      $i = 0;

      if( $query->have_posts() ) :
        while ( $query->have_posts() ) :

          $query->the_post();

          $item = [];
          $postID = $query->post->ID;

          $item = static::getDataForPost( $postID, $fields );

          $i++;
          $i = $i === 3 ? 0 : $i;

          $data['posts'][] = $item;


        endwhile;

        $data['pagination'] = Utils::getPagination( $query );
        $data['found_posts'] = $query->found_posts;
        $data['max_num_pages'] = $query->max_num_pages;
        $data['posts_per_page'] = $query->query['posts_per_page'];

        wp_reset_postdata();

      endif;

      return $data;

    } /* getPostsData() */


    /**
     * This is a function to fetch the featured
     * image for a given post.
     *
     * $data = \App\Post::getFeatureImageData();
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @return (string) some text
     */

    static public function getFeatureImageData( $postID = null, $size = 'medium_hero' )
    {
      // Check if the size was put in as the postID
      if (is_string($postID) && !is_numeric($postID) && $size == 'medium_hero') {
        return self::getFeatureImageData(null, $postID);
      }

      // Get the post ID
      $postID = $postID ?: get_the_ID();

      // Get the attachment ID
      $heroID = get_field('hero_image', $postID);
      $imageID = $heroID ? $heroID : get_post_thumbnail_id($postID);

      // Get the attachment data
      $imageDataRaw = wp_get_attachment_image_src( $imageID, $size );

      // Format the image data
      $imageData = array(
        "url" => $imageDataRaw[0],
        "width" => $imageDataRaw[1],
        "height" => $imageDataRaw[2],
        "resized" => $imageDataRaw[3],
      );

      // Send the data back to our template
      return $imageData;

    } /* getFeatureImageData() */


    /**
     * Get the next post after the current post
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string)  - Description
     * @return (object) Description
     */

    static public function getSiblingPost( $postID = false, $direction = 'next' )
    {

      // Use the current post id if one hasn't been passed in
      if ( empty( $postID ) ) {
        $postID = get_the_ID();
      }

      // Fetch the next successive post

      $siblingPost =  $direction === 'previous' ? get_previous_post() : get_next_post();

      if ( empty( $siblingPost ) ) {
        return;
      }

      return self::getDataForPost($siblingPost->ID, ['title', 'permalink', 'image']);

    } /* getSiblingPost() */


    /**
     * get the terms for a post
     *
     * this function started as just getting one term, so it's extended to pull all, or one
     * set the $getAll variable to true to return all the terms available for the post
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @package Post.php
     * @since 1.0
     * @param $postID = '', $taxonomy = '', $getAll = false
     * @return $data
     */

    static public function getTaxonomyForPost( $postID = '', $taxonomy = '', $getAll = false )
    {

      $data = array();

      $terms = get_the_terms( $postID, $taxonomy );

      if ( empty( $terms ) )
        return;

      foreach ( $terms as $item )
      {
        $term = array();
        $term[ 'slug' ] = $item->slug;
        $term[ 'name' ] = $item->name;

        $data[] = $term;
      }

      if ( $getAll ) {
        return $data;
      }

      return $data[ 0 ];

    }/* getTaxonomyForPost() */


    /**
     * @internal Modify the excerpt character length
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     */

    public function excerpt_length__setExcerptLength($length)
    {

      return 100;

    } /* excerpt_length__setExcerptLength() */


    /**
     * Fetch the headline, subheadline, image and video url for the hero area
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getHeroArea($postID = false)
    {
      $postID = $postID ? $postID : get_the_ID();

      $fields = array(
        'hero_image',
      );

      return self::getDataForPost($postID, $fields);

    } /* getHeroArea () */


    /**
     * Fetch the text block content headline, subheadline, url
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getTextBlock($postID = false, $location = 'intro')
    {
      $postID = $postID ? $postID : get_the_ID();

      $fields = array(
        $location . '_block_headline',
        $location . '_block_copy',
        $location . '_block_image',
        $location . '_block_button_headline',
        $location . '_block_button_link',
        $location . '_block_button_text',
      );

      return self::getDataForPost($postID, $fields);

    } /* getTextBlock () */


    /**
     * Fetch the text block content headline, subheadline, url
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getVideoBlock($postID = false, $location = 'video')
    {
      $postID = $postID ? $postID : get_the_ID();

      $fields = array(
        $location . '_block_headline',
        $location . '_block_image',
        $location . '_block_video_id',
      );

      return self::getDataForPost($postID, $fields);

    } /* getVideoBlock () */


    /**
     * Fetch the two column text block content headline, copy
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getTwoColumnTextBlock($postID = false)
    {
      $postID = $postID ? $postID : get_the_ID();

      $fields = array(
        'left_column_headline',
        'left_column_copy',
        'right_column_headline',
        'right_column_copy',
      );

      return self::getDataForPost($postID, $fields);

    } /* getTwoColumnTextBlock () */


    /**
     * Fetch the two column text block content headline, copy
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getBannerImageBlock($postID = false)
    {
      $postID = $postID ? $postID : get_the_ID();

      $fields = array(
        'banner_image_copy',
        'banner_image',
      );

      return self::getDataForPost($postID, $fields);

    } /* getBannerImageBlock () */


    /**
     * Fetch the data for the two column text block with image
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getTwoColumnTextBlockWithImage($postID = false, $location = 'two_column_text')
    {
      $postID = $postID ? $postID : get_the_ID();

      $fields = array(
        $location . '_block_headline',
        $location . '_block_copy',
        $location . '_block_image',
      );

      return self::getDataForPost($postID, $fields);

    } /* getTwoColumnTextBlockWithImage () */


    /**
     * Fetch array of social media outbound links
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getSocialLinksArray($postID = false)
    {

      $postID = $postID ? $postID : get_the_ID();

      $data = array();

      $fields = array(
        'instagram',
        'twitter',
        'facebook',
        'google_plus',
        'youtube',
      );

      foreach ($fields as $key => $value) {
        $item = array();
        $item['title'] = $value;
        $item['url'] = get_field($value, $postID);

        if ($item['url']) {
          $data[] = $item;
        }
      }

      return $data;

    } /* getSocialLinksArray () */


    /**
     * Fetch blog categories
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string)  $postID  - Description
     * @return (object) Description
     */

    static public function getCategories()
    {

      $data = [];
      $terms = get_categories(['hide_empty' => false]);
      $defaultTerm = !empty($_REQUEST['category']) ? $_REQUEST['category'] : "";

      if (!empty($terms)) {
        foreach ($terms as $key => $value) {
          $item = array();

          $item['slug'] = $value->slug;
          $item['title'] = $value->name;
          $item['count'] = $value->count;

          // Check if is default term
          if ($defaultTerm === $item['slug']) {
            $data['current_term'] = $item['title'];
          }

          $data['categories'][] = $item;
        }
      }

      return $data;

    } /* getCategories() */


    /**
     * @internal Get the global contact form and address to be displayed in the footer
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @return (object) Description
     */

    static public function getFooterData()
    {
      $data = [];

      $contactForm = get_field('form', 'option');
      if ($contactForm) {
        $data['contact_form']['id'] = $contactForm->id;
        $data['contact_form']['title'] = $contactForm->title;
      }

      $addressLine1 = get_field('address_line_1', 'option');
      if ($addressLine1) {
        $data['address']['address_line_1'] = $addressLine1;
      }

      $addressLine2 = get_field('address_line_2', 'option');
      if ($addressLine2) {
        $data['address']['address_line_2'] = $addressLine2;
      }

      $phoneNumber = get_field('phone_number', 'option');
      if ($phoneNumber) {
        $data['address']['phone_number'] = $phoneNumber;
      }

      return $data;

    } /* getFooterData() */


    /**
     * @internal Get the content for the video gallery page
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getVideoGallery($postID = false)
    {
      if (!$postID) {
        return;
      }

      $data = [];

      $introTitle = get_field('video_gallery_intro_title', $postID);
      $introCopy = get_field('video_gallery_intro_copy', $postID);
      $introVideo = get_field('video_gallery_intro_video_url', $postID);
      $propertyVideos = get_field('property_videos', $postID);

      if ($introTitle) {
        $data['intro_title'] = $introTitle;
      }
      if ($introCopy) {
        $data['intro_copy'] = $introCopy;
      }
      if ($introVideo) {
        $data['intro_video_url'] = $introVideo;
      }
      if ($propertyVideos) {
        $data['property_videos'] = $propertyVideos;
      }

      return $data;
    } /* getVideoGallery() */


  } /* class Post() */
