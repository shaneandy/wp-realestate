<?php

  namespace Realty;

  class CustomPostTypes
  {

    public function __construct()
    {

      // register events post type
      add_action( 'init', array( &$this, 'init__registerEvents' ) );

      // register testimonial post type
      add_action( 'init', array( &$this, 'init__registerTestimonials' ) );

      // change the title placeholders depending on the post type
      add_filter( 'enter_title_here', array( &$this, 'enter_title_here__changeTitlePlaceholder' ) );

    }/* __construct() */


    /**
     * change the title placeholder text depending on the post type
     *
     * @author Andrew Vivash
     * @package CustomPostTypes.php
     * @since 1.0
     * @param $title
     * @return $title
     */

    public function enter_title_here__changeTitlePlaceholder( $title )
    {

      $screen = get_current_screen();
      $postType = $screen->post_type;

      switch ( $postType )
      {
        case 'page':
          $title = 'Enter Page Title';
          break;

        case 'testimonial':
          $title = 'Enter Speaker Name';
          break;

        default:
          break;
      }

      return $title;

    }/* enter_title_here__changeTitlePlaceholder() */


    /**
     * Setup event custom post type
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @return null
     */

    public function init__registerEvents()
    {
      $labels = array(
        'name'                => __( 'Events' ),
        'singular_name'       => __( 'Event' ),
        'add_new'             => __( 'Add New Event' ),
        'add_new_item'        => __( 'Add New Event' ),
        'edit_item'           => __( 'Edit Event' ),
        'new_item'            => __( 'New Event' ),
        'view_item'           => __( 'View Event' ),
        'search_items'        => __( 'Search Events' ),
        'not_found'           => __( 'No Events found' ),
        'not_found_in_trash'  => __( 'No Events found in Trash' ),
        'parent_item_colon'   => __( 'Parent Event:' ),
        'menu_name'           => __( 'Events' ),
      );

      $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'has_archive'         => true,
        'menu_icon'           => 'dashicons-calendar-alt',
        // 'taxonomies'          => array( 'post_tag' ),
        'supports'            => array(
          'title',
          'editor',
          'author',
          'excerpt',
          'comments',
          'thumbnail',
        )
      );

      register_post_type( 'event', $args );

    }/* init__registerEvents() */


    /**
     * Setup testimonial custom post type
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @return null
     */

    public function init__registerTestimonials()
    {
      $labels = array(
        'name'                => __( 'Testimonials' ),
        'singular_name'       => __( 'Testimonial' ),
        'add_new'             => __( 'Add New Testimonial' ),
        'add_new_item'        => __( 'Add New Testimonial' ),
        'edit_item'           => __( 'Edit Testimonial' ),
        'new_item'            => __( 'New Testimonial' ),
        'view_item'           => __( 'View Testimonial' ),
        'search_items'        => __( 'Search Testimonials' ),
        'not_found'           => __( 'No Testimonials found' ),
        'not_found_in_trash'  => __( 'No Testimonials found in Trash' ),
        'parent_item_colon'   => __( 'Parent Testimonial:' ),
        'menu_name'           => __( 'Testimonials' ),
      );

      $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'has_archive'         => true,
        'menu_icon'           => 'dashicons-format-status',
        // 'taxonomies'          => array( 'post_tag' ),
        'supports'            => array(
          'title',
          'editor',
          'thumbnail',
        )
      );

      register_post_type( 'testimonial', $args );

    }/* init__registerTestimonials() */

  }/* class CustomPostTypes */
