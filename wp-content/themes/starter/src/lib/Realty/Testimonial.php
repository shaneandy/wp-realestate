<?php

  namespace Realty;

  class Testimonial
  {

    /**
     * Place actions and hooks inside this constructor function
     */
    function __construct() {} /* __construct() */


    /**
     * Manage Post query
     *
     * @author Andrew Vivash
     * @since 1.0
     * @param $fields = array()
     * @return $data
     */

    static public function fetchTestimonials( $fields = array(), $perPage = false, $postIDs = false )
    {

      $query = static::buildTestimonialsQuery( $perPage, $postIDs );
      $posts = static::getTestimonialsData( $query, $fields );

      return $posts;

    }/* fetchTestimonials() */


   /**
     * build the query for Posts
     *
     * @author Andrew Vivash
     * @package Post.php
     * @since 1.0
     * @param $queryType
     * @return $data
     */

    static public function buildTestimonialsQuery( $perPage = false, $postIDs = false )
    {

      $category = !empty( $_REQUEST[ 'category' ] ) ? $_REQUEST[ 'category' ] : false;
      $perPage = !$perPage ? 6 : $perPage;
      $perPage = !empty($_REQUEST['perPage']) ? $_REQUEST['perPage'] : $perPage;
      $pageNum = !empty($_REQUEST['pagenum']) ? intval($_REQUEST['pagenum']) : 1;

      $body = array(
        'perPage' => $perPage,
        'pagenum' => $pageNum,
      );

      $offset = Utils::getOffsetFromBody( $body );

      $args = [
        'post_type' => 'testimonial',
        'posts_per_page' => $perPage,
        'offset' => $offset,
        'paged' => $pageNum,
        'orderby' => 'date',
        'order' => 'ASC',
      ];

      // Only query for specific posts if specified
      if ($postIDs) {
        $args['post__in'] = $postIDs;
      }

      // Only query for specific categories if specified
      if ($category) {
        $args['category_name'] = $category;
      }

      // Exclude the featured post for a given page
      $featuredPostID = get_field('featured_article', get_the_ID());
      if ($featuredPostID) {
        $args['post__not_in'] = [$featuredPostID];
      }

      $query = new \WP_Query( $args );

      return $query;

    }/* buildTestimonialsQuery() */


    /**
     * This is a sample function to show an example
     * of how to fetch post data from a template
     *
     * $data = \App\Post::getDataForProperty();
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     */

    static public function getDataForProperty( $postID = false, $fields = array() )
    {

      $postID = !$postID ? get_the_ID() : $postID;

      if (empty($fields))
      {
        $fields = [
          'id',
          'title',
          'content',
          'speaker_title',
          'permalink',
          'image',
        ];
      }

      // Empty array to store our data
      $data = array();

      // Make sure the current post context is associated with the postID
      if ($postID !== get_the_ID()) {
        $post = get_post($postID);
        setup_postdata($post);
      }

      $title = get_the_title();


      if ( !empty( $fields ) )
      {

        foreach ( $fields as $key => $value )
        {

          switch ( $value )
          {

            case 'id':

              $data[ $value ] = get_the_ID();

              break;

            case 'title':

              $data[ $value ] = !empty($post) && is_object($post) ? $post->post_title : get_the_title();

              break;

            case 'excerpt':

              $content = !empty($post) && is_object($post) ? $post->post_content : get_the_content();
              $excerpt = $content ? wp_trim_words( $content, 25, '...' ) : false;

              $data[ $value ] = $excerpt ? apply_filters('the_excerpt', $excerpt) : null;

              break;

            case 'content':

              if (is_single()) {
                global $post;
                $data[ $value ] = $post->post_content;
              } else {
                $data[ $value ] = !empty($post) && is_object($post) ? $post->post_content : get_the_content();
              }

              break;

            case 'permalink':

              $data[ $value ] = get_permalink( $postID );

              break;

            case "author":
            case "auth":
              // Fetch the author's nicename
              // $data[$value] = get_the_author_meta('user_nicename', $post->post_author);
              $data[ $value ] = get_the_author();
              // $data[ $value ] = get_field( 'author', $postID );
              break; // case "author"

            case "date":
              // Fetch the date of the post and format like Jan 1, 1970
              $data[$value] = get_post_time("M j, Y", false, $postID);
              break; // case "date"

            case "categories":
            case "cats":
              // Fetch the category IDs
              $data[ $value ] = static::getTaxonomyForProperty( $postID, 'category', true );
              break; // case "categories"

            case 'image':

              $data[$value] = Post::getFeatureImageData($postID, 'full');

              break;

            case 'recent_posts':

              $data[$value] = self::fetchTestimonials([], 3);

              break;

            default:

              $data[ $value ] = get_field( $value, $postID );

              break;

          }

        }

      }

      // Send our data back
      return $data;

    } /* getDataForProperty() */


    /**
     * Iterate through and get posts
     *
     * @author Andrew Vivash
     * @package Post.php
     * @since 1.0
     * @param $query = array(), $fields = array()
     * @return $data
     */

    static public function getTestimonialsData( $query = array(), $fields = array() )
    {

      if (empty($query)) {
        return;
      }

      $data = array();
      $i = 0;

      if( $query->have_posts() ) :
        while ( $query->have_posts() ) :

          $query->the_post();

          $item = [];
          $postID = $query->post->ID;

          $item = static::getDataForProperty( $postID, $fields );

          $i++;
          $i = $i === 3 ? 0 : $i;

          $data['posts'][] = $item;


        endwhile;

        $data['pagination'] = Utils::getPagination( $query );
        $data['found_posts'] = $query->found_posts;
        $data['max_num_pages'] = $query->max_num_pages;
        $data['posts_per_page'] = $query->query['posts_per_page'];

        wp_reset_postdata();

      endif;

      return $data;

    } /* getTestimonialsData() */


    /**
     * get the terms for a post
     *
     * this function started as just getting one term, so it's extended to pull all, or one
     * set the $getAll variable to true to return all the terms available for the post
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @package Post.php
     * @since 1.0
     * @param $postID = '', $taxonomy = '', $getAll = false
     * @return $data
     */

    static public function getTaxonomyForProperty( $postID = '', $taxonomy = '', $getAll = false )
    {

      $data = array();

      $terms = get_the_terms( $postID, $taxonomy );

      if ( empty( $terms ) )
        return;

      foreach ( $terms as $item )
      {
        $term = array();
        $term[ 'slug' ] = $item->slug;
        $term[ 'name' ] = $item->name;

        $data[] = $term;
      }

      if ( $getAll ) {
        return $data;
      }

      return $data[ 0 ];

    }/* getTaxonomyForProperty() */


  } /* class Testimonial() */
