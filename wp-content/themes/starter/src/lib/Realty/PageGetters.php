<?php

  namespace Realty;

  class PageGetters
  {

    /**
     * Place actions and hooks inside this constructor function
     */
    function __construct() {} /* __construct() */



    /**
     * Fetch the data for the homepage template
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getHomepageTemplateData($postID = false)
    {
      $postID = $postID ? postID : get_the_ID();

      $fields = array(
        'featured_properties_hero',
        'property_search',
        'two_column_block_with_image',
        'featured_testimonials',
        'featured_properties',
        'banner_block',
      );

      return Post::getDataForPost($postID, $fields);

    } /* getHomepageTemplateData () */


    /**
     * Fetch the data for the property archive template
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getPropertyArchiveTemplateData($postID = false)
    {
      $postID = $postID ? postID : get_the_ID();

      $fields = array(
        'featured_properties_hero',
        'property_search',
        'property_archive',
      );

      return Post::getDataForPost($postID, $fields);

    } /* getPropertyArchiveTemplateData () */


    /**
     * Fetch the data for the about template
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getAboutTemplateData($postID = false)
    {
      $postID = $postID ? postID : get_the_ID();

      $fields = array(
        'hero_area',
        'property_search',
        'property_archive',
      );

      return Post::getDataForPost($postID, $fields);

    } /* getAboutTemplateData () */


    /**
     * Fetch the data for the form template
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getFormTemplateData($postID = false)
    {
      $postID = $postID ? postID : get_the_ID();

      $fields = array(
        'hero_area',
        'content',
        'form',
      );

      return Post::getDataForPost($postID, $fields);

    } /* getFormTemplateData () */


    /**
     * Fetch the data for the contact template
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getContactTemplateData($postID = false)
    {
      $postID = $postID ? postID : get_the_ID();

      $fields = array(
        'hero_area',
        'content',
        'form',
      );

      return Post::getDataForPost($postID, $fields);

    } /* getContactTemplateData () */


    /**
     * Fetch the data for the testimonial template
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getTestimonialsTemplateData($postID = false)
    {
      $postID = $postID ? postID : get_the_ID();

      $fields = array(
        'hero_area',
        'testimonials',
        'form',
      );

      return Post::getDataForPost($postID, $fields);

    } /* getTestimonialsTemplateData () */


    /**
     * Fetch the data for the blog template
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getBlogTemplateData($postID = false)
    {
      $postID = $postID ? postID : get_the_ID();

      $fields = array(
        'hero_area',
        'blog_archive',
      );

      return Post::getDataForPost($postID, $fields);

    } /* getBlogTemplateData () */


    /**
     * Fetch the data for the video gallery template
     *
     * @author Andrew Vivash <av@andrewvivash.com>
     * @since 1.0.0
     * @param (string) $arg - Description
     * @return (object) Description
     */

    static public function getVideoGalleryTemplateData($postID = false)
    {
      $postID = $postID ? postID : get_the_ID();

      $fields = array(
        'hero_area',
        'content',
        'video_gallery',
      );

      return Post::getDataForPost($postID, $fields);

    } /* getVideoGalleryTemplateData () */

  } /* class PageGetters() */
