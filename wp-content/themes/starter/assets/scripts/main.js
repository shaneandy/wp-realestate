// import external dependencies
import 'jquery';
import 'bootstrap-sass/assets/javascripts/bootstrap';

// import local dependencies
import Router from './util/router';
import common from './routes/Common';
import home from './routes/Home';
import aboutUs from './routes/About';

const routes = {
  common,
  home,
  aboutUs,
};

// Load Events
jQuery(document).ready(() => {
  new Router(routes).loadEvents();

  /**
   * Instantiate hero carousel
   */
  if ($('#hero-carousel').length) {
    $('#hero-carousel').on('slide.bs.carousel', (e) => {
      const nextIndex = $(e.relatedTarget).index();
      const $carouselSub = $('.carousel-sub');

      $carouselSub.find('.active').removeClass('active');
      $carouselSub.find(`.attributes:eq(${nextIndex})`).addClass('active');
    });
  }

  /**
   * Navigation fade on scroll
   */
  if ($('body').hasClass('page-template-template-home')) {
    setTimeout(() => {
      let heroHeight = $('#hero-carousel').height();
      heroHeight = heroHeight >= 800 ? 700 : heroHeight; // TODO: remove this
      const $navbar = $('.mobile-nav');

      $(window).on('scroll', () => {
        if (window.scrollY >= heroHeight) {
          $navbar.addClass('navbar-scroll-active');
        } else {
          $navbar.removeClass('navbar-scroll-active');
        }
      });

      $(window).on('resize', () => {
        heroHeight = $('#hero-carousel').height();
      });
    }, 100);
  } else {
    const $navbar = $('.mobile-nav');
    const HEIGHT_TO_ACTIVATE = 46; // Just choosing what looks best visually for now

    $(window).on('scroll', () => {
      if (window.scrollY > HEIGHT_TO_ACTIVATE) {
        $navbar.addClass('navbar-scroll-active');
      } else {
        $navbar.removeClass('navbar-scroll-active');
      }
    });
  }
});
